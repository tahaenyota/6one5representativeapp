//
//  TermsAndConditionsController.swift
//  LearningApp
//
//  Created by Sumit More on 1/4/17.
//  Copyright © 2017 Mac-04. All rights reserved.
//

import Foundation
import UIKit

class TermsAndConditionsController: UIViewController {
    
    
    @IBOutlet weak var webview: UIWebView!
    @IBOutlet weak var tncLabel: UILabel!
    @IBOutlet weak var disagreeBtn: UIButton!
    @IBOutlet weak var agreeBtn: UIButton!
    @IBOutlet weak var tncBackBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tncBackBtn.isHidden = true
        
        self.tncLabel.text = "tnc_title".localized
        self.disagreeBtn.setTitle("tnc_disagree".localized, for: .normal)
        self.agreeBtn.setTitle("tnc_agree".localized, for: .normal)
        
        let localfilePath = Bundle.main.url(forResource: "copyright_disclaimer2", withExtension: "html");
        let myRequest = NSURLRequest(url: localfilePath!);
        self.webview.loadRequest(myRequest as URLRequest);
        
    }
    
    @IBAction func backClick(_ sender: AnyObject) {
        let appDomain = Bundle.main.bundleIdentifier
        UserDefaults.standard.removePersistentDomain(forName: appDomain!)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func Disagree(_ sender: AnyObject)
    {
        let appDomain = Bundle.main.bundleIdentifier
        UserDefaults.standard.removePersistentDomain(forName: appDomain!)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func agreeTapped(_ sender: AnyObject)
    {
        acceptTerms()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func acceptTerms()
    {

        /*var boxView = UIView()
        boxView = UIView(frame: CGRect(x: view.frame.midX - 110, y: view.frame.midY - 25, width: 220, height: 50))
        boxView.backgroundColor = UIColor.white
        boxView.alpha = 1
        boxView.layer.cornerRadius = 10
        
        //Here the spinnier is initialized
        let activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        activityView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityView.startAnimating()
        
        let textLabel = UILabel(frame: CGRect(x: 60, y: 0, width: 200, height: 50))
        textLabel.textColor = UIColor.gray
        textLabel.text = "tnc_accept".localized
        
        boxView.addSubview(activityView)
        boxView.addSubview(textLabel)
        
        view.addSubview(boxView)*/
        
        let progressHUD = ProgressHUD()
        self.view.addSubview(progressHUD)
        UIApplication.shared.beginIgnoringInteractionEvents()

        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
        ]
        
        let dataStr: String!
        dataStr = "data={ \"accept_terms\":\"1\"\"}"
        let postData = NSMutableData(data: dataStr.data(using: String.Encoding.utf8)!)
        
        let prefs:UserDefaults = UserDefaults.standard
        let user_id = prefs.value(forKey: "user_id") as! String!

        let apiString = Constants.salesRepAcceptTermsURL + user_id!
        let request = NSMutableURLRequest(url: NSURL(string: apiString)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                let alert = UIAlertController(title: "tnc_title".localized, message: "tnc_error".localized, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
            } else {
                let httpResponse = response as? HTTPURLResponse
                print("Response: \(httpResponse)")
                do {
                    
                    progressHUD.hide()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        print("Status: \(statusVal!)")
                        
                        if(statusVal! == true)
                        {
                            let alert = UIAlertController(title: "tnc_title".localized, message: "tnc_success".localized, preferredStyle: UIAlertControllerStyle.alert)
                            let OKAction = UIAlertAction(title: "tnc_ok".localized, style: .default) { (action:UIAlertAction!) in
                                let prefs:UserDefaults = UserDefaults.standard
                                prefs.set("1", forKey: "terms_accepted")
                                prefs.synchronize()
                                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let initViewController: UIViewController = storyboard.instantiateViewController(withIdentifier: "PersonalDetails") as UIViewController
                                self.present(initViewController, animated: true, completion: nil)
//                                self.dismiss(animated: true, completion: nil)
                            }
                            alert.addAction(OKAction)
                            self.present(alert, animated: true, completion:nil)

                            
                        } else {
                            var error_msg:NSString
                            if convertedJsonIntoDict["message"] as? NSString != nil {
                                error_msg = convertedJsonIntoDict["message"] as! NSString
                            } else {
                                error_msg = "Unknown Error"
                            }
                            print("error_msg",error_msg)
                            let alertController = UIAlertController(title: "tnc_title".localized, message: "tnc_error".localized , preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "tnc_ok".localized, style: .default) { (action:UIAlertAction!) in
                                self.dismiss(animated: true, completion: nil)
                            }
                            
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                } catch let error as NSError {
                    print(error)
                    let alertController = UIAlertController(title: "tnc_title".localized, message: "tnc_error".localized , preferredStyle: .alert)
                    let OKAction = UIAlertAction(title: "tnc_ok".localized, style: .default) { (action:UIAlertAction!) in
                        self.dismiss(animated: true, completion: nil)
                    }
                    alertController.addAction(OKAction)
                    self.present(alertController, animated: true, completion:nil)
                }
            }
        })
        
        dataTask.resume()
    }
    
    
}
