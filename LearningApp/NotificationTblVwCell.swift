//
//  NotificationTblVwCell.swift
//  LearningApp
//
//  Created by Mac-04 on 22/08/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import UIKit

class NotificationTblVwCell: UITableViewCell {

    @IBOutlet weak var NotificationTitleLabel: UILabel!
    @IBOutlet weak var NotificationDaylabel: UILabel!
    @IBOutlet weak var NotificationDescLabel: UILabel!

    override func awakeFromNib() {
                super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
