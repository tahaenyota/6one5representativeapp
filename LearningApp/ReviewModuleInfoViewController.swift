//
//  ReviewModuleInfoViewController.swift
//  6one5Manager
//
//  Created by enyotalearning on 22/12/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit

class ReviewModuleInfoViewController: UIViewController,UITableViewDelegate,UITableViewDataSource
/*{

    @IBOutlet weak var navigationTitle: UILabel!
    @IBOutlet weak var tableViewTitle: UILabel!
    
    @IBOutlet weak var reviewModuleinfoTableview: UITableView!
    
    @IBOutlet weak var moduleDiscriptorView1Height: NSLayoutConstraint!
    
    
    var moduleDescriptionData1 = [String]()//["1 info ", "2 info ", "3 info ", "4 info "]
    var moduleDescriptionData2 = [String]()//["1 info ", "2 info ", "3 info ", "4 info "]
    var moduleDescriptionData3 = [String]()//["1 info ", "2 info ", "3 info ", "4 info "]
    var moduleDescriptionData4 = [String]()//["1 info ", "2 info ", "3 info ", "4 info "]
    var currentModuleLabel1:UILabel!
    var currentModuleLabel2:UILabel!
    var currentModuleLabel3:UILabel!
    var currentModuleLabel4:UILabel!
    
    var currentModuleLabel1Title:UILabel!
    var currentModuleLabel2Title:UILabel!
    var currentModuleLabel3Title:UILabel!
    var currentModuleLabel4Title:UILabel!

    @IBOutlet weak var moduleDiscriptor1Height: NSLayoutConstraint!
    
    @IBOutlet weak var moduleDiscriptor4Height: NSLayoutConstraint!
    
    var moduleDescription1Text:String!
    var moduleDescription2Text:String!
    var moduleDescription3Text:String!
    var moduleDescription4Text:String!
    
    var count1:Int = 0
    var count2:Int = 0
    var count3:Int = 0
    var count4:Int = 0
    
    var moduleid = 3
    
    let dataLabel = ["Beginner".localized, "Learner".localized, "Professional".localized, "High Performer".localized, "Expert".localized]
    
    var rowNo:Int = 0
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var rmd1:ReviewModuleInfo = ReviewModuleInfo()
        
        //print(" here is module 1 dec text \(rmd1.discripter1.descripterText)")
        //print(" here is module 1 dec text \(rmd1.discripter2.beginer[0])")
        //print(" here is module 1 dec text \(rmd1.discripter2.learner[1])")
        switch moduleid {
        case 1:
            rmd1 = ReviewModule1InfoData()
            break
        case 2:
            rmd1 = ReviewModule2InfoData()
            break
        case 3:
            rmd1 = ReviewModule3InfoData()
            break
        case 4:
            rmd1 = ReviewModule4InfoData()
            break
        case 5:
            rmd1 = ReviewModule5InfoData()
            break
        case 6:
            rmd1 = ReviewModule6InfoData()
            break
        default:
            print("Error while finding the Module id : \(moduleid)")
        }
        
        rmd1.AdddataforDiscripter()
        
        moduleDescriptionData1 = rmd1.discripter1Data
        moduleDescriptionData2 = rmd1.discripter2Data
        if moduleid != 5
        {
            moduleDescriptionData3 = rmd1.discripter3Data
            moduleDescription3Text = "<b>" + "Descriptor 3".localized + "</b><br />" + rmd1.discripter3.descripterText
        }
        if moduleid == 3
        {
            moduleDescriptionData4 = rmd1.discripter4Data
            moduleDescription4Text = "<b>" + "Descriptor 4".localized + "</b><br />" + rmd1.discripter4.descripterText
            
        }
        
        
        moduleDescription1Text = "<b>" + "Descriptor 1".localized + "</b><br />" + rmd1.discripter1.descripterText
        moduleDescription2Text = "<b>" + "Descriptor 2".localized + "</b><br />" + rmd1.discripter2.descripterText
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        switch moduleid {
        case 1:
            self.navigationTitle.text = "Welcome"
            let theString1 = String(format:NSLocalizedString("ReviewModuleInfoTitle", comment: "any comment"), "three", "Welcome")
            //print("####### Desc is " + pDataProductDesc)
            let theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                               documentAttributes: nil)
            
            tableViewTitle.attributedText = theAttributedString1
            
            // self.tabBarController?.navigationItem.title = "My Title"
            // tableViewTitle.text =
            break
        case 2:
            self.navigationTitle.text = "Understand"
            let theString1 = String(format:NSLocalizedString("ReviewModuleInfoTitle", comment: "any comment"), "three", "Understand")
            //print("####### Desc is " + pDataProductDesc)
            let theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                               documentAttributes: nil)
            
            tableViewTitle.attributedText = theAttributedString1
            break
        case 3:
            self.navigationTitle.text = "Advise"
            let theString1 = String(format:NSLocalizedString("ReviewModuleInfoTitle", comment: "any comment"), "four", "Advise")
            //print("####### Desc is " + pDataProductDesc)
            let theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                               documentAttributes: nil)
            
            tableViewTitle.attributedText = theAttributedString1
            break
        case 4:
            self.navigationTitle.text = "Close"
            let theString1 = String(format:NSLocalizedString("ReviewModuleInfoTitle", comment: "any comment"), "three", "Close")
            //print("####### Desc is " + pDataProductDesc)
            let theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                               documentAttributes: nil)
            
            tableViewTitle.attributedText = theAttributedString1
            
            break
        case 5:
            self.navigationTitle.text = "Thank"
            let theString1 =  String(format:NSLocalizedString("ReviewModuleInfoTitle", comment: "any comment"), "two", "Thank")
            //print("####### Desc is " + pDataProductDesc)
            let theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                               documentAttributes: nil)
            
            tableViewTitle.attributedText = theAttributedString1
            break
        case 6:
            self.navigationTitle.text = "Handling Customer Complaints"
            let theString1 =  String(format:NSLocalizedString("ReviewModuleInfoTitle", comment: "any comment"), "three", "Handling Customer Complaints")
            //print("####### Desc is " + pDataProductDesc)
            let theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                               documentAttributes: nil)
            
            tableViewTitle.attributedText = theAttributedString1
            break
        default:
            break
        }
    }
    @IBAction func closeButton(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    private func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if moduleid == 3
        {
            return 4
        }
        else if moduleid == 5
        {
            return 2
        }
        else
        {
            return 3
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        if indexPath.row == 3{
            
            return 350.0
        }else{
            return 340.0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:UITableViewCell = self.reviewModuleinfoTableview.dequeueReusableCell(withIdentifier: "ReviewModuleInfocell_1") as! ReviewModuleInfocell_1
        
        if indexPath.row == 0
        {
           let cell = self.reviewModuleinfoTableview.dequeueReusableCell(withIdentifier: "ReviewModuleInfocell_1") as! ReviewModuleInfocell_1
            
            let decLabel:UILabel = cell.descriptorLabel1
            decLabel.text = moduleDescription1Text //.html2String
            
            let theString1 = decLabel.text!
            //print("####### Desc is " + pDataProductDesc)
            let theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                              options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                              documentAttributes: nil)
            
            decLabel.attributedText = theAttributedString1
            
            cell.cell1NextButton.tag = indexPath.row
            
            currentModuleLabel1 = cell.moduleDescription1
            currentModuleLabel1.text = moduleDescriptionData1[count1] //.html2String
            
            
            let theString = currentModuleLabel1.text!
            //print("####### Desc is " + pDataProductDesc)
            let theAttributedString = try! NSAttributedString(data: theString.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                              options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                              documentAttributes: nil)
            
            currentModuleLabel1.attributedText = theAttributedString
            
            
            
            currentModuleLabel1Title = cell.descriptorTitleLabel1
            currentModuleLabel1Title.text = dataLabel[count1] //.html2String
            
            cell.cell1NextButton.addTarget(self, action: #selector(nextButtonTapped),for: .touchUpInside)
           
            
            cell.cell1PreviousButton.tag = indexPath.row
            cell.cell1PreviousButton.addTarget(self, action: #selector(previouButtonTapped), for: .touchUpInside);
            cell.selectionStyle =  UITableViewCellSelectionStyle.none
            return cell
        }
        else if indexPath.row == 1
        {
            let cell = self.reviewModuleinfoTableview.dequeueReusableCell(withIdentifier: "ReviewModuleInfocell_2") as! ReviewModuleInfocell_2
            cell.cell2NextButton.tag = indexPath.row
            
            //cell.descriptorLabel2.text = moduleDescription2Text //.html2String
            
            let decLabel:UILabel = cell.descriptorLabel2
            decLabel.text = moduleDescription2Text //.html2String
            
            let theString1 = decLabel.text!
            //print("####### Desc is " + pDataProductDesc)
            let theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                               documentAttributes: nil)
            
            decLabel.attributedText = theAttributedString1

            
            
            currentModuleLabel2 = cell.moduleDescription2
            currentModuleLabel2.text = moduleDescriptionData2[count2]//.html2String
            
            
            let theString = currentModuleLabel2.text!
            //print("####### Desc is " + pDataProductDesc)
            let theAttributedString = try! NSAttributedString(data: theString.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                              options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                              documentAttributes: nil)
            
            currentModuleLabel2.attributedText = theAttributedString
            
            currentModuleLabel2Title = cell.descriptorTitleLabel2
            currentModuleLabel2Title.text = dataLabel[count2]//.html2String
            
            cell.cell2NextButton.addTarget(self, action: #selector(nextButtonTapped),for: .touchUpInside)
            cell.cell2previousButton.tag = indexPath.row
            cell.cell2previousButton.addTarget(self, action: #selector(previouButtonTapped), for: .touchUpInside);
            cell.selectionStyle =  UITableViewCellSelectionStyle.none
            return cell
        }
        else if indexPath.row == 2
        {
            let cell = self.reviewModuleinfoTableview.dequeueReusableCell(withIdentifier: "ReviewModuleInfocell_3") as! ReviewModuleInfocell_3
            cell.cell3NextButton.tag = indexPath.row
  
            
            let decLabel:UILabel = cell.descriptorLabel3
            decLabel.text = moduleDescription3Text //.html2String
            
            let theString1 = decLabel.text!
            //print("####### Desc is " + pDataProductDesc)
            let theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                               documentAttributes: nil)
            
            decLabel.attributedText = theAttributedString1
            
            currentModuleLabel3 = cell.moduleDescription3
            currentModuleLabel3.text = moduleDescriptionData3[count3]
            
            let theString = currentModuleLabel3.text!
            //print("####### Desc is " + pDataProductDesc)
            let theAttributedString = try! NSAttributedString(data: theString.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                              options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                              documentAttributes: nil)
            
            currentModuleLabel3.attributedText = theAttributedString
            
            
            
            currentModuleLabel3Title = cell.descriptoTitleLabel3
            currentModuleLabel3Title.text = dataLabel[count3] //.html2String
            
            cell.cell3NextButton.addTarget(self, action: #selector(nextButtonTapped),for: .touchUpInside)
            cell.cell3PreviousButton.tag = indexPath.row
            cell.cell3PreviousButton.addTarget(self, action: #selector(previouButtonTapped), for: .touchUpInside);
            cell.selectionStyle =  UITableViewCellSelectionStyle.none
            return cell
        }
        else if indexPath.row == 3
        {
            let cell = self.reviewModuleinfoTableview.dequeueReusableCell(withIdentifier: "ReviewModuleInfocell_4") as! ReviewModuleInfocell_4
            cell.cell4NextButton.tag = indexPath.row
            
            let decLabel:UILabel = cell.descriptorLabel4
            decLabel.text = moduleDescription4Text //.html2String
            
            let theString1 = decLabel.text!
            //print("####### Desc is " + pDataProductDesc)
            let theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                               documentAttributes: nil)
            
            decLabel.attributedText = theAttributedString1
            
            
            currentModuleLabel4 = cell.moduleDescription4
            currentModuleLabel4.text = moduleDescriptionData4[count4]
            
            let theString = currentModuleLabel4.text!
            //print("####### Desc is " + pDataProductDesc)
            let theAttributedString = try! NSAttributedString(data: theString.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                              options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                              documentAttributes: nil)
            
            currentModuleLabel4.attributedText = theAttributedString
            
            
            
            currentModuleLabel4Title = cell.descriptorTitleLabel4
            currentModuleLabel4Title.text = dataLabel[count4] //.html2String
            
            
            cell.cell4NextButton.addTarget(self, action: #selector(nextButtonTapped),for: .touchUpInside)
            cell.cell4PreviousButton.tag = indexPath.row
            cell.cell4PreviousButton.addTarget(self, action: #selector(previouButtonTapped), for: .touchUpInside);
            cell.selectionStyle =  UITableViewCellSelectionStyle.none
            return cell
        }
        
        /*else{
            let cell = self.reviewModuleinfoTableview.dequeueReusableCell(withIdentifier: "ReviewModuleInfocell_4") as! ReviewModuleInfocell_4
            cell.cell4NextButton.tag = indexPath.row
            cell.cell4NextButton.addTarget(self, action: Selector(("cell4NextButton:")), for: UIControlEvents.touchUpInside);
            cell.cell4PreviousButton.tag = indexPath.row
            cell.cell4PreviousButton.addTarget(self, action: Selector(("cell4PreviousButton:")), for: UIControlEvents.touchUpInside);
            return cell
        }*/
//        cell.selectionStyle =  UITableViewCellSelectionStyle.none
        return cell
    }
    
    func nextButtonTapped(sender:UIButton!){
        print("Here I have clicked the next action \(sender.tag)")
        
        /*if count < moduleDescriptionData1.count - 1
        {
            count = count + 1;
            currentModuleLabel.text = moduleDescriptionData1[count]
        }*/
        
        switch sender.tag {
        case 0:
            if count1 < moduleDescriptionData1.count - 1
            {
                count1 = count1 + 1;
                
                //currentModuleLabel1.text = moduleDescriptionData1[count1]
                let theAttributedString = try! NSAttributedString(data: moduleDescriptionData1[count1].data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                                  options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                                  documentAttributes: nil)
                
                currentModuleLabel1.attributedText = theAttributedString

                //currentModuleLabel1.text = moduleDescriptionData1[count1]//.html2String
                currentModuleLabel1Title.text = dataLabel[count1] //.html2String
            }
            break
        case 1:
            if count2 < moduleDescriptionData2.count - 1
            {
                count2 = count2 + 1;
                //currentModuleLabel2.text = moduleDescriptionData2[count2]
                let theAttributedString = try! NSAttributedString(data: moduleDescriptionData2[count2].data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                                  options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                                  documentAttributes: nil)
                
                currentModuleLabel2.attributedText = theAttributedString
                currentModuleLabel2Title.text = dataLabel[count2]
            }
            break
        case 2:
            if count3 < moduleDescriptionData3.count - 1
            {
                count3 = count3 + 1;
                let theAttributedString = try! NSAttributedString(data: moduleDescriptionData3[count3].data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                                  options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                                  documentAttributes: nil)
                
                currentModuleLabel3.attributedText = theAttributedString
                currentModuleLabel3Title.text = dataLabel[count3]
            }
            break
        case 3:
            if count4 < moduleDescriptionData4.count - 1
            {
                count4 = count4 + 1;
                let theAttributedString = try! NSAttributedString(data: moduleDescriptionData4[count4].data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                                  options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                                  documentAttributes: nil)
                
                currentModuleLabel4.attributedText = theAttributedString
                currentModuleLabel4Title.text = dataLabel[count4]
            }
            break
        default :
            if count1 < moduleDescriptionData1.count - 1
            {
                print("Here I am in default next" )
                count1 = count1 + 1;
                currentModuleLabel1.tex		t = moduleDescriptionData1[count1]
            }
            break
            
        }

    }
    
    func previouButtonTapped(sender:UIButton!){
        print("Here I have clicked the pre action \(sender.tag)")
//        if count != 0
//        {
//            count = count - 1;
//            currentModuleLabel.text = moduleDescriptionData1[count]
//        }
        
        switch sender.tag {
        case 0:
            if count1 != 0
            {
                count1 = count1 - 1;
                //currentModuleLabel1.text = moduleDescriptionData1[count1]//.html2String
                let theAttributedString = try! NSAttributedString(data: moduleDescriptionData1[count1].data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                                  options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                                  documentAttributes: nil)
                
                currentModuleLabel1.attributedText = theAttributedString
                currentModuleLabel1Title.text = dataLabel[count1]
            }
            break
        case 1:
            if count2 != 0
            {
                count2 = count2 - 1;
                let theAttributedString = try! NSAttributedString(data: moduleDescriptionData2[count2].data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                                  options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                                  documentAttributes: nil)
                
                currentModuleLabel2.attributedText = theAttributedString
                currentModuleLabel2Title.text = dataLabel[count2]
            }
            break
        case 2:
            if count3 != 0
            {
                count3 = count3 - 1;
                let theAttributedString = try! NSAttributedString(data: moduleDescriptionData3[count3].data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                                  options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                                  documentAttributes: nil)
                
                currentModuleLabel3.attributedText = theAttributedString
                currentModuleLabel3Title.text = dataLabel[count3]
            }
            break
        case 3:
            if count4 != 0
            {
                count4 = count4 - 1;
                let theAttributedString = try! NSAttributedString(data: moduleDescriptionData4[count4].data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                                  options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                                  documentAttributes: nil)
                
                currentModuleLabel4.attributedText = theAttributedString
                currentModuleLabel4Title.text = dataLabel[count4]
            }
            break
        default :
            if count1 != 0
            {
                print("Here I am in default pre" )
                count1 = count1 - 1;
                currentModuleLabel1.text = moduleDescriptionData1[count1]
            }
            break
        }
    }
}*/

    
{
    @IBOutlet weak var navigationTitle: UILabel!
    @IBOutlet weak var tableViewTitle: UILabel!
    
    @IBOutlet weak var reviewModuleinfoTableview: UITableView!
    
    var moduleDescriptionData1 = [String]()//["1 info ", "2 info ", "3 info ", "4 info "]
    var moduleDescriptionData2 = [String]()//["1 info ", "2 info ", "3 info ", "4 info "]
    var moduleDescriptionData3 = [String]()//["1 info ", "2 info ", "3 info ", "4 info "]
    var moduleDescriptionData4 = [String]()//["1 info ", "2 info ", "3 info ", "4 info "]
    var currentModuleLabel1:UILabel!
    var currentModuleLabel2:UILabel!
    var currentModuleLabel3:UILabel!
    var currentModuleLabel4:UILabel!
    
    var currentModuleLabel1Title:UILabel!
    var currentModuleLabel2Title:UILabel!
    var currentModuleLabel3Title:UILabel!
    var currentModuleLabel4Title:UILabel!
    
    var module1NextButton:UIButton!
    var module1PreButton:UIButton!
    var module2NextButton:UIButton!
    var module2PreButton:UIButton!
    var module3NextButton:UIButton!
    var module3PreButton:UIButton!
    var module4NextButton:UIButton!
    var module4PreButton:UIButton!
    
    var moduleDescription1Text:String!
    var moduleDescription2Text:String!
    var moduleDescription3Text:String!
    var moduleDescription4Text:String!
    
    var count1:Int = 0
    var count2:Int = 0
    var count3:Int = 0
    var count4:Int = 0
    
    var moduleid = 3
    
    let dataLabel = ["Beginner".localized, "Learner".localized, "Professional".localized, "High Performer".localized, "Expert".localized]
    
    var rowNo:Int = 0
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var rmd1:ReviewModuleInfo = ReviewModuleInfo()
        
        switch moduleid {
        case 1:
            rmd1 = ReviewModule1InfoData()
            break
        case 2:
            rmd1 = ReviewModule2InfoData()
            break
        case 3:
            rmd1 = ReviewModule3InfoData()
            break
        case 4:
            rmd1 = ReviewModule4InfoData()
            break
        case 5:
            rmd1 = ReviewModule5InfoData()
            break
        case 6:
            rmd1 = ReviewModule6InfoData()
            break
        default:
            print("Error while finding the Module id : \(moduleid)")
        }
        
        rmd1.AdddataforDiscripter()
        
        moduleDescriptionData1 = rmd1.discripter1Data
        moduleDescriptionData2 = rmd1.discripter2Data
        if moduleid != 5
        {
            moduleDescriptionData3 = rmd1.discripter3Data
            moduleDescription3Text = "<b>" + "Descriptor 3".localized + "</b><br />" + rmd1.discripter3.descripterText
        }
        if moduleid == 3
        {
            moduleDescriptionData4 = rmd1.discripter4Data
            moduleDescription4Text = "<b>" + "Descriptor 4".localized + "</b><br />" + rmd1.discripter4.descripterText
            
        }
        
        
        moduleDescription1Text = "<b>" + "Descriptor 1".localized + "</b><br />" + rmd1.discripter1.descripterText
        moduleDescription2Text = "<b>" + "Descriptor 2".localized + "</b><br />" + rmd1.discripter2.descripterText
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let theString1 = String(format:NSLocalizedString("ReviewModuleInfoTitle", comment: "any comment"), "three", "Welcome")
        //print("####### Desc is " + pDataProductDesc)
        let theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                           options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                           documentAttributes: nil)
        
        tableViewTitle.attributedText = theAttributedString1
        
        
        switch moduleid {
        case 1:
            self.navigationTitle.text = "Welcome"
            var theString1 = String(format:NSLocalizedString("ReviewModuleInfoTitle", comment: "any comment"), "three", "Welcome")
            //print("####### Desc is " + pDataProductDesc)
            theString1 = Constants.DiscripterFontTag + theString1 + Constants.fontclosing
            let theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                               documentAttributes: nil)
            
            tableViewTitle.attributedText = theAttributedString1
            
            // self.tabBarController?.navigationItem.title = "My Title"
            // tableViewTitle.text =
            break
        case 2:
            self.navigationTitle.text = "Understand"
            var theString1 = String(format:NSLocalizedString("ReviewModuleInfoTitle", comment: "any comment"), "three", "Understand")
            theString1 = Constants.DiscripterFontTag + theString1 + Constants.fontclosing
            //print("####### Desc is " + pDataProductDesc)
            let theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                               documentAttributes: nil)
            
            tableViewTitle.attributedText = theAttributedString1
            break
        case 3:
            self.navigationTitle.text = "Advise"
            var theString1 = String(format:NSLocalizedString("ReviewModuleInfoTitle", comment: "any comment"), "four", "Advise")
            theString1 = Constants.DiscripterFontTag + theString1 + Constants.fontclosing
            //print("####### Desc is " + pDataProductDesc)
            let theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                               documentAttributes: nil)
            
            tableViewTitle.attributedText = theAttributedString1
            break
        case 4:
            self.navigationTitle.text = "Close"
            var theString1 = String(format:NSLocalizedString("ReviewModuleInfoTitle", comment: "any comment"), "three", "Close")
            theString1 = Constants.DiscripterFontTag + theString1 + Constants.fontclosing
            //print("####### Desc is " + pDataProductDesc)
            let theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                               documentAttributes: nil)
            
            tableViewTitle.attributedText = theAttributedString1
            
            break
        case 5:
            self.navigationTitle.text = "Thank"
            var theString1 =  String(format:NSLocalizedString("ReviewModuleInfoTitle", comment: "any comment"), "two", "Thank")
            theString1 = Constants.DiscripterFontTag + theString1 + Constants.fontclosing
            //print("####### Desc is " + pDataProductDesc)
            let theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                               documentAttributes: nil)
            
            tableViewTitle.attributedText = theAttributedString1
            break
        case 6:
            self.navigationTitle.text = "Handling Customer Complaints"
            var theString1 =  String(format:NSLocalizedString("ReviewModuleInfoTitle", comment: "any comment"), "three", "Handling Customer Complaints")
            theString1 = Constants.DiscripterFontTag + theString1 + Constants.fontclosing
            //print("####### Desc is " + pDataProductDesc)
            let theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                               documentAttributes: nil)
            
            tableViewTitle.attributedText = theAttributedString1
            break
        default:
            break
        }

        
        //tableViewTitle.text = "ReviewModuleInfoTitle".localized
    }
    @IBAction func closeButton(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    private func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if moduleid == 3
        {
            return 4
        }
        else if moduleid == 5
        {
            return 2
        }
        else
        {
            return 3
        }
    }
    /* func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
     
     switch indexPath.row {
     case 0:
     return 201.0
     case 1:
     return 367.0
     case 2:
     return 515.0
     case 3:
     return 500.0
     case 4:
     return 190.0
     default:
     return 190.0
     
     }
     }*/
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:UITableViewCell = self.reviewModuleinfoTableview.dequeueReusableCell(withIdentifier: "ReviewModuleInfocell_1") as! ReviewModuleInfocell_1
        
        if indexPath.row == 0
        {
            let cell = self.reviewModuleinfoTableview.dequeueReusableCell(withIdentifier: "ReviewModuleInfocell_1") as! ReviewModuleInfocell_1
            
            let decLabel:UILabel = cell.descriptorLabel1
            decLabel.text = moduleDescription1Text //.html2String
            
            var theString1 = decLabel.text!
            //print("####### Desc is " + pDataProductDesc)
            //theString1 = Constants.DiscripterFontTag + theString1 + Constants.fontclosing
            let theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                               documentAttributes: nil)
            
            decLabel.attributedText = theAttributedString1
            module1NextButton = cell.cell1NextButton
            cell.cell1NextButton.tag = indexPath.row
            
            currentModuleLabel1 = cell.moduleDescription1
            currentModuleLabel1.text = moduleDescriptionData1[count1] //.html2String
            
            
            let theString = currentModuleLabel1.text!
            
            //print("####### Desc is " + pDataProductDesc)
            let theAttributedString = try! NSAttributedString(data: theString.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                              options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                              documentAttributes: nil)
            
            currentModuleLabel1.attributedText = theAttributedString
            
            
            
            currentModuleLabel1Title = cell.descriptorTitleLabel1
            currentModuleLabel1Title.text = dataLabel[count1] //.html2String
            
            cell.cell1NextButton.addTarget(self, action: #selector(nextButtonTapped),for: .touchUpInside)
            module1PreButton = cell.cell1PreviousButton
            
            if count1 == 0
            {
                module1PreButton.alpha = 0.7
                module1PreButton.isEnabled = false
            }
            
            cell.cell1PreviousButton.tag = indexPath.row
            cell.cell1PreviousButton.addTarget(self, action: #selector(previouButtonTapped), for: .touchUpInside);
            cell.selectionStyle =  UITableViewCellSelectionStyle.none
            return cell
        }
        else if indexPath.row == 1
        {
            let cell = self.reviewModuleinfoTableview.dequeueReusableCell(withIdentifier: "ReviewModuleInfocell_2") as! ReviewModuleInfocell_2
            cell.cell2NextButton.tag = indexPath.row
            
            //cell.descriptorLabel2.text = moduleDescription2Text //.html2String
            
            let decLabel:UILabel = cell.descriptorLabel2
            decLabel.text = moduleDescription2Text //.html2String
            
            let theString1 = decLabel.text!
            //print("####### Desc is " + pDataProductDesc)
            let theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                               documentAttributes: nil)
            
            decLabel.attributedText = theAttributedString1
            
            
            module2NextButton = cell.cell2NextButton
            currentModuleLabel2 = cell.moduleDescription2
            currentModuleLabel2.text = moduleDescriptionData2[count2]//.html2String
            
            
            let theString = currentModuleLabel2.text!
            //print("####### Desc is " + pDataProductDesc)
            let theAttributedString = try! NSAttributedString(data: theString.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                              options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                              documentAttributes: nil)
            
            currentModuleLabel2.attributedText = theAttributedString
            
            currentModuleLabel2Title = cell.descriptorTitleLabel2
            currentModuleLabel2Title.text = dataLabel[count2]//.html2String
            
            cell.cell2NextButton.addTarget(self, action: #selector(nextButtonTapped),for: .touchUpInside)
            cell.cell2previousButton.tag = indexPath.row
            
            module2PreButton = cell.cell2previousButton
            
            if count2 == 0
            {
                module2PreButton.alpha = 0.7
                module2PreButton.isEnabled = false
            }
            
            
            cell.cell2previousButton.addTarget(self, action: #selector(previouButtonTapped), for: .touchUpInside);
            cell.selectionStyle =  UITableViewCellSelectionStyle.none
            return cell
        }
        else if indexPath.row == 2
        {
            let cell = self.reviewModuleinfoTableview.dequeueReusableCell(withIdentifier: "ReviewModuleInfocell_3") as! ReviewModuleInfocell_3
            cell.cell3NextButton.tag = indexPath.row
            
            
            let decLabel:UILabel = cell.descriptorLabel3
            decLabel.text = moduleDescription3Text //.html2String
            
            let theString1 = decLabel.text!
            //print("####### Desc is " + pDataProductDesc)
            let theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                               documentAttributes: nil)
            
            decLabel.attributedText = theAttributedString1
            
            currentModuleLabel3 = cell.moduleDescription3
            currentModuleLabel3.text = moduleDescriptionData3[count3]
            
            let theString = currentModuleLabel3.text!
            //print("####### Desc is " + pDataProductDesc)
            let theAttributedString = try! NSAttributedString(data: theString.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                              options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                              documentAttributes: nil)
            
            currentModuleLabel3.attributedText = theAttributedString
            
            
            module3NextButton = cell.cell3NextButton
            currentModuleLabel3Title = cell.descriptoTitleLabel3
            currentModuleLabel3Title.text = dataLabel[count3] //.html2String
            
            cell.cell3NextButton.addTarget(self, action: #selector(nextButtonTapped),for: .touchUpInside)
            cell.cell3PreviousButton.tag = indexPath.row
            
            module3PreButton = cell.cell3PreviousButton
            
            if count3 == 0
            {
                module3PreButton.alpha = 0.7
                module3PreButton.isEnabled = false
            }
            
            
            cell.cell3PreviousButton.addTarget(self, action: #selector(previouButtonTapped), for: .touchUpInside);
            cell.selectionStyle =  UITableViewCellSelectionStyle.none
            return cell
        }
        else if indexPath.row == 3
        {
            let cell = self.reviewModuleinfoTableview.dequeueReusableCell(withIdentifier: "ReviewModuleInfocell_4") as! ReviewModuleInfocell_4
            cell.cell4NextButton.tag = indexPath.row
            
            let decLabel:UILabel = cell.descriptorLabel4
            decLabel.text = moduleDescription4Text //.html2String
            
            let theString1 = decLabel.text!
            //print("####### Desc is " + pDataProductDesc)
            let theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                               documentAttributes: nil)
            
            decLabel.attributedText = theAttributedString1
            
            module4NextButton = cell.cell4NextButton
            currentModuleLabel4 = cell.moduleDescription4
            currentModuleLabel4.text = moduleDescriptionData4[count4]
            
            let theString = currentModuleLabel4.text!
            //print("####### Desc is " + pDataProductDesc)
            let theAttributedString = try! NSAttributedString(data: theString.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                              options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                              documentAttributes: nil)
            
            currentModuleLabel4.attributedText = theAttributedString
            
            
            
            currentModuleLabel4Title = cell.descriptorTitleLabel4
            currentModuleLabel4Title.text = dataLabel[count4] //.html2String
            
            
            cell.cell4NextButton.addTarget(self, action: #selector(nextButtonTapped),for: .touchUpInside)
            cell.cell4PreviousButton.tag = indexPath.row
            
            module4PreButton = cell.cell4PreviousButton
            
            if count4 == 0
            {
                module4PreButton.alpha = 0.7
                module4PreButton.isEnabled = false
            }
            
            
            cell.cell4PreviousButton.addTarget(self, action: #selector(previouButtonTapped), for: .touchUpInside);
            cell.selectionStyle =  UITableViewCellSelectionStyle.none
            return cell
        }
        
        /*else{
         let cell = self.reviewModuleinfoTableview.dequeueReusableCell(withIdentifier: "ReviewModuleInfocell_4") as! ReviewModuleInfocell_4
         cell.cell4NextButton.tag = indexPath.row
         cell.cell4NextButton.addTarget(self, action: Selector(("cell4NextButton:")), for: UIControlEvents.touchUpInside);
         cell.cell4PreviousButton.tag = indexPath.row
         cell.cell4PreviousButton.addTarget(self, action: Selector(("cell4PreviousButton:")), for: UIControlEvents.touchUpInside);
         return cell
         }*/
        //        cell.selectionStyle =  UITableViewCellSelectionStyle.none
        return cell
    }
    
    func nextButtonTapped(sender:UIButton!){
        print("Here I have clicked the next action \(sender.tag)")
        
        /*if count < moduleDescriptionData1.count - 1
         {
         count = count + 1;
         currentModuleLabel.text = moduleDescriptionData1[count]
         }*/
        
        switch sender.tag {
        case 0:
            
            
            if count1 < moduleDescriptionData1.count - 1
            {
                /* module1PreButton.alpha = 1
                 module1PreButton.isEnabled = true
                 
                 if count1 == moduleDescriptionData1.count - 2
                 {
                 
                 module1NextButton.alpha = 0.7
                 module1NextButton.isEnabled = false
                 }
                 else
                 {
                 module1NextButton.alpha = 1
                 module1NextButton.isEnabled = true
                 }
                 */
                
                nextButtonValidation(nextButtonObj: module1NextButton, preButtonObj: module1PreButton, counter: count1, discripterCount: moduleDescriptionData1.count - 2)
                //nextButtonValidation(nextButtonObj: module1PreButton, counter:count1, discripterCount:moduleDescriptionData1.count - 2)
                count1 = count1 + 1;
                
                //currentModuleLabel1.text = moduleDescriptionData1[count1]
                let theAttributedString = try! NSAttributedString(data: moduleDescriptionData1[count1].data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                                  options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                                  documentAttributes: nil)
                
                currentModuleLabel1.attributedText = theAttributedString
                
                //currentModuleLabel1.text = moduleDescriptionData1[count1]//.html2String
                currentModuleLabel1Title.text = dataLabel[count1] //.html2String
            }
            break
        case 1:
            if count2 < moduleDescriptionData2.count - 1
            {
                nextButtonValidation(nextButtonObj: module2NextButton, preButtonObj: module2PreButton, counter:count2, discripterCount:moduleDescriptionData2.count - 2)
                
                count2 = count2 + 1;
                //currentModuleLabel2.text = moduleDescriptionData2[count2]
                let theAttributedString = try! NSAttributedString(data: moduleDescriptionData2[count2].data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                                  options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                                  documentAttributes: nil)
                
                currentModuleLabel2.attributedText = theAttributedString
                currentModuleLabel2Title.text = dataLabel[count2]
            }
            break
        case 2:
            if count3 < moduleDescriptionData3.count - 1
            {
                
                nextButtonValidation(nextButtonObj: module3NextButton, preButtonObj: module3PreButton, counter:count3, discripterCount:moduleDescriptionData3.count - 2)
                count3 = count3 + 1;
                let theAttributedString = try! NSAttributedString(data: moduleDescriptionData3[count3].data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                                  options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                                  documentAttributes: nil)
                
                currentModuleLabel3.attributedText = theAttributedString
                currentModuleLabel3Title.text = dataLabel[count3]
            }
            break
        case 3:
            if count4 < moduleDescriptionData4.count - 1
            {
                nextButtonValidation(nextButtonObj: module4NextButton, preButtonObj: module4PreButton, counter:count4, discripterCount:moduleDescriptionData4.count - 2)
                
                count4 = count4 + 1;
                let theAttributedString = try! NSAttributedString(data: moduleDescriptionData4[count4].data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                                  options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                                  documentAttributes: nil)
                
                currentModuleLabel4.attributedText = theAttributedString
                currentModuleLabel4Title.text = dataLabel[count4]
            }
            break
        default :
            if count1 < moduleDescriptionData1.count - 1
            {
                print("Here I am in default next" )
                count1 = count1 + 1;
                currentModuleLabel1.text = moduleDescriptionData1[count1]
            }
            break
            
        }
        
    }
    
    func previouButtonTapped(sender:UIButton!)
    {
        switch sender.tag {
        case 0:
            if count1 != 0
            {
                module1NextButton.alpha = 1
                module1NextButton.isEnabled = true
                
                if count1 == 1
                {
                    module1PreButton.alpha = 0.7
                    module1PreButton.isEnabled = false
                }
                else
                {
                    module1PreButton.alpha = 1
                    module1PreButton.isEnabled = true
                }
                //preButtonValidation(preButtonObj: module1PreButton, nextButtonObj: module1NextButton, counter: count1)
                
                count1 = count1 - 1;
                //currentModuleLabel1.text = moduleDescriptionData1[count1]//.html2String
                let theAttributedString = try! NSAttributedString(data: moduleDescriptionData1[count1].data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                                  options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                                  documentAttributes: nil)
                
                currentModuleLabel1.attributedText = theAttributedString
                currentModuleLabel1Title.text = dataLabel[count1]
                
            }
            break
        case 1:
            if count2 != 0
            {
                preButtonValidation(preButtonObj: module2PreButton, nextButtonObj: module2NextButton, counter: count2)
                
                count2 = count2 - 1;
                let theAttributedString = try! NSAttributedString(data: moduleDescriptionData2[count2].data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                                  options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                                  documentAttributes: nil)
                
                currentModuleLabel2.attributedText = theAttributedString
                currentModuleLabel2Title.text = dataLabel[count2]
            }
            break
        case 2:
            if count3 != 0
            {
                preButtonValidation(preButtonObj: module3PreButton, nextButtonObj: module3NextButton, counter: count3)
                
                count3 = count3 - 1;
                let theAttributedString = try! NSAttributedString(data: moduleDescriptionData3[count3].data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                                  options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                                  documentAttributes: nil)
                
                currentModuleLabel3.attributedText = theAttributedString
                currentModuleLabel3Title.text = dataLabel[count3]
            }
            break
        case 3:
            if count4 != 0
            {
                preButtonValidation(preButtonObj: module4PreButton, nextButtonObj: module4NextButton, counter: count4)
                
                count4 = count4 - 1;
                let theAttributedString = try! NSAttributedString(data: moduleDescriptionData4[count4].data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                                  options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                                  documentAttributes: nil)
                
                currentModuleLabel4.attributedText = theAttributedString
                currentModuleLabel4Title.text = dataLabel[count4]
            }
            break
        default :
            if count1 != 0
            {
                print("Here I am in default pre" )
                count1 = count1 - 1;
                currentModuleLabel1.text = moduleDescriptionData1[count1]
            }
            break
        }
    }
    
    func preButtonValidation(preButtonObj:UIButton, nextButtonObj:UIButton, counter:Int)
    {
        
        /*module1NextButton.alpha = 1
         module1NextButton.isEnabled = true
         
         if count1 == 1
         {
         module1PreButton.alpha = 0.7
         module1PreButton.isEnabled = false
         }
         else
         {
         module1PreButton.alpha = 1
         module1PreButton.isEnabled = true
         }*/
        
        enabledButton(button: nextButtonObj)
        if counter == 1
        {
            desableButton(button: preButtonObj)
        }
        else
        {
            enabledButton(button: preButtonObj)
        }
    }
    
    func nextButtonValidation(nextButtonObj:UIButton, preButtonObj:UIButton, counter:Int, discripterCount:Int)
    {
        /*module1PreButton.alpha = 1
         module1PreButton.isEnabled = true
         
         if count1 == moduleDescriptionData1.count - 2
         {
         
         module1NextButton.alpha = 0.7
         module1NextButton.isEnabled = false
         }
         else
         {
         module1NextButton.alpha = 1
         module1NextButton.isEnabled = true
         }
         */
        
        enabledButton(button: preButtonObj)
        
        if counter == discripterCount
        {
            desableButton(button: nextButtonObj)
        }
        else
        {
            enabledButton(button: nextButtonObj)
        }
    }
    
    func desableButton(button:UIButton)
    {
        button.alpha = 0.7
        button.isEnabled = false
    }
    
    func enabledButton(button:UIButton)
    {
        button.alpha = 1
        button.isEnabled = true
    }
    
}


