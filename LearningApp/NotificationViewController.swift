//
//  NotificationViewController.swift
//  LearningApp
//
//  Created by Mac-04 on 02/09/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableview: UITableView!
    var progressHUD = ProgressHUD()
    var notifsData: [Notification6one5] = []
    var currentNotifIndex: Int = 0
    
    var fontsize:Int = 10
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController!.navigationBar.topItem!.title = "Rep_Notifs".localized;
       
        self.tableview!.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.tableview!.layer.shadowColor = UIColor.black.cgColor
        self.tableview!.layer.shadowRadius = 4
        self.tableview!.layer.shadowOpacity = 0.25
        self.tableview!.layer.masksToBounds = false;
        
        
        //self.tableview.RowHeight = UITableView.AutomaticDimension;
        //self.tableview.EstimatedRowHeight = 60;
        if Constants.device == "iPhone"
        {
            fontsize = 16
        }
        else
        {
            fontsize = 22
        }
        //print("Here I am in the VDL -----------------------------------")
        
        //NotificationCenter.default.addObserver(self, selector: "loadList:",name:NSNotification.Name(rawValue: "load"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(NotificationViewController.loadList), name: NSNotification.Name(rawValue: "reloadList"), object: nil)
    }
    
    func loadList(){
        //load data here
        getNotifs()
        self.getNotifCount()
        print("YEEEEEEEEEEE")
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        let notifCount = APIFunctions.getNotifCount()
//        tabBarController?.tabBar.items![2].badgeValue = String(notifCount);
        
        
        getNotifs()
        self.getNotifCount()
        //self.tableview.reloadData()
        //self.tableview.reloadData()
        //print("Here I am in the VWA -----------------------------------")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    let NotificationTitle = ["New Module Added","New Module Added","New Module Added","New Module Added","New Module Added","New Module Added","New Module Added","New Module Added"]
    let NotificationDate = ["Today","AUG 10","AUG 9","AUG 8","AUG 7","AUG 7","AUG 6","AUG 6"]

     func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifsData.count
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NoticficationCell", for: indexPath) as! NotificationTblVwCell
        
        cell.NotificationTitleLabel.text = notifsData[(indexPath as NSIndexPath).row].notifName
        cell.NotificationDescLabel.text = notifsData[(indexPath as NSIndexPath).row].notifContent
        // MARK: ReadUnread
        let statusStr = notifsData[indexPath.row].notificationStauts
        
        
        
        
        if statusStr == "1"
        {
            
            cell.NotificationTitleLabel.font = UIFont.systemFont(ofSize: CGFloat(fontsize))
            cell.NotificationTitleLabel.textColor = UIColor.black
            cell.NotificationDescLabel.textColor = UIColor.darkGray
            cell.NotificationDaylabel.textColor = UIColor.darkGray
            
        }
        else
        {
            cell.NotificationTitleLabel.font = UIFont.boldSystemFont(ofSize: CGFloat(fontsize))
            cell.NotificationTitleLabel.textColor = UIColor.black
            cell.NotificationDescLabel.textColor = UIColor.darkGray
            cell.NotificationDaylabel.textColor = UIColor.darkGray
          
            
        }
       
        let strTime = notifsData[(indexPath as NSIndexPath).row].notifAssignedAt
        print("Notif Date: \(strTime)")
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateVal: Date = formatter.date(from: strTime)!
        if(NSCalendar.current.isDateInToday(dateVal))
        {
            
                let dateFormatter = DateFormatter()
                //if some one want 12AM,5PM then use "hh mm a"
                //if some one want 00AM,17PM then use "HH mm a"
                dateFormatter.dateFormat = "hh:mm a"
                let stringDate = dateFormatter.string(from:dateVal)
                print (stringDate)
                cell.NotificationDaylabel.text = stringDate
//                let strSplit = strTime.characters.split(separator: " ")
//            cell.NotificationDaylabel.text = String(strSplit.last!)
        } else {
            let strSplit = strTime.characters.split(separator: " ")
            cell.NotificationDaylabel.text = String(strSplit.first!)
//            cell.NotificationDaylabel.text = notifsData[(indexPath as NSIndexPath).row].notifAssignedAt
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if notifsData[(indexPath as NSIndexPath).row].notifNotificationType == "1" {
            print("Notif type: 1")
            self.currentNotifIndex = (indexPath as NSIndexPath).row
            let popOverVC = UIStoryboard (name: "Main", bundle: nil).instantiateViewController(withIdentifier: "notifDetailedView") as! NotificationDetailViewController
            //self.addChildViewController(popOverVC)
            //popOverVC.view.frame = self.view.frame
            self.navigationController?.pushViewController(popOverVC, animated: true)
            //popOverVC.didMove(toParentViewController: self)
            self.navigationController?.isNavigationBarHidden = true
            print((indexPath as NSIndexPath).row)
        }
        else
        {
            print("Notif type: 2")
            let popOverVC = UIStoryboard (name: "Main", bundle: nil).instantiateViewController(withIdentifier: "feedbackPopupID") as! FeedbackPopUpVwCtrlr
            
            popOverVC.notificationID = notifsData[indexPath.row].notifId
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            //self.navigationController?.pushViewController(popOverVC, animated: true)
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParentViewController: self)
            self.navigationController?.isNavigationBarHidden = true
            print((indexPath as NSIndexPath).row)
            
        }
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle
    {
        return UITableViewCellEditingStyle.delete
        
        /*if notificationData.getNotificationType(index: indexPath.row) == "3"
         {
         return UITableViewCellEditingStyle.none
         }
         else
         {
         return UITableViewCellEditingStyle.delete
         }*/
        /*let object = items[indexPath.row]
         if object.name == "joyce" {
         return UITableViewCellEditingStyle.Delete
         } else {
         return UITableViewCellEditingStyle.None
         }*/
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        if editingStyle == .delete
        {
            print("Delete request found \(indexPath.row) ")
            
            
            let notificationId = notifsData[indexPath.row].notifId
            removeNotification(notificationId: notificationId)
            
            tableView.reloadData()
            //let status = removeNotification()
            // TODO: Here Manager asked to delete this notification
            // This is normal notification
        }
        
    }

    
    func getNotifs() {
        
//        var apiNotif: [Notification6one5] = []
        
        /*var boxView = UIView()
        boxView = UIView(frame: CGRect(x: view.frame.midX - 125, y: view.frame.midY - 25, width: 250, height: 50))
        boxView.backgroundColor = UIColor.white
        boxView.alpha = 1
        boxView.layer.cornerRadius = 10
        let activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        activityView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityView.startAnimating()
        let textLabel = UILabel(frame: CGRect(x: 60, y: 0, width: 200, height: 50))
        textLabel.textColor = UIColor.gray
        textLabel.text = "notif_update".localized
        boxView.addSubview(activityView)
        boxView.addSubview(textLabel)
        view.addSubview(boxView)*/
        
        let progressHUD = ProgressHUD()
        self.view.addSubview(progressHUD)
        UIApplication.shared.beginIgnoringInteractionEvents()
        

        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            ]
        
        let prefs:UserDefaults = UserDefaults.standard
        let user_id = prefs.value(forKey: "user_id") as! String!
        
        let notifURL =  Constants.salesRepNotifInfoURL + user_id!
        let request = NSMutableURLRequest(url: NSURL(string: notifURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("Notifs not updated. Error: \(error)")
                
                DispatchQueue.main.async {
                    progressHUD.hide()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    let alert = UIAlertController(title: "notif_title".localized, message: "notif_error".localized, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "notif_ok".localized, style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            } else {
                let httpResponse = response as? HTTPURLResponse
                print("HTTP Response: \(httpResponse)")
                do {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        print("Notification Data ",convertedJsonIntoDict)
                        
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        
                        print("Status is \(statusVal)")
                        
                        if statusVal == true {
                            print("True! Status is \(statusVal)")
                            if let notifs = convertedJsonIntoDict["data"] as? NSArray {
                                
                                //progressHUD.hide()
                                //UIApplication.shared.endIgnoringInteractionEvents()
                                self.notifsData = []
                                let notifAPICount = notifs.count
                                let not = convertedJsonIntoDict["count"] as? Int
                               // self.tabBarController?.tabBar.items![2].badgeValue = String(describing: not!)
                                print("Notif Count: \(convertedJsonIntoDict["count"] as? Int)")
                                for i in 0..<notifAPICount {
                                    if let notifData = notifs[i] as? NSDictionary {
                                        let notif: Notification6one5 = Notification6one5()
                                        notif.notifId = notifData["id"] as! String
                                        notif.notifName = notifData["name"] as! String
                                        notif.notifContent = notifData["content"] as! String
                                        notif.notifNotificationId = notifData["notification_id"] as! String
                                        notif.notifUserId = notifData["user_id"] as! String
                                        notif.notifBadgeId = notifData["badge_id"] as! String
                                        notif.notifCourseId = notifData["course_id"] as! String
                                        notif.notifModuleId = notifData["module_id"] as! String
                                        notif.notifRepresentativeId = notifData["representative_id"] as! String
                                        notif.notifNotificationType = notifData["notification_type"] as! String
                                        notif.notifAssignedAt = notifData["assigned_at"] as! String
                                        notif.notificationStauts = notifData["is_read"] as! String
                                       
                                        self.notifsData.append(notif)
                                    }
                                    DispatchQueue.main.async
                                        {
                                            //self.tabBarController?.tabBar.items![2].badgeValue = String(describing: not!)
                                          
                                            self.tableview.reloadData()
                                            progressHUD.hide()
                                            UIApplication.shared.endIgnoringInteractionEvents()
                                    }
                                }
                                
                            } else {
                                print("No notif data parsed!")
                                
                                progressHUD.hide()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                print("Error: \(error)")
                                DispatchQueue.main.async {
                                    let alert = UIAlertController(title: "notif_title".localized, message: "notif_error".localized, preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "notif_ok".localized, style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                }
                            }
                        } else {
                            print("Not true! Status is \(statusVal)")
                            progressHUD.hide()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            print("Error: \(error)")
                            DispatchQueue.main.async {
                                let alert = UIAlertController(title: "notif_title".localized, message: "notif_error".localized, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "notif_ok".localized, style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                        DispatchQueue.main.async
                            {
                               self.getNotifCount()
                                self.tableview.reloadData()
                                progressHUD.hide()
                                UIApplication.shared.endIgnoringInteractionEvents()
                        }
                    }
                } catch let error as NSError {
                    print(error)
                    print("Notifs not updated. Error: \(error)")
                    progressHUD.hide()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    print("Error: \(error)")
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "notif_title".localized, message: "notif_error".localized, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "notif_ok".localized, style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        })
        
        dataTask.resume()
        
    }
    
    func removeNotification(notificationId:String)
    {
        let progressHUD = ProgressHUD()
        self.view.addSubview(progressHUD)
        //progressHUD.activityIndictor.color = UIColor.black
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        RemoveUserNotification.sharedInstance.loadData(notificationId:notificationId, completionHandler: { (status:Bool) -> () in
            DispatchQueue.main.async() {
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
            }
            if (self.notifsData.count >= 0 )
            {
                let login = Login.sharedInstance
                
                if login.status != nil
                {
                    let alertController = UIAlertController(title: "User status", message: login.message , preferredStyle: .alert)
                    let OKAction = UIAlertAction(title: "ok".localized, style: .default) {
                        (action:UIAlertAction!) in
                        login.logoutFromDeviceCache()
                        self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
                    }
                    alertController.addAction(OKAction)
                    self.present(alertController, animated: true, completion:nil)
                    progressHUD.hide()
                    
                    UIApplication.shared.endIgnoringInteractionEvents()
                    
                }
                if status
                {
                    DispatchQueue.main.async()
                        {
                            //self.loadLernerData()
                            self.notifsData = []
                            self.getNotifs()
                            progressHUD.hide()
                            //self.reviewNotificationData.desc()
                            //self.tableview.reloadData()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            print("Notification deleted sucessfuly")
                            progressHUD.hide()
                            
                            UIApplication.shared.endIgnoringInteractionEvents()
                    } //learner details service completed
                }
                else
                {
                    DispatchQueue.main.async()
                        {
                    print("Problem while loading data ")
                    let alert = UIAlertController(title: "notificationerror".localized, message: "errordetails".localized, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "ok".localized, style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    progressHUD.hide()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    }
                    
                }
            }// end if learnerDetails.getLearnerCount
            
        })
    }

    func getNotifCount()
    {
        
        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            ]
        
        //        var notifAPICount: Int = 0
        
        let prefs:UserDefaults = UserDefaults.standard
        let user_id = prefs.value(forKey: "user_id") as! String!
        
        let notifURL =  Constants.salesRepNotifInfoURL + user_id!
        let request = NSMutableURLRequest(url: NSURL(string: notifURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            DispatchQueue.main.async()
                {
                    
                    if (error != nil)
                    {
                        print("Notifs not updated. Error: \(error)")
                        self.progressHUD.hide()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        
                    } else
                    {
                        let httpResponse = response as? HTTPURLResponse
                        print("HTTP Response: \(httpResponse)")
                        do {
                            if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                                
                                print("Print converted dictionary",convertedJsonIntoDict)
                                
                                let statusVal = convertedJsonIntoDict["status"] as? Bool
                                
                                print("Status is \(statusVal)")
                                if statusVal == true {
                                    print("True! Status is \(statusVal)")
                                    if let notifs = convertedJsonIntoDict["data"] as? NSArray {
                                        let notifAPICount = notifs.count
                                        let not = convertedJsonIntoDict["count"] as? Int
                                        self.tabBarController?.tabBar.items![2].badgeValue = String(describing: not!)
                                        print("Notif Count: \(notifAPICount)")
                                        //self.tabBarController?.tabBar.items![2].badgeValue = String(notifAPICount);
                                    } else {
                                        print("No notif data parsed!")
                                    }
                                } else {
                                    print("Not true! Status is \(statusVal)")
                                }
                            }
                            self.progressHUD.hide()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            
                        } catch let error as NSError {
                            print(error)
                            print("Notifs not updated. Error: \(error)")
                            self.progressHUD.hide()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            
                        }
                    }
            }
        })
        
        dataTask.resume()
        
    }

}
