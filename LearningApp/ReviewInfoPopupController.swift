//
//  ReviewInfoPopupController.swift
//  6one5Manager
//
//  Created by enyotalearning on 03/01/17.
//  Copyright © 2017 enyotalearning. All rights reserved.
//

import UIKit

class ReviewInfoPopupController: UIViewController {
    
    
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var bottomBar: UIView!
    
    var popupNo = 0;
    var rowNo = 0;
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        //let introductionScreenView = self.navigationController!.viewControllers[0] as! ReviewInfoViewController
        var theString1:String!
        var theAttributedString1:NSAttributedString!

        if rowNo == 1
        {
            switch popupNo {
            case 1:
                self.titleText.text = "Step 1".localized
          
                
                
                
//                theString1 = "Step 1".localized
//                //print("####### Desc is " + pDataProductDesc)
//                theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
//                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
//                                                               documentAttributes: nil)
//                
//                self.titleText.attributedText = theAttributedString1
                
                
                theString1 = "Step 1_Explanation".localized
                //print("####### Desc is " + pDataProductDesc)
                theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                               documentAttributes: nil)
                
                self.message.attributedText = theAttributedString1
                
                self.bottomBar.backgroundColor = UIColor.init(red: 55.0/255.0, green: 69.0/255.0, blue: 95.0/255.0, alpha: 1.0)
                break
            case 2:
                self.titleText.text = "Step 2".localized
                //                theString1 = "Step 2".localized
//                //print("####### Desc is " + pDataProductDesc)
//                theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
//                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
//                                                               documentAttributes: nil)
//                
//                self.titleText.attributedText = theAttributedString1
                
                theString1 = "Step 2_Explanation".localized
                //print("####### Desc is " + pDataProductDesc)
                theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                               documentAttributes: nil)
                
                self.message.attributedText = theAttributedString1
                
                self.bottomBar.backgroundColor = UIColor.init(red: 100.0/255.0, green: 120.0/255.0, blue: 59.0/255.0, alpha: 1.0)
                break
            case 3:
                self.titleText.text = "Step 3".localized
                

//                theString1 = "Step 3".localized
//                //print("####### Desc is " + pDataProductDesc)
//                theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
//                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
//                                                               documentAttributes: nil)
//                
//                self.titleText.attributedText = theAttributedString1
                
                theString1 = "Step 3_Explanation".localized
                //print("####### Desc is " + pDataProductDesc)
                theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                               documentAttributes: nil)
                
                self.message.attributedText = theAttributedString1
                
                
                self.bottomBar.backgroundColor = UIColor.init(red: 215.0/255.0, green: 163.0/255.0, blue: 52.0/255.0, alpha: 1.0)
                break
            case 4:
                self.titleText.text = "Step 4".localized
                
//                theString1 = "Step 4".localized
//                //print("####### Desc is " + pDataProductDesc)
//                theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
//                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
//                                                               documentAttributes: nil)
//                
//                self.titleText.attributedText = theAttributedString1
                
                
                theString1 = "Step 4_Explanation".localized
                //print("####### Desc is " + pDataProductDesc)
                theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                               documentAttributes: nil)
                
                self.message.attributedText = theAttributedString1
                
                self.bottomBar.backgroundColor = UIColor.init(red: 170.0/255.0, green: 68.0/255.0, blue: 44.0/255.0, alpha: 1.0)
                break
            case 5:
                self.titleText.text = "Step 5".localized
                
//                theString1 = "Step 5".localized
//                //print("####### Desc is " + pDataProductDesc)
//                theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
//                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
//                                                               documentAttributes: nil)
//                
//                self.titleText.attributedText = theAttributedString1
                
                
                theString1 = "Step 5_Explanation".localized
                //print("####### Desc is " + pDataProductDesc)
                theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                               documentAttributes: nil)
                
                self.message.attributedText = theAttributedString1
                
                
                self.bottomBar.backgroundColor = UIColor.init(red: 133.0/255.0, green: 40.0/255.0, blue: 41.0/255.0, alpha: 1.0)
                break
            default:
                break
            }
        }
        
        if rowNo == 2
        {
            switch popupNo {
            case 1:
                self.titleText.text = "competencyTitle".localized
                
//                theString1 = "competencyTitle".localized
//                //print("####### Desc is " + pDataProductDesc)
//                theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
//                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
//                                                               documentAttributes: nil)
//                
//                self.titleText.attributedText = theAttributedString1
                
                
                theString1 = "competency_Explanation".localized
                theString1 = Constants.fontTag + theString1 + Constants.fontclosing
                //print("####### Desc is " + pDataProductDesc)
                theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                               documentAttributes: nil)
                
                self.message.attributedText = theAttributedString1
                self.bottomBar.backgroundColor = UIColor.init(red: 253.0/255.0, green: 164.0/255.0, blue: 12.0/255.0, alpha: 1.0)
                break
            case 2:
                self.titleText.text = "descriptorTitle".localized
                
//                theString1 = "descriptorTitle".localized
//                //print("####### Desc is " + pDataProductDesc)
//                theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
//                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
//                                                               documentAttributes: nil)
//                
//                self.titleText.attributedText = theAttributedString1
                
                
                theString1 = "descriptor_Explanation".localized
                theString1 = Constants.fontTag + theString1 + Constants.fontclosing
                //print("####### Desc is " + pDataProductDesc)
                theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                               documentAttributes: nil)
                
                self.message.attributedText = theAttributedString1
                self.bottomBar.backgroundColor = UIColor.init(red: 97.0/255.0, green: 173.0/255.0, blue: 10.0/255.0, alpha: 1.0)
                break
            case 3:
                self.titleText.text = "proficiency levelsTitle".localized
                

//                theString1 = "proficiency levelsTitle".localized
//                //print("####### Desc is " + pDataProductDesc)
//                theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
//                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
//                                                               documentAttributes: nil)
//                
//                self.titleText.attributedText = theAttributedString1
                
                theString1 = "proficiency levels_Explanation".localized
                theString1 = Constants.fontTag + theString1 + Constants.fontclosing
                //print("####### Desc is " + pDataProductDesc)
                theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                               documentAttributes: nil)
                
                self.message.attributedText = theAttributedString1
                self.bottomBar.backgroundColor = UIColor.init(red: 220.0/255.0, green: 7.0/255.0, blue: 9.0/255.0, alpha: 1.0)
                break
            case 4:
                self.titleText.text = "5 proficiency levelsTitle".localized
                
//                theString1 = "5 proficiency levelsTitle".localized
//                //print("####### Desc is " + pDataProductDesc)
//                theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
//                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
//                                                               documentAttributes: nil)
//                
//                self.titleText.attributedText = theAttributedString1
                
                
                theString1 = "5 proficiency levels_Explanation".localized
                theString1 = Constants.fontTag + theString1 + Constants.fontclosing
                //print("####### Desc is " + pDataProductDesc)
                theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                               documentAttributes: nil)
                
                self.message.attributedText = theAttributedString1
                self.bottomBar.backgroundColor = UIColor.init(red: 249.0/255.0, green: 98.0/255.0, blue: 15.0/255.0, alpha: 1.0)
                break
            case 5:
                self.titleText.text = "score calculatedTitle".localized
//                theString1 = "score calculatedTitle".localized
//                //print("####### Desc is " + pDataProductDesc)
//                theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
//                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
//                                                               documentAttributes: nil)
//                
//                self.titleText.attributedText = theAttributedString1
                
                theString1 = "score calculated_Explanation".localized
                theString1 = Constants.fontTag + theString1 + Constants.fontclosing
                //print("####### Desc is " + pDataProductDesc)
                theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                               documentAttributes: nil)
                
                self.message.attributedText = theAttributedString1
                self.bottomBar.backgroundColor = UIColor.init(red: 84.0/255.0, green: 192.0/255.0, blue: 211.0/255.0, alpha: 1.0)
                break
            default:
                break
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func closeClick(_ sender: AnyObject) {
        self.view.removeFromSuperview()
    }
    
}
