//
//  CerificatesData.swift
//  LearningApp
//
//  Created by Mac-04 on 06/09/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import Foundation

public struct CerificateData: Decodable {
    public let data: [App]?
    
    public init?(json: JSON) {
        data = "data" <~~ json
    }
}