//
//  Module3QuestionsController.swift
//  LearningApp
//
//  Created by Sumit More on 12/5/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import Foundation
import UIKit

class Module3QuestionsController: UIViewController {
    
    @IBOutlet weak var questionsExplanationLbl: UILabel!
    @IBOutlet weak var questionLbl: UILabel!
    @IBOutlet weak var bottomBar: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        let introductionScreenView = self.navigationController!.viewControllers[0] as! Module3IntroController 
        switch introductionScreenView.bulbNo {
        case 1:
            self.questionLbl.text = "Rep_Intro_M3_B1".localized
            self.questionsExplanationLbl.text = "Rep_Intro_M3_B1_Explanation".localized
            self.bottomBar.backgroundColor = UIColor.init(red: 255.0/255.0, green: 65.0/255.0, blue: 100.0/255.0, alpha: 1.0)
            break
        case 2:
            self.questionLbl.text = "Rep_Intro_M3_B2".localized
            self.questionsExplanationLbl.text = "Rep_Intro_M3_B2_Explanation".localized
            self.bottomBar.backgroundColor = UIColor.init(red: 255.0/255.0, green: 140.0/255.0, blue: 38.0/255.0, alpha: 1.0)
            break
        case 3:
            self.questionLbl.text = "Rep_Intro_M3_B3".localized
            self.questionsExplanationLbl.text = "Rep_Intro_M3_B3_Explanation".localized
            self.bottomBar.backgroundColor = UIColor.init(red: 54.0/255.0, green: 175.0/255.0, blue: 241.0/255.0, alpha: 1.0)
            break
        default:
            break
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func closeClick(_ sender: AnyObject) {
        self.view.removeFromSuperview()
    }
}
