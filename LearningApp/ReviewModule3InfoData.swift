//
//  ReviewModule3InfoData.swift
//  6one5Manager
//
//  Created by enyotalearning on 06/01/17.
//  Copyright © 2017 enyotalearning. All rights reserved.
//

import Foundation

class ReviewModule3InfoData:ReviewModuleInfo
{
    
    override func AdddataforDiscripter()
    {
        discripter1 = DiscripterModule()
        discripter2 = DiscripterModule()
        discripter3 = DiscripterModule()
        discripter4 = DiscripterModule()
        discripter4Data = [String]()
        
        discripter1Data = [String]()
        discripter2Data = [String]()
        discripter3Data = [String]()        //discripter 1 Data
        discripter1.descripterText = "M3D1Text".localized
        
        discripter1.addBeginer(input: "M3D1B1".localized)
        discripter1.addBeginer(input: "M3D1B2".localized)
        discripter1.addBeginer(input: "M3D1B3".localized)
        
        discripter1.addLearner(input: "M3D1L1".localized)
        discripter1.addLearner(input: "M3D1L2".localized)
        discripter1.addLearner(input: "M3D1L3".localized)
        
        discripter1.addProfessional(input: "M3D1P1".localized)
        discripter1.addProfessional(input: "M3D1P2".localized)
        discripter1.addProfessional(input: "M3D1P3".localized)
        
        discripter1.addHighPerformer(input: "M3D1H1".localized)
        discripter1.addHighPerformer(input: "M3D1H2".localized)
        discripter1.addHighPerformer(input: "M3D1H3".localized)
        
        discripter1.addExpert(input: "M3D1E1".localized)
        discripter1.addExpert(input: "M3D1E2".localized)
        discripter1.addExpert(input: "M3D1E3".localized)
        
        //discripter 2 Data
        discripter2.descripterText = "M3D2Text".localized
        
        discripter2.addBeginer(input: "M3D2B1".localized)
        discripter2.addBeginer(input: "M3D2B2".localized)
        discripter2.addBeginer(input: "M3D2B3".localized)
        
        
        discripter2.addLearner(input: "M3D2L1".localized)
        discripter2.addLearner(input: "M3D2L2".localized)
        discripter2.addLearner(input: "M3D2L3".localized)
        
        discripter2.addProfessional(input: "M3D2P1".localized)
        discripter2.addProfessional(input: "M3D2P2".localized)
        
        discripter2.addHighPerformer(input: "M3D2H1".localized)
        discripter2.addHighPerformer(input: "M3D2H2".localized)
        discripter2.addHighPerformer(input: "M3D2H3".localized)
        
        discripter2.addExpert(input: "M3D2E1".localized)
        discripter2.addExpert(input: "M3D2E2".localized)
        discripter2.addExpert(input: "M3D2E3".localized)
        
        //discripter 3 Data
        discripter3.descripterText = "M3D3Text".localized
        
        discripter3.addBeginer(input: "M3D3B1".localized)
        
        discripter3.addLearner(input: "M3D3L1".localized)
        
        discripter3.addProfessional(input: "M3D3P1".localized)
        
        discripter3.addHighPerformer(input: "M3D3H1".localized)
        
        discripter3.addExpert(input: "M3D3E1".localized)
        
        //discripter 4 Data
        discripter4.descripterText = "M3D4Text".localized
        
        discripter4.addBeginer(input: "M3D4B1".localized)
        discripter4.addBeginer(input: "M3D4B2".localized)
        
        discripter4.addLearner(input: "M3D4L1".localized)
        discripter4.addLearner(input: "M3D4L2".localized)
        
        
        discripter4.addProfessional(input: "M3D4P1".localized)
        discripter4.addProfessional(input: "M3D4P2".localized)
        discripter4.addProfessional(input: "M3D4P3".localized)
        
        discripter4.addHighPerformer(input: "M3D4H1".localized)
        discripter4.addHighPerformer(input: "M3D4H2".localized)
        discripter4.addHighPerformer(input: "M3D4H3".localized)
        
        discripter4.addExpert(input: "M3D4E1".localized)
        discripter4.addExpert(input: "M3D4E2".localized)
        
        
        
        
        discripter1Data = createDataArray(dis: discripter1)
        discripter2Data = createDataArray(dis: discripter2)
        discripter3Data = createDataArray(dis: discripter3)
        discripter4Data = createDataArray(dis: discripter4)
    }
    
    func createDataArray( dis:DiscripterModule) -> [String]
    {
        var disDataArray = [String]()
        disDataArray.append(getArrayData(dataArray: dis.beginer))
        disDataArray.append(getArrayData(dataArray: dis.learner))
        disDataArray.append(getArrayData(dataArray: dis.professional))
        disDataArray.append(getArrayData(dataArray: dis.highPerformer))
        disDataArray.append(getArrayData(dataArray: dis.expert))
        
        return disDataArray
    }
    
    
    func getArrayData(dataArray:[String]) ->String
    {
        var allText:String = "<ul>"
        
        for item in dataArray
        {
            allText.append("<li>" + item + "</li>")
        }
        
        return allText + "</ul>"
    }
}
