//
//  StringExtension.swift
//  LearningApp
//
//  Created by Sumit More on 11/18/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import Foundation

extension String {
    
    var localized: String {
            return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
}
