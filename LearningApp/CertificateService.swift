//
//  CertificateService.swift
//  LearningApp
//
//  Created by Sumit More on 1/2/17.
//  Copyright © 2017 Mac-04. All rights reserved.
//

import Foundation
import UIKit


class CertificateService
{
    
    class var sharedInstance: CertificateService
    {
        //2
        struct Singleton
        {
            //3
            static let instance = CertificateService()
        }
        //4
        return Singleton.instance
    }
    
    func loadData(completionHandler:@escaping (Certificate)->())
    {
        let headers = [
            "cache-control": "no-cache",
            ]
        
        let prefs:UserDefaults = UserDefaults.standard
        var user_id = "4"
//        var course_id = "21"
        user_id = prefs.value(forKey: "user_id") as! String!
//        course_id = prefs.value(forKey: "course_id") as! String!
        
        let apiURL = Constants.salesRepCertificateURL + user_id
//        let apiURL = Constants.salesRepCertificateURL + user_id + "/" + course_id

        let request = NSMutableURLRequest(url: NSURL(string: apiURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil)
            {
                print("Error: \(error)")
                let certificates = Certificate.sharedInstance
                completionHandler(certificates)
            } //end of error if
            else
            {
                //let httpResponse = response as? HTTPURLResponse
                print("HTTP Response: \(data)")
                
                do
                {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                    {
                        
                        print("2222 Print converted dictionary",convertedJsonIntoDict)
                        
                        // Get value by key
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        if statusVal == nil {
                            print("status val is null")
                            let statusVal = convertedJsonIntoDict["status"] as? String
                            print("status valule is \(statusVal)")
                            let login = Login.sharedInstance
                            login.status = String(describing: convertedJsonIntoDict["status"] as? Bool)
                            login.message = convertedJsonIntoDict["message"] as? String
                            
                        } else {
                            if(statusVal)! {
                                let certificates = Certificate.sharedInstance                                
                                let certsArrJ: NSArray = convertedJsonIntoDict["data"] as! NSArray
                                print("Certificate Array Count: \(certsArrJ.count)")
                                print("Certificate Array: \(certsArrJ)")
                                
//                                let json = JSON(convertedJsonIntoDict)
                                var certDetails:CertificateDetails
                                for i in 0..<certsArrJ.count
                                {
                                    let certObj: JSON = certsArrJ[i] as! JSON
                                    print("Certificate object: \(certObj)")
                                    certDetails = CertificateDetails()
 
                                    let certId = certObj["id"] as! String
                                    let certPath = certObj["path"] as! String
                                    print("Certificate id: \(certId)")
                                    print("Certificate path: \(certPath)")
                                    certDetails.id = certId
                                    certDetails.path = certPath
                                    print("Certificate id \(certDetails.id!)")
                                    print("Certificate path \(certDetails.path!)")
                                    if i == 1 // Avoid mobile data
                                    {
                                        let url = URL(string: certDetails.path)
                                        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                                        certDetails.image = UIImage(data: data!)
                                    }
                                    certificates.addCertificateData(certificateDetails: certDetails)
                                }
                                
                                completionHandler(certificates)
                            } else {
                                print("In else!")
                            }//end if
                        }
                    }
                } catch let error as NSError
                {
                    print(error)
                }                
            }// End else
            
            
        })
        
        dataTask.resume()
    }
    
}


