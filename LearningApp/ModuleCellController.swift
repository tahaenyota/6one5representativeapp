//
//  ModuleCellController.swift
//  LearningApp
//
//  Created by Sumit More on 12/14/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import Foundation
import UIKit

class ModuleCellController: UICollectionViewCell {
    @IBOutlet weak var modelCellImageView: UIImageView!
    @IBOutlet weak var moduleCellHeadingLabel: UILabel!
    @IBOutlet weak var moduleRating: CosmosView!
    
    
}
