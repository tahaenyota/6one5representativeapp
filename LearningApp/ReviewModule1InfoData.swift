//
//  ReviewModule1InfoData.swift
//  6one5Manager
//
//  Created by enyotalearning on 06/01/17.
//  Copyright © 2017 enyotalearning. All rights reserved.
//

import UIKit
import Foundation

class ReviewModule1InfoData :ReviewModuleInfo
{
    
override func AdddataforDiscripter()
    {
        discripter1 = DiscripterModule()
        discripter2 = DiscripterModule()
        discripter3 = DiscripterModule()
        discripter4 = DiscripterModule()
        discripter4Data = [String]()
        
        discripter1Data = [String]()
        discripter2Data = [String]()
        discripter3Data = [String]()
        //discripter 1 Data
        discripter1.descripterText = "M1D1Text".localized
        
        discripter1.addBeginer(input: "M1D1B1".localized)
        discripter1.addBeginer(input: "M1D1B2".localized)
        
        discripter1.addLearner(input: "M1D1L1".localized)
        discripter1.addLearner(input: "M1D1L2".localized)
        
        discripter1.addProfessional(input: "M1D1P1".localized)
        discripter1.addProfessional(input: "M1D1P2".localized)
        discripter1.addProfessional(input: "M1D1P3".localized)
        
        discripter1.addHighPerformer(input: "M1D1H1".localized)
        discripter1.addHighPerformer(input: "M1D1H2".localized)
        
        discripter1.addExpert(input: "M1D1E1".localized)
        discripter1.addExpert(input: "M1D1E2".localized)
        
        //discripter 2 Data
        discripter2.descripterText = "M1D2Text".localized
        
        discripter2.addBeginer(input: "M1D2B1".localized)
        discripter2.addBeginer(input: "M1D2B2".localized)
        
        discripter2.addLearner(input: "M1D2L1".localized)
        discripter2.addLearner(input: "M1D2L2".localized)
        
        discripter2.addProfessional(input: "M1D2P1".localized)
        discripter2.addProfessional(input: "M1D2P2".localized)
        
        discripter2.addHighPerformer(input: "M1D2H1".localized)
        discripter2.addHighPerformer(input: "M1D2H2".localized)
        
        discripter2.addExpert(input: "M1D2E1".localized)
        discripter2.addExpert(input: "M1D2E2".localized)
        
        //discripter 3 Data
        discripter3.descripterText = "M1D3Text".localized
    
        discripter3.addBeginer(input: "M1D3B1".localized)
        discripter3.addBeginer(input: "M1D3B2".localized)
        
        discripter3.addLearner(input: "M1D3L1".localized)
        
        discripter3.addProfessional(input: "M1D3P1".localized)
        
        discripter3.addHighPerformer(input: "M1D3H1".localized)
        discripter3.addHighPerformer(input: "M1D3H2".localized)
        
        discripter3.addExpert(input: "M1D3E1".localized)
        discripter3.addExpert(input: "M1D3E2".localized)
        
        
        discripter1Data = createDataArray(dis: discripter1)
        discripter2Data = createDataArray(dis: discripter2)
        discripter3Data = createDataArray(dis: discripter3)
    }
    
    
    func createDataArray( dis:DiscripterModule) -> [String]
    {
        var disDataArray = [String]()
        disDataArray.append(getArrayData(dataArray: dis.beginer))
        disDataArray.append(getArrayData(dataArray: dis.learner))
        disDataArray.append(getArrayData(dataArray: dis.professional))
        disDataArray.append(getArrayData(dataArray: dis.highPerformer))
        disDataArray.append(getArrayData(dataArray: dis.expert))
        
        return disDataArray
    }
    
    
    func getArrayData(dataArray:[String]) ->String
    {
        var allText:String = "<ul>"
        
        for item in dataArray
        {
            allText.append("<li>" + item + "</li>")
        }
        
        return allText + "</ul>"
    }
    
    /*func getDescripterText(rowNo:Int) -> String
    {
        if rowNo == 1
        {
            return discripter1.descripterText
        }
        if rowNo == 2
        {
            return discripter2.descripterText
        }
        if rowNo == 3
        {
            return discripter3.descripterText
        }
        return ""
    }*/
}
