//
//  LoginController.swift
//  LearningApp
//
//  Created by Sumit More on 1/3/17.
//  Copyright © 2017 Mac-04. All rights reserved.
//

import Foundation
import UIKit

class LoginController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var UserNameTxtField: UITextField!
    @IBOutlet weak var PasswordTxtField: UITextField!
    @IBOutlet weak var LoginButton: UIButton!
    @IBOutlet weak var copyrightLbl: UILabel!
    
    let login =  Login.sharedInstance
    let progressHUD = ProgressHUD()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        UserNameTxtField.delegate = self
        PasswordTxtField.delegate = self
        
        LoginButton.titleLabel?.text = "login_btn".localized
        copyrightLbl.text = "login_copyright".localized
        
        self.hideKeyboardWhenTappedAround()
    }
    
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1;
    }
    
    /**
     * Called when 'return' key pressed. return NO to ignore.
     */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    /**
     * Called when the user click on the view (outside the UITextField).
     */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        let defaults = UserDefaults.standard
        let isLogged = defaults.string(forKey: "isLoggedIn")
        
        if isLogged == "1" {
            let login = Login.sharedInstance
            login.setup(userName:  defaults.string(forKey: "username")!,
                        password: defaults.string(forKey: "password")!,
                        id: defaults.string( forKey: "id")!,
                        courseId: defaults.string( forKey: "courseId")!,
                        isLoggedIn: "1",
                        fname: defaults.string( forKey: "fName")!,
                        lName: defaults.string( forKey: "lName")!,
                        email: defaults.string( forKey: "email")!,
                        phone: defaults.string( forKey: "phone")!)
            login.setErrorMessage(msg: "")
            
            DispatchQueue.main.async(){
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let initViewController: UIViewController = storyboard.instantiateViewController(withIdentifier: "homeviewController") as UIViewController
                self.present(initViewController, animated: true, completion: nil)
                self.progressHUD.hide()
            }
        } else {
            let login = Login.sharedInstance
            login.setup(userName: "test_uname",
                        password: "test_pswd",
                        id: "2",
                        courseId: "2",
                        isLoggedIn: "0",
                        fname: "test_fname",
                        lName: "test_lname",
                        email: "test@6one5.com",
                        phone: "test_phone")
            login.setErrorMessage(msg: "")
            login.setIsLogin(islogin: "0")
        }
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func LoginButtonTapped(_ sender: UIButton)
    {
        UserNameTxtField.resignFirstResponder();
        PasswordTxtField.resignFirstResponder();
        
        self.view.addSubview(progressHUD)
        //progressHUD.activityIndictor.color = UIColor.black
        
        self.progressHUD.show()
        
        var username = self.UserNameTxtField.text
        var password = self.PasswordTxtField.text
        if ( username=="j" || password=="j" ){
            username = "charliewilson@6one5.com"
            password = "Admin@123"
        }
        
        
        if ( username=="" || password=="" )
        {
            username = "shaneli4@6one5.com"
            password = "Admin@123"
            /*username = "shaneli4@6one5.com"
            password = "Admin@123"

            let alertController = UIAlertController(title: "signin_failed".localized , message: "signin_error".localized, preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "login_ok".localized, style: .default) { (action:UIAlertAction!) in }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
            progressHUD.hide()*/
            
        }
        //{
            username = UserNameTxtField.text
            password = PasswordTxtField.text
            
            
            if login.getIsLogged() == "0" {
                LoginService.sharedInstance.validateLoginDetailsOnline(userName: username!, passwd: password!, completionHandler: { (userData:Login) -> () in
                    DispatchQueue.main.async() {
                        self.progressHUD.hide()
                        UIApplication.shared.endIgnoringInteractionEvents()
                    }
                    
                    let islog:String = self.login.getIsLogged()
                    //print("Is log in found \(islog)" )
                    if (islog == "1" ) {
                        self.login.saveLoginToDevice()
                        DispatchQueue.main.async(){
                            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let initViewController: UIViewController = storyboard.instantiateViewController(withIdentifier: "EmployeeHomeTabView") as UIViewController
                            self.present(initViewController, animated: true, completion: nil)
                            self.progressHUD.hide()
                        }
                    } else {
                        DispatchQueue.main.async() {
                            let alert = UIAlertController(title: "login_error".localized, message: self.login.errorMessage, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "login_ok".localized, style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            self.progressHUD.hide()
                            self.login.setIsLogin(islogin: "0")
                        }
                    }
                })
            } else {
                DispatchQueue.main.async(){
                    let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let initViewController: UIViewController = storyboard.instantiateViewController(withIdentifier: "EmployeeHomeTabView") as UIViewController
                    self.present(initViewController, animated: true, completion: nil)
                    self.progressHUD.hide()
                }
            }
        //}
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}
