

import UIKit


class HelpViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{
    
    
    
    @IBOutlet weak var tableView: UITableView!
    
    var sectionTitleArray = [String]()
    
    var sectionDetailArray = [String]()
    
    var sectionImageArray = [String]()
    
    @IBOutlet weak var tableHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = false
        
        self.tableView.delegate = self
        
        self.tableView.dataSource = self
        
        self.tableView.estimatedRowHeight = 50.0
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.tableView.tableFooterView = UIView()
        
        self.tableView.setContentOffset(CGPoint.zero, animated: true)
        
        //tableHeightConstraint.constant = 3000.0
        
        getTableData()
        
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        //tableView = UITableView(frame: UIScreen.main.bounds, style: UITableViewStyle.plain)
        
    }
    
    
    
    func getTableData()
        
    {
        
        sectionTitleArray  = ["","HelpSectionTitle1".localized ,"HelpSectionTitle2".localized ,"HelpSectionTitle3".localized,"HelpSectionTitle4".localized,"HelpSectionTitle5".localized,"HelpSectionTitle6".localized,"HelpSectionTitle7".localized ,"HelpSectionTitle8".localized ,"HelpSectionTitle9".localized,"HelpSectionTitle11".localized,"HelpSectionTitle12".localized,"HelpSectionTitle13".localized,"HelpSectionTitle14".localized,"","",""]
        
        
        
        sectionDetailArray = ["Header_help".localized,"HelpSectionDetail1".localized ,"HelpSectionDetail2".localized ,"HelpSectionDetail3".localized,"HelpSectionDetail4".localized,"HelpSectionDetail5".localized,"HelpSectionDetail6".localized,"HelpSectionDetail7".localized ,"HelpSectionDetail8".localized , "HelpSectionDetail9".localized,"HelpSectionDetail11".localized,"HelpSectionDetail12".localized,"HelpSectionDetail13".localized,"HelpSectionDetail14".localized,"","",""]
        
        
        
        sectionImageArray = ["","RepHelp_login","RepHelp_Dashboard","RepHelp_ModuleMenu","RepHelp_Module","RepHelp_Certificate","RepHelp_Scorecard","RepHelp_Faq","RepHelp_Badges","RepHelp_Profile","RepHelp_Notification","RepHelp_Help","RepHelp_PD","RepHelp_TC","","",""]
        
    }
    
    
    
    
    
    // TableView DataSource methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return sectionTitleArray.count
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:ExpandableCell = tableView.dequeueReusableCell(withIdentifier: "ExpandableCell") as! ExpandableCell
        
        cell.cellIndex = indexPath.row
        
        
        cell.selectionStyle = .none
        
        if indexPath.row >= 14{
            
            cell.arrowImage .image = nil
            
            cell.isHidden = true
                        
        }else if indexPath.row == 0 {
            
            cell.arrowImage.isHidden = true
            
            cell.img.isHidden = true
            
            cell.textview.isHidden = true
            
           
            
            var detailText = sectionDetailArray [indexPath.row]
            if Constants.device == "iPhone"
            {
                 //cell.sectionTitleLabel.font = UIFont.systemFont(ofSize:15 )
                detailText = Constants.HelpfontTag_iPHONE + detailText + Constants.fontclosing
                let theAttributedString = try! NSAttributedString(data:detailText.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                                  
                                                                  options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                                  
                                                                  documentAttributes: nil)
                cell.sectionTitleLabel.attributedText = theAttributedString
            }else{
                detailText = Constants.HelpfontTag_iPAD + detailText + Constants.fontclosing
                let theAttributedString = try! NSAttributedString(data:detailText.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                                  
                                                                  options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                                  
                                                                  documentAttributes: nil)
                cell.sectionTitleLabel.attributedText = theAttributedString
            }
            
            cell.isExpanded = false
        }
            
        else{
            
            cell.arrowImage.isHidden = false
            
            cell.img.isHidden = false
            
            cell.textview.isHidden = false
            
            
            
            cell.sectionTitleLabel.isHidden = false
            
            cell.arrowImage .image = UIImage(named:"arrow_side1")
            
            cell.img.image = UIImage (named: sectionImageArray[indexPath.row])
            
            var detailText2 = sectionTitleArray [indexPath.row]
            if Constants.device == "iPhone"
            {
                detailText2 = Constants.HelpfontTag_iPHONE + detailText2 + Constants.fontclosing
                let theAttributedString = try! NSAttributedString(data:detailText2.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                                  
                                                                  options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                                  
                                                                  documentAttributes: nil)
                
                
                
                cell.sectionTitleLabel.attributedText = theAttributedString
            }else{
                detailText2 = Constants.HelpfontTag_iPAD + detailText2 + Constants.fontclosing
                let theAttributedString = try! NSAttributedString(data:detailText2.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                                  
                                                                  options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                                  
                                                                  documentAttributes: nil)
                
                
                
                cell.sectionTitleLabel.attributedText = theAttributedString
            }

            
            
            
            
            
            
            var detailText1 = sectionDetailArray [indexPath.row]
            if Constants.device == "iPhone"
            {
                detailText1 = Constants.HelpfontTag_iPHONE + detailText1 + Constants.fontclosing
                let theAttributedString = try! NSAttributedString(data:detailText1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                                  
                                                                  options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                                  
                                                                  documentAttributes: nil)
                
                
                
                cell.textview.attributedText = theAttributedString
            }else{
                detailText1 = Constants.HelpfontTag_iPAD + detailText1 + Constants.fontclosing
                let theAttributedString = try! NSAttributedString(data:detailText1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                                  
                                                                  options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                                  
                                                                  documentAttributes: nil)
                
                
                
                cell.textview.attributedText = theAttributedString
            }
            
            
            cell.isExpanded = false
            
        }
        
        
        
        return cell
        
    }
    
    
    
    // TableView Delegate methods
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if indexPath.row == 0 {
            
            return
            
        }
        
        guard let cell = tableView.cellForRow(at: indexPath) as? ExpandableCell
            
            else {
                
                return
                
                
                
        }
        
        
        
        if cell.arrowImage.image == UIImage(named:"arrow-down_black")  {
            
            cell.arrowImage.image = UIImage(named:"arrow_side1")
            
            
        }else{
            
            cell.arrowImage.image = UIImage(named:"arrow-down_black")
            
        }
        
        
        UIView.animate(withDuration: 0.2, animations: {
            
            tableView.beginUpdates()
            
            cell.isExpanded = !cell.isExpanded
            
           // tableView.scrollToRow(at: indexPath, at: UITableViewScrollPosition.top, animated: false)
            
            tableView.endUpdates()
            
        })
        
        
        
    }
    
    
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        guard let cell = tableView.cellForRow(at: indexPath) as? ExpandableCell
            
            
            else { return }
        
        
        UIView.animate(withDuration: 0.2, animations: {
            
            tableView.beginUpdates()
            
            cell.isExpanded = false
            
            tableView.endUpdates()
            
        })
        
    }
    
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
            
        case 0:
            
            return 60.0
            
            //        case 1:
            
            //            return 430.0
            
            //        case 2:
            
            //            return 1000.0
            
        default:
            
            return UITableViewAutomaticDimension;
            
            
        }
        
    }
    
    
    
}
