//
//  QuizHomeScreenViewCtrlr.swift
//  LearningApp
//
//  Created by Mac-04 on 16/09/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class QuizHomeScreenViewCtrlr: UIViewController, AVAudioPlayerDelegate {
    
    var score = 0
    var totRight = 0
    var user_id: String = "61"
    var course_id = "1"
    var module_id = "2"
    var totalScore = 0
    
    var GTTimer : Timer = Timer()
    var GTTimer2 : Timer = Timer()
    var isPaused = false
    var intervalVal: Int = 5
    var intervalVal2: Int = 1
    var alertControllerRes: UIAlertController?
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var IslandFlagSetImageview: UIImageView!
    @IBOutlet weak var lavel5Btn: UIButton!
    @IBOutlet weak var lavel4Btn: UIButton!
    @IBOutlet weak var lavel3Btn: UIButton!
    @IBOutlet weak var lavel2Btn: UIButton!
    @IBOutlet weak var lavel1Btn: UIButton!
    @IBOutlet weak var lavel1FlagImgVw: UIImageView!
    @IBOutlet weak var lavel2FlagImgVw: UIImageView!
    @IBOutlet weak var lavel3FlagImgVw: UIImageView!
    @IBOutlet weak var lavel4FlagImgVw: UIImageView!
    @IBOutlet weak var lavel5FlagImgVw: UIImageView!
    
    @IBOutlet weak var multimediaQuiz: UIButton!
    
    @IBOutlet weak var resultLbl: UILabel!
    @IBOutlet weak var resultLbl0: UILabel!
    
    @IBOutlet weak var quizAttemptedLbl: UILabel!
    
    @IBOutlet weak var resultBackBut: UIButton!
    
    @IBAction func resultBackClick(_ sender: AnyObject) {
        self.resultBackBut.isHidden = true
        self.resultLbl.isHidden = true
        self.resultLbl0.isHidden = true
        self.lavel1FlagImgVw.isHidden = false
        self.lavel2FlagImgVw.isHidden = false
        self.lavel3FlagImgVw.isHidden = false
        self.lavel4FlagImgVw.isHidden = false
        self.lavel5FlagImgVw.isHidden = false
        self.QuizwithoutFlagImg.image = UIImage(named: "quiz_bg2")
    }
    
    var level1Attend = 0
    var level2Attend = 0
    var level3Attend = 0
    var level4Attend = 0
    var level5Attend = 0
    
    var Level1Attempt = 0
    var Level2Attempt = 0
    var Level3Attempt = 0
    var Level4Attempt = 0
    var Level5Attempt = 0

    var Level1Score = 0
    var Level2Score = 0
    var Level3Score = 0
    var Level4Score = 0
    var Level5Score = 0
    
    var playFlagQuizAud = 1
    var audioPlayerQuizAud: AVAudioPlayer?
    
    var totalAttenedLevel = 0
    
    @IBAction func BeginBtnTouch(_ sender: AnyObject) {
        
        if let image = UIImage(named: "ic_play_arrow_white_36pt.png") {
            multimediaQuiz.setImage(image, for: .normal)
        }
        audioPlayerQuizAud?.stop()
        
        multimediaQuiz.isEnabled = false
        
        QuizwithoutFlagImg.image = UIImage(named: "quiz_bg2")
        lavel1Btn.isHidden = false
        lavel2Btn.isHidden = false
        lavel3Btn.isHidden = false
        lavel4Btn.isHidden = false
        lavel5Btn.isHidden = false
        QuizHelpBtn.isHidden = true
        QuizBeginBtn.isHidden = true
        //self.QuizBeginBtn.isHidden = true
    }
    

    
    @IBAction func QuizHelpBtnTouch(_ sender: AnyObject) {
        
        playFlagQuizAud = 3
        audioPlayerQuizAud?.pause()
        if let image = UIImage(named: "ic_replay_white_36pt.png") {
            multimediaQuiz.setImage(image, for: .normal)
        }

        let popOverVC = UIStoryboard (name: "Module2SB", bundle: nil).instantiateViewController(withIdentifier: "HelpPopupID") as! HelpPopupViewController
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        popOverVC.modalTransitionStyle = .crossDissolve
        
        
//        self.view.addSubview(popOverVC.view)
//        popOverVC.didMove(toParentViewController: self)
//        
    }
    
    @IBOutlet weak var QuizHelpBtn: UIButton!
    
    
    @IBOutlet weak var QuizBeginBtn: UIButton!
    
    
    @IBOutlet weak var QuizwithoutFlagImg: UIImageView!
    
    @IBAction func Lavel1BtnTouch(_ sender: AnyObject) {
        
        level1Attend = 1
        
        let popOverVC = UIStoryboard (name: "Module2SB", bundle: nil).instantiateViewController(withIdentifier: "QutionAnswerPopupID") as! QuetionOptionsPopupVwCtrlr
        
        popOverVC.SelectedIsland = 1
        popOverVC.score = self.score
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
        
    }
    
    
    
    @IBAction func Lavel2BtnTouch(_ sender: AnyObject) {
        
        level2Attend = 1
        
        let popOverVC = UIStoryboard (name: "Module2SB", bundle: nil).instantiateViewController(withIdentifier: "QutionAnswerPopupID") as! QuetionOptionsPopupVwCtrlr
        
        popOverVC.SelectedIsland = 2
        popOverVC.score = self.score
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)

    }
    
    
    
    @IBAction func Lavel3BtnTouch(_ sender: AnyObject) {
        
        
        level3Attend = 1

        let popOverVC = UIStoryboard (name: "Module2SB", bundle: nil).instantiateViewController(withIdentifier: "QutionAnswerPopupID") as! QuetionOptionsPopupVwCtrlr
        
        popOverVC.SelectedIsland = 3
        popOverVC.score = self.score
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
        
    }
    
    @IBAction func Lavel4BtnTouch(_ sender: AnyObject) {
        
        level4Attend = 1
        
        let popOverVC = UIStoryboard (name: "Module2SB", bundle: nil).instantiateViewController(withIdentifier: "QutionAnswerPopupID") as! QuetionOptionsPopupVwCtrlr
        
        popOverVC.SelectedIsland = 4
        popOverVC.score = self.score
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    
    
    @IBAction func Lavel5BtnTouch(_ sender: AnyObject) {
     
        level5Attend = 1
        
        let popOverVC = UIStoryboard (name: "Module2SB", bundle: nil).instantiateViewController(withIdentifier: "QutionAnswerPopupID") as! QuetionOptionsPopupVwCtrlr
        
        popOverVC.SelectedIsland = 5
        popOverVC.score = self.score
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
        
    }
    
    func showScore() {
        
        var scoreMessage = ""
        
        
        if self.score == 10{
            scoreMessage = "Congratulations! You have answered all questions correctly! Here’s your score for this quiz."
        }else {
            scoreMessage = "Not quite! You have answered a few questions correctly! Here’s your score for this quiz."
        }
        
        print(scoreMessage)
        
//        let alertController = UIAlertController(title: scoreMessage, message: String(self.score) , preferredStyle: .alert)
//        
//        
//        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
//            print("you have pressed OK button");
        
        
//        }
//        alertController.addAction(OKAction)
//        
//        self.present(alertController, animated: true, completion:nil)
    }
    
    
    
    func showSecondChanceAlertBox() {
        
        let alertController = UIAlertController(title: "Thats incorrect. But you've got another shot at this! Try again to earn that one!", message: "" , preferredStyle: .alert)
        
        
        let OKAction = UIAlertAction(title: "Retry", style: .default) { (action:UIAlertAction!) in
            print("you have pressed Retry button");
        }
        alertController.addAction(OKAction)
        
        self.present(alertController, animated: true, completion:nil)
    }
    
    
    
    func checkAttendLevelsForScore() {
        
        totalAttenedLevel = self.level5Attend + self.level4Attend + self.level3Attend + self.level2Attend + self.level1Attend
        print(totalAttenedLevel)
        if totalAttenedLevel == 5 {
            
            print("Saving the quiz attempt")
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set("attempted", forKey: "module2_attempted")
            prefs.synchronize()

//            let popOverVC = UIStoryboard (name: "Module2SB", bundle: nil).instantiateViewController(withIdentifier: "ResultPopupID") as! ResultPopupVwCtrlr
//            popOverVC.TotalScore = score
//            
//            self.addChildViewController(popOverVC)
//            popOverVC.view.frame = self.view.frame
//            
//            self.view.addSubview(popOverVC.view)
//            popOverVC.didMove(toParentViewController: self)
            

            if (self.totRight == 5) {
                self.resultLbl0.text = "Mgr_Quiz_P100_P1".localized
                // self.resultLbl.text  = "Mgr_Quiz_P100_P2".localized()
                
                self.resultLbl.text = String(format: NSLocalizedString("Mgr_Quiz_P100_P2", comment: "any comment"), self.score)
            } else {
                self.resultLbl0.text = "Mgr_Quiz_PB100_P1".localized
                self.resultLbl.text = String(format: NSLocalizedString("Mgr_Quiz_P100_P2", comment: "any comment"), self.score)
            }
//            if(self.score == 10) {
            
//            alertControllerRes = UIAlertController(title: "Result", message: "You have got \(self.totRight) right!" , preferredStyle: .alert)
//            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                self.QuizwithoutFlagImg.image = UIImage(named: "quiz_result")
                self.resultBackBut.isHidden = false
                self.lavel1FlagImgVw.isHidden = true
                self.lavel2FlagImgVw.isHidden = true
                self.lavel3FlagImgVw.isHidden = true
                self.lavel4FlagImgVw.isHidden = true
                self.lavel5FlagImgVw.isHidden = true
                self.resultLbl0.isHidden = false
                self.resultLbl.isHidden = false
                self.sendData()
//            }
//            alertControllerRes?.addAction(OKAction)

//            self.showScore()
        }
    }
    
    @IBOutlet weak var quizBtmLbl: UILabel!
    
    override func viewDidLoad() {
        
        print("View loaded")
        
        if UserDefaults.standard.bool(forKey: "isPlusversion") == true {
            
            self.QuizHelpBtn.removeConstraints(self.QuizHelpBtn.constraints)
            let top_QuizHelpBtn = NSLayoutConstraint(item: self.QuizHelpBtn,
                                                     attribute: NSLayoutAttribute.top,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.top,
                                                     multiplier: 1,
                                                     constant: 635)
            let height_QuizHelpBtn = NSLayoutConstraint(item: self.QuizHelpBtn,
                                                        attribute: NSLayoutAttribute.height,
                                                        relatedBy: NSLayoutRelation.equal,
                                                        toItem: nil,
                                                        attribute: NSLayoutAttribute.notAnAttribute,
                                                        multiplier: 1,
                                                        constant: 51)
            let left_QuizHelpBtn = NSLayoutConstraint(item: self.QuizHelpBtn,
                                                      attribute: NSLayoutAttribute.left,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.left,
                                                      multiplier: 1,
                                                      constant: 11)
            self.view.addConstraint(top_QuizHelpBtn)
            self.view.addConstraint(height_QuizHelpBtn)
            self.view.addConstraint(left_QuizHelpBtn)
            //            self.QuizHelpBtn.backgroundColor = UIColor.red
            
            self.QuizBeginBtn.removeConstraints(self.QuizBeginBtn.constraints)
            let top_QuizBeginBtn = NSLayoutConstraint(item: self.QuizBeginBtn,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: 635)
            let height_QuizBeginBtn = NSLayoutConstraint(item: self.QuizBeginBtn,
                                                         attribute: NSLayoutAttribute.height,
                                                         relatedBy: NSLayoutRelation.equal,
                                                         toItem: nil,
                                                         attribute: NSLayoutAttribute.notAnAttribute,
                                                         multiplier: 1,
                                                         constant: 51)
            let left_QuizBeginBtn = NSLayoutConstraint(item: self.QuizBeginBtn,
                                                       attribute: NSLayoutAttribute.left,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.view,
                                                       attribute: NSLayoutAttribute.left,
                                                       multiplier: 1,
                                                       constant: 351)
            self.view.addConstraint(top_QuizBeginBtn)
            self.view.addConstraint(height_QuizBeginBtn)
            self.view.addConstraint(left_QuizBeginBtn)
            //            self.QuizBeginBtn.backgroundColor = UIColor.red
            
            self.lavel1Btn.removeConstraints(self.lavel1Btn.constraints)
            let top_lavel1Btn = NSLayoutConstraint(item: self.lavel1Btn,
                                                   attribute: NSLayoutAttribute.top,
                                                   relatedBy: NSLayoutRelation.equal,
                                                   toItem: self.view,
                                                   attribute: NSLayoutAttribute.top,
                                                   multiplier: 1,
                                                   constant: 523)
            let left_lavel1Btn = NSLayoutConstraint(item: self.lavel1Btn,
                                                    attribute: NSLayoutAttribute.left,
                                                    relatedBy: NSLayoutRelation.equal,
                                                    toItem: self.view,
                                                    attribute: NSLayoutAttribute.left,
                                                    multiplier: 1,
                                                    constant: 286)
            let width_lavel1Btn = NSLayoutConstraint(item: self.lavel1Btn,
                                                     attribute: NSLayoutAttribute.width,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: nil,
                                                     attribute: NSLayoutAttribute.notAnAttribute,
                                                     multiplier: 1,
                                                     constant: 96)
            let height_lavel1Btn = NSLayoutConstraint(item: self.lavel1Btn,
                                                      attribute: NSLayoutAttribute.height,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: nil,
                                                      attribute: NSLayoutAttribute.notAnAttribute,
                                                      multiplier: 1,
                                                      constant: 121)
            self.view.addConstraint(top_lavel1Btn)
            self.view.addConstraint(left_lavel1Btn)
            self.view.addConstraint(width_lavel1Btn)
            self.view.addConstraint(height_lavel1Btn)
            
            self.lavel2Btn.removeConstraints(self.lavel2Btn.constraints)
            let top_lavel2Btn = NSLayoutConstraint(item: self.lavel2Btn,
                                                   attribute: NSLayoutAttribute.top,
                                                   relatedBy: NSLayoutRelation.equal,
                                                   toItem: self.view,
                                                   attribute: NSLayoutAttribute.top,
                                                   multiplier: 1,
                                                   constant: 575)
            let left_lavel2Btn = NSLayoutConstraint(item: self.lavel2Btn,
                                                    attribute: NSLayoutAttribute.left,
                                                    relatedBy: NSLayoutRelation.equal,
                                                    toItem: self.view,
                                                    attribute: NSLayoutAttribute.left,
                                                    multiplier: 1,
                                                    constant: 65)
            let width_lavel2Btn = NSLayoutConstraint(item: self.lavel2Btn,
                                                     attribute: NSLayoutAttribute.width,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: nil,
                                                     attribute: NSLayoutAttribute.notAnAttribute,
                                                     multiplier: 1,
                                                     constant: 65)
            let height_lavel2Btn = NSLayoutConstraint(item: self.lavel2Btn,
                                                      attribute: NSLayoutAttribute.height,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: nil,
                                                      attribute: NSLayoutAttribute.notAnAttribute,
                                                      multiplier: 1,
                                                      constant: 55)
            self.view.addConstraint(top_lavel2Btn)
            self.view.addConstraint(left_lavel2Btn)
            self.view.addConstraint(width_lavel2Btn)
            self.view.addConstraint(height_lavel2Btn)
            
            self.lavel3Btn.removeConstraints(self.lavel3Btn.constraints)
            let top_lavel3Btn = NSLayoutConstraint(item: self.lavel3Btn,
                                                   attribute: NSLayoutAttribute.top,
                                                   relatedBy: NSLayoutRelation.equal,
                                                   toItem: self.view,
                                                   attribute: NSLayoutAttribute.top,
                                                   multiplier: 1,
                                                   constant: 390)
            let left_lavel3Btn = NSLayoutConstraint(item: self.lavel3Btn,
                                                    attribute: NSLayoutAttribute.left,
                                                    relatedBy: NSLayoutRelation.equal,
                                                    toItem: self.view,
                                                    attribute: NSLayoutAttribute.left,
                                                    multiplier: 1,
                                                    constant: 207)
            let width_lavel3Btn = NSLayoutConstraint(item: self.lavel3Btn,
                                                     attribute: NSLayoutAttribute.width,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: nil,
                                                     attribute: NSLayoutAttribute.notAnAttribute,
                                                     multiplier: 1,
                                                     constant: 110)
            let height_lavel3Btn = NSLayoutConstraint(item: self.lavel3Btn,
                                                      attribute: NSLayoutAttribute.height,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: nil,
                                                      attribute: NSLayoutAttribute.notAnAttribute,
                                                      multiplier: 1,
                                                      constant: 70)
            self.view.addConstraint(top_lavel3Btn)
            self.view.addConstraint(left_lavel3Btn)
            self.view.addConstraint(width_lavel3Btn)
            self.view.addConstraint(height_lavel3Btn)
            
            self.lavel4Btn.removeConstraints(self.lavel4Btn.constraints)
            let top_lavel4Btn = NSLayoutConstraint(item: self.lavel4Btn,
                                                   attribute: NSLayoutAttribute.top,
                                                   relatedBy: NSLayoutRelation.equal,
                                                   toItem: self.view,
                                                   attribute: NSLayoutAttribute.top,
                                                   multiplier: 1,
                                                   constant: 315)
            let left_lavel4Btn = NSLayoutConstraint(item: self.lavel4Btn,
                                                    attribute: NSLayoutAttribute.left,
                                                    relatedBy: NSLayoutRelation.equal,
                                                    toItem: self.view,
                                                    attribute: NSLayoutAttribute.left,
                                                    multiplier: 1,
                                                    constant: 75)
            let width_lavel4Btn = NSLayoutConstraint(item: self.lavel4Btn,
                                                     attribute: NSLayoutAttribute.width,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: nil,
                                                     attribute: NSLayoutAttribute.notAnAttribute,
                                                     multiplier: 1,
                                                     constant: 55)
            let height_lavel4Btn = NSLayoutConstraint(item: self.lavel4Btn,
                                                      attribute: NSLayoutAttribute.height,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: nil,
                                                      attribute: NSLayoutAttribute.notAnAttribute,
                                                      multiplier: 1,
                                                      constant: 51)
            self.view.addConstraint(top_lavel4Btn)
            self.view.addConstraint(left_lavel4Btn)
            self.view.addConstraint(width_lavel4Btn)
            self.view.addConstraint(height_lavel4Btn)
            
            self.lavel5Btn.removeConstraints(self.lavel5Btn.constraints)
            let top_lavel5Btn = NSLayoutConstraint(item: self.lavel5Btn,
                                                   attribute: NSLayoutAttribute.top,
                                                   relatedBy: NSLayoutRelation.equal,
                                                   toItem: self.view,
                                                   attribute: NSLayoutAttribute.top,
                                                   multiplier: 1,
                                                   constant: 180)
            let left_lavel5Btn = NSLayoutConstraint(item: self.lavel5Btn,
                                                    attribute: NSLayoutAttribute.left,
                                                    relatedBy: NSLayoutRelation.equal,
                                                    toItem: self.view,
                                                    attribute: NSLayoutAttribute.left,
                                                    multiplier: 1,
                                                    constant: 201)
            let width_lavel5Btn = NSLayoutConstraint(item: self.lavel5Btn,
                                                     attribute: NSLayoutAttribute.width,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: nil,
                                                     attribute: NSLayoutAttribute.notAnAttribute,
                                                     multiplier: 1,
                                                     constant: 145)
            let height_lavel5Btn = NSLayoutConstraint(item: self.lavel5Btn,
                                                      attribute: NSLayoutAttribute.height,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: nil,
                                                      attribute: NSLayoutAttribute.notAnAttribute,
                                                      multiplier: 1,
                                                      constant: 75)
            self.view.addConstraint(top_lavel5Btn)
            self.view.addConstraint(left_lavel5Btn)
            self.view.addConstraint(width_lavel5Btn)
            self.view.addConstraint(height_lavel5Btn)
            
            self.resultLbl0.removeConstraints(self.resultLbl0.constraints)
            let top_resultLbl0 = NSLayoutConstraint(item: self.resultLbl0,
                                                    attribute: NSLayoutAttribute.top,
                                                    relatedBy: NSLayoutRelation.equal,
                                                    toItem: self.view,
                                                    attribute: NSLayoutAttribute.top,
                                                    multiplier: 1,
                                                    constant: 360)
            let left_resultLbl0 = NSLayoutConstraint(item: self.resultLbl0,
                                                     attribute: NSLayoutAttribute.left,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.left,
                                                     multiplier: 1,
                                                     constant: 96)
            let width_resultLbl0 = NSLayoutConstraint(item: self.resultLbl0,
                                                      attribute: NSLayoutAttribute.width,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: nil,
                                                      attribute: NSLayoutAttribute.notAnAttribute,
                                                      multiplier: 1,
                                                      constant: 221)
            self.view.addConstraint(top_resultLbl0)
            self.view.addConstraint(left_resultLbl0)
            self.view.addConstraint(width_resultLbl0)
            
            self.resultBackBut.removeConstraints(self.resultBackBut.constraints)
            let top_resultBackBut = NSLayoutConstraint(item: self.resultBackBut,
                                                       attribute: NSLayoutAttribute.top,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.resultLbl,
                                                       attribute: NSLayoutAttribute.bottom,
                                                       multiplier: 1,
                                                       constant: 21)
            let left_resultBackBut = NSLayoutConstraint(item: self.resultBackBut,
                                                        attribute: NSLayoutAttribute.left,
                                                        relatedBy: NSLayoutRelation.equal,
                                                        toItem: self.view,
                                                        attribute: NSLayoutAttribute.left,
                                                        multiplier: 1,
                                                        constant: 101)
            self.view.addConstraint(top_resultBackBut)
            self.view.addConstraint(left_resultBackBut)
            
        }

        let device = UIDevice.current.model
        if(device == "iPhone") {
            print("Changing size to 16")
            let attrs = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont.systemFont(ofSize: 16.0)
            ]
            self.navigationController!.navigationBar.titleTextAttributes = attrs
            
            let button: UIButton = UIButton(type: UIButtonType.custom)
            button.setImage(UIImage(named: "sof.png"), for: UIControlState.normal)
            button.backgroundColor = UIColor.white
            //set frame
            button.frame = CGRect(x: 0, y: 0, width: 56, height: 45)
            let barButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            
            
            let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            negativeSpacer.width = -17;
            
            self.navigationItem.setRightBarButtonItems([negativeSpacer, barButton ], animated: false)
            
        } else {
            print("Changing size to 32")
            let attrs = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont.systemFont(ofSize: 32.0)
            ]
            self.navigationController!.navigationBar.titleTextAttributes = attrs
            
            let button: UIButton = UIButton(type: UIButtonType.custom)
            button.setImage(UIImage(named: "sof.png"), for: UIControlState.normal)
            button.backgroundColor = UIColor.white
            //set frame
            button.frame = CGRect(x: 0, y: 0, width: 56, height: 45)
            let barButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            
            
            let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            negativeSpacer.width = -21;
            
            self.navigationItem.setRightBarButtonItems([negativeSpacer, barButton ], animated: false)
        }
        self.navigationItem.title = "Rep_M2_ModuleName".localized

        self.quizBtmLbl.text = "Rep_M2_QuizBtmLbl".localized
        
        let prefs:UserDefaults = UserDefaults.standard
        let module2_attempted = prefs.value(forKey: "module2_attempted") as! String!

        let welcomeDisc = prefs.value(forKey: "UnderstandModule") as! NSDictionary
        let fed = welcomeDisc["quize"] as! Bool

        
        if(module2_attempted == "attempted" || fed == true) {
            
            print("In quiz already attempted. Changing image to overlay")
            
            QuizwithoutFlagImg.image = UIImage(named: "quiz_overlay")

            self.quizAttemptedLbl.isHidden = false
            self.quizAttemptedLbl.text = "Rep_M2_Quiz_Attempted".localized
            
            QuizHelpBtn.isHidden = true
            QuizBeginBtn.isHidden = true
            
            self.multimediaQuiz.isEnabled = false
        } else {

            print("In quiz yet to be attempted")

            self.quizAttemptedLbl.isHidden = true
            QuizHelpBtn.isHidden = true
            QuizBeginBtn.isHidden = true

            do{
                let urlVideo = Bundle.main.url(forResource: "REP_M02_S02", withExtension: "mp3")
                audioPlayerQuizAud = try AVAudioPlayer(contentsOf: urlVideo!)
                audioPlayerQuizAud!.delegate = self
                audioPlayerQuizAud!.prepareToPlay()
                audioPlayerQuizAud!.play()
            } catch {
                print("Error getting the audio file")
            }

            QuizwithoutFlagImg.image = UIImage(named: "quiz_bg2.png")
            
            intervalVal = 4
            let timeInterval: TimeInterval = TimeInterval(intervalVal)
            self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(QuizHomeScreenViewCtrlr.screen1), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(QuizHomeScreenViewCtrlr.monitorTimer), userInfo: nil, repeats: true)

        }


        self.resultBackBut.isHidden = true

        self.resultLbl0.isHidden = true
        self.resultLbl.isHidden = true
        lavel1Btn.isHidden = true
        lavel2Btn.isHidden = true
        lavel3Btn.isHidden = true
        lavel4Btn.isHidden = true
        lavel5Btn.isHidden = true
        
        menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: UIControlEvents.touchUpInside)
        
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
    }

    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("In Finish Playing")
        
        QuizwithoutFlagImg.image = UIImage(named: "quiz_help_screen3")
        self.QuizHelpBtn.isUserInteractionEnabled = true
        self.QuizBeginBtn.isUserInteractionEnabled = true
        
        playFlagQuizAud = 2
        if let image = UIImage(named: "ic_replay_white_36pt.png") {
            multimediaQuiz.setImage(image, for: .normal)
        }
    }

    @IBAction func multimediaQuizClicked(_ sender: AnyObject) {
//        if (playFlagQuizAud == 2) {
//            playFlagQuizAud = 1
//            if let image = UIImage(named: "ic_pause_white_36pt.png") {
//                multimediaQuiz.setImage(image, for: .normal)
//                audioPlayerQuizAud?.play()
//            }
//            self.QuizHelpBtn.isHidden = false
//            self.QuizBeginBtn.isHidden = false
//            self.resultBackBut.isHidden = true
//            self.resultLbl0.isHidden = true
//            self.resultLbl.isHidden = true
//            lavel1Btn.isHidden = true
//            lavel2Btn.isHidden = true
//            lavel3Btn.isHidden = true
//            lavel4Btn.isHidden = true
//            lavel5Btn.isHidden = true
//            QuizwithoutFlagImg.image = UIImage(named: "quiz_help_screen2.png")
//
//        } else {
//            playFlagQuizAud = 2
//            if let image = UIImage(named: "ic_play_arrow_white_36pt.png") {
//                multimediaQuiz.setImage(image, for: .normal)
//                audioPlayerQuizAud?.pause()
//            }
//        }
      if (playFlagQuizAud == 3)
        {
            
            QuizwithoutFlagImg.image = UIImage(named: "quiz_bg2.png")
            
            self.quizAttemptedLbl.isHidden = true
            QuizHelpBtn.isHidden = true
            QuizBeginBtn.isHidden = true
            
            self.resultBackBut.isHidden = true
            
            self.resultLbl0.isHidden = true
            self.resultLbl.isHidden = true
            lavel1Btn.isHidden = true
            lavel2Btn.isHidden = true
            lavel3Btn.isHidden = true
            lavel4Btn.isHidden = true
            lavel5Btn.isHidden = true
            
            self.intervalVal = 5
            //self.screen1 = 1
            print("In screen 1 after replay")
            
            let timeInterval: TimeInterval = TimeInterval(intervalVal)
            self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module1QuizController.screen1), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module1QuizController.monitorTimer), userInfo: nil, repeats: true)
            
            playFlagQuizAud = 1
            if let image = UIImage(named: "ic_pause_white_36pt.png") {
                multimediaQuiz.setImage(image, for: .normal)
                audioPlayerQuizAud?.play()
            }
        }
        else if (playFlagQuizAud == 2) {
            let timeInterval: TimeInterval = TimeInterval(self.intervalVal)
            print("Interval: " + String(self.intervalVal))
            
            self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(QuizHomeScreenViewCtrlr.screen1), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(QuizHomeScreenViewCtrlr.monitorTimer), userInfo: nil, repeats: true)
            
            playFlagQuizAud = 1
            QuizwithoutFlagImg.image = UIImage(named: "quiz_help_screen2.png")
            if let image = UIImage(named: "ic_pause_white_36pt.png") {
                multimediaQuiz.setImage(image, for: .normal)
                audioPlayerQuizAud?.play()
            }
        } else  {
            
            self.GTTimer.invalidate()
            self.GTTimer2.invalidate()
            
            playFlagQuizAud = 2
            if let image = UIImage(named: "ic_play_arrow_white_36pt.png") {
                multimediaQuiz.setImage(image, for: .normal)
                audioPlayerQuizAud?.pause()
            }
        }
        
        
    }

    func monitorTimer() {
        self.intervalVal = self.intervalVal - 1
        print("Interval in monitor: " + String(self.intervalVal))
    }

    func screen1() {
        self.QuizHelpBtn.isHidden = false
        self.QuizBeginBtn.isHidden = false
        self.QuizHelpBtn.isUserInteractionEnabled = false
        self.QuizBeginBtn.isUserInteractionEnabled = false
        self.resultBackBut.isHidden = true
        self.resultLbl0.isHidden = true
        self.resultLbl.isHidden = true
        lavel1Btn.isHidden = true
        lavel2Btn.isHidden = true
        lavel3Btn.isHidden = true
        lavel4Btn.isHidden = true
        lavel5Btn.isHidden = true
        QuizwithoutFlagImg.image = UIImage(named: "quiz_help_screen2.png")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        audioPlayerQuizAud?.stop()
    }

    func sendData() {
        
        let prefs:UserDefaults = UserDefaults.standard
        self.user_id = prefs.value(forKey: "user_id") as! String!
        self.course_id = prefs.value(forKey: "course_id") as! String!
        self.module_id = prefs.value(forKey: "module_id") as! String!

        print("User ID: \(self.user_id)")
        print("Course ID: \(self.course_id)")
        let headers = [
            "cache-control": "no-cache",
            "postman-token": "903256f1-b72e-e060-e9ed-ac52bae07802"
        ]
        
        print("Level 1 Score: \(self.Level1Score)")
        print("Level 2 Score: \(self.Level2Score)")
        print("Level 3 Score: \(self.Level3Score)")
        print("Level 4 Score: \(self.Level4Score)")
        print("Level 5 Score: \(self.Level5Score)")
        
        let postDataString: String = "data={ \"\(String(self.module_id) as NSString)\": { \"1\": { \"answer\": \"2\", \"marks\": \"\(String(self.Level1Score) as NSString)\" }, \"2\": { \"answer\": \"1\", \"marks\": \"\(String(self.Level2Score) as NSString)\" }, \"3\": { \"answer\": \"1\", \"marks\": \"\(String(self.Level3Score) as NSString)\" }, \"4\": { \"answer\": \"2\", \"marks\": \"\(String(self.Level4Score)  as NSString)\" }, \"5\": { \"answer\": \"3\", \"marks\":  \"\(String(self.Level5Score)  as NSString)\"  } } }"
        print("Post Data String: \(postDataString)")
        
        let postData = NSData(data: postDataString.data(using: String.Encoding.utf8)!)
        print("Post Data: \(postData)")
        
        let apiString: String = Constants.salesRepQuizSubmitForReviewURL + self.user_id + "/" + self.course_id
        print("API String: \(apiString)")
        
        let request = NSMutableURLRequest(url: NSURL(string: apiString)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("Error: \(error)")
            } else {
                let httpResponse = response as? HTTPURLResponse
                print("Response: \(httpResponse)")
                print("Response data: \(data)")
            }
        })
        dataTask.resume()
     
    }
}
