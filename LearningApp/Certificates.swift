//
//  Certificates.swift
//  LearningApp
//
//  Created by Mac-04 on 06/09/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import Foundation

public struct Certificates: Decodable {
    
    public let data: CerificatesData?
    
    public init?(json: JSON) {
        data = "data" <~~ json
    }
    
}
