//
//  CertiData.swift
//  LearningApp
//
//  Created by Mac-04 on 08/09/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import Foundation

public struct CertiData: Decodable {
    public let entries: [App]?
    
    public init?(json: JSON) {
        entries = "data" <~~ json
    }
}