//
//  Login.swift
//  LearningApp
//
//  Created by Sumit More on 1/3/17.
//  Copyright © 2017 Mac-04. All rights reserved.
//

import Foundation
import UIKit

public class Login: NSObject
{
    
    var userName : String!
    var password : String!
    var id : String!
    var courseId:String!
    var isLoggedIn:String!
    var errorMessage:String!
    var fName:String!
    var lName:String!
    var email:String!
    var phone:String!
    
    var status:String!
    var message:String!
    
    //Singleton pattern
    //This is to make sure will use only one object for creating the login
    class var sharedInstance: Login
    {
        //2
        struct Singleton {
            //3
            static let instance = Login()
        }
        //4
        return Singleton.instance
    }
    
    //This is init method but I have not used the default method just because of the future purpose and we can achive the singleton pattern
    func setup(userName: String, password: String, id: String, courseId:String, isLoggedIn:String, fname:String, lName:String, email:String, phone:String)
    {
        print("islogged in value for save \(isLoggedIn)");
        //super.init()
        self.userName = userName
        self.password = password
        self.id = id
        self.courseId = courseId
        self.fName = fname
        self.lName = lName
        self.email = email
        self.phone = phone
        self.isLoggedIn = isLoggedIn
    }
    
    //Showing object description for the login object
    override public var description: String
    {
        return "userName: \(userName) \t" +
            "password: \(password) \t" +
            "id: \(id) \t" +
            "course id: \(courseId) \t" +
            "isLoggedIn: \(isLoggedIn) \t"
    }
    
    //will return the result as we want to check the user is loged in or not
    func getIsLogged()->String
    {
        return self.isLoggedIn
    }
    
    func setIsLogin(islogin:String){
        self.isLoggedIn = islogin
    }
    
    //we want to save records to the device memory for the login persistancy with device.
    func saveLoginToDevice()
    {
        let defaults = UserDefaults.standard // This is just to set this is device cache.
        print("login details saved in cache \(self.isLoggedIn)")
        defaults.set("Coding Explorer", forKey: "userNameKey")
        defaults.set(self.userName, forKey: "uname")
        defaults.set(self.password, forKey: "upass")
        defaults.set(self.id, forKey: "user_id")
        defaults.set(self.courseId, forKey: "course_id")
        defaults.set(self.isLoggedIn, forKey: "ISLOGGEDIN")
        defaults.set(self.fName,forKey:"fName")
        defaults.set(self.lName,forKey:"lName")
        let username = self.fName + " " + self.lName
        defaults.set(username,forKey:"username")
        defaults.set(self.email,forKey:"email")
        defaults.set(self.phone,forKey:"phone")
        
    }
    
    func setErrorMessage(msg:String)
    {
        errorMessage = msg
    }
    
    func getManagerId() -> String
    {
        let prefs:UserDefaults = UserDefaults.standard
        let repId = prefs.value(forKey: "user_id") as! String!

        return repId!
    }
        
    func logoutFromDeviceCache()
    {
        setup(userName:  "",
              password: "",
              id: "",
              courseId: "",
              isLoggedIn: "0",
              fname: "",
              lName: "",
              email: "",
              phone: "")
        saveLoginToDevice()
    }
    
}
