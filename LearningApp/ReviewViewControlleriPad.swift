//
//  ReviewViewControlleriPad.swift
//  LearningApp
//
//  Created by enyotalearning on 22/02/17.
//  Copyright © 2017 Mac-04. All rights reserved.
//

import UIKit
import Charts

class ReviewViewControlleriPad: UIViewController , UITableViewDelegate, UITableViewDataSource{
        
        @IBOutlet weak var ReviewTblControler: UITableView!
        @IBOutlet weak var reviewPerCentViewCont: UIView!
        
        @IBOutlet weak var mgrBtmV: UIView!
        @IBOutlet weak var quizBtmV: UIView!
        
        @IBOutlet weak var managerReviewGraph: PieChartView!
        @IBOutlet weak var quizReviewGraph: PieChartView!
        @IBOutlet weak var graphBackground: UIView!
        
        @IBOutlet weak var overallPercentLbl: UILabel!
        
        @IBOutlet weak var competencyLbl: UILabel!
        
        @IBOutlet weak var managerTapView: UIView!
        @IBOutlet weak var quizTapView: UIView!
        var managerReviewLabel:[String]!
        var managerReviewValue:[Double]!
        var quizReviewLabel:[String]!
        var quizReviewValue:[Double]!
        
        @IBOutlet weak var quizReviewTextField: UILabel!
        @IBOutlet weak var managerReviewTextField: UILabel!
        var user_id: String = "61"
        var course_id: String = "2"
        
        var moduleTitle = ["score_m1".localized,"score_m2".localized,"score_m3".localized,"score_m4".localized,"score_m5".localized,"score_m6".localized]
        var modulePercentVal = [0, 0, 0, 0, 0, 0]
        var modulePercentValMgr = [0, 0, 0, 0, 0, 0]
        var modulePercentValQuiz = [0, 0, 0, 0, 0, 0]
        
        var overallMgrPercent = 0
        var overallQuizPercent = 0
        var overallPercent = 0
        
        override func viewDidLoad() {
            
            super.viewDidLoad()
            
            self.navigationController!.navigationBar.topItem!.title = "scorecard_title".localized;
            
            reviewPerCentViewCont.layer.shadowOffset = CGSize(width: 1, height: 0);
            reviewPerCentViewCont.layer.shadowColor = UIColor.black.cgColor
            reviewPerCentViewCont.layer.shadowRadius = 5;
            reviewPerCentViewCont.layer.shadowOpacity = 0.25;
            
            managerReviewLabel = ["", ""]
            managerReviewValue = [40, 60]
            quizReviewLabel = ["", ""]
            quizReviewValue = [40, 60]
            
            ReviewTblControler.rowHeight = UITableViewAutomaticDimension
            ReviewTblControler.estimatedRowHeight = 101
            ReviewTblControler.tableFooterView = UIView()
            
            
            updateGraphs()
            updateModuleData()
            
            let tapMgr = UITapGestureRecognizer(target: self, action: #selector(ReviewViewController.updatePercentMgrVals))
            let tapQuiz = UITapGestureRecognizer(target: self, action: #selector(ReviewViewController.updatePercentQuizVals))
            
            self.managerReviewGraph.isUserInteractionEnabled = false
            self.quizReviewGraph.isUserInteractionEnabled = false
            //        self.managerReviewGraph.addGestureRecognizer(tapMgr)
            //        self.quizReviewGraph.addGestureRecognizer(tapQuiz)
            
            self.managerTapView.addGestureRecognizer(tapMgr)
            self.quizTapView.addGestureRecognizer(tapQuiz)
            
            self.mgrBtmV.isHidden = false
            self.quizBtmV.isHidden = true
            managerReviewTextField.textColor = UIColor.white
            // Do any additional setup after loading the view.
        }
        
        override func viewDidAppear(_ animated: Bool) {
            //self.graphBackground.backgroundColor = Constants.salesRep_hole_color
            //self.managerReviewGraph.backgroundColor = Constants.salesRep_hole_color
            //self.quizReviewGraph.backgroundColor = Constants.salesRep_hole_color
            self.overallPercentLbl.text = String(overallPercent) + "%"
            
            quizReviewGraph.entryLabelColor = UIColor.black
            quizReviewGraph.noDataTextColor = UIColor.black
            
            let btn1 = UIButton(type: .infoDark)
            // btn1.setImage(UIImage(named: "plusbtnImage"), for: .normal)
            btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
            btn1.addTarget(self, action: #selector(infoTapped), for: .touchUpInside)
            let item1 = UIBarButtonItem(customView: btn1)
            self.navigationItem.setRightBarButtonItems([item1], animated: true)
            
            
        }
        
        func infoTapped (){
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController:ReviewInfoViewController = storyboard.instantiateViewController(withIdentifier: "ReviewInfoViewController") as! ReviewInfoViewController
            self.navigationController?.present(viewController, animated: true, completion: nil)
            
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        func updatePercentMgrVals() {
            
            self.mgrBtmV.isHidden = false
            self.quizBtmV.isHidden = true
            
            for i in 0..<6 {
                modulePercentVal[i] = modulePercentValMgr[i]
            }
            self.ReviewTblControler!.reloadData()
            print("Manager selected")
            managerReviewTextField.textColor = UIColor.white
            quizReviewTextField.textColor = UIColor.black
        }
        
        func updatePercentQuizVals() {
            
            self.mgrBtmV.isHidden = true
            self.quizBtmV.isHidden = false
            
            for i in 0..<6 {
                modulePercentVal[i] = modulePercentValQuiz[i]
            }
            self.ReviewTblControler!.reloadData()
            print("Quiz selected")
            managerReviewTextField.textColor = UIColor.black
            quizReviewTextField.textColor = UIColor.white
        }
        
        func updateGraphs() {
            
            setPieChartData(dataPoints: managerReviewLabel, values: managerReviewValue, chartObject: managerReviewGraph, chartLabel: "score_mgr_lbl".localized, grcolor: Constants.salesRep_manager_graph_color, overallpercent: 40);
            managerReviewGraph.legend.enabled = false
            managerReviewGraph.animate(xAxisDuration: 2)
            
            setPieChartData(dataPoints: quizReviewLabel, values: quizReviewValue, chartObject: quizReviewGraph, chartLabel: "score_quiz_lbl".localized, grcolor: Constants.salesRep_quiz_graph_color, overallpercent: 40);
            quizReviewGraph.legend.enabled = false
            quizReviewGraph.animate(xAxisDuration: 2)
            
        }
        
        func setPieChartData(dataPoints: [String], values: [Double], chartObject:PieChartView!, chartLabel:String, grcolor: UIColor, overallpercent: Int)
        {
            var dataEntries: [ChartDataEntry] = []
            
            for i in 0..<dataPoints.count {
                let dataEntry = ChartDataEntry(x: values[i], y: values[i], data: values[i] as AnyObject?)
                dataEntries.append(dataEntry)
            }
            
            let pieChartDataSet = PieChartDataSet(values: dataEntries, label: chartLabel)
            
            let pieChartData = PieChartData(dataSet: pieChartDataSet)
            pieChartData.setValueTextColor(UIColor.black)
            
            pieChartData.setValueFont(UIFont(name: "System", size: 0))
            
            chartObject.data = pieChartData
            chartObject.descriptionText = ""// chartLabel
            
            var colors: [UIColor] = []
            colors.append(grcolor)
            colors.append(UIColor.white)
            pieChartDataSet.colors = colors
            chartObject.holeColor = Constants.salesRep_hole_color
            chartObject.centerText = String(overallpercent) + "%"
            
        }
        
        let moduleList = ["score_m1".localized,"score_m2".localized,"score_m3".localized,"score_m4".localized,"score_m5".localized,"score_m6".localized]
        let modulePercent = ["50%","60%","30%","40%","70%","90%"]
        
        
        func numberOfSections(in tableView: UITableView) -> Int {
            // #warning Incomplete implementation, return the number of sections
            return 1
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            // #warning Incomplete implementation, return the number of rows
            return moduleList.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewTblVwCell_ID", for: indexPath) as! ReviewTblViewCell
            
            cell.reviewTblCellLabel1.text = moduleList[(indexPath as NSIndexPath).row]
            cell.reviewTblCellLabel2.text = String(modulePercentVal[(indexPath as NSIndexPath).row]) + "%"
            
            // Configure the cell...
            
            return cell
        }
        
        
        func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath)
        {
            /*print("Here I have selected Accessory no :\(indexPath.row)")
             
             let alertController = UIAlertController(title: "Score Card", message: "You have selected the module \(indexPath.row)" , preferredStyle: .alert)
             let OKAction = UIAlertAction(title: "ok".localized, style: .default) { (action:UIAlertAction!) in
             //self.navigationController?.popViewController(animated: true)
             //self.navigationController?.isNavigationBarHidden = false
             //self.view.removeFromSuperview()
             }
             alertController.addAction(OKAction)
             self.present(alertController, animated: true, completion:nil)*/
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            // let viewController:ReviewInfoViewController = storyboard.instantiateViewController(withIdentifier: "ReviewInfoViewController") as! ReviewInfoViewController
            
            
            let viewController:ReviewModuleInfoViewController = storyboard.instantiateViewController(withIdentifier: "ReviewModuleInfoViewController") as! ReviewModuleInfoViewController
            viewController.moduleid = indexPath.row + 1
            self.navigationController?.present(viewController, animated: true, completion: nil)
            
        }
        
        
        func updateModuleData() {
            
            /*var boxView = UIView()
             boxView = UIView(frame: CGRect(x: view.frame.midX - 110, y: view.frame.midY - 25, width: 220, height: 50))
             boxView.backgroundColor = UIColor.white
             boxView.alpha = 1
             boxView.layer.cornerRadius = 10
             managerReviewTextField.textColor = UIColor.white
             //Here the spinnier is initialized
             let activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
             activityView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
             activityView.startAnimating()
             
             let textLabel = UILabel(frame: CGRect(x: 60, y: 0, width: 200, height: 50))
             textLabel.textColor = UIColor.gray
             textLabel.text = "Updating review"
             
             //boxView.addSubview(activityView)
             //boxView.addSubview(textLabel)
             
             //view.addSubview(boxView)
             
             UIApplication.shared.beginIgnoringInteractionEvents()*/
            
            let progressHUD = ProgressHUD()
            self.view.addSubview(progressHUD)
            UIApplication.shared.beginIgnoringInteractionEvents()
            
            let headers = [
                "content-type": "application/x-www-form-urlencoded",
                "cache-control": "no-cache",
                ]
            
            let prefs:UserDefaults = UserDefaults.standard
            user_id = prefs.value(forKey: "user_id") as! String!
            course_id = prefs.value(forKey: "course_id") as! String!
            
            let moduleURL =  Constants.salesRepModulesInfoURL + user_id + "/" + course_id
            let request = NSMutableURLRequest(url: NSURL(string: moduleURL)! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 10.0)
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = headers
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                if (error != nil) {
                    
                    print("Error: \(error)")
                    progressHUD.hide()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    
                } else {
                    
                    DispatchQueue.main.sync {
                        let httpResponse = response as? HTTPURLResponse
                        print("HTTP Response: \(httpResponse)")
                        
                        do {
                            if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                                
                                print("Print converted dictionary",convertedJsonIntoDict)
                                
                                // Get value by key
                                let statusVal = convertedJsonIntoDict["status"] as? Bool
                                print(statusVal!)
                                self.overallQuizPercent = (convertedJsonIntoDict["overall_quiz_percentage"] as? Int)!
                                self.overallMgrPercent = (convertedJsonIntoDict["overall_manager_review_percentage"] as? Int)!
                                self.overallPercent = (convertedJsonIntoDict["overall_percentage"] as? Int)!
                                if (convertedJsonIntoDict["competency_level"] != nil) {
                                    self.competencyLbl.text = "You have unlocked " + (convertedJsonIntoDict["competency_level"] as? String)! + " Level"
                                } else {
                                    self.competencyLbl.text = "You have unlocked No Level!"
                                }
                                self.overallPercentLbl.text = String(self.overallPercent) + "%"
                                
                                self.managerReviewValue.removeAll()
                                self.managerReviewValue.append(Double(self.overallMgrPercent))
                                self.managerReviewValue.append(Double(100 - self.overallMgrPercent))
                                self.setPieChartData(dataPoints: self.managerReviewLabel, values: self.managerReviewValue, chartObject: self.managerReviewGraph, chartLabel: "Manager Review", grcolor: Constants.salesRep_manager_graph_color, overallpercent: self.overallMgrPercent);
                                self.managerReviewGraph.legend.enabled = false
                                self.managerReviewGraph.animate(xAxisDuration: 2)
                                
                                self.quizReviewValue.removeAll()
                                self.quizReviewValue.append(Double(self.overallQuizPercent))
                                self.quizReviewValue.append(Double(100 - self.overallQuizPercent))
                                self.setPieChartData(dataPoints: self.quizReviewLabel, values: self.quizReviewValue, chartObject: self.quizReviewGraph, chartLabel: "Quiz Review", grcolor: Constants.salesRep_quiz_graph_color, overallpercent: self.overallQuizPercent);
                                self.quizReviewGraph.legend.enabled = false
                                self.quizReviewGraph.animate(xAxisDuration: 2)
                                
                                if(statusVal! == true)
                                {
                                    OperationQueue.main.addOperation{
                                        
                                        if let modules = convertedJsonIntoDict["module"] as? NSDictionary {
                                            print("Modules: \(modules)")
                                            if let module1Data = modules["1"] as? NSDictionary {
                                                print("Module 1 Data: \(module1Data)")
                                                //                                        let mod1Title: String = (String(describing: module1Data["status"]))
                                                self.modulePercentVal[0] = (module1Data["manager_review_percentage"] as! Int)
                                                self.modulePercentValMgr[0] = (module1Data["manager_review_percentage"] as! Int)
                                                self.modulePercentValQuiz[0] = (module1Data["quiz_score_percentage"] as! Int)
                                            }
                                            if let module2Data = modules["2"] as? NSDictionary {
                                                print("Module 2 Data: \(module2Data)")
                                                //                                        let mod2Title: String = (String(describing: module2Data["status"]))
                                                self.modulePercentVal[1] = (module2Data["manager_review_percentage"] as! Int)
                                                self.modulePercentValMgr[1] = (module2Data["manager_review_percentage"] as! Int)
                                                self.modulePercentValQuiz[1] = (module2Data["quiz_score_percentage"] as! Int)
                                            }
                                            if let module3Data = modules["3"] as? NSDictionary {
                                                print("Module 3 Data: \(module3Data)")
                                                //                                        let mod3Title: String = (String(describing: module3Data["status"]))
                                                self.modulePercentVal[2] = (module3Data["manager_review_percentage"] as! Int)
                                                self.modulePercentValMgr[2] = (module3Data["manager_review_percentage"] as! Int)
                                                self.modulePercentValQuiz[2] = (module3Data["quiz_score_percentage"] as! Int)
                                            }
                                            if let module4Data = modules["4"] as? NSDictionary {
                                                print("Module 4 Data: \(module4Data)")
                                                //                                        let mod4Title: String = (String(describing: module4Data["status"]))
                                                self.modulePercentVal[3] = (module4Data["manager_review_percentage"] as! Int)
                                                self.modulePercentValMgr[3] = (module4Data["manager_review_percentage"] as! Int)
                                                self.modulePercentValQuiz[3] = (module4Data["quiz_score_percentage"] as! Int)
                                            }
                                            if let module5Data = modules["5"] as? NSDictionary {
                                                print("Module 5 Data: \(module5Data)")
                                                //                                        let mod5Title: String = (String(describing: module5Data["status"]))
                                                self.modulePercentVal[4] = (module5Data["manager_review_percentage"] as! Int)
                                                self.modulePercentValMgr[4] = (module5Data["manager_review_percentage"] as! Int)
                                                self.modulePercentValQuiz[4] = (module5Data["quiz_score_percentage"] as! Int)
                                            }
                                            if let module6Data = modules["6"] as? NSDictionary {
                                                print("Module 6 Data: \(module6Data)")
                                                //                                        let mod6Title: String = (String(describing: module6Data["status"]))
                                                self.modulePercentVal[5] = (module6Data["manager_review_percentage"] as! Int)
                                                self.modulePercentValMgr[5] = (module6Data["manager_review_percentage"] as! Int)
                                                self.modulePercentValQuiz[5] = (module6Data["quiz_score_percentage"] as! Int)
                                            }
                                            
                                            //                                    self.managerReviewGraph.isUserInteractionEnabled = true
                                            //                                    self.quizReviewGraph.isUserInteractionEnabled = true
                                            
                                            self.ReviewTblControler!.reloadData()
                                            
                                            progressHUD.hide()
                                            UIApplication.shared.endIgnoringInteractionEvents()
                                        }
                                    }
                                } else {
                                    var error_msg:NSString
                                    if convertedJsonIntoDict["message"] as? NSString != nil {
                                        error_msg = convertedJsonIntoDict["message"] as! NSString
                                    } else {
                                        error_msg = "Unknown Error"
                                    }
                                    print("error_msg",error_msg)
                                    progressHUD.hide()
                                    UIApplication.shared.endIgnoringInteractionEvents()
                                }
                            }
                        } catch let error as NSError {
                            print(error)
                            progressHUD.hide()
                            UIApplication.shared.endIgnoringInteractionEvents()
                        }
                    }
                }
            })
            
            dataTask.resume()
        }
        
}
