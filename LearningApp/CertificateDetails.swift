//
//  CertificateDetails.swift
//  LearningApp
//
//  Created by Sumit More on 1/2/17.
//  Copyright © 2017 Mac-04. All rights reserved.
//

import Foundation
import UIKit

class CertificateDetails
{
    var path:String!
    var id:String!
    var image:UIImage!
}
