//
//  PersonalDetailsController.swift
//  LearningApp
//
//  Created by Sumit More on 1/4/17.
//  Copyright © 2017 Mac-04. All rights reserved.
//

import Foundation
import UIKit
import libPhoneNumber_iOS

class PersonalDetailsController: UIViewController {
    
    @IBOutlet weak var pdLbl: UILabel!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var phoneTxt: UITextField!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var skipBtn: UIButton!
    @IBOutlet weak var switchBtn: UISwitch!
    @IBOutlet weak var notifLbl: UILabel!
    
    var use_personal_details: String = "0"
    
    @IBAction func switchClick(_ sender: AnyObject) {
        if self.switchBtn.isOn {
            self.use_personal_details = "1"
        } else {
            self.use_personal_details = "0"
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        self.view.frame.origin.y = 0
        super.touchesBegan(touches, with: event)
    }
    
    func keyboardWillShow(sender: NSNotification) {
        self.view.frame.origin.y = -150
    }
    
    func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 0
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.pdLbl.text = "pd_title".localized
        self.skipBtn.setTitle("pd_submit".localized, for: .normal)
        self.skipBtn.setTitle("pd_skip".localized, for: .normal)
        self.notifLbl.text = "pd_notif_txt".localized
        
        let prefs = UserDefaults.standard
        let emailV = prefs.value(forKey: "personal_email") as! String!
        let phoneV = prefs.value(forKey: "personal_phone") as! String!

//        self.emailTxt.text = ""
//        self.phoneTxt.text = ""
        self.emailTxt.text = emailV
        self.phoneTxt.text = phoneV
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)

    }
    
    @IBAction func skipClick(_ sender: AnyObject) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let initViewController: UIViewController = storyboard.instantiateViewController(withIdentifier: "EmployeeHomeTabView") as UIViewController
        self.present(initViewController, animated: true, completion: nil)
    }
    
    @IBAction func submitClick(_ sender: AnyObject) {
        let phoneUtil = NBPhoneNumberUtil()
        do {
            let phoneNumber: NBPhoneNumber = try phoneUtil.parse(self.phoneTxt.text, defaultRegion: "AU")
            if (phoneUtil.isValidNumber(phoneNumber)) || (self.phoneTxt.text == "") {
                let company_email = "profile_company_email_extension".localized
                let emailTxt_content = self.emailTxt.text
                
                if (emailTxt_content?.contains(company_email))! {
                    let alert = UIAlertController(title: "profile_update_email".localized, message: "profile_update_email_error".localized, preferredStyle: UIAlertControllerStyle.alert)
                    let OKAction = UIAlertAction(title: "profile_update_ok".localized, style: .default) { (action:UIAlertAction!) in
                        self.emailTxt.text = ""
                        self.emailTxt.becomeFirstResponder()
                    }
                    alert.addAction(OKAction)
                    self.present(alert, animated: true, completion: nil)
                } else {
                    if isValidEmail(testStr: emailTxt_content!) {
                        sendData()
                    } else {
                        let alert = UIAlertController(title: "profile_update_email".localized, message: "profile_update_email_valid_error".localized, preferredStyle: UIAlertControllerStyle.alert)
                        let OKAction = UIAlertAction(title: "profile_update_ok".localized, style: .default) { (action:UIAlertAction!) in
                            self.emailTxt.becomeFirstResponder()
                        }
                        alert.addAction(OKAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            } else {
                let alert = UIAlertController(title: "profile_update_phone".localized, message: "profile_update_phone_error".localized, preferredStyle: UIAlertControllerStyle.alert)
                let OKAction = UIAlertAction(title: "profile_update_ok".localized, style: .default) { (action:UIAlertAction!) in
                    self.phoneTxt.becomeFirstResponder()
                }
                alert.addAction(OKAction)
                self.present(alert, animated: true, completion: nil)
            }
        } catch let error as NSError
        {
            if(self.phoneTxt.text != "") {
                print("Phone error: \(error)")
                let alert = UIAlertController(title: "profile_update_phone".localized, message: "profile_update_phone_parse_error".localized, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "profile_update_ok".localized, style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            } else {
                let company_email = "profile_company_email_extension".localized
                let emailTxt_content = self.emailTxt.text
                
                if (emailTxt_content?.contains(company_email))! {
                    let alert = UIAlertController(title: "profile_update_email".localized, message: "profile_update_email_error".localized, preferredStyle: UIAlertControllerStyle.alert)
                    let OKAction = UIAlertAction(title: "profile_update_ok".localized, style: .default) { (action:UIAlertAction!) in
                        self.emailTxt.becomeFirstResponder()
                    }
                    alert.addAction(OKAction)
                    self.present(alert, animated: true, completion: nil)
                } else {
                    if isValidEmail(testStr: emailTxt_content!) {
                        sendData()
                    } else {
                        let alert = UIAlertController(title: "profile_update_email".localized, message: "profile_update_email_valid_error".localized, preferredStyle: UIAlertControllerStyle.alert)
                        let OKAction = UIAlertAction(title: "profile_update_ok".localized, style: .default) { (action:UIAlertAction!) in
                            self.emailTxt.becomeFirstResponder()
                        }
                        alert.addAction(OKAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        if(testStr == "") {
            return true
        } else {
            let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
            
            let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            return emailTest.evaluate(with: testStr)
        }
    }
    
    func sendData() {
        
        /*var boxView = UIView()
        boxView = UIView(frame: CGRect(x: view.frame.midX - 110, y: view.frame.midY - 25, width: 220, height: 50))
        boxView.backgroundColor = UIColor.white
        boxView.alpha = 1
        boxView.layer.cornerRadius = 10
        let activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        activityView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityView.startAnimating()
        let textLabel = UILabel(frame: CGRect(x: 60, y: 0, width: 200, height: 50))
        textLabel.textColor = UIColor.gray
        textLabel.text = "profile_update_progress".localized
        boxView.addSubview(activityView)
        boxView.addSubview(textLabel)
        
        view.addSubview(boxView)*/
        
        let progressHUD = ProgressHUD()
        self.view.addSubview(progressHUD)
        UIApplication.shared.beginIgnoringInteractionEvents()

        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
        ]
        
        let emailVal = self.emailTxt.text!
        let phoneVal = self.phoneTxt.text!

        let dataStr: String = "data={\"use_personal_details\": \"\(use_personal_details)\", \"personal_email\": \"\(emailVal)\",  \"personal_phone\":\"\(phoneVal)\"}"
        print("POST Params: \(dataStr)")
        let postData = NSMutableData(data: dataStr.data(using: String.Encoding.utf8)!)

        
        let prefs = UserDefaults.standard
        let userIdVal = prefs.string( forKey: "user_id")!

        let personalDetailsURL: String = Constants.salesRepPersonalDetailsURL + userIdVal
        
        let request = NSMutableURLRequest(url: NSURL(string: personalDetailsURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "pd_title".localized, message: "pd_error".localized, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "pd_ok".localized, style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
            } else {
                let httpResponse = response as? HTTPURLResponse
                print("Response: \(httpResponse)")
                do {
                    print("Data: \(data)")
                    
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        print("@@@Print converted dictionary",convertedJsonIntoDict)
                        
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        
                        if(statusVal == true)
                        {
                            
                            let setprefs = UserDefaults.standard
                            setprefs.set(emailVal, forKey: "personal_email")
                            setprefs.set(phoneVal, forKey: "personal_phone")
                            setprefs.synchronize()
                            
                            DispatchQueue.main.async {
                                
                                progressHUD.hide()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                let alert = UIAlertController(title: "pd_title".localized, message: "pd_success".localized, preferredStyle: UIAlertControllerStyle.alert)
                                let OKAction = UIAlertAction(title: "pd_ok".localized, style: .default) { (action:UIAlertAction!) in
                                    let prefs:UserDefaults = UserDefaults.standard
                                    prefs.set("1", forKey: "terms_accepted")
                                    prefs.synchronize()
                                    let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let initViewController: UIViewController = storyboard.instantiateViewController(withIdentifier: "EmployeeHomeTabView") as UIViewController
                                    self.present(initViewController, animated: true, completion: nil)
                                    //                                self.dismiss(animated: true, completion: nil)
                                }
                                alert.addAction(OKAction)
                                self.present(alert, animated: true, completion:nil)
                            }
                        }
                        else
                        {
                            DispatchQueue.main.async {
                                progressHUD.hide()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                let alert = UIAlertController(title: "pd_title".localized, message: "pd_error".localized, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "pd_ok".localized, style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                        
                    }
                } catch let error as NSError
                {
                    print(error)
                }
            }
        })
        
        dataTask.resume()
    }
}
