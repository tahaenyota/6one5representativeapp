//
//  BadgesCollectionViewController.swift
//  LearningApp
//
//  Created by Mac-04 on 22/08/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import UIKit
import Foundation

private let reuseIdentifier = "badgesCollectionCell"

class BadgesCollectionViewController: UICollectionViewController {
    
    var badgesImageArray: [UIImage] = []// UIImage(named: "badges1.png")!, UIImage(named: "badges2.png")!, UIImage(named: "badges3.png")!, UIImage(named: "badges4.png")!, UIImage(named: "badges5.png")!, UIImage(named: "badges6.png")!,UIImage(named: "badges7.png")!, UIImage(named: "badges8.png")!]
    
    var badgesTitle: [String] = [] //["Badges1Lbl".localized, "Badges2Lbl".localized, "Badges3Lbl".localized, "Badges4Lbl".localized, "Badges5Lbl".localized, "Badges6Lbl".localized, "Badges7Lbl".localized, "Badges8Lbl".localized]
    
    var badgesDesc: [String] = [] //["Badges1Desc".localized, "Badges2Desc".localized, "Badges3Desc".localized, "Badges4Desc".localized, "Badges5Desc".localized, "Badges6Desc".localized, "Badges7Desc".localized, "Badges8Desc".localized]
    
    var badgeCount = 0
    
    override func viewDidLoad() {
        
        let device = UIDevice.current.model
        print("Device type: " + device)
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()

        if(device == "iPhone") {
            layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
            let widthSize = (collectionView?.frame.size.width)! / 2
            layout.itemSize = CGSize(width: widthSize-15, height: widthSize+25)
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing = 10
        } else {
            layout.sectionInset = UIEdgeInsets(top: 25, left: 25, bottom: 25, right: 25)
            let widthSize = (collectionView?.frame.size.width)! / 2
            layout.itemSize = CGSize(width: widthSize-37, height: widthSize+50)
            layout.minimumInteritemSpacing = 0
            layout.minimumLineSpacing = 25
        }
        
        collectionView!.collectionViewLayout = layout
        
        loadBadgeData()
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return badgesTitle.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! BadgesCollionVwCell
    
        cell.BadgesImage.image = badgesImageArray[(indexPath as NSIndexPath).row]
        cell.BadgesTitleLbl.lineBreakMode = .byWordWrapping
        cell.BadgesTitleLbl.numberOfLines = 0;
        
//        if (indexPath as NSIndexPath).row == 0 {
//            cell.BadgesTitleLbl.text = "Most Improved UPT for Month" // badgesTitle[(indexPath as NSIndexPath).row]
//        } else {
        cell.BadgesTitleLbl.text = badgesTitle[(indexPath as NSIndexPath).row]

        cell.badgesDownloadBtn.layer.setValue(indexPath.row, forKey: "index")
        cell.badgesDownloadBtn.addTarget(self, action: #selector(downloadImg), for: .touchUpInside)

        cell.badgesShareBtn.layer.setValue(indexPath.row, forKey: "index")
        cell.badgesShareBtn.addTarget(self, action: #selector(shareImg), for: .touchUpInside)

        return cell
    }

    func shareImg(sender:UIButton) {
        let i : Int = (sender.layer.value(forKey: "index")) as! Int
        
        let imageToShare = [ badgesImageArray[i] ]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
        
    }

    func downloadImg(sender:UIButton) {
        let i : Int = (sender.layer.value(forKey: "index")) as! Int
        UIImageWriteToSavedPhotosAlbum(badgesImageArray[i], self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)

    }
    
    func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        print("Badge image function called")
        if let error = error {
            print("In error")
            // we got back an error!
            let ac = UIAlertController(title: "badge_save_error".localized, message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "badge_ok".localized, style: .default))
            present(ac, animated: true)
        } else {
            print("In success")
            let ac = UIAlertController(title: "badge_save_success".localized, message: "badge_image_saved".localized, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "badge_ok".localized, style: .default))
            present(ac, animated: true)
        }
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        print((indexPath as NSIndexPath).row)
        
        self.badgeCount = (indexPath as NSIndexPath).row
        
        let popOverVC = UIStoryboard (name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BadgesPopupID") as! BadgesPopupVwCtrlr
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        popOverVC.view.frame.size.height = self.view.frame.size.height
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
        
    }

    func disableBtns() {
        print("Disabling tab bar")
        self.tabBarController?.tabBar.isUserInteractionEnabled = false
    }
    
    func enableBtns() {
        print("Enabling tab bar")
        self.tabBarController?.tabBar.isUserInteractionEnabled = true
    }
    
    func loadBadgeData() {
        
        /*var boxView = UIView()
         
         boxView = UIView(frame: CGRect(x: view.frame.midX - 110, y: view.frame.midY - 25, width: 220, height: 50))
         boxView.backgroundColor = UIColor.white
         boxView.alpha = 1
         boxView.layer.cornerRadius = 10
         
         //Here the spinnier is initialized
         let activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
         activityView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
         activityView.startAnimating()
         
         let textLabel = UILabel(frame: CGRect(x: 60, y: 0, width: 200, height: 50))
         textLabel.textColor = UIColor.gray
         textLabel.text = "Updating Badges"
         
         boxView.addSubview(activityView)
         boxView.addSubview(textLabel)
         
         view.addSubview(boxView)*/
        
        let progressHUD = ProgressHUD()
        self.view.addSubview(progressHUD)
        
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        //        disableBtns()
        
        let headers = [
            "cache-control": "no-cache",
            ]
        
        let prefs:UserDefaults = UserDefaults.standard
        let user_id = prefs.value(forKey: "user_id") as! String!
        let badgesURL =  Constants.salesRepGetBadgesURL + user_id!
        
        print("Badges URL: \(badgesURL)")
        
        let request = NSMutableURLRequest(url: NSURL(string: badgesURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
                progressHUD.hide()
                //boxView.removeFromSuperview()
                UIApplication.shared.endIgnoringInteractionEvents()
                //                self.enableBtns()
                let alertController = UIAlertController(title: "badges_title".localized, message: "badge_save_error".localized , preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion:nil)
            } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse)
                
                do {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        DispatchQueue.main.sync {
                            print("Print converted dictionary",convertedJsonIntoDict["status"])
                            let badgesArrJ: NSArray
                            let counter = convertedJsonIntoDict["status"] as! Bool
                            if counter {
                                badgesArrJ = convertedJsonIntoDict["data"] as! NSArray
                                print("Badges Array Count: \(badgesArrJ.count)")
                                print("Badges Array: \(badgesArrJ)")
                                
                                self.badgesImageArray.removeAll()
                                self.badgesDesc.removeAll()
                                self.badgesTitle.removeAll()
                                
                                for i in 0..<badgesArrJ.count {
                                    let badgeObj: JSON = badgesArrJ[i] as! JSON
                                    let badgeId = badgeObj["badge_id"] as! String
                                    let badgeTitleObj = badgeObj["name"] as! String
                                    let badgeDescObj = badgeObj["description"] as! String
                                    
                                    if self.badgesTitle .contains(badgeTitleObj){
                                    }else{
                                        self.badgesTitle.append(badgeTitleObj)
                                        self.badgesDesc.append(badgeDescObj)
                                    }
                                    
                                    
                                    switch badgeId {
                                    case "1": self.badgesImageArray.append(UIImage(named: "badges1.png")!)
                                        break
                                    case "2": self.badgesImageArray.append(UIImage(named: "badges2.png")!)
                                        break
                                    case "3": self.badgesImageArray.append(UIImage(named: "badges3.png")!)
                                        break
                                    case "4": self.badgesImageArray.append(UIImage(named: "badges4.png")!)
                                        break
                                    case "5": self.badgesImageArray.append(UIImage(named: "badges5.png")!)
                                        break
                                    case "6": self.badgesImageArray.append(UIImage(named: "badges6.png")!)
                                        break
                                    case "7": self.badgesImageArray.append(UIImage(named: "badges7.png")!)
                                        break
                                    case "8": self.badgesImageArray.append(UIImage(named: "badges8.png")!)
                                        break
                                    case "9": self.badgesImageArray.append(UIImage(named: "badges9.png")!)
                                        break
                                    case "10": self.badgesImageArray.append(UIImage(named: "badges10.png")!)
                                        break
                                    case "11": self.badgesImageArray.append(UIImage(named: "badges11.png")!)
                                        break
                                    case "12": self.badgesImageArray.append(UIImage(named: "badges12.png")!)
                                        break
                                    case "13": self.badgesImageArray.append(UIImage(named: "badges13.png")!)
                                        break
                                    default: break
                                    }
                                }
                                 self.collectionView?.reloadData()
                            }else{
                                let alertController = UIAlertController(title: "badges_title".localized, message: "No badges recieved yet" , preferredStyle: .alert)
                                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.navigationController?.popViewController(animated: true)
                                }
                                alertController.addAction(OKAction)
                                self.present(alertController, animated: true, completion:nil)
                            }
                           
                            // boxView.removeFromSuperview()
                            progressHUD.hide()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            //                            self.enableBtns()
                        }
                        
                    }
                } catch let error as NSError {
                    print(error)
                    //boxView.removeFromSuperview()
                    progressHUD.hide()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    //                    self.enableBtns()
                    let alertController = UIAlertController(title: "badges_title".localized, message: "badge_save_error".localized , preferredStyle: .alert)
                    let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in }
                    alertController.addAction(OKAction)
                    self.present(alertController, animated: true, completion:nil)
                }
            }
        })
        
        dataTask.resume()
    }
    
}
