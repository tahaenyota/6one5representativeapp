//
//  HomeViewController.swift
//  LearningApp
//
//  Created by Mac-04 on 17/08/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    @IBOutlet weak var collectionVw: UICollectionView!
    
    @IBOutlet var DashboardbackUIView: UIView!
    
    @IBOutlet weak var dashboardUname: UILabel!
    
    @IBAction func logoutClicked(_ sender: AnyObject) {
        
        let appDomain = Bundle.main.bundleIdentifier
        UserDefaults.standard.removePersistentDomain(forName: appDomain!)
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let initViewController: UIViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewControllerID") as UIViewController
        self.present(initViewController, animated: true, completion: nil)
        
    }
    var progressHUD = ProgressHUD()
    var listOfImages: [UIImage] = [ UIImage(named: "module.png")!,
                                    UIImage(named: "scorecard_db.png")!,
                                    UIImage(named: "badges.png")!,
                                    UIImage(named: "certificate.png")!,
                                    UIImage(named: "profile_db.png")!,
                                    UIImage(named: "faqs.png")!
    ]
    
    var listOfDashboardItem = ["Rep_Modules".localized,
                            "Rep_Reviews".localized,
                            "Rep_Badges".localized,
                            "Rep_Certificate".localized,
                            "Rep_Profile".localized,
                            "Rep_FAQ".localized
                            ]
    
    @IBOutlet weak var userProfilePic: UIImageView!
    
    fileprivate let reuseIdentifier = "HomeGridCell"
    
    var notifCount: Int = 0
    
    override func viewWillLayoutSubviews() {
        var tabframe615: CGRect = (tabBarController?.tabBar.frame)!
        tabframe615.size.height = 52
        tabframe615.origin.y = self.view.frame.size.height - 52
        tabBarController?.tabBar.frame = tabframe615
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! HomeCollonVwCell
        cell.HomeCollnVwGridImg.image = listOfImages[(indexPath as NSIndexPath).row]
        cell.HomeCollnVwGridLbl.text = listOfDashboardItem[(indexPath as NSIndexPath).row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        switch (indexPath as NSIndexPath).row {
        case 0: performSegue(withIdentifier: "ModulesVwCtrlSegueID2", sender: nil)
        //                let alertController = UIAlertController(title: "This section is under development", message: "" , preferredStyle: .alert)
        //                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in }
        //                alertController.addAction(OKAction)
        //                self.present(alertController, animated: true, completion:nil)
            break
        case 1:
            
            let device = UIDevice.current.model
            if (device == "iPhone")
            {
                performSegue(withIdentifier: "ReviewVwCtrlSegueID", sender: nil)//ReviewVwCtrlSegueIDiPad
            }
            else
            {
                performSegue(withIdentifier: "ReviewViewControlleriPad", sender: nil)//
            }
            break
        case 2: performSegue(withIdentifier: "BadgesVwCtrlSegueID", sender: nil)
            break
        case 3: performSegue(withIdentifier: "CertificateSegueID", sender: nil)
            break
        case 4: performSegue(withIdentifier: "ProfileSegueID", sender: nil)
            break
        case 5: performSegue(withIdentifier: "FAQVwCtrlSegueID", sender: nil)
            break
        default: print((indexPath as NSIndexPath).row)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        let widthSize = collectionView.frame.size.width / 2
        return CGSize(width: widthSize-1, height: widthSize)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0.5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 1.0
    }
    
    var username: String = "Profile Name"
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        self.navigationController!.navigationBar.topItem!.title = "";
        
        let DashBlueColor = UIColor(red: 0/255.0, green: 174/255.0, blue: 239/255.0, alpha: 1.0)
        self.DashboardbackUIView.backgroundColor = DashBlueColor
        
        collectionVw!.contentInset = UIEdgeInsets(top: -10, left: 0, bottom: 0, right: 1)
    
        //[self setAutomaticallyAdjustsScrollViewInsets:NO];
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        let device = UIDevice.current.model
        
        let image2 = self.userProfilePic.image?.resizeWith(width: Constants.profile_image_size)
        
        self.userProfilePic.image = image2
        
        userProfilePic.image = UIImage(named: "profile_db")
        self.userProfilePic.layoutIfNeeded()
        self.userProfilePic.layer.borderWidth = 2
        self.userProfilePic.layer.masksToBounds = false
        self.userProfilePic.layer.borderColor = UIColor.white.cgColor
        
        if(device == "iPhone") {
            self.userProfilePic.layer.cornerRadius = 60
            Constants.fontTag = "<font face ='Helvetica Neue' size='3'>"
            Constants.FAQfontTag = "<font face ='Helvetica Neue' size='5'>"
            Constants.DiscripterFontTag = "<font face ='Helvetica Neue' size='4'>"
        } else {
            self.userProfilePic.layer.cornerRadius = 100
            Constants.fontTag = "<font face ='Helvetica Neue' size='5'>"
            Constants.FAQfontTag = "<font face ='Helvetica Neue' size='6'>"
            Constants.DiscripterFontTag = "<font face ='Helvetica Neue' size='5'>"
        }
        self.userProfilePic.clipsToBounds = true
        
        
        /*let prefs:UserDefaults = UserDefaults.standard
        username = prefs.value(forKey: "username") as! String!
        self.dashboardUname.text = username
        
        if (prefs.value(forKey: "profile_pic") != nil)
        {
            let profile_pic:String = prefs.value(forKey: "profile_pic") as! String!
            
            if profile_pic != ""
            {
                print("Here is the profile pic link : \(profile_pic)")
                
                let url = URL(string: profile_pic)// + "1617_1485337054.png")
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                userProfilePic.image = UIImage(data: data!)
            }
        }
        
        */
        getProfileDetails()
        print("Notif count in home")
        //getNotifCount() // Now managed from profile
        //        tabBarController?.tabBar.items![2].badgeValue = String(self.notifCount);
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func getNotifCount()
    {
        
        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            ]
        
        //        var notifAPICount: Int = 0
        
        let prefs:UserDefaults = UserDefaults.standard
        let user_id = prefs.value(forKey: "user_id") as! String!
        
        let notifURL =  Constants.salesRepNotifInfoURL + user_id!
        let request = NSMutableURLRequest(url: NSURL(string: notifURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        

        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            DispatchQueue.main.async()
            {
                
                if (error != nil)
                {
                    print("Notifs not updated. Error: \(error)")
                    self.progressHUD.hide()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    
                } else
                {
                    let httpResponse = response as? HTTPURLResponse
                    print("HTTP Response: \(httpResponse)")
                    do {
                        if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                            
                            print("Print converted dictionary",convertedJsonIntoDict)
                            
                            let statusVal = convertedJsonIntoDict["status"] as? Bool
                            
                            print("Status is \(statusVal)")
                            if statusVal == true {
                                print("True! Status is \(statusVal)")
                                if let notifs = convertedJsonIntoDict["data"] as? NSArray {
                                    let notifAPICount = notifs.count
                                    let not = convertedJsonIntoDict["count"] as? Int
                                    self.tabBarController?.tabBar.items![2].badgeValue = String(describing: not!)
                                    print("Notif Count: \(notifAPICount)")
                                    //self.tabBarController?.tabBar.items![2].badgeValue = String(notifAPICount);
                                } else {
                                    print("No notif data parsed!")
                                }
                            } else {
                                print("Not true! Status is \(statusVal)")
                            }
                        }
                        self.progressHUD.hide()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        
                    } catch let error as NSError {
                        print(error)
                        print("Notifs not updated. Error: \(error)")
                        self.progressHUD.hide()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        
                    }
                }
            }
        })
        
        dataTask.resume()
        
    }
    
    
    
    func getProfileDetails()
    {
        progressHUD = ProgressHUD()

        self.view.addSubview(progressHUD)
        //progressHUD.activityIndictor.color = UIColor.black
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            ]
        let prefs:UserDefaults = UserDefaults.standard
        let user_id = prefs.value(forKey: "user_id") as! String!
        
        let apiURL: String = "https://key2train.in/admin/api/v1/get_user_profile_details/" + user_id!
        let request = NSMutableURLRequest(url: NSURL(string: apiURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        print("Status check \(apiURL)")
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            DispatchQueue.main.async()
            {
                if (error != nil) {
                    print("Network Error: \(error)")
                    let alert = UIAlertController(title: "profileupdate".localized, message: "profileerrordetails".localized, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "ok".localized, style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    self.getNotifCount()
                } else {
                    let httpResponse = response as? HTTPURLResponse
                    print("HTTP Response: \(httpResponse)")
                    
                    do {
                        if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                            
                            print("profile Api call :\(apiURL)" +  "Print Profile details ",convertedJsonIntoDict)
                            
                            // Get value by key
                            let statusVal = convertedJsonIntoDict["status"] as? Bool
                            print("status value is \(statusVal!)")
                            
                            if(statusVal! == true)
                            {
                                OperationQueue.main.addOperation{
                                    
                                    let details = convertedJsonIntoDict["details"] as! NSDictionary
                                    
                                    let userId = details["l_id"] as? String
                                    let lFName = details["l_fname"] as? String
                                    let lLName = details["last_name"] as? String
                                    let pEmail = details["personal_email"] as? String
                                    let pPhone = details["personal_phone"] as? String
                                    let profilePic = details["profile_pic_link"] as? String
                                    let prefs:UserDefaults = UserDefaults.standard
                                    prefs.set(profilePic, forKey:"profile_pic")
                                    prefs.set(pEmail, forKey:"personal_email")
                                    prefs.set(pPhone, forKey:"personal_phone")
                                    prefs.set(lFName! + " " + lLName! , forKey: "username")
                                    
                                    prefs.synchronize()
                                    
                                    self.username = prefs.value(forKey: "username") as! String!
                                    self.dashboardUname.text = self.username
                                    
                                    if profilePic != ""
                                    {
                                        print("Here is the profile pic link : \(profilePic)")
                                        
                                        let url = URL(string: profilePic!)// + "1617_1485337054.png")
                                        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                                        self.userProfilePic.image = UIImage(data: data!)
                                    }
                                    
                                    let image2 = self.userProfilePic.image?.resizeWith(width: Constants.profile_image_size)
                                    
                                    self.userProfilePic.image = image2
                                    
                                    
                                    self.getNotifCount()
                                    
                                    //self.progressHUD.hide()
                                    
                                    
                                    
                                }
                            } else
                            {
                                self.getNotifCount()
                                /*var error_msg:NSString
                                 if convertedJsonIntoDict["message"] as? NSString != nil
                                 {
                                 error_msg = convertedJsonIntoDict["message"] as! NSString
                                 } else {
                                 error_msg = "Unknown Error"
                                 }
                                 print("error_msg",error_msg)*/
                                
                                /*progressHUD.hide();
                                 
                                 
                                 let alertController = UIAlertController(title: "profileupdate", message: "No review request" , preferredStyle: .alert)
                                 let OKAction = UIAlertAction(title: "ok".localized, style: .default) { (action:UIAlertAction!) in
                                 //self.navigationController?.popViewController(animated: true)
                                 self.navigationController?.isNavigationBarHidden = false
                                 self.view.removeFromSuperview()
                                 }
                                 alertController.addAction(OKAction)
                                 self.present(alertController, animated: true, completion:nil)*/
                                
                            }
                        }
                    } catch let error as NSError {
                        print(error)
                        //self.progressHUD.hide()
                        self.getNotifCount()
                        
                    }
                }
            }
        })
        
        dataTask.resume()
    }

    
    
    
}


