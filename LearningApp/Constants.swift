//
//  Constants.swift
//  LearningApp
//
//  Created by Sumit More on 9/24/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import Foundation

class Constants {
    
    static let profile_image_size: CGFloat = 200.0
    static let profile_image_radius: CGFloat = 100.0
    static let GlobalFont:UIFont = UIFont(name: "Helvetica New", size: 8.0)!
    static var fontTag:String = "<font size='5'>";
    static let fontclosing:String = "</font>";
    
    static var FAQfontTag:String = "<font size='6'>";
    static let FAQfontclosing:String = "</font>";
    
    static var HelpfontTag_iPAD:String = "<font face = 'Helvetica Neue' size='6'>";
    static var HelpfontTag_iPHONE:String = "<font face = 'Helvetica Neue' size='5'>";
    
    static var DiscripterFontTag:String = "<font size='6'>";
    
    static let device = UIDevice.current.model
    static let screenSize = UIScreen.main.bounds
    static let salesRepFeedbackURL: String = "https://key2train.in/admin/api/v1/feedback/" 
//    static let salesRepFeedbackURL: String = "https://api.enyotalms.com/lms/test/admin/api/v1/feedback/"

    static let salesRepQuizSubmitForReviewURL: String = "https://key2train.in/admin/api/v1/module_data/"
//    static let salesRepQuizSubmitForReviewURL: String = "https://api.enyotalms.com/lms/test/admin/api/v1/module_data/"

//    static let loginURL: String = "https://api.enyotalms.com/lms/test/admin/api/v1/login"
    static let loginURL: String = "https://key2train.in/admin/api/v1/login"

    static let salesRepModulesInfoURL: String = "https://key2train.in/admin/api/v1/fetch_learner_manager_review/"
//    static let salesRepModulesInfoURL: String = "https://api.enyotalms.com/lms/test/admin/api/v1/fetch_learner_manager_review/"
   
    static let salesManagerLatestModuleInfo = "https://key2train.in/admin/api/v1/module_data_status/"
    
    
    static let managerGetNotifsURL: String = "https://key2train.in/admin/api/v1/module_review_request/"
//    static let managerGetNotifsURL: String = "https://api.enyotalms.com/lms/test/admin/api/v1/module_review_request/"

    static let managerSubmitRatingURL: String = "https://key2train.in/admin/api/v1/manager_descriptor_ratings/"
//    static let managerSubmitRatingURL: String = "https://api.enyotalms.com/lms/test/admin/api/v1/manager_descriptor_ratings/"
    
    static let salesRep_manager_graph_color: UIColor = UIColor(red: 125/255, green: 208/255, blue: 26/255, alpha: 1)
    static let salesRep_quiz_graph_color: UIColor = UIColor(red: 242/255, green: 101/255, blue: 34/255, alpha: 1)
    static let salesRep_blank_graph_color: UIColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
    static let salesRep_hole_color: UIColor = UIColor(red: 235/255, green: 235/255, blue: 241/255, alpha: 1)

    static let salesRepPracticeWithManagerURL: String = "https://key2train.in/admin/api/v1/review_request/"
//    static let salesRepPracticeWithManagerURL: String = "https://key2train.in/admin/api/v1/review_request/"

    static let salesRepPracticeWithCustomerURL: String = "https://key2train.in/admin/api/v1/customer_role_play_review_request/"
    
    static let salesRepCertificateURL: String = "https://key2train.in/admin/api/v1/send_certificate/"
//    static let salesRepCertificateURL: String = "https://api.enyotalms.com/lms/test/admin/api/v1/send_certificate/"
 
    static let salesRepGetBadgesURL: String = "https://key2train.in/admin/api/v1/get_user_badges/"

    static let salesRepUpdateProfileURL: String = "https://key2train.in/admin/api/v1/update_user_profile"

    static let salesRepAcceptTermsURL: String = "https://key2train.in/admin/api/v1/accept_terms/"
 
    static let salesRepPersonalDetailsURL: String = "https://key2train.in/admin/api/v1/update_personal_email_mobile/"
    
    static let salesRepNotifInfoURL: String = "https://key2train.in/admin/api/v1/user_notifications/"
    
    static let updateUserNotification:String = "https://key2train.in/admin/api/v1/update_user_notification/"
    
    //  leading treading constraints for 6s plus and 7plus device
    
    // module 0
    static let M0_welcomeLblConstraints_6splus = [170,167]
    static let M0_UnderstandLblConstraints_6splus = [121,280]
    static let M0_AdviseLblConstraints_6splus = [141,355]
    static let M0_CloseLblConstraints_6splus = [151,405]
    static let M0_ThankLblConstraints_6splus = [170,520]
    
    static let M0_intro_m0e1l1Constraints_6splus = [170,605]
    static let M0_intro_m0e2l1Constraints_6splus = [170,525]
    
    static let M0_intro_m0e10l1Constraints_6splus = [110,550]
    static let M0_intro_m0e10l2Constraints_6splus = [110,550,260]
    
    static let M0_intro_m0e12l1Constraints_6splus = [175,230]
    static let M0_intro_m0e12l2Constraints_6splus = [110,290]
    static let M0_intro_m0e12l3Constraints_6splus = [110,352]
    static let M0_intro_m0e12l4Constraints_6splus = [110,415]
    static let M0_intro_m0e12l5Constraints_6splus = [110,478]
    
    //module 1
    static let M1_welcomeLblConstraints_6splus = [175,180]
    static let M1_UnderstandLblConstraints_6splus = [121,290]
    static let M1_AdviseLblConstraints_6splus = [141,365]
    static let M1_CloseLblConstraints_6splus = [151,415]
    static let M1_ThankLblConstraints_6splus = [165,540]
    
    static let M1_top_intro_m1e2l1Constraints_6splus = [580,136]

    static let M1_Buttonb1Constraints_6splus = [141,141,141,201]
    static let M1_Buttonb2Constraints_6splus = [141,141,215,351]
    static let M1_Buttonb3Constraints_6splus = [141,141,55,351]

    // module 2
    static let M2_welcomeLblConstraints_6splus = [185,176]
    static let M2_UnderstandLblConstraints_6splus = [121,290]
    static let M2_AdviseLblConstraints_6splus = [141,365]
    static let M2_CloseLblConstraints_6splus = [151,415]
    static let M2_ThankLblConstraints_6splus = [194,540]
    
    static let M2_CustomersLblConstraints_6splus = 400
    static let M2_AskConstraints_6splus = [11,501,110]
    static let M2_ListenConstraints_6splus = [151,561,110]
    static let M2_ConfirmConstraints_6splus = [295,501,110]

    static let M2_whyLblConstraints_6splus = [41,136,121,91]
    static let M2_whatLblConstraints_6splus = [175,88,121,91]
    static let M2_willLblConstraints_6splus = [265,170,121,91]
    static let M2_whenLblConstraints_6splus = [211,275,121,91]
    static let M2_howLblConstraints_6splus = [146,375,121,91]
    
    // module 3
    static let M3_welcomeLblConstraints_6splus = [201,171]
    static let M3_UnderstandLblConstraints_6splus = [121,285]
    static let M3_AdviseLblConstraints_6splus = [141,358]
    static let M3_CloseLblConstraints_6splus = [151,411]
    static let M3_ThankLblConstraints_6splus = [194,535]
    
    static let M3_intro_m3e3b1Constraints_6splus = [100,271,75,151]
    static let M3_intro_m3e3b2Constraints_6splus = [170,328,75,151]
    static let M3_intro_m3e3b3Constraints_6splus = [270,271,101,151]
    
    static let M3_intro_m3e2l1Constraints_6splus = [131,205,66]
    static let M3_intro_m3e2l2Constraints_6splus = [125,246,230]
    static let M3_intro_m3e2l3Constraints_6splus = [131,240,60]
    static let M3_intro_m3e2l4Constraints_6splus = [125,325,230]
    static let M3_intro_m3e2l5Constraints_6splus = [131,360]
    
    
    // module 4
    static let M4_welcomeLblConstraints_6splus = [205,125]
    static let M4_UnderstandLblConstraints_6splus = [131,251]
    static let M4_AdviseLblConstraints_6splus = [171,325]
    static let M4_CloseLblConstraints_6splus = [151,411]
    static let M4_ThankLblConstraints_6splus = [211,535]
    
    static let M4_intro_m4e2b1Constraints_6splus = [90,280,25,100]
    static let M4_intro_m4e2b2Constraints_6splus = [100,380,90]
    static let M4_intro_m4e2b3Constraints_6splus = [90,280,300,90]
    static let M4_intro_m4e2b4Constraints_6splus = [100,380,295,100]
    
    
    
    
    // module 5
    static let M5_welcomeLblConstraints_6splus = [215,146]
    static let M5_UnderstandLblConstraints_6splus = [131,271]
    static let M5_AdviseLblConstraints_6splus = [171,315]
    static let M5_CloseLblConstraints_6splus = [151,411]
    static let M5_ThankLblConstraints_6splus = [211,545]
    
    static let M5_intro_m5e2l1Constraints_6splus = [230,205]
    static let M5_intro_m5e2l2Constraints_6splus = [230,364]
    static let M5_intro_m5e2l3Constraints_6splus = [230,472]
    
    
    
    // module 6
    
    static let M6_intro_m6e3l1Constraints_6splus = [170,120]
    static let M6_intro_m6e3l2Constraints_6splus = [170,220]
    static let M6_intro_m6e3l3Constraints_6splus = [170,480]
    static let M6_intro_m6e3l4Constraints_6splus = [170,570]
    
    //   L   T  H  W
    static let M6_intro_m6e4b1Constraints_6splus = [230,180,51,81]
    static let M6_intro_m6e4b2Constraints_6splus = [105,268,51,81]
    static let M6_intro_m6e4b3Constraints_6splus = [225,346,51,81]
    static let M6_intro_m6e4b4Constraints_6splus = [105,430,51,81]
    static let M6_intro_m6e4b5Constraints_6splus = [228,510,51,81]

}
