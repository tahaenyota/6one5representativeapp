//
//  ProfileController.swift
//  LearningApp
//
//  Created by Sumit More on 1/3/17.
//  Copyright © 2017 Mac-04. All rights reserved.
//

import Foundation
import UIKit
import libPhoneNumber_iOS

class ProfileController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var editPhotoBtn: UIButton!
    @IBOutlet weak var fnameLbl: UILabel!
    @IBOutlet weak var fnameTxt: UITextField!
    @IBOutlet weak var lnameLbl: UILabel!
    @IBOutlet weak var lnameTxt: UITextField!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordLbl: UILabel!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var phoneTxt: UITextField!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    var popover:UIPopoverController?=nil
    @IBOutlet weak var editView: UIView!
    let ImagePicker = UIImagePickerController()
    @IBAction func editPhotoClick(_ sender: AnyObject) {
        selectProfPic()
    }
    
    
    
    
    /*func selectProfPic() {
     
        ImagePicker.delegate = self
        
        
        /*if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            ImagePicker.sourceType = UIImagePickerControllerSourceType.camera
            self.present(ImagePicker, animated: true, completion: nil)
        }else{
            let alert = UIAlertView()
            alert.title = "Warning"
            alert.message = "You don't have camera"
            alert.addButton(withTitle: "OK")
            alert.show()
        }*/
        
        
        ImagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(ImagePicker, animated: true, completion: nil)
    }*/
    
    func selectProfPic()
    {
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallary", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        // Add the actions
        ImagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        // Present the controller
        if UIDevice.current.userInterfaceIdiom == .phone
        {
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            popover = UIPopoverController(contentViewController: alert)
            popover!.present(from: self.editPhotoBtn.frame, in: self.view, permittedArrowDirections: UIPopoverArrowDirection.any, animated: true)
        }
    }
    func CGRectMake(_ x: CGFloat, _ y: CGFloat, _ width: CGFloat, _ height: CGFloat) -> CGRect {
        return CGRect(x: x, y: y, width: width, height: height)
    }
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            ImagePicker.sourceType = UIImagePickerControllerSourceType.camera
            self .present(ImagePicker, animated: true, completion: nil)
        }else{
            let alert = UIAlertView()
            alert.title = "Warning"
            alert.message = "You don't have camera"
            alert.addButton(withTitle: "OK")
            alert.show()
        }
    }
    func openGallary(){
        ImagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        self.present(ImagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let profileImage = image
//            let image2 = profileImage.resize(CGSize(width: Constants.profile_image_size, height: Constants.profile_image_size))
            let image2 = profileImage.resizeWith(width: Constants.profile_image_size)
            self.profileImg.image = image2
//            self.profileImg.transform = CGAffineTransform.init(rotationAngle: 360.0)
//
//            self.profileImg.image = image
            
            if let data = UIImagePNGRepresentation(image) {
                print("Writing image to directory")
                let filename = getDocumentsDirectory().appendingPathComponent("profile_img_6one5.png")
                try? data.write(to: filename)
            }

        } else{
            print("Something went wrong")
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
//    private func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
//        self.profileImg.image = info[UIImagePickerControllerOriginalImage] as? UIImage
//        self.dismiss(animated: true, completion: nil)
//    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    @IBAction func saveClick(_ sender: AnyObject) {
        
       
        
        fnameTxt.resignFirstResponder()
        lnameTxt.resignFirstResponder()
        emailTxt.resignFirstResponder()
        
        passwordTxt.resignFirstResponder()
        phoneTxt.resignFirstResponder()
        checkAndSendData()
    }

    func checkAndSendData() {
        let phoneUtil = NBPhoneNumberUtil()
        do {
            let phoneNumber: NBPhoneNumber = try phoneUtil.parse(self.phoneTxt.text, defaultRegion: "AU")
            if (phoneUtil.isValidNumber(phoneNumber)) || (self.phoneTxt.text == "") {
                let company_email = "profile_company_email_extension".localized
                print("Company email: \(company_email)")
                let emailTxt_content = self.emailTxt.text
                print("Email Content: \(emailTxt_content)")
                
                if (emailTxt_content?.contains(company_email))! {
                    let alert = UIAlertController(title: "profile_update_email".localized, message: "profile_update_email_error".localized, preferredStyle: UIAlertControllerStyle.alert)
                    let OKAction = UIAlertAction(title: "profile_update_ok".localized, style: .default) { (action:UIAlertAction!) in
                        self.emailTxt.text = ""
                        self.emailTxt.becomeFirstResponder()
                    }
                    alert.addAction(OKAction)
                    self.present(alert, animated: true, completion: nil)
                } else {
                    if isValidEmail(testStr: emailTxt_content!) {
                        sendData()
                    } else {
                        let alert = UIAlertController(title: "profile_update_email".localized, message: "profile_update_email_valid_error".localized, preferredStyle: UIAlertControllerStyle.alert)
                        let OKAction = UIAlertAction(title: "profile_update_ok".localized, style: .default) { (action:UIAlertAction!) in
                            self.emailTxt.becomeFirstResponder()
                        }
                        alert.addAction(OKAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            } else {
                let alert = UIAlertController(title: "profile_update_phone".localized, message: "profile_update_phone_error".localized, preferredStyle: UIAlertControllerStyle.alert)
                let OKAction = UIAlertAction(title: "profile_update_ok".localized, style: .default) { (action:UIAlertAction!) in
                    self.phoneTxt.becomeFirstResponder()
                }
                alert.addAction(OKAction)
                self.present(alert, animated: true, completion: nil)
            }
        } catch let error as NSError
        {
            if(self.phoneTxt.text != "") {
                print("Phone error: \(error)")
                let alert = UIAlertController(title: "profile_update_phone".localized, message: "profile_update_phone_parse_error".localized, preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "profile_update_ok".localized, style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            } else {
                let company_email = "profile_company_email_extension".localized
                let emailTxt_content = self.emailTxt.text
                
                if (emailTxt_content?.contains(company_email))! {
                    let alert = UIAlertController(title: "profile_update_email".localized, message: "profile_update_email_error".localized, preferredStyle: UIAlertControllerStyle.alert)
                    let OKAction = UIAlertAction(title: "profile_update_ok".localized, style: .default) { (action:UIAlertAction!) in
                        self.emailTxt.becomeFirstResponder()
                    }
                    alert.addAction(OKAction)
                    self.present(alert, animated: true, completion: nil)
                } else {
                    if isValidEmail(testStr: emailTxt_content!) {
                        sendData()
                    } else {
                        let alert = UIAlertController(title: "profile_update_email".localized, message: "profile_update_email_valid_error".localized, preferredStyle: UIAlertControllerStyle.alert)
                        let OKAction = UIAlertAction(title: "profile_update_ok".localized, style: .default) { (action:UIAlertAction!) in
                            self.emailTxt.becomeFirstResponder()
                        }
                        alert.addAction(OKAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    func sendData() {
        let progressHUD = ProgressHUD()
        self.view.addSubview(progressHUD)
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        let boundary = generateBoundaryString()
        
        let headers = [
            "boundary": boundary,
            "content-type": "application/x-www-form-urlencoded",
            ]
        
        let passwordVal = self.passwordTxt.text!
        let firstnameVal = self.fnameTxt.text!
        let lastnameVal = self.lnameTxt.text!
        let emailVal = self.emailTxt.text!
        var phoneVal = self.phoneTxt.text!
        
        phoneVal.remove(at: phoneVal.startIndex)
        
        let login = Login.sharedInstance
        //let prefs = UserDefaults.standard
        let userIdVal = login.getManagerId()//prefs.string( forKey: "user_id")!
        
        //let base64 =  self.profileImg.base64EncodedStringWithOptions(.allZeros)
//        let imageData:NSData = UIImagePNGRepresentation(self.profileImg.image!)! as NSData
//        var base:String!
//              if(profileImg.image != nil)
//               {
              // let base = convertImageToBase64(image: self.profileImg.image!)
            // }
        
        
        
        var base:String = convertImageToBase64(image: UIImage(named: "profile_db")!)
        if(profileImg.image != nil)
        {
            base = convertImageToBase64(image: self.profileImg.image!)
        }
        /*else
        {
            base = convertImageToBase64(image: UIImage(named: "profile_db")!)
        }*/
        
        
        
        
        let dataStr: String = "data={\"user_id\": \"\(userIdVal)\", \"first_name\": \"\(firstnameVal)\",  \"password\":\"\(passwordVal)\", \"last_name\": \"\(lastnameVal)\", \"personal_email\": \"\(emailVal)\",\"personal_phone\": \"\(phoneVal)\",\"profile_pic\": \"\(base)\" }"
        
        
        //let paramOrKey = dataStr//"https://some.website.com/path/to/page.srf?a=1&b=2#top"
        //dataStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        print("POST Params: \(dataStr)")
        let postData = NSMutableData(data: dataStr.data(using: String.Encoding.utf8)!)
        
        let updateURL: String = Constants.salesRepUpdateProfileURL
        let request = NSMutableURLRequest(url: NSURL(string: updateURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                
                print(error)
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "profile_update_title".localized, message: "profile_update_error".localized, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "profile_update_ok".localized, style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
            } else {
                let httpResponse = response as? HTTPURLResponse
                print("Response: \(httpResponse)")
                do {
                    print("Data: \(data)")
                    
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        print("@@@Print converted dictionary",convertedJsonIntoDict)
                        
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        
                        if(statusVal == true)
                        {
                            
                            let setprefs = UserDefaults.standard
                            let usernameV = firstnameVal + " " + lastnameVal
                            setprefs.set(firstnameVal, forKey: "fName")
                            setprefs.set(lastnameVal, forKey: "lName")
                            setprefs.set(emailVal, forKey: "personal_email")
                            setprefs.set(phoneVal, forKey: "personal_phone")
                            setprefs.set(passwordVal, forKey: "upass")
                            setprefs.set(usernameV, forKey: "username")
                            setprefs.synchronize()
                            
                            
//                            let login = Login.sharedInstance
//                            
//                            login.setup(userName: usernameV, password: passwordVal, id: login.getManagerId(), courseId: login.courseId, isLoggedIn: login.isLoggedIn, fname: firstnameVal, lName: lastnameVal, language: "en", notification_count: login.getNotificationcount(), termAccepted: login.termAccepted)
//                            login.saveLoginToDevice()
                            
                            // let defaults = UserDefaults.standard // This is just to set this is device cache.
                            //print("login details saved in cache \(self.isLoggedIn)")
                            //defaults.set("Coding Explorer", forKey: "userNameKey")
                            //                            defaults.set(emailVal, forKey: "emailid")
                            //                            defaults.set(phoneVal, forKey: "phoneno")
                            //                            defaults.synchronize()
                            DispatchQueue.main.async {
                                progressHUD.hide()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                let alert = UIAlertController(title: "profile_update_title".localized, message: "profile_update_success".localized, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "profile_update_ok".localized, style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                
                            }
                        }
                        else
                        {
                            progressHUD.hide()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            DispatchQueue.main.async {
                                let alert = UIAlertController(title: "profile_update_title".localized, message: "profile_update_error".localized, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "profile_update_ok".localized, style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                
                            }
                        }
                    }
                    
                } catch let error as NSError
                {
                    print(error)
                    let alert = UIAlertController(title: "profile_update_title".localized, message: "profile_update_error".localized, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "profile_update_ok".localized, style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    progressHUD.hide()
                    UIApplication.shared.endIgnoringInteractionEvents()
                }
            }
        })
        
        dataTask.resume()
    }
    
    @IBAction func cancelClick(_ sender: AnyObject) {
        _ = navigationController?.popToRootViewController(animated: false)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        self.view.frame.origin.y = 0
        super.touchesBegan(touches, with: event)
    }

    func keyboardWillShow(sender: NSNotification) {
        self.view.frame.origin.y = -150
    }
    
    func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y = 0
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        ImagePicker.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        
        let device = UIDevice.current.model
        
        //let image2 = self.profileImg.resizeWith(width: Constants.profile_image_size)
        //self.profileImg.image = image2
        
        self.profileImg.layoutIfNeeded()
        self.profileImg.layer.borderWidth = 2
        self.profileImg.layer.masksToBounds = false
        self.profileImg.layer.borderColor = UIColor.white.cgColor
        if(device == "iPhone") {
            self.profileImg.layer.cornerRadius = 50
        } else {
            self.profileImg.layer.cornerRadius = 100
        }
        self.profileImg.clipsToBounds = true
        self.profileImg.image = UIImage(named: "profile_db.png")

        fnameLbl.text = "firstname_lbl".localized
        lnameLbl.text = "lastname_lbl".localized
        emailLbl.text = "email_lbl".localized
        passwordLbl.text = "password_lbl".localized
        phoneLbl.text = "phone_lbl".localized
        saveBtn.titleLabel?.text = "save_title".localized
        cancelBtn.titleLabel?.text = "cancel_title".localized
        
        let prefs:UserDefaults = UserDefaults.standard
        let firstName = prefs.value(forKey: "fName") as! String!
        let lastName = prefs.value(forKey: "lName") as! String!
        let email = prefs.value(forKey: "personal_email") as! String!
        let password = prefs.value(forKey: "upass") as! String!
        let phone = prefs.value(forKey: "personal_phone") as! String!

        fnameTxt.text = firstName
        lnameTxt.text = lastName
        passwordTxt.text = password
        emailTxt.text = email
        
        let trimmedString = phone?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        if trimmedString != ""
        {
            phoneTxt.text =  "+" + trimmedString!
        }
        else
        {
            phoneTxt.text = trimmedString!
        }
        
        
        
        let tapEditProf = UITapGestureRecognizer(target: self, action: #selector(ProfileController.selectProfPic))
        self.editView.addGestureRecognizer(tapEditProf)
        
        
        if (prefs.value(forKey: "profile_pic") != nil)
        {
            let profile_pic:String = prefs.value(forKey: "profile_pic") as! String!
            
            if profile_pic != ""
            {
                print("Here is the profile pic link : \(profile_pic)")
                
                let url = URL(string: profile_pic)// + "1617_1485337054.png")
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                self.profileImg.image = UIImage(data: data!)
            }
        }
        

    }
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        if(testStr == "") {
            return true
        } else {
            let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
            
            let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
            return emailTest.evaluate(with: testStr)
        }
    }
    
    func convertImageToBase64(image: UIImage) -> String {
        
        let imageData = UIImagePNGRepresentation(image)
        let base64String = imageData?.base64EncodedString(options: .lineLength64Characters)//  .base64EncodedStringWithOptions(.allZeros)
        
        print("Here we are doing the base 64 \(base64String)")
        return base64String!
        
    }
    
}
