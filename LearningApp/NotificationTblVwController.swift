//
//  NotificationTblVwController.swift
//  LearningApp
//
//  Created by Mac-04 on 22/08/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import UIKit

class NotificationTblVwController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView!.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.items![2].badgeValue = "0";
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source


    
    let NotificationTitle = ["New Module Added","New Module Added","New Module Added","New Module Added","New Module Added","New Module Added","New Module Added","New Module Added"]
    let NotificationDate = ["Today","AUG 10","AUG 9","AUG 8","AUG 7","AUG 7","AUG 6","AUG 6"]
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return NotificationTitle.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NoticficationCell", for: indexPath) as! NotificationTblVwCell
        
            cell.NotificationTitleLabel.text = NotificationTitle[(indexPath as NSIndexPath).row]
            cell.NotificationDescLabel.text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry"
        
            cell.NotificationDaylabel.text = NotificationDate[(indexPath as NSIndexPath).row]
        
//        
//        let currentDate = NSDate()
//        let dateFormatter = NSDateFormatter()
//        dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
//        
//        let timeString = dateFormatter.stringFromDate(currentDate)
        
        
//        let calendar = NSCalendar.currentCalendar()
//        let components = calendar.components([.Day , .Month , .Year], fromDate: currentDate)
//        
//        let year =  components.year
//        let month = components.month
//        let day = components.day
//        
//        print(year)
//        print(month)
//        print(day)
//        
//        print(timeString)
//        // Configure the cell...
        
        //for shadow
        
       /* cell.layer.shadowColor = UIColor.grayColor().CGColor;
        cell.layer.shadowOffset = CGSizeMake(0, 2.0);
        cell.layer.shadowRadius = 2.0;
        cell.layer.shadowOpacity = 1.0;
        cell.layer.masksToBounds = false;
        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).CGPath;  */
        
        
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let popOverVC = UIStoryboard (name: "Main", bundle: nil).instantiateViewController(withIdentifier: "feedbackPopupID") as! FeedbackPopUpVwCtrlr
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
        
      print((indexPath as NSIndexPath).row)
    }
    
//    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> (UIView!)
//    {
//        let headerView = UIView(frame: CGRectMake(0, 0, tableView.bounds.size.width, 30))
//       
//        headerView.backgroundColor = UIColor.blueColor()
//       
//        return headerView
//    }

    
    
    
//    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
//    
//    override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
