//
//  HomeCollonVwCell.swift
//  LearningApp
//
//  Created by Mac-04 on 22/08/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import UIKit

class HomeCollonVwCell: UICollectionViewCell {
    
    @IBOutlet weak var HomeCollnVwGridImg: UIImageView!
    
    @IBOutlet weak var HomeCollnVwGridLbl: UILabel!
}
