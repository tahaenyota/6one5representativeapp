//
//  Module4IntroController.swift
//  LearningApp
//
//  Created by Sumit More on 12/5/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import AVFoundation

class Module3IntroController: UIViewController, AVAudioPlayerDelegate {
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var multimediaIntro: UIButton!
    @IBOutlet weak var introductionImage: UIImageView!
    
    @IBOutlet weak var customersLbl: UILabel!
    @IBOutlet weak var askQLbl: UILabel!
    @IBOutlet weak var listenLbl: UILabel!
    @IBOutlet weak var confirmLbl: UILabel!
    
    @IBOutlet weak var intro_m3e2l1: UILabel!
    @IBOutlet weak var intro_m3e2l2: UILabel!
    @IBOutlet weak var intro_m3e2l3: UILabel!
    @IBOutlet weak var intro_m3e2l4: UILabel!
    @IBOutlet weak var intro_m3e2l5: UILabel!
    
    @IBOutlet weak var intro_m3e3b1: UIButton!
    @IBOutlet weak var intro_m3e3b2: UIButton!
    @IBOutlet weak var intro_m3e3b3: UIButton!
    @IBOutlet weak var intro_m3e3l1: UILabel!
    
    @IBOutlet weak var welcomeLbl: UILabel!
    @IBOutlet weak var understandLbl: UILabel!
    @IBOutlet weak var adviseLbl: UILabel!
    @IBOutlet weak var closeLbl: UILabel!
    @IBOutlet weak var thankLbl: UILabel!
    
    @IBAction func introFwdClick(_ sender: AnyObject) {
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        audioPlayerIntroAud?.pause()
    }
    
    @IBAction func bulb1Click(_ sender: AnyObject) {
        self.bulbNo = 1
        let popOverVC = UIStoryboard (name: "Module3SB", bundle: nil).instantiateViewController(withIdentifier: "m3_wQID") as! Module3QuestionsController
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        popOverVC.modalTransitionStyle = .crossDissolve
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    @IBAction func bulb2Click(_ sender: AnyObject) {
        self.bulbNo = 2
        let popOverVC = UIStoryboard (name: "Module3SB", bundle: nil).instantiateViewController(withIdentifier: "m3_wQID") as! Module3QuestionsController
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        popOverVC.modalTransitionStyle = .crossDissolve
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    @IBAction func bulb3Click(_ sender: AnyObject) {
        self.bulbNo = 3
        let popOverVC = UIStoryboard (name: "Module3SB", bundle: nil).instantiateViewController(withIdentifier: "m3_wQID") as! Module3QuestionsController
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        popOverVC.modalTransitionStyle = .crossDissolve
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    var playFlagIntroAud = 1
    var audioPlayerIntroAud: AVAudioPlayer?
    
    var GTTimer : Timer = Timer()
    var GTTimer2 : Timer = Timer()
    var isPaused = false
    var intervalVal: Int = 5
    var intervalVal2: Int = 1
    var bulbNo: Int = 0
    var questionNo: Int = 1
    
    @IBOutlet weak var introBtmLbl: UILabel!
    
    override func viewDidLoad() {
        
        self.introBtmLbl.text = "Rep_M3_IntroductionBtmLbl".localized
        
        //        let image = UIImage(named: "sof.png")
        //        self.navigationItem.titleView = UIImageView(image: image)
        
        let device = UIDevice.current.model
        print("Device type: " + device)
        
        if(device == "iPhone") {
            print("Changing size to 16")
            let attrs = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont.systemFont(ofSize: 16.0)
            ]
            self.navigationController!.navigationBar.titleTextAttributes = attrs
            
            let button: UIButton = UIButton(type: UIButtonType.custom)
            button.setImage(UIImage(named: "sof.png"), for: UIControlState.normal)
            button.backgroundColor = UIColor.white
            button.frame = CGRect(x: 0, y: 0, width: 56, height: 45)
            let barButton = UIBarButtonItem(customView: button)
            
            
            let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            negativeSpacer.width = -17;
            
            self.navigationItem.setRightBarButtonItems([negativeSpacer, barButton ], animated: false)
            
        } else {
            print("Changing size to 32")
            let attrs = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont.systemFont(ofSize: 32.0)
            ]
            self.navigationController!.navigationBar.titleTextAttributes = attrs
            
            let button: UIButton = UIButton(type: UIButtonType.custom)
            button.setImage(UIImage(named: "sof.png"), for: UIControlState.normal)
            button.backgroundColor = UIColor.white
            //set frame
            button.frame = CGRect(x: 0, y: 0, width: 56, height: 45)
            let barButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            
            
            let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            negativeSpacer.width = -21;
            
            self.navigationItem.setRightBarButtonItems([negativeSpacer, barButton ], animated: false)
        }
        
        self.navigationItem.title = "Rep_M3_ModuleName".localized
        
        self.customersLbl.isHidden = true
        self.askQLbl.isHidden = true
        self.listenLbl.isHidden = true
        self.confirmLbl.isHidden = true
        
        self.intro_m3e2l1.isHidden = true
        self.intro_m3e2l2.isHidden = true
        self.intro_m3e2l3.isHidden = true
        self.intro_m3e2l4.isHidden = true
        self.intro_m3e2l5.isHidden = true

        self.intro_m3e3b1.isHidden = true
        self.intro_m3e3b2.isHidden = true
        self.intro_m3e3b3.isHidden = true
        self.intro_m3e3l1.isHidden = true

        self.welcomeLbl.isHidden = false
        self.understandLbl.isHidden = false
        self.adviseLbl.isHidden = false
        self.closeLbl.isHidden = false
        self.thankLbl.isHidden = false
        
        self.introductionImage.image = UIImage(named: "rep_m3_intro1")
        
        self.intro_m3e3b1.setTitle("Rep_Intro_M3_B1".localized, for: .normal)
//        self.intro_m3e3b1.isHighlighted = true
//        self.intro_m3e3b1.showsTouchWhenHighlighted = false
//        self.intro_m3e3b1.adjustsImageWhenHighlighted = false
        self.intro_m3e3b2.setTitle("Rep_Intro_M3_B2".localized, for: .normal)
//        self.intro_m3e3b2.isHighlighted = true
//        self.intro_m3e3b2.showsTouchWhenHighlighted = false
//        self.intro_m3e3b2.adjustsImageWhenHighlighted = false
        self.intro_m3e3b3.setTitle("Rep_Intro_M3_B3".localized, for: .normal)
//        self.intro_m3e3b3.isHighlighted = true
//        self.intro_m3e3b3.showsTouchWhenHighlighted = false
//        self.intro_m3e3b2.adjustsImageWhenHighlighted = false
        
        if UserDefaults.standard.bool(forKey: "isPlusversion") == true {

            self.welcomeLbl.removeConstraints(self.welcomeLbl.constraints)
            let lead_welcome = NSLayoutConstraint(item: self.welcomeLbl,
                                                  attribute: NSLayoutAttribute.left,
                                                  relatedBy: NSLayoutRelation.equal,
                                                  toItem: self.view,
                                                  attribute: NSLayoutAttribute.left,
                                                  multiplier: 1,
                                                  constant: CGFloat(Constants.M3_welcomeLblConstraints_6splus[0]))
            let top_welcome = NSLayoutConstraint(item: self.welcomeLbl,
                                                 attribute: NSLayoutAttribute.top,
                                                 relatedBy: NSLayoutRelation.equal,
                                                 toItem: self.view,
                                                 attribute: NSLayoutAttribute.top,
                                                 multiplier: 1,
                                                 constant: CGFloat(Constants.M3_welcomeLblConstraints_6splus[1]))
            self.view.addConstraint(lead_welcome)
            self.view.addConstraint(top_welcome)
            
            self.understandLbl.removeConstraints(self.understandLbl.constraints)
            let lead_und = NSLayoutConstraint(item: self.understandLbl,
                                              attribute: NSLayoutAttribute.left,
                                              relatedBy: NSLayoutRelation.equal,
                                              toItem: self.view,
                                              attribute: NSLayoutAttribute.left,
                                              multiplier: 1,
                                              constant: CGFloat(Constants.M3_UnderstandLblConstraints_6splus[0]))
            let top_und = NSLayoutConstraint(item: self.understandLbl,
                                             attribute: NSLayoutAttribute.top,
                                             relatedBy: NSLayoutRelation.equal,
                                             toItem: self.view,
                                             attribute: NSLayoutAttribute.top,
                                             multiplier: 1,
                                             constant: CGFloat(Constants.M3_UnderstandLblConstraints_6splus[1]))
            self.view.addConstraint(lead_und)
            self.view.addConstraint(top_und)
            
            self.adviseLbl.removeConstraints(self.adviseLbl.constraints)
            let lead_advise = NSLayoutConstraint(item: self.adviseLbl,
                                                 attribute: NSLayoutAttribute.left,
                                                 relatedBy: NSLayoutRelation.equal,
                                                 toItem: self.view,
                                                 attribute: NSLayoutAttribute.left,
                                                 multiplier: 1,
                                                 constant: CGFloat(Constants.M3_AdviseLblConstraints_6splus[0]))
            
            let top_advise = NSLayoutConstraint(item: self.adviseLbl,
                                                attribute: NSLayoutAttribute.top,
                                                relatedBy: NSLayoutRelation.equal,
                                                toItem: self.view,
                                                attribute: NSLayoutAttribute.top,
                                                multiplier: 1,
                                                constant: CGFloat(Constants.M3_AdviseLblConstraints_6splus[1]))
            self.view.addConstraint(lead_advise)
            self.view.addConstraint(top_advise)
            
            self.closeLbl.removeConstraints(self.closeLbl.constraints)
            let lead_close = NSLayoutConstraint(item: self.closeLbl,
                                                attribute: NSLayoutAttribute.left,
                                                relatedBy: NSLayoutRelation.equal,
                                                toItem: self.view,
                                                attribute: NSLayoutAttribute.left,
                                                multiplier: 1,
                                                constant: CGFloat(Constants.M3_CloseLblConstraints_6splus[0]))
            let top_close = NSLayoutConstraint(item: self.closeLbl,
                                               attribute: NSLayoutAttribute.top,
                                               relatedBy: NSLayoutRelation.equal,
                                               toItem: self.view,
                                               attribute: NSLayoutAttribute.top,
                                               multiplier: 1,
                                               constant: CGFloat(Constants.M3_CloseLblConstraints_6splus[1]))
            self.view.addConstraint(lead_close)
            self.view.addConstraint(top_close)
            
            
            self.thankLbl.removeConstraints(self.thankLbl.constraints)
            let lead_thank = NSLayoutConstraint(item: self.thankLbl,
                                                attribute: NSLayoutAttribute.left,
                                                relatedBy: NSLayoutRelation.equal,
                                                toItem: self.view,
                                                attribute: NSLayoutAttribute.left,
                                                multiplier: 1,
                                                constant: CGFloat(Constants.M3_ThankLblConstraints_6splus[0]))
            
            let top_thank = NSLayoutConstraint(item: self.thankLbl,
                                               attribute: NSLayoutAttribute.top,
                                               relatedBy: NSLayoutRelation.equal,
                                               toItem: self.view,
                                               attribute: NSLayoutAttribute.top,
                                               multiplier: 1,
                                               constant: CGFloat(Constants.M3_ThankLblConstraints_6splus[1]))
            self.view.addConstraint(lead_thank)
            self.view.addConstraint(top_thank)
            
            
            self.customersLbl.removeConstraints(self.customersLbl.constraints)
            let top_customersLbl = NSLayoutConstraint(item: self.customersLbl,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: 400)
            self.view.addConstraint(top_customersLbl)
            
            self.intro_m3e3b1.removeConstraints(self.intro_m3e3b1.constraints)
            let top_intro_m3e3b1 = NSLayoutConstraint(item: self.intro_m3e3b1,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M3_intro_m3e3b1Constraints_6splus[1]))
            let width_intro_m3e3b1 = NSLayoutConstraint(item: self.intro_m3e3b1,
                                                   attribute: NSLayoutAttribute.width,
                                                   relatedBy: NSLayoutRelation.equal,
                                                   toItem: nil,
                                                   attribute: NSLayoutAttribute.notAnAttribute,
                                                   multiplier: 1,
                                                   constant: CGFloat(Constants.M3_intro_m3e3b1Constraints_6splus[2]))
            let height_intro_m3e3b1 = NSLayoutConstraint(item: self.intro_m3e3b1,
                                                    attribute: NSLayoutAttribute.height,
                                                    relatedBy: NSLayoutRelation.equal,
                                                    toItem: nil,
                                                    attribute: NSLayoutAttribute.notAnAttribute,
                                                    multiplier: 1,
                                                    constant: CGFloat(Constants.M3_intro_m3e3b1Constraints_6splus[3]))
            self.view.addConstraint(top_intro_m3e3b1)
            self.view.addConstraint(width_intro_m3e3b1)
            self.view.addConstraint(height_intro_m3e3b1)
//            self.intro_m3e3b1.backgroundColor = UIColor.red
            
            self.intro_m3e3b2.removeConstraints(self.intro_m3e3b2.constraints)
            let top_intro_m3e3b2 = NSLayoutConstraint(item: self.intro_m3e3b2,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M3_intro_m3e3b2Constraints_6splus[1]))
            let width_intro_m3e3b2 = NSLayoutConstraint(item: self.intro_m3e3b2,
                                                        attribute: NSLayoutAttribute.width,
                                                        relatedBy: NSLayoutRelation.equal,
                                                        toItem: nil,
                                                        attribute: NSLayoutAttribute.notAnAttribute,
                                                        multiplier: 1,
                                                        constant: CGFloat(Constants.M3_intro_m3e3b2Constraints_6splus[2]))
            let height_intro_m3e3b2 = NSLayoutConstraint(item: self.intro_m3e3b2,
                                                         attribute: NSLayoutAttribute.height,
                                                         relatedBy: NSLayoutRelation.equal,
                                                         toItem: nil,
                                                         attribute: NSLayoutAttribute.notAnAttribute,
                                                         multiplier: 1,
                                                         constant: CGFloat(Constants.M3_intro_m3e3b2Constraints_6splus[3]))
            self.view.addConstraint(top_intro_m3e3b2)
            self.view.addConstraint(width_intro_m3e3b2)
            self.view.addConstraint(height_intro_m3e3b2)
//            self.intro_m3e3b2.backgroundColor = UIColor.red

            self.intro_m3e3b3.removeConstraints(self.intro_m3e3b3.constraints)
            let lead_intro_m3e3b3 = NSLayoutConstraint(item: self.intro_m3e3b3,
                                                       attribute: NSLayoutAttribute.left,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.view,
                                                       attribute: NSLayoutAttribute.left,
                                                       multiplier: 1,
                                                       constant:CGFloat(Constants.M3_intro_m3e3b3Constraints_6splus[0]))
            let top_intro_m3e3b3 = NSLayoutConstraint(item: self.intro_m3e3b3,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M3_intro_m3e3b3Constraints_6splus[1]))
            let width_intro_m3e3b3 = NSLayoutConstraint(item: self.intro_m3e3b3,
                                                        attribute: NSLayoutAttribute.width,
                                                        relatedBy: NSLayoutRelation.equal,
                                                        toItem: nil,
                                                        attribute: NSLayoutAttribute.notAnAttribute,
                                                        multiplier: 1,
                                                        constant: CGFloat(Constants.M3_intro_m3e3b3Constraints_6splus[2]))
            let height_intro_m3e3b3 = NSLayoutConstraint(item: self.intro_m3e3b3,
                                                         attribute: NSLayoutAttribute.height,
                                                         relatedBy: NSLayoutRelation.equal,
                                                         toItem: nil,
                                                         attribute: NSLayoutAttribute.notAnAttribute,
                                                         multiplier: 1,
                                                         constant: CGFloat(Constants.M3_intro_m3e3b3Constraints_6splus[3]))
            self.view.addConstraint(lead_intro_m3e3b3)
            self.view.addConstraint(top_intro_m3e3b3)
            self.view.addConstraint(width_intro_m3e3b3)
            self.view.addConstraint(height_intro_m3e3b3)
//            self.intro_m3e3b3.backgroundColor = UIColor.red
            
            self.intro_m3e2l5.removeConstraints(self.intro_m3e2l5.constraints)
            let width_intro_m3e2l5 = NSLayoutConstraint(item: self.intro_m3e2l5,
                                                        attribute: NSLayoutAttribute.width,
                                                        relatedBy: NSLayoutRelation.equal,
                                                        toItem: nil,
                                                        attribute: NSLayoutAttribute.notAnAttribute,
                                                        multiplier: 1,
                                                        constant:  CGFloat(Constants.M3_intro_m3e2l5Constraints_6splus[0]))
            let top_intro_m3e2l5 = NSLayoutConstraint(item: self.intro_m3e2l5,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M3_intro_m3e2l5Constraints_6splus[1]))
            self.view.addConstraint(top_intro_m3e2l5)
            self.view.addConstraint(width_intro_m3e2l5)
            
            self.intro_m3e2l4.removeConstraints(self.intro_m3e2l4.constraints)
            let width_intro_m3e2l4 = NSLayoutConstraint(item: self.intro_m3e2l4,
                                                        attribute: NSLayoutAttribute.width,
                                                        relatedBy: NSLayoutRelation.equal,
                                                        toItem: nil,
                                                        attribute: NSLayoutAttribute.notAnAttribute,
                                                        multiplier: 1,
                                                        constant: CGFloat(Constants.M3_intro_m3e2l4Constraints_6splus[0]))
            
            let top_intro_m3e2l4 = NSLayoutConstraint(item: self.intro_m3e2l4,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M3_intro_m3e2l4Constraints_6splus[1]))
            
            let lead_intro_m3e2l4 = NSLayoutConstraint(item: self.intro_m3e2l4,
                                                       attribute: NSLayoutAttribute.leading,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.view,
                                                       attribute: NSLayoutAttribute.leading,
                                                       multiplier: 1,
                                                       constant: CGFloat(Constants.M3_intro_m3e2l4Constraints_6splus[2]))
            self.view.addConstraint(lead_intro_m3e2l4)
            self.view.addConstraint(width_intro_m3e2l4)
            self.view.addConstraint(top_intro_m3e2l4)
            
            self.intro_m3e2l3.removeConstraints(self.intro_m3e2l3.constraints)
            let width_intro_m3e2l3 = NSLayoutConstraint(item: self.intro_m3e2l3,
                                                        attribute: NSLayoutAttribute.width,
                                                        relatedBy: NSLayoutRelation.equal,
                                                        toItem: nil,
                                                        attribute: NSLayoutAttribute.notAnAttribute,
                                                        multiplier: 1,
                                                        constant: CGFloat(Constants.M3_intro_m3e2l3Constraints_6splus[0]))
            let top_intro_m3e2l3 = NSLayoutConstraint(item: self.intro_m3e2l3,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M3_intro_m3e2l3Constraints_6splus[1]))
            let lead_intro_m3e2l3 = NSLayoutConstraint(item: self.intro_m3e2l3,
                                                       attribute: NSLayoutAttribute.leading,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.view,
                                                       attribute: NSLayoutAttribute.leading,
                                                       multiplier: 1,
                                                       constant: CGFloat(Constants.M3_intro_m3e2l3Constraints_6splus[2]))
            self.view.addConstraint(width_intro_m3e2l3)
            self.view.addConstraint(top_intro_m3e2l3)
            self.view.addConstraint(lead_intro_m3e2l3)
            
            
            
            self.intro_m3e2l2.removeConstraints(self.intro_m3e2l2.constraints)
            let width_intro_m3e2l2 = NSLayoutConstraint(item: self.intro_m3e2l2,
                                                        attribute: NSLayoutAttribute.width,
                                                        relatedBy: NSLayoutRelation.equal,
                                                        toItem: nil,
                                                        attribute: NSLayoutAttribute.notAnAttribute,
                                                        multiplier: 1,
                                                        constant: CGFloat(Constants.M3_intro_m3e2l2Constraints_6splus[0]))
            let top_intro_m3e2l2 = NSLayoutConstraint(item: self.intro_m3e2l2,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M3_intro_m3e2l2Constraints_6splus[1]))
            let lead_intro_m3e2l2 = NSLayoutConstraint(item: self.intro_m3e2l2,
                                                       attribute: NSLayoutAttribute.leading,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.view,
                                                       attribute: NSLayoutAttribute.leading,
                                                       multiplier: 1,
                                                       constant: CGFloat(Constants.M3_intro_m3e2l2Constraints_6splus[2]))
            let parentViewConstraints: [NSLayoutConstraint] = self.view.constraints
            for parentConstraint in parentViewConstraints {
                if let viewLbl = parentConstraint.firstItem as? UILabel {
                    if viewLbl.text! == "Rep_M3_Intro_E32".localized {
                        self.view.removeConstraint(parentConstraint)
                    }
                }
            }
            self.view.addConstraint(lead_intro_m3e2l2)
            self.view.addConstraint(width_intro_m3e2l2)
            self.view.addConstraint(top_intro_m3e2l2)
            
            self.intro_m3e2l1.removeConstraints(self.intro_m3e2l1.constraints)
            let width_intro_m3e2l1 = NSLayoutConstraint(item: self.intro_m3e2l1,
                                                        attribute: NSLayoutAttribute.width,
                                                        relatedBy: NSLayoutRelation.equal,
                                                        toItem: nil,
                                                        attribute: NSLayoutAttribute.notAnAttribute,
                                                        multiplier: 1,
                                                        constant: CGFloat(Constants.M3_intro_m3e2l1Constraints_6splus[0]))
            
            let top_intro_m3e2l1 = NSLayoutConstraint(item: self.intro_m3e2l1,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M3_intro_m3e2l1Constraints_6splus[1]))
            let left_intro_m3e2l1 = NSLayoutConstraint(item: self.intro_m3e2l1,
                                                      attribute: NSLayoutAttribute.left,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.left,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M3_intro_m3e2l1Constraints_6splus[2]))
            self.view.addConstraint(width_intro_m3e2l1)
            self.view.addConstraint(top_intro_m3e2l1)
            self.view.addConstraint(left_intro_m3e2l1)
            
        }

        intervalVal = 8
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module3IntroController.screen2_1), userInfo: nil, repeats: false)
        
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module3IntroController.monitorTimer), userInfo: nil, repeats: true)
        
        if self.revealViewController() != nil {
            menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: UIControlEvents.touchUpInside)
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            
        }
        
        do{
            let urlVideo = Bundle.main.url(forResource: "REP_M03_S01", withExtension: "mp3")
            audioPlayerIntroAud = try AVAudioPlayer(contentsOf: urlVideo!)
            audioPlayerIntroAud!.delegate = self
            audioPlayerIntroAud!.prepareToPlay()
            audioPlayerIntroAud!.play()
        } catch {
            print("Error getting the audio file")
        }
        
    }
    
    func monitorTimer() {
        self.intervalVal = self.intervalVal - 1
        print("Interval in monitor: " + String(self.intervalVal))
    }
    
  /*  func screen1(){
        
        print("In screen1")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "rep_m3_intro3")
        
        self.customersLbl.isHidden = false
        self.askQLbl.isHidden = false
        self.listenLbl.isHidden = false
        self.confirmLbl.isHidden = false
        
        self.intro_m3e2l1.isHidden = true
        self.intro_m3e2l2.isHidden = true
        self.intro_m3e2l3.isHidden = true
        self.intro_m3e2l4.isHidden = true
        self.intro_m3e2l5.isHidden = true
        
        self.intro_m3e3b1.isHidden = true
        self.intro_m3e3b2.isHidden = true
        self.intro_m3e3b3.isHidden = true
        self.intro_m3e3l1.isHidden = true
        
        self.welcomeLbl.isHidden = true
        self.understandLbl.isHidden = true
        self.adviseLbl.isHidden = true
        self.closeLbl.isHidden = true
        self.thankLbl.isHidden = true
        
        self.intervalVal = 6
        self.bulbNo = 1
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module3IntroController.screen2_1), userInfo: nil, repeats: false)
        
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module3IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }*/
    
    func screen2_1(){
        
        print("In screen2.1")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "rep_m3_intro3")
        
        self.customersLbl.isHidden = true
        self.askQLbl.isHidden = true
        self.listenLbl.isHidden = true
        self.confirmLbl.isHidden = true
        
        self.intro_m3e2l1.isHidden = true
        self.intro_m3e2l2.isHidden = true
        self.intro_m3e2l3.isHidden = true
        self.intro_m3e2l4.isHidden = true
        self.intro_m3e2l5.isHidden = false
        
        self.intro_m3e3b1.isHidden = true
        self.intro_m3e3b2.isHidden = true
        self.intro_m3e3b3.isHidden = true
        self.intro_m3e3l1.isHidden = true
        
        self.welcomeLbl.isHidden = true
        self.understandLbl.isHidden = true
        self.adviseLbl.isHidden = true
        self.closeLbl.isHidden = true
        self.thankLbl.isHidden = true
        
        UIView.transition(with: self.intro_m3e2l5,
                          duration: 2.0,
                          options: [.transitionCrossDissolve],
                          animations: {
                            
                            self.intro_m3e2l5.text = "Rep_M3_Intro_E35".localized
                            
            }, completion: nil)
        
        self.intervalVal = 6
        self.bulbNo = 2
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module3IntroController.screen2_2), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module3IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen2_2(){
        
        print("In screen22")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "rep_m3_intro3")
        
        self.customersLbl.isHidden = true
        self.askQLbl.isHidden = true
        self.listenLbl.isHidden = true
        self.confirmLbl.isHidden = true
        
        self.intro_m3e2l1.isHidden = true
        self.intro_m3e2l2.isHidden = true
        self.intro_m3e2l3.isHidden = true
        self.intro_m3e2l4.isHidden = false
        self.intro_m3e2l5.isHidden = false
        
        self.intro_m3e3b1.isHidden = true
        self.intro_m3e3b2.isHidden = true
        self.intro_m3e3b3.isHidden = true
        self.intro_m3e3l1.isHidden = true
        
        self.welcomeLbl.isHidden = true
        self.understandLbl.isHidden = true
        self.adviseLbl.isHidden = true
        self.closeLbl.isHidden = true
        self.thankLbl.isHidden = true
        
        UIView.transition(with: self.intro_m3e2l4,
                          duration: 2.0,
                          options: [.transitionCrossDissolve],
                          animations: {
                            
                            self.intro_m3e2l4.text = "Rep_M3_Intro_E34".localized
                            
            }, completion: nil)
        
        self.intervalVal = 8
        self.bulbNo = 22
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module3IntroController.screen2_3), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module3IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen2_3(){
        
        print("In screen23")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "rep_m3_intro3")
        
        self.customersLbl.isHidden = true
        self.askQLbl.isHidden = true
        self.listenLbl.isHidden = true
        self.confirmLbl.isHidden = true
        
        self.intro_m3e2l1.isHidden = true
        self.intro_m3e2l2.isHidden = true
        self.intro_m3e2l3.isHidden = false
        self.intro_m3e2l4.isHidden = false
        self.intro_m3e2l5.isHidden = false
        
        self.intro_m3e3b1.isHidden = true
        self.intro_m3e3b2.isHidden = true
        self.intro_m3e3b3.isHidden = true
        self.intro_m3e3l1.isHidden = true
        
        self.welcomeLbl.isHidden = true
        self.understandLbl.isHidden = true
        self.adviseLbl.isHidden = true
        self.closeLbl.isHidden = true
        self.thankLbl.isHidden = true
        
        UIView.transition(with: self.intro_m3e2l3,
                          duration: 2.0,
                          options: [.transitionCrossDissolve],
                          animations: {
                            
                            self.intro_m3e2l3.text = "Rep_M3_Intro_E33".localized
                            
            }, completion: nil)
        
        self.intervalVal = 11
        self.bulbNo = 23
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module3IntroController.screen2_4), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module3IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }

    func screen2_4(){
        
        print("In screen24")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "rep_m3_intro3")
        
        self.customersLbl.isHidden = true
        self.askQLbl.isHidden = true
        self.listenLbl.isHidden = true
        self.confirmLbl.isHidden = true
        
        self.intro_m3e2l1.isHidden = true
        self.intro_m3e2l2.isHidden = false
        self.intro_m3e2l3.isHidden = false
        self.intro_m3e2l4.isHidden = false
        self.intro_m3e2l5.isHidden = false
        
        self.intro_m3e3b1.isHidden = true
        self.intro_m3e3b2.isHidden = true
        self.intro_m3e3b3.isHidden = true
        self.intro_m3e3l1.isHidden = true
        
        self.welcomeLbl.isHidden = true
        self.understandLbl.isHidden = true
        self.adviseLbl.isHidden = true
        self.closeLbl.isHidden = true
        self.thankLbl.isHidden = true
        
        UIView.transition(with: self.intro_m3e2l2,
                          duration: 2.0,
                          options: [.transitionCrossDissolve],
                          animations: {
                            
                            self.intro_m3e2l2.text = "Rep_M3_Intro_E32".localized
                            
            }, completion: nil)
        
        self.intervalVal = 8
        self.bulbNo = 24
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module3IntroController.screen2_5), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module3IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }

    func screen2_5(){
        
        print("In screen25")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "rep_m3_intro3")
        
        self.customersLbl.isHidden = true
        self.askQLbl.isHidden = true
        self.listenLbl.isHidden = true
        self.confirmLbl.isHidden = true
        
        self.intro_m3e2l1.isHidden = false
        self.intro_m3e2l2.isHidden = false
        self.intro_m3e2l3.isHidden = false
        self.intro_m3e2l4.isHidden = false
        self.intro_m3e2l5.isHidden = false
        
        self.intro_m3e3b1.isHidden = true
        self.intro_m3e3b2.isHidden = true
        self.intro_m3e3b3.isHidden = true
        self.intro_m3e3l1.isHidden = true
        
        self.welcomeLbl.isHidden = true
        self.understandLbl.isHidden = true
        self.adviseLbl.isHidden = true
        self.closeLbl.isHidden = true
        self.thankLbl.isHidden = true
        
        UIView.transition(with: self.intro_m3e2l1,
                          duration: 2.0,
                          options: [.transitionCrossDissolve],
                          animations: {
                            
                            self.intro_m3e2l1.text = "Rep_M3_Intro_E31".localized
                            
            }, completion: nil)
        
        self.intervalVal = 8
        self.bulbNo = 25
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module3IntroController.screen3), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module3IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }

    func screen3(){
        
        print("In screen3")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "rep_m3_intro4")
        
        self.customersLbl.isHidden = true
        self.askQLbl.isHidden = true
        self.listenLbl.isHidden = true
        self.confirmLbl.isHidden = true
        
        self.intro_m3e2l1.isHidden = true
        self.intro_m3e2l2.isHidden = true
        self.intro_m3e2l3.isHidden = true
        self.intro_m3e2l4.isHidden = true
        self.intro_m3e2l5.isHidden = true
        
        self.intro_m3e3b1.isHidden = false
        self.intro_m3e3b2.isHidden = false
        self.intro_m3e3b3.isHidden = false
        self.intro_m3e3l1.isHidden = false
        
        self.welcomeLbl.isHidden = true
        self.understandLbl.isHidden = true
        self.adviseLbl.isHidden = true
        self.closeLbl.isHidden = true
        self.thankLbl.isHidden = true
        
        self.intro_m3e3b1.isEnabled = false
        self.intro_m3e3b2.isEnabled = false
        self.intro_m3e3b3.isEnabled = false
        
        self.intervalVal = 11
        self.bulbNo = 3
        
        self.intro_m3e3l1.text = "Rep_M3_Intro_E3Lbl".localized
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module3IntroController.screen4), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module3IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen4(){
        
        //        self.GTTimer.invalidate()
        //        self.GTTimer2.invalidate()
        
        print("In screen4")
        
        self.intro_m3e3b1.isEnabled = true
        self.intro_m3e3b2.isEnabled = true
        self.intro_m3e3b3.isEnabled = true
        
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("In Finish Playing")
        playFlagIntroAud = 3
        if let image = UIImage(named: "ic_replay_white_36pt.png") {
            multimediaIntro.setImage(image, for: .normal)
        }
    }
    
    @IBAction func multimediaIntroClicked(_ sender: AnyObject) {
        //        let layer = introductionImage.layer
        
        if (playFlagIntroAud == 3) {
            
            self.introductionImage.image = UIImage(named: "rep_m3_intro1")
            
            self.customersLbl.isHidden = true
            self.askQLbl.isHidden = true
            self.listenLbl.isHidden = true
            self.confirmLbl.isHidden = true
            
            self.intro_m3e2l1.isHidden = true
            self.intro_m3e2l2.isHidden = true
            self.intro_m3e2l3.isHidden = true
            self.intro_m3e2l4.isHidden = true
            self.intro_m3e2l5.isHidden = true
            
            self.intro_m3e3b1.isHidden = true
            self.intro_m3e3b2.isHidden = true
            self.intro_m3e3b3.isHidden = true
            self.intro_m3e3l1.isHidden = true
            
            self.welcomeLbl.isHidden = false
            self.understandLbl.isHidden = false
            self.adviseLbl.isHidden = false
            self.closeLbl.isHidden = false
            self.thankLbl.isHidden = false
            
            self.GTTimer.invalidate()
            self.GTTimer2.invalidate()
            
            self.intervalVal = 5
            self.bulbNo = 0
            print("In screen 1 after replay")
            let timeInterval: TimeInterval = TimeInterval(intervalVal)
            self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module3IntroController.screen2_1), userInfo: nil, repeats: false)
            
            let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module3IntroController.monitorTimer), userInfo: nil, repeats: true)
            
            playFlagIntroAud = 1
            if let image = UIImage(named: "ic_pause_white_36pt.png") {
                multimediaIntro.setImage(image, for: .normal)
                audioPlayerIntroAud?.play()
                //                resumeLayer(layer: layer)
            }
        } else if (playFlagIntroAud == 2) {
            
            let timeInterval: TimeInterval = TimeInterval(self.intervalVal)
            print("Interval: " + String(self.intervalVal))
            
            switch self.bulbNo {
            case 0: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module3IntroController.screen2_1), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module3IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 1: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module3IntroController.screen2_1), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module3IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 2: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module3IntroController.screen2_2), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module3IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 22: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module3IntroController.screen2_3), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module3IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 23: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module3IntroController.screen2_4), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module3IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 24: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module3IntroController.screen2_5), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module3IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 25: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module3IntroController.screen3), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module3IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 3: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module3IntroController.screen4), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module3IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            default: break
            }
            
            playFlagIntroAud = 1
            if let image = UIImage(named: "ic_pause_white_36pt.png") {
                multimediaIntro.setImage(image, for: .normal)
                audioPlayerIntroAud?.play()
            }
        } else {
            
            self.GTTimer.invalidate()
            self.GTTimer2.invalidate()
            
            playFlagIntroAud = 2
            if let image = UIImage(named: "ic_play_arrow_white_36pt.png") {
                multimediaIntro.setImage(image, for: .normal)
                audioPlayerIntroAud?.pause()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        audioPlayerIntroAud?.stop()
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
    }
    
}
