//
//  LoginService.swift
//  LearningApp
//
//  Created by Sumit More on 1/3/17.
//  Copyright © 2017 Mac-04. All rights reserved.
//

import Foundation
import UIKit


public class LoginService : NSObject
{
    
    //using Singleton pattern for single object handling...
    class var sharedInstance: LoginService
    {
        //2
        struct Singleton {
            //3
            static let instance = LoginService()
        }
        //4
        return Singleton.instance
    }
    
    
    
    func validateLoginDetailsOnline(userName:String, passwd:String, completionHandler:@escaping (Login)->())
    {
        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            ]
        let login = Login.sharedInstance
        let dataStr: String = "data={ \"uname\" : \"\(userName as NSString)\" , \"upass\" : \"\(passwd as NSString)\" }"
        //print("POST Params: \(dataStr)")
        let postData = NSMutableData(data: dataStr.data(using: String.Encoding.utf8)!)
        
        let loginURL: String = Constants.loginURL
        let request = NSMutableURLRequest(url: NSURL(string: loginURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("Error: \(error)")
                DispatchQueue.main.async {
                    /*let alert = UIAlertController(title: "Login Error", message: "Error in connection. Please check your internet connection and try again.", preferredStyle: UIAlertControllerStyle.alert)
                     alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                     self.present(alert, animated: true, completion: nil)*/
                    
                    login.setErrorMessage(msg: "Server Error Please check the server")
                    login.setIsLogin(islogin: "0")
                    completionHandler(login)
                }
            } else {
                let httpResponse = response as? HTTPURLResponse
                //var errorFlag = 0
                _ = "Invalid Credentials"
                print("Response: \(httpResponse)")
                do {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        print("@@@Print converted dictionary",convertedJsonIntoDict)
                        
                        
                        // Get value by key
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        let user_id  = convertedJsonIntoDict["user_id"] as? String
                        let course_id  = convertedJsonIntoDict["course_id"] as? String
                        let fName  = convertedJsonIntoDict["first_name"] as? String
                        let lName  = convertedJsonIntoDict["last_name"] as? String
                        let email  = convertedJsonIntoDict["email"] as? String
                        let profilePic  = convertedJsonIntoDict["profile_pic_link"] as? String
                        let phone  = "1234567890" // convertedJsonIntoDict["phone"] as? String
                        print("Profile Pic \(profilePic)")
                        //let UserRole = convertedJsonIntoDict["user_role"] as? String
                        // if(statusVal == true && UserRole == "LM")
                        if(statusVal == true)
                        {
                            let usernameVal = fName! + " " + lName!
                            login.setup(userName: usernameVal, password: passwd, id: user_id!, courseId: course_id!, isLoggedIn: "1", fname:fName!, lName:lName!, email:email!, phone:phone)
                            login.setErrorMessage(msg: "")
                            login.saveLoginToDevice()
                            login.fName = fName
                            login.lName = lName
                            login.email = email
                            login.phone = phone
                            
                            let setprefs = UserDefaults.standard
                            setprefs.set(user_id, forKey: "user_id")
                            setprefs.set(course_id, forKey: "course_id")
                            setprefs.set(passwd, forKey: "upass")
                            setprefs.set(fName, forKey: "fName")
                            setprefs.set(lName, forKey: "lName")
                            setprefs.set(email, forKey: "email")
                            setprefs.set(phone, forKey: "fName")
                            setprefs.set(profilePic, forKey:"profile_pic")
                            setprefs.synchronize()

                            completionHandler(login)
                            
                        }
                        else
                        {
                            var error_msg:NSString
                            if convertedJsonIntoDict["message"] as? NSString != nil {
                                error_msg = convertedJsonIntoDict["message"] as! NSString
                            } else {
                                error_msg = "Unknown Error"
                            }
                            //errorFlag = 1
                            print("error_msg",error_msg)
                            login.setErrorMessage(msg: error_msg as String)
                            login.setIsLogin(islogin: "0")
                            completionHandler(login)
                        }
                    }
                } catch let error as NSError
                {
                    print(error)
                }
                
            }
        })
        
        dataTask.resume()
        
        
        
        
        
    }//end of validation
}
