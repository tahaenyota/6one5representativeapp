//
//  APIFunctions.swift
//  LearningApp
//
//  Created by Sumit More on 1/5/17.
//  Copyright © 2017 Mac-04. All rights reserved.
//

import Foundation

class APIFunctions {
    
    static func getNotifCount() -> Int {
        
        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            ]
        
        let notifAPICount: Int = 0
        
        let prefs:UserDefaults = UserDefaults.standard
        let user_id = prefs.value(forKey: "user_id") as! String!
        
        let notifURL =  Constants.salesRepNotifInfoURL + user_id!
        let request = NSMutableURLRequest(url: NSURL(string: notifURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("Notifs not updated. Error: \(error)")
            } else {
                let httpResponse = response as? HTTPURLResponse
                print("HTTP Response: \(httpResponse)")
                do {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        print("Print converted dictionary",convertedJsonIntoDict)
                        
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        
                        print("Status is \(statusVal)")
                        if statusVal == true {
                            print("True! Status is \(statusVal)")
                            if let notifs = convertedJsonIntoDict["data"] as? NSArray {
                                let notifAPICount = notifs.count
                                print("Notif Count: \(notifAPICount)")
                            } else {
                                print("No notif data parsed!")
                            }
                        } else {
                            print("Not true! Status is \(statusVal)")
                        }
                    }
                } catch let error as NSError {
                    print(error)
                    print("Notifs not updated. Error: \(error)")
                }
                
                
            }
        })
        
        dataTask.resume()
        
        return notifAPICount
    }

    static func getNotifs() -> [Notification6one5] {
        
        var apiNotif: [Notification6one5] = []
        
        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            ]
        
        let prefs:UserDefaults = UserDefaults.standard
        let user_id = prefs.value(forKey: "user_id") as! String!
        
        let notifURL =  Constants.salesRepNotifInfoURL + user_id!
        let request = NSMutableURLRequest(url: NSURL(string: notifURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("Notifs not updated. Error: \(error)")
            } else {
                let httpResponse = response as? HTTPURLResponse
                print("HTTP Response: \(httpResponse)")
                do {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        print("Print converted dictionary",convertedJsonIntoDict)
                        
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        
                        print("Status is \(statusVal)")
                        if statusVal == true {
                            print("True! Status is \(statusVal)")
                            if let notifs = convertedJsonIntoDict["data"] as? NSArray {
                                let notifAPICount = notifs.count
                                print("Notif Count: \(notifAPICount)")
                                for i in 0..<notifAPICount {
                                    if let notifData = notifs[i] as? NSDictionary {
                                        let notif: Notification6one5 = Notification6one5()
                                        notif.notifId = notifData["id"] as! String
                                        notif.notifName = notifData["name"] as! String
                                        notif.notifContent = notifData["content"] as! String
                                        notif.notifNotificationId = notifData["notification_id"] as! String
                                        notif.notifUserId = notifData["user_id"] as! String
                                        notif.notifBadgeId = notifData["badge_id"] as! String
                                        notif.notifCourseId = notifData["course_id"] as! String
                                        notif.notifModuleId = notifData["module_id"] as! String
                                        notif.notifRepresentativeId = notifData["representative_id"] as! String
                                        notif.notifNotificationType = notifData["notification_type"] as! String
                                        notif.notifAssignedAt = notifData["assigned_at"] as! String
                                        
                                        apiNotif.append(notif)
                                    }
                                }
                            } else {
                                print("No notif data parsed!")
                            }
                        } else {
                            print("Not true! Status is \(statusVal)")
                        }
                    }
                } catch let error as NSError {
                    print(error)
                    print("Notifs not updated. Error: \(error)")
                }
                
                
            }
        })
        
        dataTask.resume()

        return apiNotif
        
    }
}
