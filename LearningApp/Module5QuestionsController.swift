//
//  Module5QuestionsController.swift
//  LearningApp
//
//  Created by Sumit More on 12/8/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import Foundation
import UIKit

class Module5QuestionsController: UIViewController {
    
    @IBOutlet weak var questionsExplanationLbl: UILabel!
    @IBOutlet weak var questionLbl: UILabel!
    @IBOutlet weak var bottomBar: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        let introductionScreenView = self.navigationController!.viewControllers[0] as! Module5IntroController
        switch introductionScreenView.bulbNo {
        case 1:
            self.questionLbl.text = "Rep_Intro_M5_B1".localized
            self.questionsExplanationLbl.text = "Rep_Intro_M5_B1_Explanation".localized
            self.bottomBar.backgroundColor = UIColor.init(red: 236.0/255.0, green: 56.0/255.0, blue: 103.0/255.0, alpha: 1.0)
            break
        case 2:
            self.questionLbl.text = "Rep_Intro_M5_B2".localized
            self.questionsExplanationLbl.text = "Rep_Intro_M5_B2_Explanation".localized
            self.bottomBar.backgroundColor = UIColor.init(red: 255.0/255.0, green: 130.0/255.0, blue: 20.0/255.0, alpha: 1.0)
            break
        case 3:
            self.questionLbl.text = "Rep_Intro_M5_B3".localized
            self.questionsExplanationLbl.text = "Rep_Intro_M5_B3_Explanation".localized
            self.bottomBar.backgroundColor = UIColor.init(red: 0.0/255.0, green: 199.0/255.0, blue: 178.0/255.0, alpha: 1.0)
            break
        case 4:
            self.questionLbl.text = "Rep_Intro_M5_B4".localized
            self.questionsExplanationLbl.text = "Rep_Intro_M5_B4_Explanation".localized
            self.bottomBar.backgroundColor = UIColor.init(red: 0.0/255.0, green: 155.0/255.0, blue: 213.0/255.0, alpha: 1.0)
            break
        default:
            break
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func closeClick(_ sender: AnyObject) {
        self.view.removeFromSuperview()
    }
}
