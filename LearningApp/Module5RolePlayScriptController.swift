//
//  Module5RolePlayScriptController.swift
//  LearningApp
//
//  Created by Sumit More on 12/7/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import AVFoundation

class Module5RolePlayScriptController: UIViewController, AVAudioPlayerDelegate, UICollectionViewDataSource ,UICollectionViewDelegate {
    
    @IBOutlet weak var multimediaScript: UIButton!
    @IBOutlet weak var headingLbl: UILabel!
    
    var scriptChatArray = ["Rep_Script_M5_C11".localized]
    var scriptChatArray2 = ["Rep_Script_M5_C12".localized]
    
    var cellHeight: [Int] = []
    var playFlagScript = 1
    var audioPlayerScript: AVAudioPlayer?
    
    @IBOutlet weak var menuButton: UIButton!
    
    @IBOutlet weak var scriptCollectionview: UICollectionView!
    
    @IBOutlet weak var scriptBtmLbl: UILabel!
    
    var htFlag = false
    
    var GTTimer : Timer = Timer()
    var intervalVal: Int = 1
    
    override func viewDidLoad() {
        
        let device = UIDevice.current.model
        if(device == "iPhone") {
            print("Changing size to 16")
            let attrs = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont.systemFont(ofSize: 16.0)
            ]
            self.navigationController!.navigationBar.titleTextAttributes = attrs
            
            let button: UIButton = UIButton(type: UIButtonType.custom)
            button.setImage(UIImage(named: "sof.png"), for: UIControlState.normal)
            button.backgroundColor = UIColor.white
            //set frame
            button.frame = CGRect(x: 0, y: 0, width: 56, height: 45)
            let barButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            
            
            let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            negativeSpacer.width = -17;
            
            self.navigationItem.setRightBarButtonItems([negativeSpacer, barButton ], animated: false)
            
        } else {
            print("Changing size to 32")
            let attrs = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont.systemFont(ofSize: 32.0)
            ]
            self.navigationController!.navigationBar.titleTextAttributes = attrs
            
            let button: UIButton = UIButton(type: UIButtonType.custom)
            button.setImage(UIImage(named: "sof.png"), for: UIControlState.normal)
            button.backgroundColor = UIColor.white
            //set frame
            button.frame = CGRect(x: 0, y: 0, width: 56, height: 45)
            let barButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            
            
            let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            negativeSpacer.width = -21;
            
            self.navigationItem.setRightBarButtonItems([negativeSpacer, barButton ], animated: false)
        }
        
        self.navigationItem.title = "Rep_M5_ModuleName".localized
        
        self.scriptBtmLbl.text = "Rep_M5_RPScriptBtmLbl".localized
        
        self.headingLbl.text = "Rep_M5_Script".localized
        
        self.scriptCollectionview.delegate = self
        self.scriptCollectionview.dataSource = self
        
        scriptCollectionview!.contentInset = UIEdgeInsets(top: 12, left: 10, bottom: 10, right: 10)
        
        menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: UIControlEvents.touchUpInside)
        
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        do{
            let urlVideo = Bundle.main.url(forResource: "REP_M05_S04", withExtension: "mp3")
            audioPlayerScript = try AVAudioPlayer(contentsOf: urlVideo!)
            audioPlayerScript!.delegate = self
            audioPlayerScript!.prepareToPlay()
            audioPlayerScript!.play()
        } catch {
            print("Error getting the audio file")
        }
        
        intervalVal = 5
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module4ScriptController.reldCV), userInfo: nil, repeats: false)
        
    }
    
    func reldCV() {
        self.scriptCollectionview.reloadData()
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("In Finish Playing")
        playFlagScript = 2
        if let image = UIImage(named: "ic_replay_white_36pt.png") {
            multimediaScript.setImage(image, for: .normal)
        }
    }
    
    @IBAction func multimediaScriptClicked(_ sender: AnyObject) {
        if (playFlagScript == 2) {
            playFlagScript = 1
            if let image = UIImage(named: "ic_pause_white_36pt.png") {
                multimediaScript.setImage(image, for: .normal)
                audioPlayerScript?.play()
            }
        } else {
            playFlagScript = 2
            if let image = UIImage(named: "ic_play_arrow_white_36pt.png") {
                multimediaScript.setImage(image, for: .normal)
                audioPlayerScript?.pause()
            }
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("Here Chat Count: \(scriptChatArray.count)")
        if(htFlag == false) {
            for ind in 0...scriptChatArray.count {
                _ = ind
                self.cellHeight.append(301)
            }
        }
        return scriptChatArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ScriptCollectionViewCell", for: indexPath) as! ScriptCollectionViewCell
        
        print("Here Chat Index: \(indexPath.row)")
        cell.User1Lbl.text = scriptChatArray[indexPath.row]
        cell.User2Lbl.text = scriptChatArray2[indexPath.row]
        let cellHt = Int(cell.User1Lbl.frame.height) + Int(cell.User2Lbl.frame.height) + 21
        print("Cell Height: \(cellHt)")
        self.cellHeight.append(Int(cell.User1Lbl.frame.height) + Int(cell.User2Lbl.frame.height) + 21)
        htFlag = true
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(2.0 - 1))
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(1.0))
        
        let cellIndPathInd = indexPath.row
        
        print("Cell Height @ indexPath: \(self.cellHeight[cellIndPathInd])")
        return CGSize(width: size - 10, height: self.cellHeight[cellIndPathInd])
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        audioPlayerScript?.stop()
    }
}
