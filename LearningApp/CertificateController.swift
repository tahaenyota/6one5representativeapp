//
//  CertificateController.swift
//  LearningApp
//
//  Created by Sumit More on 1/2/17.
//  Copyright © 2017 Mac-04. All rights reserved.
//

import Foundation
import UIKit

class CertificateController: UIViewController {
    
    @IBOutlet weak var certificateMainView: UIView!
    
    let certificates = Certificate.sharedInstance
    
    @IBOutlet weak var downloadBtn: UIButton!
    @IBOutlet weak var shareBtn: UIButton!
    
    @IBOutlet weak var certImageView: UIImageView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.downloadBtn.isEnabled = false
        self.shareBtn.isEnabled = false
        
        certificateMainView.layer.shadowColor = UIColor.gray.cgColor
        certificateMainView.layer.shadowOpacity = 1
        certificateMainView.layer.shadowOffset = CGSize.zero
        certificateMainView.layer.shadowRadius = 3
        
        loadCertificateData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    @IBAction func shareBtnTapped(_ sender: AnyObject)
    {
        //TODO: Share button funcanality
        
        let cert = certificates.certificates[0]
        
        let imageToShare = [ cert.image ]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func downloadBtnTapped(_ sender: AnyObject)
    {
        //TODO: Download button funcanality
        var certImage = certificates.certificates[0].image
        certImage = self.certImageView.image
        UIImageWriteToSavedPhotosAlbum(certImage!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    func loadCertificateData()
    {
        if (self.certificates.certificates.count >= 0 )
        {
            certificates.freeCertificateData()
        }
        let progressHUD = ProgressHUD()
        self.view.addSubview(progressHUD)
       // progressHUD.activityIndictor.color = UIColor.black
        
        UIApplication.shared.beginIgnoringInteractionEvents()

        let headers = [
            "cache-control": "no-cache",
            ]
        
        let prefs:UserDefaults = UserDefaults.standard
        var user_id = "4"
        user_id = prefs.value(forKey: "user_id") as! String!
        
        let apiURL = Constants.salesRepCertificateURL + user_id
        
        let request = NSMutableURLRequest(url: NSURL(string: apiURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("Error: \(error)")
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "certificate_title".localized, message: "certificate_save_error".localized, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "certificate_ok".localized, style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
            } else {
                print("HTTP Response: \(data)")
                do {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        print("2222 Print converted dictionary",convertedJsonIntoDict)
                        
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        if statusVal == nil {
                            print("status val is null")
                            let statusVal = convertedJsonIntoDict["status"] as? String
                            print("status valule is \(statusVal)")
                            let login = Login.sharedInstance
                            login.status = String(describing: convertedJsonIntoDict["status"] as? Bool)
                            login.message = convertedJsonIntoDict["message"] as? String
                            progressHUD.hide()
                            UIApplication.shared.endIgnoringInteractionEvents()

                        } else {
                            if(statusVal)! {
                                let certificates = Certificate.sharedInstance
                                let certsArrJ: NSArray = convertedJsonIntoDict["data"] as! NSArray

                                var certDetails:CertificateDetails
                                for i in 0..<1
                                {
                                    let certObj: JSON = certsArrJ[i] as! JSON
                                    certDetails = CertificateDetails()
                                    let certId = certObj["id"] as! String
                                    let certPath = certObj["path"] as! String
                                    certDetails.id = certId
                                    certDetails.path = certPath
                                    if i == 1 // Avoid mobile data
                                    {
                                        let url = URL(string: certDetails.path)
                                        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                                        certDetails.image = UIImage(data: data!)
                                    }
                                    certificates.addCertificateData(certificateDetails: certDetails)
                                    let imagePath : String = self.certificates.certificates[0].path!
                                    let url = URL(string : imagePath)
                                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                                    if(data != nil) {
                                        DispatchQueue.main.sync {
                                            progressHUD.hide()
                                            UIApplication.shared.endIgnoringInteractionEvents()
                                            self.certImageView.image = UIImage(data: data!)
                                            self.downloadBtn.isEnabled = true
                                            self.shareBtn.isEnabled = true
                                        }
                                        
                                    } else {
                                        DispatchQueue.main.async {
                                            let alert = UIAlertController(title: "certificate_title".localized, message: "certificate_no_certificates".localized, preferredStyle: UIAlertControllerStyle.alert)
                                            alert.addAction(UIAlertAction(title: "certificate_ok".localized, style: UIAlertActionStyle.default, handler: nil))
                                            self.present(alert, animated: true, completion: nil)
                                        }
                                    }
                                }
                                
                            } else {
                                print("In else!")
                                DispatchQueue.main.async {
                                    let alert = UIAlertController(title: "certificate_title".localized, message: "certificate_no_certificates".localized, preferredStyle: UIAlertControllerStyle.alert)
                                    alert.addAction(UIAlertAction(title: "certificate_ok".localized, style: UIAlertActionStyle.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                    progressHUD.hide()
                                    UIApplication.shared.endIgnoringInteractionEvents()
                                }
                            }
                        }
                    }
                } catch let error as NSError
                {
                    print(error)
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "certificate_title".localized, message: "certificate_save_error".localized, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "certificate_ok".localized, style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                    progressHUD.hide()
                }
            }
        })
        dataTask.resume()
    }
    
    func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        print("Certificate image function called")
        if let error = error {
            print("In error")
            // we got back an error!
            let ac = UIAlertController(title: "certificate_save_error".localized, message: "certificate_gal_save_error".localized, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "certificate_ok".localized, style: .default))
            present(ac, animated: true)
        } else {
            print("In success")
            let ac = UIAlertController(title: "certificate_save_success".localized, message: "certificate_image_saved".localized, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "certificate_ok".localized, style: .default))
            present(ac, animated: true)
        }
    }
    
    
}
