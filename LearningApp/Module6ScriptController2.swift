//
//  Module6ScriptController2.swift
//  LearningApp
//
//  Created by Sumit More on 12/18/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import AVFoundation

class Module6ScriptController2: UIViewController, AVAudioPlayerDelegate, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var multimediaScript: UIButton!
    @IBOutlet weak var headingLbl: UILabel!
    
    var scriptChatArray = ["Rep_Script_M6_C11".localized, "Rep_Script_M6_C21".localized, "Rep_Script_M6_C31".localized, "Rep_Script_M6_C41".localized, "Rep_Script_M6_C51".localized, "Rep_Script_M6_C61".localized, "Rep_Script_M6_C71".localized, "Rep_Script_M6_C81".localized, "Rep_Script_M6_C91".localized]
    var scriptChatArray2 = ["Rep_Script_M6_C12".localized, "Rep_Script_M6_C22".localized, "Rep_Script_M6_C32".localized, "Rep_Script_M6_C42".localized, "Rep_Script_M6_C52".localized, "Rep_Script_M6_C62".localized, "Rep_Script_M6_C72".localized, "Rep_Script_M6_C82".localized, "Rep_Script_M6_C92".localized]
    
    var playFlagScript = 1
    var audioPlayerScript: AVAudioPlayer?
    
    @IBOutlet weak var menuButton: UIButton!
    
    @IBOutlet weak var scriptBtmLbl: UILabel!
    
    var htFlag = false
    
    var GTTimer : Timer = Timer()
    var intervalVal: Int = 1
    
    @available(iOS 2.0, *)
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "ScriptTVCell", for: indexPath as IndexPath) as! ScriptTableviewCell
        let row = indexPath.row
        cell.managerLbl.text = scriptChatArray[row]
        cell.customerLbl.text = scriptChatArray2[row]
        return cell
        
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.setNeedsLayout()
        self.tableView.layoutIfNeeded()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 351
        
        
        let device = UIDevice.current.model
        if(device == "iPhone") {
            print("Changing size to 16")
            let attrs = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont.systemFont(ofSize: 16.0)
            ]
            self.navigationController!.navigationBar.titleTextAttributes = attrs
            
            let button: UIButton = UIButton(type: UIButtonType.custom)
            button.setImage(UIImage(named: "sof.png"), for: UIControlState.normal)
            button.backgroundColor = UIColor.white
            //set frame
            button.frame = CGRect(x: 0, y: 0, width: 56, height: 45)
            let barButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            
            
            let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            negativeSpacer.width = -17;
            
            self.navigationItem.setRightBarButtonItems([negativeSpacer, barButton ], animated: false)
            
        } else {
            print("Changing size to 32")
            let attrs = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont.systemFont(ofSize: 32.0)
            ]
            self.navigationController!.navigationBar.titleTextAttributes = attrs
            
            let button: UIButton = UIButton(type: UIButtonType.custom)
            button.setImage(UIImage(named: "sof.png"), for: UIControlState.normal)
            button.backgroundColor = UIColor.white
            //set frame
            button.frame = CGRect(x: 0, y: 0, width: 56, height: 45)
            let barButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            
            
            let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            negativeSpacer.width = -21;
            
            self.navigationItem.setRightBarButtonItems([negativeSpacer, barButton ], animated: false)
        }
        
        self.navigationItem.title = "Rep_M6_ModuleName".localized
        
        self.scriptBtmLbl.text = "Rep_M6_RPScriptBtmLbl".localized
        
        self.headingLbl.text = "Rep_M6_Script".localized
        
        menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: UIControlEvents.touchUpInside)
        
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        do{
            let urlVideo = Bundle.main.url(forResource: "REP_M06_S04", withExtension: "mp3")
            audioPlayerScript = try AVAudioPlayer(contentsOf: urlVideo!)
            audioPlayerScript!.delegate = self
            audioPlayerScript!.prepareToPlay()
            audioPlayerScript!.play()
        } catch {
            print("Error getting the audio file")
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return scriptChatArray.count
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("In Finish Playing")
        playFlagScript = 2
        if let image = UIImage(named: "ic_replay_white_36pt.png") {
            multimediaScript.setImage(image, for: .normal)
        }
    }
    
    @IBAction func multimediaScriptClicked(_ sender: AnyObject) {
        if (playFlagScript == 2) {
            playFlagScript = 1
            if let image = UIImage(named: "ic_pause_white_36pt.png") {
                multimediaScript.setImage(image, for: .normal)
                audioPlayerScript?.play()
            }
        } else {
            playFlagScript = 2
            if let image = UIImage(named: "ic_play_arrow_white_36pt.png") {
                multimediaScript.setImage(image, for: .normal)
                audioPlayerScript?.pause()
            }
        }
    }
    
}
