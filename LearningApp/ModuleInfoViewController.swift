//
//  ModuleInfoViewController.swift
//  LearningApp
//
//  Created by enyotalearning on 03/02/17.
//  Copyright © 2017 Mac-04. All rights reserved.
//

import UIKit

class ModuleInfoViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var table: UITableView!
    var items: [String] = ["Assessment Explanation Summary","Welcome","Understand","Advise","Close","Thank","Handling Customer Complaints"]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    private func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        var cell:UITableViewCell = self.table.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell
        
        cell.textLabel?.text = self.items[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        //let indexPath = tableView.indexPathForSelectedRow //optional, to get from any UIButton for example
        
        if indexPath.row == 0
        {
            print("Global Help is selected")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController:ReviewInfoViewController = storyboard.instantiateViewController(withIdentifier: "ReviewInfoViewController") as! ReviewInfoViewController
            //let viewController:ReviewModuleInfoViewController = storyboard.instantiateViewController(withIdentifier: "ReviewModuleInfoViewController") as! ReviewModuleInfoViewController
            self.navigationController?.present(viewController, animated: true, completion: nil)
            
        }
        else
        {
            print("Help is selected")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            // let viewController:ReviewInfoViewController = storyboard.instantiateViewController(withIdentifier: "ReviewInfoViewController") as! ReviewInfoViewController
            
            let viewController:ReviewModuleInfoViewController = storyboard.instantiateViewController(withIdentifier: "ReviewModuleInfoViewController") as! ReviewModuleInfoViewController
            viewController.moduleid = indexPath.row
            self.navigationController?.present(viewController, animated: true, completion: nil)
        }
        
    }
}
