//
//  Module1ScriptController3.swift
//  LearningApp
//
//  Created by Sumit More on 12/18/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import Foundation
//
//  Module1ScriptController2.swift
//  LearningApp
//
//  Created by Sumit More on 12/15/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import Foundation


class Module1ScriptController3: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @available(iOS 2.0, *)
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = self.tableView.dequeueReusableCell(withIdentifier: "ScriptTVCell2", for: indexPath as IndexPath) as! ScriptTableviewCell2
        let row = indexPath.row
        cell.managerLbl.text = swiftBlogs[row]
        
        return cell
        
    }
    
    let swiftBlogs = ["NSHipsterNSHipsterNSHipsterNSHipsterNSHipsterNSHipsterNSHipsterNSHipsterNSHipsterNSHipsterNSHipsterNSHipsterNSHipsterNSHipsterNSHipsterNSHipsterNSHipsterNSHipsterNSHipsterNSHipsterNSHipsterNSHipsterNSHipsterNSHipsterNSHipsterNSHipsterNSHipsterRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay WenderlichRay Wenderlich", "NSHipster", "iOS Developer Tips", "Jameson Quave", "Natasha The Robot", "Coding Explorer", "That Thing In Swift", "Andrew Bancroft", "iAchieved.it", "Airspeed Velocity"]
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
//        self.tableView.setNeedsLayout()
//        self.tableView.layoutIfNeeded()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 351
        
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        self.tableView.reloadData()
//    }
    
    private func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return swiftBlogs.count
    }
    
}
