//
//  Module5IntroController.swift
//  LearningApp
//
//  Created by Sumit More on 12/7/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import AVFoundation

class Module5IntroController: UIViewController, AVAudioPlayerDelegate {
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var multimediaIntro: UIButton!
    @IBOutlet weak var introductionImage: UIImageView!
    
    @IBOutlet weak var intro_m5e2l1: UILabel!
    @IBOutlet weak var intro_m5e2l2: UILabel!
    @IBOutlet weak var intro_m5e2l3: UILabel!
    
    @IBOutlet weak var intro_m5e3b1: UIButton!
    @IBOutlet weak var intro_m5e3b2: UIButton!
    @IBOutlet weak var intro_m5e3b3: UIButton!
    @IBOutlet weak var intro_m5e3b4: UIButton!
    @IBOutlet weak var intro_m5e3l1: UILabel!
    
    @IBOutlet weak var welcomeLbl: UILabel!
    @IBOutlet weak var understandLbl: UILabel!
    @IBOutlet weak var adviseLbl: UILabel!
    @IBOutlet weak var closeLbl: UILabel!
    @IBOutlet weak var thankLbl: UILabel!
    
    @IBAction func introFwdClick(_ sender: AnyObject) {
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        audioPlayerIntroAud?.pause()
    }
    
    @IBAction func bulb1Click(_ sender: AnyObject) {
        self.bulbNo = 1
        let popOverVC = UIStoryboard (name: "Module5SB", bundle: nil).instantiateViewController(withIdentifier: "m5_wQID") as! Module5QuestionsController
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        popOverVC.modalTransitionStyle = .crossDissolve
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    @IBAction func bulb2Click(_ sender: AnyObject) {
        self.bulbNo = 2
        let popOverVC = UIStoryboard (name: "Module5SB", bundle: nil).instantiateViewController(withIdentifier: "m5_wQID") as! Module5QuestionsController
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        popOverVC.modalTransitionStyle = .crossDissolve
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    @IBAction func bulb3Click(_ sender: AnyObject) {
        self.bulbNo = 3
        let popOverVC = UIStoryboard (name: "Module5SB", bundle: nil).instantiateViewController(withIdentifier: "m5_wQID") as! Module5QuestionsController
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        popOverVC.modalTransitionStyle = .crossDissolve
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    @IBAction func bulb4Click(_ sender: AnyObject) {
        self.bulbNo = 4
        let popOverVC = UIStoryboard (name: "Module5SB", bundle: nil).instantiateViewController(withIdentifier: "m5_wQID") as! Module5QuestionsController
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        popOverVC.modalTransitionStyle = .crossDissolve
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    var playFlagIntroAud = 1
    var audioPlayerIntroAud: AVAudioPlayer?
    
    var GTTimer : Timer = Timer()
    var GTTimer2 : Timer = Timer()
    var isPaused = false
    var intervalVal: Int = 5
    var intervalVal2: Int = 1
    var bulbNo: Int = 0
    var questionNo: Int = 1
    
    @IBOutlet weak var introBtmLbl: UILabel!
    
    override func viewDidLoad() {
        
        self.introBtmLbl.text = "Rep_M5_IntroductionBtmLbl".localized
        
        //        let image = UIImage(named: "sof.png")
        //        self.navigationItem.titleView = UIImageView(image: image)
        
        let device = UIDevice.current.model
        print("Device type: " + device)
        
        if(device == "iPhone") {
            print("Changing size to 16")
            let attrs = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont.systemFont(ofSize: 16.0)
            ]
            self.navigationController!.navigationBar.titleTextAttributes = attrs
            
            let button: UIButton = UIButton(type: UIButtonType.custom)
            button.setImage(UIImage(named: "sof.png"), for: UIControlState.normal)
            button.backgroundColor = UIColor.white
            button.frame = CGRect(x: 0, y: 0, width: 56, height: 45)
            let barButton = UIBarButtonItem(customView: button)
            
            
            let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            negativeSpacer.width = -17;
            
            self.navigationItem.setRightBarButtonItems([negativeSpacer, barButton ], animated: false)
            
        } else {
            print("Changing size to 32")
            let attrs = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont.systemFont(ofSize: 32.0)
            ]
            self.navigationController!.navigationBar.titleTextAttributes = attrs
            
            let button: UIButton = UIButton(type: UIButtonType.custom)
            button.setImage(UIImage(named: "sof.png"), for: UIControlState.normal)
            button.backgroundColor = UIColor.white
            //set frame
            button.frame = CGRect(x: 0, y: 0, width: 56, height: 45)
            let barButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            
            
            let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            negativeSpacer.width = -21;
            
            self.navigationItem.setRightBarButtonItems([negativeSpacer, barButton ], animated: false)
        }
        
        self.navigationItem.title = "Rep_M5_ModuleName".localized
        
        self.intro_m5e2l1.isHidden = true
        self.intro_m5e2l2.isHidden = true
        self.intro_m5e2l3.isHidden = true
        
        self.intro_m5e3b1.isHidden = true
        self.intro_m5e3b2.isHidden = true
        self.intro_m5e3b3.isHidden = true
        self.intro_m5e3b4.isHidden = true
        self.intro_m5e3l1.isHidden = true
        
        self.welcomeLbl.isHidden = false
        self.understandLbl.isHidden = false
        self.adviseLbl.isHidden = false
        self.closeLbl.isHidden = false
        self.thankLbl.isHidden = false
        
        self.introductionImage.image = UIImage(named: "rep_m5_intro1")

        self.welcomeLbl.text = "Rep_Intro_M5_E1_L1".localized
        self.understandLbl.text = "Rep_Intro_M5_E1_L2".localized
        self.adviseLbl.text = "Rep_Intro_M5_E1_L3".localized
        self.closeLbl.text = "Rep_Intro_M5_E1_L4".localized
        self.thankLbl.text = "Rep_Intro_M5_E1_L5".localized

        self.intro_m5e2l1.text = "".localized
        
//        self.intro_m5e3b1.setTitle("Rep_Intro_M5_B1".localized, for: .normal)
//        self.intro_m5e3b2.setTitle("Rep_Intro_M5_B2".localized, for: .normal)
//        self.intro_m5e3b3.setTitle("Rep_Intro_M5_B3".localized, for: .normal)
//        self.intro_m5e3b4.setTitle("Rep_Intro_M5_B4".localized, for: .normal)
        
        if UserDefaults.standard.bool(forKey: "isPlusversion") == true {
            
            self.welcomeLbl.removeConstraints(self.welcomeLbl.constraints)
            let lead_welcome = NSLayoutConstraint(item: self.welcomeLbl,
                                                  attribute: NSLayoutAttribute.left,
                                                  relatedBy: NSLayoutRelation.equal,
                                                  toItem: self.view,
                                                  attribute: NSLayoutAttribute.left,
                                                  multiplier: 1,
                                                  constant: CGFloat(Constants.M5_welcomeLblConstraints_6splus[0]))
            let top_welcome = NSLayoutConstraint(item: self.welcomeLbl,
                                                 attribute: NSLayoutAttribute.top,
                                                 relatedBy: NSLayoutRelation.equal,
                                                 toItem: self.view,
                                                 attribute: NSLayoutAttribute.top,
                                                 multiplier: 1,
                                                 constant: CGFloat(Constants.M5_welcomeLblConstraints_6splus[1]))
            self.view.addConstraint(lead_welcome)
            self.view.addConstraint(top_welcome)
            
            self.understandLbl.removeConstraints(self.understandLbl.constraints)
            let lead_und = NSLayoutConstraint(item: self.understandLbl,
                                              attribute: NSLayoutAttribute.left,
                                              relatedBy: NSLayoutRelation.equal,
                                              toItem: self.view,
                                              attribute: NSLayoutAttribute.left,
                                              multiplier: 1,
                                              constant: CGFloat(Constants.M5_UnderstandLblConstraints_6splus[0]))
            let top_und = NSLayoutConstraint(item: self.understandLbl,
                                             attribute: NSLayoutAttribute.top,
                                             relatedBy: NSLayoutRelation.equal,
                                             toItem: self.view,
                                             attribute: NSLayoutAttribute.top,
                                             multiplier: 1,
                                             constant: CGFloat(Constants.M5_UnderstandLblConstraints_6splus[1]))
            self.view.addConstraint(lead_und)
            self.view.addConstraint(top_und)
            
            self.adviseLbl.removeConstraints(self.adviseLbl.constraints)
            let lead_advise = NSLayoutConstraint(item: self.adviseLbl,
                                                 attribute: NSLayoutAttribute.left,
                                                 relatedBy: NSLayoutRelation.equal,
                                                 toItem: self.view,
                                                 attribute: NSLayoutAttribute.left,
                                                 multiplier: 1,
                                                 constant: CGFloat(Constants.M5_AdviseLblConstraints_6splus[0]))
            
            let top_advise = NSLayoutConstraint(item: self.adviseLbl,
                                                attribute: NSLayoutAttribute.top,
                                                relatedBy: NSLayoutRelation.equal,
                                                toItem: self.view,
                                                attribute: NSLayoutAttribute.top,
                                                multiplier: 1,
                                                constant: CGFloat(Constants.M5_AdviseLblConstraints_6splus[1]))
            self.view.addConstraint(lead_advise)
            self.view.addConstraint(top_advise)
            
            self.closeLbl.removeConstraints(self.closeLbl.constraints)
            let lead_close = NSLayoutConstraint(item: self.closeLbl,
                                                attribute: NSLayoutAttribute.left,
                                                relatedBy: NSLayoutRelation.equal,
                                                toItem: self.view,
                                                attribute: NSLayoutAttribute.left,
                                                multiplier: 1,
                                                constant: CGFloat(Constants.M5_CloseLblConstraints_6splus[0]))
            let top_close = NSLayoutConstraint(item: self.closeLbl,
                                               attribute: NSLayoutAttribute.top,
                                               relatedBy: NSLayoutRelation.equal,
                                               toItem: self.view,
                                               attribute: NSLayoutAttribute.top,
                                               multiplier: 1,
                                               constant: CGFloat(Constants.M5_CloseLblConstraints_6splus[1]))
            self.view.addConstraint(lead_close)
            self.view.addConstraint(top_close)
            
            
            self.thankLbl.removeConstraints(self.thankLbl.constraints)
            let lead_thank = NSLayoutConstraint(item: self.thankLbl,
                                                attribute: NSLayoutAttribute.left,
                                                relatedBy: NSLayoutRelation.equal,
                                                toItem: self.view,
                                                attribute: NSLayoutAttribute.left,
                                                multiplier: 1,
                                                constant: CGFloat(Constants.M5_ThankLblConstraints_6splus[0]))
            let top_thank = NSLayoutConstraint(item: self.thankLbl,
                                               attribute: NSLayoutAttribute.top,
                                               relatedBy: NSLayoutRelation.equal,
                                               toItem: self.view,
                                               attribute: NSLayoutAttribute.top,
                                               multiplier: 1,
                                               constant: CGFloat(Constants.M5_ThankLblConstraints_6splus[1]))
            self.view.addConstraint(lead_thank)
            self.view.addConstraint(top_thank)
           
            self.intro_m5e2l1.removeConstraints(self.intro_m5e2l1.constraints)
            let width_intro_m5e2l1 = NSLayoutConstraint(item: self.intro_m5e2l1,
                                                        attribute: NSLayoutAttribute.width,
                                                        relatedBy: NSLayoutRelation.equal,
                                                        toItem: nil,
                                                        attribute: NSLayoutAttribute.notAnAttribute,
                                                        multiplier: 1,
                                                        constant: CGFloat(Constants.M5_intro_m5e2l1Constraints_6splus[0]))
            let top_intro_m5e2l1 = NSLayoutConstraint(item: self.intro_m5e2l1,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M5_intro_m5e2l1Constraints_6splus[1]))
            self.view.addConstraint(width_intro_m5e2l1)
            self.view.addConstraint(top_intro_m5e2l1)

            self.intro_m5e2l2.removeConstraints(self.intro_m5e2l2.constraints)
            let width_intro_m5e2l2 = NSLayoutConstraint(item: self.intro_m5e2l2,
                                                        attribute: NSLayoutAttribute.width,
                                                        relatedBy: NSLayoutRelation.equal,
                                                        toItem: nil,
                                                        attribute: NSLayoutAttribute.notAnAttribute,
                                                        multiplier: 1,
                                                        constant: CGFloat(Constants.M5_intro_m5e2l2Constraints_6splus[0]))
            let top_intro_m5e2l2 = NSLayoutConstraint(item: self.intro_m5e2l2,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M5_intro_m5e2l2Constraints_6splus[1]))
            self.view.addConstraint(width_intro_m5e2l2)
            self.view.addConstraint(top_intro_m5e2l2)
            
            self.intro_m5e2l3.removeConstraints(self.intro_m5e2l3.constraints)
            let width_intro_m5e2l3 = NSLayoutConstraint(item: self.intro_m5e2l3,
                                                        attribute: NSLayoutAttribute.width,
                                                        relatedBy: NSLayoutRelation.equal,
                                                        toItem: nil,
                                                        attribute: NSLayoutAttribute.notAnAttribute,
                                                        multiplier: 1,
                                                        constant: CGFloat(Constants.M5_intro_m5e2l3Constraints_6splus[0]))
            let top_intro_m5e2l3 = NSLayoutConstraint(item: self.intro_m5e2l3,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M5_intro_m5e2l3Constraints_6splus[1]))
            self.view.addConstraint(width_intro_m5e2l3)
            self.view.addConstraint(top_intro_m5e2l3)
        }

        intervalVal = 6
        self.bulbNo = 1

        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module5IntroController.screen2_1), userInfo: nil, repeats: false)
        
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module5IntroController.monitorTimer), userInfo: nil, repeats: true)
        
        if self.revealViewController() != nil {
            menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: UIControlEvents.touchUpInside)
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            
        }
        
        do{
            let urlVideo = Bundle.main.url(forResource: "REP_M05_S01", withExtension: "mp3")
            audioPlayerIntroAud = try AVAudioPlayer(contentsOf: urlVideo!)
            audioPlayerIntroAud!.delegate = self
            audioPlayerIntroAud!.prepareToPlay()
            audioPlayerIntroAud!.play()
        } catch {
            print("Error getting the audio file")
        }
        
    }
    
    func monitorTimer() {
        self.intervalVal = self.intervalVal - 1
        print("Interval in monitor: " + String(self.intervalVal))
    }
    
    func screen2_1(){
        
        print("In screen2.1")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "rep_m5_intro21")
        
        self.intro_m5e2l1.isHidden = false
        self.intro_m5e2l2.isHidden = true
        self.intro_m5e2l3.isHidden = true
        
        self.intro_m5e3b1.isHidden = true
        self.intro_m5e3b2.isHidden = true
        self.intro_m5e3b3.isHidden = true
        self.intro_m5e3b4.isHidden = true
        self.intro_m5e3l1.isHidden = true
        
        self.welcomeLbl.isHidden = true
        self.understandLbl.isHidden = true
        self.adviseLbl.isHidden = true
        self.closeLbl.isHidden = true
        self.thankLbl.isHidden = true
        
        self.intervalVal = 5
        self.bulbNo = 21
        
        UIView.transition(with: self.intro_m5e2l1,
                          duration: 1.0,
                          options: [.transitionCrossDissolve],
                          animations: {
                            
                            self.intro_m5e2l1.text = "Rep_Intro_M5_E2_L1".localized
                            
            }, completion: nil)

        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module5IntroController.screen2_2), userInfo: nil, repeats: false)
        
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module5IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen2_2(){
        
        print("In screen2.2")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "rep_m5_intro22")
        
        self.intro_m5e2l1.isHidden = false
        self.intro_m5e2l2.isHidden = false
        self.intro_m5e2l3.isHidden = true
        
        self.intro_m5e3b1.isHidden = true
        self.intro_m5e3b2.isHidden = true
        self.intro_m5e3b3.isHidden = true
        self.intro_m5e3b4.isHidden = true
        self.intro_m5e3l1.isHidden = true
        
        self.welcomeLbl.isHidden = true
        self.understandLbl.isHidden = true
        self.adviseLbl.isHidden = true
        self.closeLbl.isHidden = true
        self.thankLbl.isHidden = true
        
        UIView.transition(with: self.intro_m5e2l2,
                          duration: 1.0,
                          options: [.transitionCrossDissolve],
                          animations: {
                            
                            self.intro_m5e2l2.text = "Rep_Intro_M5_E2_L2".localized
                            
            }, completion: nil)
        
        self.intervalVal = 6
        self.bulbNo = 22
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module3IntroController.screen2_3), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module3IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen2_3(){
        
        print("In screen2.3")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "rep_m5_intro23")
        
        self.intro_m5e2l1.isHidden = false
        self.intro_m5e2l2.isHidden = false
        self.intro_m5e2l3.isHidden = false
        
        self.intro_m5e3b1.isHidden = true
        self.intro_m5e3b2.isHidden = true
        self.intro_m5e3b3.isHidden = true
        self.intro_m5e3b4.isHidden = true
        self.intro_m5e3l1.isHidden = true
        
        self.welcomeLbl.isHidden = true
        self.understandLbl.isHidden = true
        self.adviseLbl.isHidden = true
        self.closeLbl.isHidden = true
        self.thankLbl.isHidden = true
        
        UIView.transition(with: self.intro_m5e2l3,
                          duration: 2.0,
                          options: [.transitionCrossDissolve],
                          animations: {
                            
                            self.intro_m5e2l3.text = "Rep_Intro_M5_E2_L3".localized
                            
            }, completion: nil)
        
        self.intervalVal = 6
        self.bulbNo = 23
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module3IntroController.screen3), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module3IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen3(){
        
        print("In screen3")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "rep_m5_intro3")
        
        self.intro_m5e2l1.isHidden = true
        self.intro_m5e2l2.isHidden = true
        self.intro_m5e2l3.isHidden = true
        
        self.intro_m5e3b1.isHidden = false
        self.intro_m5e3b2.isHidden = false
        self.intro_m5e3b3.isHidden = false
        self.intro_m5e3b4.isHidden = false
        self.intro_m5e3l1.isHidden = false
        
        self.welcomeLbl.isHidden = true
        self.understandLbl.isHidden = true
        self.adviseLbl.isHidden = true
        self.closeLbl.isHidden = true
        self.thankLbl.isHidden = true
        
        self.intro_m5e3b1.isEnabled = false
        self.intro_m5e3b2.isEnabled = false
        self.intro_m5e3b3.isEnabled = false
        self.intro_m5e3b4.isEnabled = false
        
        self.intervalVal = 7
        self.bulbNo = 3
        
        self.intro_m5e3l1.text = "Rep_Intro_M5_E3_L1".localized
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module3IntroController.screen4), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module3IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen4(){
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        print("In screen4")
        
        self.intro_m5e3b1.isEnabled = true
        self.intro_m5e3b2.isEnabled = true
        self.intro_m5e3b3.isEnabled = true
        self.intro_m5e3b4.isEnabled = true
        
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("In Finish Playing")
        playFlagIntroAud = 3
        if let image = UIImage(named: "ic_replay_white_36pt.png") {
            multimediaIntro.setImage(image, for: .normal)
        }
    }
    
    @IBAction func multimediaIntroClicked(_ sender: AnyObject) {
        //        let layer = introductionImage.layer
        
        if (playFlagIntroAud == 3) {
            
            self.intro_m5e2l1.isHidden = true
            self.intro_m5e2l2.isHidden = true
            self.intro_m5e2l3.isHidden = true
            
            self.intro_m5e3b1.isHidden = true
            self.intro_m5e3b2.isHidden = true
            self.intro_m5e3b3.isHidden = true
            self.intro_m5e3b4.isHidden = true
            self.intro_m5e3l1.isHidden = true
            
            self.welcomeLbl.isHidden = false
            self.understandLbl.isHidden = false
            self.adviseLbl.isHidden = false
            self.closeLbl.isHidden = false
            self.thankLbl.isHidden = false
            
            self.introductionImage.image = UIImage(named: "rep_m5_intro1")
            
            self.GTTimer.invalidate()
            self.GTTimer2.invalidate()
            
            self.intervalVal = 6
            self.bulbNo = 1
            print("In screen 1 after replay")
            let timeInterval: TimeInterval = TimeInterval(intervalVal)
            self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module3IntroController.screen2_1), userInfo: nil, repeats: false)
            
            let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module3IntroController.monitorTimer), userInfo: nil, repeats: true)
            
            playFlagIntroAud = 1
            if let image = UIImage(named: "ic_pause_white_36pt.png") {
                multimediaIntro.setImage(image, for: .normal)
                audioPlayerIntroAud?.play()
                //                resumeLayer(layer: layer)
            }
        } else if (playFlagIntroAud == 2) {
            
            let timeInterval: TimeInterval = TimeInterval(self.intervalVal)
            print("Interval: " + String(self.intervalVal))
            
            switch self.bulbNo {
            case 1: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module5IntroController.screen2_1), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module5IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 21: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module5IntroController.screen2_2), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module5IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 22: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module5IntroController.screen2_3), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module5IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 23: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module5IntroController.screen3), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module5IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 3: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module3IntroController.screen4), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module3IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            default: break
            }
            
            playFlagIntroAud = 1
            if let image = UIImage(named: "ic_pause_white_36pt.png") {
                multimediaIntro.setImage(image, for: .normal)
                audioPlayerIntroAud?.play()
            }
        } else {
            
            self.GTTimer.invalidate()
            self.GTTimer2.invalidate()
            
            playFlagIntroAud = 2
            if let image = UIImage(named: "ic_play_arrow_white_36pt.png") {
                multimediaIntro.setImage(image, for: .normal)
                audioPlayerIntroAud?.pause()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        audioPlayerIntroAud?.stop()
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
    }
    
}
