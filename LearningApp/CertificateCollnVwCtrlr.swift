//
//  CertificateCollnVwCtrlr.swift
//  LearningApp
//
//  Created by Mac-04 on 10/09/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import UIKit

class CertificateCollnVwCtrlr: UICollectionViewController {

    
    let certificateDetail  = ["Obtaining the Retail Business Credential demonstrates understanding of basic retail operations.","Obtaining the Retail Business Credential demonstrates understanding of basic retail operations.","Obtaining the Retail Business Credential demonstrates understanding of basic retail operations."]
    
    var certificateImageArray: [UIImage] = [ UIImage(named: "certificate1.png")!, UIImage(named: "certificate2.png")!, UIImage(named: "certificate3.png")!]
    var certificateURLs: [String] = []
    
    var user_id: String = "61"
    var course_id: String = "2"
    var certificateCount: Int = 3
    
    var imgDwnld = 0
    
    //var boxView = UIView()
    let progressHUD = ProgressHUD()

    @IBOutlet var certificateCollectionview: UICollectionView!
    
    override func viewDidLoad() {
        collectionView!.contentInset = UIEdgeInsets(top: 12, left: 10, bottom: 10, right: 10)
        
        let grayColorCerti = UIColor(red: 232/255.0, green: 232/255.0, blue: 232/255.0, alpha: 1.0)
        self.view.backgroundColor = grayColorCerti
        
        self.navigationController!.navigationBar.topItem!.title = "Certificate";
        
        certificateCollectionview.layer.shadowColor = UIColor.gray.cgColor;
        certificateCollectionview.layer.shadowOffset = CGSize(width: 0, height: 2.0);
        certificateCollectionview.layer.shadowRadius = 1.0;
        certificateCollectionview.layer.shadowOpacity = 1.0;
        certificateCollectionview.layer.masksToBounds = false;
        
        getCertificates()
        
    }
    
 
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 8;
    }
    
  
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return self.certificateURLs.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CertificateCollVwCell", for: indexPath) as! CerificateCollnVwCell
       
        if(imgDwnld == 1) {
//            if let checkedUrl = URL(string: self.certificateURLs[(indexPath as NSIndexPath).row]) {
//                getDataFromUrl(url: checkedUrl) { (data, response, error)  in
//                    DispatchQueue.main.sync() { () -> Void in
//                        guard let data = data, error == nil else { return }
//                        print(response?.suggestedFilename ?? checkedUrl.lastPathComponent)
//                        print("Download Finished")
//                        cell.CertificateCellImageView.image = UIImage(data: data)
//                    }
//                }
//            }
            cell.CertificateCellImageView.image = certificateImageArray[(indexPath as NSIndexPath).row]
        } else {
            cell.CertificateCellImageView.image = certificateImageArray[(indexPath as NSIndexPath).row]
        }

//        cell.CetiCellDescripnLbel.text = certificateDetail[(indexPath as NSIndexPath).row]
        
        cell.layer.shadowColor = UIColor.gray.cgColor;
        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0);
        cell.layer.shadowRadius = 1.0;
        cell.layer.shadowOpacity = 1.0;
        cell.layer.masksToBounds = false;
        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath;
        
        
        return cell
    }
    
    //for no. of columns
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(3.0 - 1))
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(1.0))
        let cvht = Int(certificateImageArray[0].size.height) + 50
        return CGSize(width: size, height: cvht)
    }
    
    func getCertificates() {
        
        print("In get certificates")
        
        /*self.boxView = UIView(frame: CGRect(x: view.frame.midX - 125, y: view.frame.midY - 25, width: 251, height: 51))
        self.boxView.backgroundColor = UIColor.white
        self.boxView.alpha = 1
        self.boxView.layer.cornerRadius = 10
        
        //Here the spinnier is initialized
        let activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        activityView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityView.startAnimating()
        
        let textLabel = UILabel(frame: CGRect(x: 40, y: 0, width: 200, height: 50))
        textLabel.textColor = UIColor.gray
        textLabel.text = "Updating Certificate List"
        
        self.boxView.addSubview(activityView)
        self.boxView.addSubview(textLabel)
        
        view.addSubview(self.boxView)*/
        
        self.view.addSubview(progressHUD)
        UIApplication.shared.beginIgnoringInteractionEvents()

        let prefs:UserDefaults = UserDefaults.standard
        user_id = prefs.value(forKey: "user_id") as! String!
        course_id = prefs.value(forKey: "course_id") as! String!

        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
        ]
        
        let getURL = Constants.salesRepCertificateURL + user_id + "/" + course_id
        
        let request = NSMutableURLRequest(url: NSURL(string: getURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)

        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in

            if (error != nil) {
                //self.boxView.removeFromSuperview()
                //self.boxView.alpha = 0
                self.progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
                print("Error: \(error)")
                DispatchQueue.main.async {
                    let errStr: String = "Server not responding. Please try again later"
                    if (error != nil) {
                        let alert = UIAlertController(title: "Updating certificate list", message: errStr, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        let alert = UIAlertController(title: "Updating certificate list", message: error as? String, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            } else {
                let httpResponse = response as? HTTPURLResponse
                print("HTTP Response: \(httpResponse)")

                self.progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
                do {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        print("11111 Print converted dictionary",convertedJsonIntoDict)
                        
                        // Get value by key
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        print("Status value: \(statusVal)")
                        
                        if(statusVal! == true)
                        {
                            OperationQueue.main.addOperation{
                                
                                self.certificateURLs.removeAll()
                                
                                let certsArrJ: NSArray = convertedJsonIntoDict["data"] as! NSArray
                                print("Certificate Array Count: \(certsArrJ.count)")
                                print("Certificate Array: \(certsArrJ)")
                                
                                let cert0Obj: JSON = certsArrJ[0] as! JSON
                                let certPath: String = cert0Obj["path"] as! String

                                self.certificateURLs.append(certPath)
                                print("Certificate Path: \(certPath)")
                                self.imgDwnld = 1
                                self.collectionView?.reloadData()
                                
                            }
                        } else {
                            print("Here!!!")
                            var error_msg:NSString
                            if convertedJsonIntoDict["message"] as? NSString != nil {
                                error_msg = convertedJsonIntoDict["message"] as! NSString
                            } else {
                                error_msg = "Unknown Error"
                            }
                            print("error_msg",error_msg)
                            DispatchQueue.main.async {
                                let alert = UIAlertController(title: "Updating certificate list", message: error_msg as String, preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                        }
                    }
                } catch let error as NSError {
                    print(error)
                }
            
            }
        })
        
        dataTask.resume()
        
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) {
            (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
    
    func downloadImage(url: URL) {
        print("Download Started")
        getDataFromUrl(url: url) { (data, response, error)  in
            DispatchQueue.main.sync() { () -> Void in
                guard let data = data, error == nil else { return }
                _ = data
                print(response?.suggestedFilename ?? url.lastPathComponent)
                print("Download Finished")
            }
        }
    }
    
    
}
