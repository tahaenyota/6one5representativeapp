//
//  ReviewInfoViewController.swift
//  6one5Manager
//
//  Created by enyotalearning on 21/12/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit

class ReviewInfoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableview: UITableView!
    
    var bulbNo = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        //tableview.rowHeight = UITableViewAutomaticDimension
        //tableview.estimatedRowHeight = 140
    }
    @IBAction func closeButton(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        if indexPath.row == 0{
            return 220
        }else if indexPath.row == 1{
            return 515
        }else{
            return 44
        }
    }
    private func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //let cell1 = tableview.dequeueReusableCell(withIdentifier: "ReviewInfoTableViewCell_1", for: indexPath)
        
        if indexPath.row == 0 {
            
           let cell:ReviewInfoTableViewCell_1 = self.tableview.dequeueReusableCell(withIdentifier: "ReviewInfoTableViewCell_1", for: indexPath) as! ReviewInfoTableViewCell_1
            //cell.textFiled.text = "ReviewInfoRow1".localized
            
            let theString1 = "ReviewInfoRow1".localized
            //print("####### Desc is " + pDataProductDesc)
            let theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                               documentAttributes: nil)
            
            cell.textFiled.attributedText = theAttributedString1
            
            cell.selectionStyle = .none
            return cell
        }
       else {
            
            let cell:ReviewInfoTableViewCell_3 = self.tableview.dequeueReusableCell(withIdentifier: "ReviewInfoTableViewCell_3", for: indexPath) as! ReviewInfoTableViewCell_3
            
            
            
            let theString1 = "ReviewInfoRow3".localized
            //print("####### Desc is " + pDataProductDesc)
            let theAttributedString1 = try! NSAttributedString(data: theString1.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                               options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                               documentAttributes: nil)
            
            cell.rowTitle.attributedText = theAttributedString1
            
            //cell = tableview.dequeueReusableCell(withIdentifier: "ReviewInfoTableViewCell_2", for: indexPath)
            cell.row3Button1.tag = 1
            cell.row3Button1.addTarget(self, action:#selector(row3buttonTapped), for: .touchUpInside)
            //cell.label1.text = "competencyTitle".localized
            
            cell.row3Button2.tag = 2
            cell.row3Button2.addTarget(self, action:#selector(row3buttonTapped), for: .touchUpInside)
            //cell.label2.text = "descriptorTitle".localized
            
            cell.row3Button3.tag = 3
            cell.row3Button3.addTarget(self, action:#selector(row3buttonTapped), for: .touchUpInside)
            //cell.label3.text = "proficiency levelsTitle".localized
            
            cell.row3Button4.tag = 4
            cell.row3Button4.addTarget(self, action:#selector(row3buttonTapped), for: .touchUpInside)
            //cell.label4.text = "5 proficiency levelsTitle".localized
            
            cell.row3Button5.tag = 5
            cell.row3Button5.addTarget(self, action:#selector(row3buttonTapped), for: .touchUpInside)
           // cell.label5.text = "score calculatedTitle".localized
            
            //cell.row3Button6.addTarget(self, action:#selector(row2button5Tapped), for: .touchUpInside)
            cell.selectionStyle = .none
            return cell
            
        }
        
      /*  else if indexPath.row == 3 {
            
             let cell:ReviewInfoTableViewCell_4 = self.tableview.dequeueReusableCell(withIdentifier: "ReviewInfoTableViewCell_4", for: indexPath) as! ReviewInfoTableViewCell_4
            
            cell.rowTitle.text = "ReviewInfoRow4".localized
            
            cell.label1.text = "Expert".localized
            cell.label2.text = "High Performer".localized
            cell.label3.text = "Professional".localized
            cell.label4.text = "Learner".localized
            cell.label5.text = "Beginner".localized
            
            cell.value1.text = "5%".localized
            cell.value2.text = "20%".localized
            cell.value3.text = "50% to 60%".localized
            cell.value4.text = "15% to 20%".localized
            cell.value5.text = "10% to 15%".localized
            
            cell.selectionStyle = .none
            return cell
        } else if indexPath.row == 1 {
            let cell:ReviewInfoTableViewCell_2 = self.tableview.dequeueReusableCell(withIdentifier: "ReviewInfoTableViewCell_2", for: indexPath) as! ReviewInfoTableViewCell_2
            
            cell.rowTitle.text = "ReviewInfoRow2".localized
            
            //cell = tableview.dequeueReusableCell(withIdentifier: "ReviewInfoTableViewCell_2", for: indexPath)
            cell.row2Button1.tag = 1
            cell.row2Button1.addTarget(self, action:#selector(row2ButtonTapped), for: .touchUpInside)
            cell.step1Title.text = "Step 1".localized
            
            cell.row2Button2.tag = 2
            cell.row2Button2.addTarget(self, action:#selector(row2ButtonTapped), for: .touchUpInside)
            cell.step2Title.text = "Step 2".localized
            
            cell.row2Button3.tag = 3
            cell.row2Button3.addTarget(self, action:#selector(row2ButtonTapped), for: .touchUpInside)
            cell.step3Title.text = "Step 3".localized
            
            cell.row2Button4.tag = 4
            cell.row2Button4.addTarget(self, action:#selector(row2ButtonTapped), for: .touchUpInside)
            cell.step4Title.text = "Step 4".localized
            
            cell.row2Button5.tag = 5
            cell.row2Button5.addTarget(self, action:#selector(row2ButtonTapped), for: .touchUpInside)
            cell.step5Title.text = "Step 5".localized
            
            cell.row2Button6.tag = 6
            cell.row2Button6.addTarget(self, action:#selector(row2ButtonTapped), for: .touchUpInside)
            
            
            cell.selectionStyle = .none
            return cell
            
        }
        else {
            
            let cell:ReviewInfoTableViewCell_5 = self.tableview.dequeueReusableCell(withIdentifier: "ReviewInfoTableViewCell_5", for: indexPath) as! ReviewInfoTableViewCell_5
            cell.rowTitle.text = "ReviewInfoRow5".localized
            
            cell.selectionStyle = .none
            return cell
        }*/
        //return cell
    }
    
    func row2ButtonTapped(sender:UIButton)
    {
        if sender.tag == 4
        {
            let popOverVC = UIStoryboard (name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ReviewInfoPopupController") as! ReviewInfoPopupController
            popOverVC.popupNo = sender.tag
            popOverVC.rowNo = 1
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            popOverVC.modalTransitionStyle = .crossDissolve
            
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParentViewController: self)
        }
        else
        {
            let popOverVC = UIStoryboard (name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ReviewInfoSmallPopupController") as! ReviewInfoSmallPopupController
            popOverVC.popupNo = sender.tag
            popOverVC.rowNo = 1
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            popOverVC.modalTransitionStyle = .crossDissolve
            
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParentViewController: self)
        }
        
        
    }
    
    
    /*func row2button1Tapped()
     {
     print("Here I am in the 1 cell")
     
     let popOverVC = UIStoryboard (name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ReviewInfoPopupController") as! ReviewInfoPopupController
     popOverVC.popupNo = 1
     self.addChildViewController(popOverVC)
     popOverVC.view.frame = self.view.frame
     popOverVC.modalTransitionStyle = .crossDissolve
     
     self.view.addSubview(popOverVC.view)
     popOverVC.didMove(toParentViewController: self)
     
     }
     
     func row2button2Tapped()
     {
     print("Here I am in the 2 cell")
     
     let popOverVC = UIStoryboard (name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ReviewInfoPopupController") as! ReviewInfoPopupController
     popOverVC.popupNo = 2
     self.addChildViewController(popOverVC)
     popOverVC.view.frame = self.view.frame
     popOverVC.modalTransitionStyle = .crossDissolve
     
     self.view.addSubview(popOverVC.view)
     popOverVC.didMove(toParentViewController: self)
     }
     
     func row2button3Tapped()
     {
     print("Here I am in the 3 cell")
     let popOverVC = UIStoryboard (name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ReviewInfoPopupController") as! ReviewInfoPopupController
     popOverVC.popupNo = 3
     self.addChildViewController(popOverVC)
     popOverVC.view.frame = self.view.frame
     popOverVC.modalTransitionStyle = .crossDissolve
     
     self.view.addSubview(popOverVC.view)
     popOverVC.didMove(toParentViewController: self)
     }
     
     func row2button4Tapped()
     {
     print("Here I am in the row  cell")
     let popOverVC = UIStoryboard (name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ReviewInfoPopupController") as! ReviewInfoPopupController
     popOverVC.popupNo = 4
     self.addChildViewController(popOverVC)
     popOverVC.view.frame = self.view.frame
     popOverVC.modalTransitionStyle = .crossDissolve
     
     self.view.addSubview(popOverVC.view)
     popOverVC.didMove(toParentViewController: self)
     }
     
     func row2button5Tapped()
     {
     print("Here I am in the 5 cell")
     let popOverVC = UIStoryboard (name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ReviewInfoPopupController") as! ReviewInfoPopupController
     popOverVC.popupNo = 5
     self.addChildViewController(popOverVC)
     popOverVC.view.frame = self.view.frame
     popOverVC.modalTransitionStyle = .crossDissolve
     
     self.view.addSubview(popOverVC.view)
     popOverVC.didMove(toParentViewController: self)
     }
     
     func row2button6Tapped()
     {
     print("Here I am in the 6 cell")
     let popOverVC = UIStoryboard (name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ReviewInfoPopupController") as! ReviewInfoPopupController
     popOverVC.popupNo = 5
     self.addChildViewController(popOverVC)
     popOverVC.view.frame = self.view.frame
     popOverVC.modalTransitionStyle = .crossDissolve
     
     self.view.addSubview(popOverVC.view)
     popOverVC.didMove(toParentViewController: self)
     }*/
    
    func row3buttonTapped(sender:UIButton)
    {
        print("Here I am in the  cell \(sender.tag)")
        if sender.tag == 4
        {
            let popOverVC = UIStoryboard (name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ReviewInfoPopupController") as! ReviewInfoPopupController
            popOverVC.popupNo = sender.tag
            popOverVC.rowNo = 2
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            popOverVC.modalTransitionStyle = .crossDissolve
            
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParentViewController: self)
        }
        else
        {
            let popOverVC = UIStoryboard (name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ReviewInfoSmallPopupController") as! ReviewInfoSmallPopupController
            popOverVC.popupNo = sender.tag
            popOverVC.rowNo = 2
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            popOverVC.modalTransitionStyle = .crossDissolve
            
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParentViewController: self)
        }
        
    }
    
    /* func row3button2Tapped()
     {
     print("Here I am in the  cell \(sender.tag)")
     }
     
     func row3button3Tapped()
     {
     print("Here I am in the  cell \(sender.tag)")
     }
     
     func row3button4Tapped()
     {
     print("Here I am in the  cell \(sender.tag)")
     }
     
     func row3button5Tapped()
     {
     print("Here I am in the 1 cell")
     }*/
    
    
}
