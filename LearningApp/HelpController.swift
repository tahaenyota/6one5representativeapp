//
//  HelpController.swift
//  LearningApp
//
//  Created by Sumit More on 1/10/17.
//  Copyright © 2017 Mac-04. All rights reserved.
//

import Foundation
import UIKit

class HelpController: UIViewController {
    
    
    @IBOutlet weak var webview: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let localfilePath = Bundle.main.url(forResource: "help", withExtension: "html");
        let myRequest = NSURLRequest(url: localfilePath!);
        self.webview.loadRequest(myRequest as URLRequest);
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
