//
//  LApp.swift
//  LearningApp
//
//  Created by Mac-04 on 08/09/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import Foundation

public struct LApp: Decodable {
    
    public let name: String
    public let image: String
    public let description: String
    
    public init?(json: JSON) {
        guard let container: JSON = "data" <~~ json
            else { return nil }
        
        guard let image: String = "image" <~~ container,
            let description: String = "description" <~~ container,
            let name: String = "name" <~~ container
            else { return nil }

        self.description = description
        self.name = name
        self.image = image
    }
}
