//
//  BadgesCollionVwCell.swift
//  LearningApp
//
//  Created by Mac-04 on 22/08/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import UIKit

class BadgesCollionVwCell: UICollectionViewCell {
    
    @IBOutlet weak var BadgesImage: UIImageView!
    @IBOutlet weak var BadgesTitleLbl: UILabel!
    @IBOutlet weak var badgesDownloadBtn: UIButton!
    @IBOutlet weak var badgesShareBtn: UIButton!
    
}
