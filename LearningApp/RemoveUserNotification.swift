//
//  RemoveUserNotification.swift
//  LearningApp
//
//  Created by enyotalearning on 16/01/17.
//  Copyright © 2017 Mac-04. All rights reserved.
//

import Foundation

class RemoveUserNotification
{
    //using Singleton pattern for single object handling...
    class var sharedInstance: RemoveUserNotification
    {
        //2
        struct Singleton
        {
            //3
            static let instance = RemoveUserNotification()
        }
        //4
        return Singleton.instance
    }
    
    func loadData(notificationId:String, completionHandler:@escaping (_ status:Bool)->())
    {
        let headers = [
            "cache-control": "no-cache",
            ]
        //let login = Login.sharedInstance
        let apiURL: String = Constants.updateUserNotification + notificationId
        let request = NSMutableURLRequest(url: NSURL(string: apiURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("Error: \(error)")
                
                
                completionHandler(false)
            } else
            {
                //let httpResponse = response as? HTTPURLResponse
                //print("HTTP Response for the learner list @@@@ : \(data)")
                
                do
                {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                    {
                        
                        
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        if statusVal == nil
                        {
                            print("status val is null")
                            let statusVal = convertedJsonIntoDict["status"] as? String
                            print("status valule is \(statusVal)")
                            let login = Login.sharedInstance
                            login.status = String(describing: convertedJsonIntoDict["status"] as? Bool)
                            login.message = convertedJsonIntoDict["message"] as? String
                            
                            
                        }
                        else
                        {
                            if(statusVal)!
                            {
                                completionHandler(true)
                            }
                            else
                            {
                                completionHandler(false)
                            }
                        }
                    }
                    
                } catch let error as NSError
                {
                    print(error)
                }
                
                
            }
            
            
        })
        
        dataTask.resume()
    }
}
