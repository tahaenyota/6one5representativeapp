//
//  LoginViewControler.swift
//  LearningApp
//
//  Created by Mac-04 on 18/08/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {
    
    
    
    @IBOutlet weak var UserNameTxtField: UITextField!
    @IBOutlet weak var PasswordTxtField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var copyrightLbl: UILabel!

    @IBAction func loginButtonTouch(_ sender: UIButton) {
        login()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.PasswordTxtField.autocapitalizationType = UITextAutocapitalizationType.none
        self.UserNameTxtField.autocapitalizationType = UITextAutocapitalizationType.none
        
        designPage() // for placeholder and line draw
    }
    
    override func viewDidAppear(_ animated: Bool) {

//        designPage() // for placeholder and line draw
        
        let prefs:UserDefaults = UserDefaults.standard
        let isLoggedIn = prefs.integer(forKey: "ISLOGGEDIN") as Int
        print("Is Logged In val: \(isLoggedIn)")
        
        if(isLoggedIn == 1) {
            
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let initViewController: UIViewController = storyboard.instantiateViewController(withIdentifier: "EmployeeHomeTabView") as UIViewController
            self.present(initViewController, animated: true, completion: nil)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    func designPage() {
        
        
        PasswordTxtField.updateConstraintsIfNeeded()
            print(" Password Width: \(PasswordTxtField.frame.size.width)")
        
            UserNameTxtField.leftViewMode = UITextFieldViewMode.always
            let imageView2 = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: 20))
            let image2 = UIImage(named: "username")
            imageView2.image = image2
            imageView2.contentMode = UIViewContentMode.scaleAspectFit
            UserNameTxtField.leftView = imageView2        
        
            PasswordTxtField.leftViewMode = UITextFieldViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 15, height: 20))
            let image = UIImage(named: "pass")
            imageView.image = image
            imageView.contentMode = UIViewContentMode.scaleAspectFit
            imageView.layoutMargins.right = 5
            PasswordTxtField.leftView = imageView
            PasswordTxtField.layoutMargins.left = 5
            
            let border = CALayer()
            let width = CGFloat(1.0)
            border.borderColor = UIColor.gray.cgColor
            border.frame = CGRect(x: 0, y: UserNameTxtField.frame.size.height - width, width:  UserNameTxtField.frame.size.width, height: UserNameTxtField.frame.size.height)
            border.borderWidth = width
            self.UserNameTxtField.layer.addSublayer(border)
            self.UserNameTxtField.layer.masksToBounds = true
        
            let border2 = CALayer()
            let width2 = CGFloat(1.0)
            border2.borderColor = UIColor.gray.cgColor
            border2.frame = CGRect(x: 0, y: PasswordTxtField.frame.size.height - width2, width:  PasswordTxtField.frame.size.width, height: PasswordTxtField.frame.size.height)
            border2.borderWidth = width2
            self.PasswordTxtField.layer.addSublayer(border2)
            self.PasswordTxtField.layer.masksToBounds = true
 
        loginButton.titleLabel?.text = "login_btn".localized
        copyrightLbl.text = "login_copyright".localized

        
    }
    

    
    func login() {

        /*var boxView = UIView()
        boxView = UIView(frame: CGRect(x: view.frame.midX - 110, y: view.frame.midY - 25, width: 220, height: 50))
        boxView.backgroundColor = UIColor.white
        boxView.alpha = 1
        boxView.layer.cornerRadius = 10
        
        //Here the spinnier is initialized
        let activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        activityView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityView.startAnimating()
        
        let textLabel = UILabel(frame: CGRect(x: 60, y: 0, width: 200, height: 50))
        textLabel.textColor = UIColor.gray
        textLabel.text = "Login in progress"
        
        boxView.addSubview(activityView)
        boxView.addSubview(textLabel)
        
        view.addSubview(boxView)*/
        
        let progressHUD = ProgressHUD()
        self.view.addSubview(progressHUD)
        UIApplication.shared.beginIgnoringInteractionEvents()

        var username = self.UserNameTxtField.text
        var password = self.PasswordTxtField.text
        
        //username = "shaneli4@6one5.com"
        //password = "Admin@123"
        
        if ( username=="" || password=="" ) {
            username = "shaneli4@6one5.com"
            password = "Admin@123"
            //boxView.removeFromSuperview()
            /*progressHUD.hide()
            UIApplication.shared.endIgnoringInteractionEvents()
            let alertController = UIAlertController(title: "Sign in Failed! ", message: "Please enter Username and Password." , preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)*/
        }
        //else {
        
            let headers = [
                "content-type": "application/x-www-form-urlencoded",
                "cache-control": "no-cache",
            ]
            
            let dataStr: String = "data={ \"uname\" : \"\(username! as NSString)\" , \"upass\" : \"\(password! as NSString)\" }"
            print("POST Params: \(dataStr)")
            let postData = NSMutableData(data: dataStr.data(using: String.Encoding.utf8)!)
            
            let loginURL: String = Constants.loginURL
            let request = NSMutableURLRequest(url: NSURL(string: loginURL)! as URL,
                                              cachePolicy: .useProtocolCachePolicy,
                                              timeoutInterval: 10.0)
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = headers
            request.httpBody = postData as Data
            
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
                

                if (error != nil) {
                    progressHUD.hide()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    print("Error: \(error)")
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "Login Error", message: "Error in connection. Please check your internet connection and try again.", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                } else {
                    let httpResponse = response as? HTTPURLResponse
                    var errorFlag = 0
                    let errorMsg = "Invalid Credentials"
                    print("Login url : \(loginURL) Response 111 : \(httpResponse)")
                    do {
                        if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                            
                            print("Print converted dictionary",convertedJsonIntoDict)
                            	
                            // Get value by key
                            let statusVal = convertedJsonIntoDict["status"] as? Bool
                            
                            if(statusVal == true)
                            {
                                progressHUD.hide()
                                UIApplication.shared.endIgnoringInteractionEvents()

                                let user_id  = convertedJsonIntoDict["user_id"] as? String
                                let course_id  = convertedJsonIntoDict["course_id"] as? String
                                let user_name = (convertedJsonIntoDict["first_name"] as! String) + " " + (convertedJsonIntoDict["last_name"] as! String)
                                let fName  = convertedJsonIntoDict["first_name"] as! String
                                let lName  = convertedJsonIntoDict["last_name"] as! String
                                let email  = convertedJsonIntoDict["email"] as! String
                                let termsaccepted  = convertedJsonIntoDict["terms_accepted"] as! String
                                let profilePic  = convertedJsonIntoDict["profile_pic_link"] as? String
                                let phone  = "1234567890" // convertedJsonIntoDict["phone"] as? String
                                print("User Id will set to \(user_id)")
                                print("Profile Pic \(profilePic)")
                                
                                let adviseModule = convertedJsonIntoDict["Advise"] as! NSDictionary
                                let CloseModule = convertedJsonIntoDict["Close"] as! NSDictionary
                                let HandlingCustomerComplaintsModule = convertedJsonIntoDict["Handling Customer Complaints"] as! NSDictionary
                                let ThankModule = convertedJsonIntoDict["Thank"] as! NSDictionary
                                let UnderstandModule = convertedJsonIntoDict["Understand"] as! NSDictionary
                                let WelcomeModule = convertedJsonIntoDict["Welcome"] as! NSDictionary
                                
                                
                                
                                
                                
                                let prefs:UserDefaults = UserDefaults.standard
                                prefs.set(username, forKey: "uname")
                                prefs.set(password, forKey: "upass")
                                prefs.set(user_id, forKey: "user_id")
                                prefs.set(course_id, forKey: "course_id")
                                prefs.set(user_name, forKey: "username")
                                prefs.set(1, forKey: "ISLOGGEDIN")
                                prefs.set(fName, forKey: "fName")
                                prefs.set(lName, forKey: "lName")
                                prefs.set(email, forKey: "email")
                                prefs.set(phone, forKey: "phone")
                                prefs.set(termsaccepted, forKey: "terms_accepted")
                                prefs.set(profilePic, forKey:"profile_pic")
                                
                                prefs.set(adviseModule, forKey:"adviseModule")
                                prefs.set(CloseModule, forKey:"CloseModule")
                                prefs.set(HandlingCustomerComplaintsModule, forKey:"HandlingCustomerComplaintsModule")
                                
                                prefs.set(ThankModule, forKey:"ThankModule")
                                prefs.set(UnderstandModule, forKey:"UnderstandModule")
                                prefs.set(WelcomeModule, forKey:"WelcomeModule")
                                
                                
                                prefs.synchronize()
                                
                                OperationQueue.main.addOperation{
                                    if termsaccepted == "0" {
                                        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                        let initViewController: UIViewController = storyboard.instantiateViewController(withIdentifier: "TermsAndConditions") as UIViewController
                                        self.present(initViewController, animated: true, completion: nil)
                                    } else {
                                        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                        let initViewController: UIViewController = storyboard.instantiateViewController(withIdentifier: "EmployeeHomeTabView") as UIViewController
                                        self.present(initViewController, animated: true, completion: nil)
                                    }
                                }
                            } else {
                                progressHUD.hide()
                                UIApplication.shared.endIgnoringInteractionEvents()

                                var error_msg:NSString
                                if convertedJsonIntoDict["message"] as? NSString != nil {
                                    error_msg = convertedJsonIntoDict["message"] as! NSString
                                } else {
                                    error_msg = "Unknown Error"
                                }
                                errorFlag = 1
                                print("error_msg",error_msg)
                            }
                        }
                    } catch let error as NSError {
                        progressHUD.hide()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        print(error)
                    }
                    
                    if(errorFlag == 1) {

                        progressHUD.hide()
                        UIApplication.shared.endIgnoringInteractionEvents()

                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "Login Error", message: errorMsg, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }
            })
            
            dataTask.resume()
        //}
    }
}
