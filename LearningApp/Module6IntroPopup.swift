//
//  Module6IntroPopup.swift
//  LearningApp
//
//  Created by Sumit More on 12/7/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import Foundation
import UIKit

class Module6IntroPopup: UIViewController {
    
    @IBOutlet weak var questionsExplanationLbl: UILabel!
    @IBOutlet weak var questionLbl: UILabel!
    @IBOutlet weak var bottomBar: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        let introductionScreenView = self.navigationController!.viewControllers[0] as! Module6IntroController
        switch introductionScreenView.bulbNo {
        case 1:
            self.questionLbl.text = "Rep_Intro_M6_B1".localized
            self.questionsExplanationLbl.text = "Rep_Intro_M6_B1_Explanation".localized
            self.bottomBar.backgroundColor = UIColor.init(red: 255.0/255.0, green: 37.0/255.0, blue: 48.0/255.0, alpha: 1.0)
            break
        case 2:
            self.questionLbl.text = "Rep_Intro_M6_B2".localized
            self.questionsExplanationLbl.text = "Rep_Intro_M6_B2_Explanation".localized
            self.bottomBar.backgroundColor = UIColor.init(red: 255.0/255.0, green: 142.0/255.0, blue: 9.0/255.0, alpha: 1.0)
            break
        case 3:
            self.questionLbl.text = "Rep_Intro_M6_B3".localized
            self.questionsExplanationLbl.text = "Rep_Intro_M6_B3_Explanation".localized
            self.bottomBar.backgroundColor = UIColor.init(red: 149.0/255.0, green: 203.0/255.0, blue: 1.0/255.0, alpha: 1.0)
            break
        case 4:
            self.questionLbl.text = "Rep_Intro_M6_B4".localized
            self.questionsExplanationLbl.text = "Rep_Intro_M6_B4_Explanation".localized
            self.bottomBar.backgroundColor = UIColor.init(red: 40.0/255.0, green: 151.0/255.0, blue: 204.0/255.0, alpha: 1.0)
            break
        case 5:
            self.questionLbl.text = "Rep_Intro_M6_B5".localized
            self.questionsExplanationLbl.text = "Rep_Intro_M6_B5_Explanation".localized
            self.bottomBar.backgroundColor = UIColor.init(red: 162.0/255.0, green: 105.0/255.0, blue: 205.0/255.0, alpha: 1.0)
            break
        default:
            break
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func closeClick(_ sender: AnyObject) {
        self.view.removeFromSuperview()
    }
}
