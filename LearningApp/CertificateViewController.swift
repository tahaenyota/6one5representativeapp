//
//  CertificateViewController.swift
//  LearningApp
//
//  Created by Mac-04 on 03/09/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import UIKit

class CertificateViewController: UIViewController {

    @IBOutlet weak var certificateTbl: UITableView!
    let certificateDetail  = ["Lorem Ipsum is simply dummy text of the printing and typesetting industry","Lorem Ipsum is simply dummy text of the printing and typesetting industry","Lorem Ipsum is simply dummy text of the printing and typesetting industry"]
    
    var certificateImageArray: [UIImage] = [ UIImage(named: "certificate1.png")!, UIImage(named: "certificate2.png")!, UIImage(named: "certificate3.png")!]
    
    override func viewDidLoad() {
        
        self.certificateTbl!.layer.shadowOffset = CGSizeMake(0, 0)
        self.certificateTbl!.layer.shadowColor = UIColor.grayColor().CGColor
        self.certificateTbl!.layer.shadowRadius = 4
        self.certificateTbl!.layer.shadowOpacity = 0.25
        self.certificateTbl!.layer.masksToBounds = false;
        self.certificateTbl!.clipsToBounds = false;
        
        self.navigationController!.navigationBar.topItem!.title = "Notification";

        super.viewDidLoad()
    }
    
    
    override func viewWillAppear(animated: Bool) {
        
        DataManager.getLearningAppsDataFromFileWithSuccess { (data) -> Void in
            var json: [String: AnyObject]!
            
            do {
                json = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions()) as? [String: AnyObject]
                print("<<<<>>>>",json)
            } catch {
                print(error)
                
            }
            
            guard let topApps = TopApps(json: json) else {
                print("Error initializing object")
                return
            }
            
            guard let lastItem = topApps.feed?.entries?.first else {
                print("No such item")
                return
            }
            
            print(lastItem)
            
        }
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return certificateDetail.count
    }
    
    
     func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CertificateTblCell", forIndexPath: indexPath) as! CertificateTblViewCell
        cell.CertiDescriptionLbl.lineBreakMode = .ByWordWrapping
        cell.CertiDescriptionLbl.numberOfLines = 3;
        cell.CertiDescriptionLbl.lineBreakMode = NSLineBreakMode.ByWordWrapping
        
        cell.CertiDescriptionLbl.text = certificateDetail[indexPath.row]
        
        cell.CertiTitleLbl.text = "Certificate of Merit"
        cell.cerificateImgView.image = certificateImageArray [indexPath.row]
        
        cell.layer.shadowColor = UIColor.grayColor().CGColor;
        cell.layer.shadowOffset = CGSizeMake(0, 2.0);
        cell.layer.shadowRadius = 1.0;
        cell.layer.shadowOpacity = 1.0;
        cell.layer.masksToBounds = false;
        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).CGPath;
        
        return cell
    }
}
