//
//  ScriptCollectionViewCell.swift
//  LearningApp
//
//  Created by NonStop io on 01/10/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import UIKit

class ScriptCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var User2Image: UIImageView!
    
    @IBOutlet weak var User1Image: UIImageView!
    
    @IBOutlet weak var user1Txt: UIImageView!
    
    @IBOutlet weak var user2Txt: UIImageView!
    
    @IBOutlet weak var User1Lbl: UILabel!
    
    @IBOutlet weak var User2Lbl: UILabel!
    
}
