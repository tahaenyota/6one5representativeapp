//
//  Notification6one5.swift
//  LearningApp
//
//  Created by Sumit More on 1/5/17.
//  Copyright © 2017 Mac-04. All rights reserved.
//

import Foundation

public class Notification6one5: NSObject {

    var notifId: String = ""
    var notifName: String = ""
    var notifContent: String = ""
    var notifNotificationId: String = ""
    var notifUserId: String = ""
    var notifBadgeId: String = ""
    var notifCourseId: String = ""
    var notifModuleId: String = ""
    var notifRepresentativeId: String = ""
    var notifNotificationType: String = ""
    var notifAssignedAt: String = ""
    var notificationStauts:String = ""
   
}
