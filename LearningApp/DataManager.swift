//
//  DataManager.swift
//  LearningApp
//
//  Created by Mac-04 on 31/08/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import Foundation


let TopAppURL = "https://itunes.apple.com/us/rss/topgrossingipadapplications/limit=25/json"



open class DataManager {
    
    
    open class func getTopAppsDataFromFileWithSuccess(_ success: @escaping ((_ data: Data) -> Void)) {
        
        
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
            let filePath = Bundle.main.path(forResource: "topapps", ofType:"json")
            let data = try! Data(contentsOf: URL(fileURLWithPath: filePath!),
                options: NSData.ReadingOptions.uncached)
            success(data)
        })
    }
    
    
    open class func getLearningAppsDataFromFileWithSuccess(_ success: @escaping ((_ data: Data) -> Void)){
        
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
            let filePath = Bundle.main.path(forResource: "learningApps", ofType: "json")
            let data = try! Data(contentsOf: URL(fileURLWithPath: filePath!), options: NSData.ReadingOptions.uncached)
            success(data)
            print("----->>>",data)
            
            print("---------------------------------")
            
        })
        
    }
    
    
    open class func loadDataFromURL(_ url: URL, completion:@escaping (_ data: Data?, _ error: NSError?) -> Void) {
        let session = URLSession.shared
        
        let loadDataTask = session.dataTask(with: url, completionHandler: { (data, response, error) -> Void in
            if let responseError = error {
                completion(nil, responseError as NSError?)
            } else if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    let statusError = NSError(domain:"com.raywenderlich", code:httpResponse.statusCode, userInfo:[NSLocalizedDescriptionKey : "HTTP status code has unexpected value."])
                    completion(nil, statusError)
                } else {
                    completion(data, nil)
                }
            }
        }) 
        
        loadDataTask.resume()
    }
    
    
    open class func getTopAppsDataFromItunesWithSuccess(_ success: @escaping ((_ iTunesData: Data?) -> Void)) {
        
        loadDataFromURL(URL(string: TopAppURL)!, completion:{(data, error) -> Void in
            
            if let data = data {
              
                success(data)
            }
        })
    }
    
    
    
}
