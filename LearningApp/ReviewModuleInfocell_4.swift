//
//  ReviewModuleInfocell_4.swift
//  6one5Manager
//
//  Created by enyotalearning on 22/12/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit

class ReviewModuleInfocell_4: UITableViewCell {
    @IBOutlet weak var descriptorLabel4: UILabel!
    @IBOutlet weak var moduleDescription4: UILabel!
    @IBOutlet weak var descriptorTitleLabel4: UILabel!
    
    @IBOutlet weak var cell4NextButton: UIButton!
    @IBOutlet weak var cell4PreviousButton: UIButton!
    
    @IBOutlet weak var discriptorHeight: NSLayoutConstraint!
    
    @IBOutlet weak var discriptorViewHeight: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
