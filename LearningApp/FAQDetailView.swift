//
//  FAQDetailView.swift
//  LearningApp
//
//  Created by Sumit More on 12/30/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import Foundation
import UIKit

class FAQDetailView: UITableViewCell {
    
    // MARK: - Outlets
    @IBOutlet var detailLabel: UILabel!
    
}
