//
//  CertificateData.swift
//  LearningApp
//
//  Created by Mac-04 on 08/09/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import Foundation
public struct CertificateData: Decodable {
    
    public let certdata: Feed?
    
    public init?(json: JSON) {
        certdata = "data" <~~ json
    }
    
}
