//
//  DiscripterModule.swift
//  6one5Manager
//
//  Created by enyotalearning on 06/01/17.
//  Copyright © 2017 enyotalearning. All rights reserved.
//

import Foundation

class DiscripterModule
{
    var beginer = [String]()
    var learner = [String]()
    var professional = [String]()
    var highPerformer = [String]()
    var expert = [String]()

    var descripterText:String!
    
    func addBeginer(input:String)
    {
        beginer.append(Constants.DiscripterFontTag + input + Constants.fontclosing)
    }
    
    func addLearner(input:String)
    {
        learner.append(Constants.DiscripterFontTag + input + Constants.fontclosing)
    }
    
    func addProfessional(input:String)
    {
        professional.append(Constants.DiscripterFontTag + input + Constants.fontclosing)
    }
    
    func addHighPerformer(input:String)
    {
        highPerformer.append(Constants.DiscripterFontTag + input + Constants.fontclosing)
    }
    
    func addExpert(input:String)
    {
        expert.append(Constants.DiscripterFontTag + input + Constants.fontclosing)
    }
    
}


