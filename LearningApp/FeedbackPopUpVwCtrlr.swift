//
//  FeedbackPopUpVwCtrlr.swift
//  LearningApp
//
//  Created by Mac-04 on 29/08/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import UIKit
import DLRadioButton

class FeedbackPopUpVwCtrlr: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var feedbackTextView: UITextView!
    @IBOutlet weak var feedbackpopupCancelBtn: UIButton!
    @IBOutlet weak var feedbackPopupSubmitBtn: UIButton!
    
    @IBOutlet weak var radioOutlet: DLRadioButton!
    
    @IBOutlet weak var feedbackCourseRating: CosmosView!
    @IBOutlet weak var feedbackManagerRating: CosmosView!
    
    var user_id: String = "1"
    
    var notificationID:String!
    
    override func viewDidLoad()
    {
        
        super.viewDidLoad()
        
        let prefs:UserDefaults = UserDefaults.standard
        user_id = prefs.value(forKey: "user_id") as! String!

        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        
       let borderColor = UIColor(red: 232/255.0, green: 232/255.0, blue: 232/255.0, alpha: 1.0)

        let titleRD = self.radioOutlet.titleLabel?.text
        print("Title: \(titleRD!)")
        if titleRD == "Yes"  {
            self.radioOutlet.isSelected = true
        }
        
        feedbackTextView.layer.borderColor = borderColor.cgColor;
        feedbackTextView.layer.borderWidth = 1.0;
        feedbackTextView.layer.cornerRadius = 5.0;
        
        feedbackpopupCancelBtn.layer.borderColor = borderColor.cgColor;
        feedbackpopupCancelBtn.layer.borderWidth = 1.0;
        feedbackpopupCancelBtn.layer.cornerRadius = 1.0;
        
        feedbackPopupSubmitBtn.layer.borderColor = borderColor.cgColor;
        feedbackPopupSubmitBtn.layer.borderWidth = 1.0;
        feedbackPopupSubmitBtn.layer.cornerRadius = 1.0;
        
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func CancelPopupBtnClick(_ sender: AnyObject) {
        self.navigationController?.isNavigationBarHidden = false
        self.view.removeFromSuperview()
    
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadList"), object: nil)
        
        //self.navigationController?.isNavigationBarHidden = false
        //self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func SubmitPopupBtnClick(_ sender: AnyObject) {
        print("Submit Button Touch")
//      self.navigationController?.navigationBarHidden = false
        feedbackTextView.resignFirstResponder()
        
        let progressHUD = ProgressHUD()
        self.view.addSubview(progressHUD)
        UIApplication.shared.beginIgnoringInteractionEvents()

        
        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
        ]
        
        let course_id = 2
        let feedbackUpdateURL: String = Constants.salesRepFeedbackURL + String(user_id)
        print("Feedback URL: \(feedbackUpdateURL)")
        
        let courseRating = Int(self.feedbackCourseRating.rating)
        let mgrRating = Int(self.feedbackManagerRating.rating)
        let feedbackStr = self.feedbackTextView.text!
        let postStr = "data={\"course_rating\":\"\(courseRating)\",\"course_id\":\"\(course_id)\",\"manager_rating\":\"\(mgrRating)\",\"description\":\"\(feedbackStr)\",\"course_help\":4 }"
        print("Post Data: \(postStr)")

        let postData = NSMutableData(data: postStr.data(using: String.Encoding.utf8)!)

        let request = NSMutableURLRequest(url: NSURL(string: feedbackUpdateURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                DispatchQueue.main.async()
                    {
                        progressHUD.hide()
                        
                        UIApplication.shared.endIgnoringInteractionEvents()
                print("HTTP Error: \(error)")
                let alertController = UIAlertController(title: "Feedback", message: "Error in connecting to the server. Please try later." , preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                    self.navigationController?.isNavigationBarHidden = false
                    self.view.removeFromSuperview()
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion:nil)
                }
            } else {
                let httpResponse = response as? HTTPURLResponse
                print("HTTP Response: \(httpResponse)")

                do {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        print("Print converted dictionary",convertedJsonIntoDict)
                        
                        // Get value by key
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        
                        if(statusVal == true)
                        {
                            DispatchQueue.main.async()
                                {
                                    progressHUD.hide()
                                    
                                    UIApplication.shared.endIgnoringInteractionEvents()
                            let alertController = UIAlertController(title: "Feedback", message: "Feedback shared successfully." , preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.navigationController?.isNavigationBarHidden = false
                                self.view.removeFromSuperview()
                                
                                self.removeNotification(notificationId: self.notificationID)
                            }
                            

                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                            }
                        } else {
                            /*var error_msg:NSString
                            if convertedJsonIntoDict["message"] as? NSString != nil {
                                error_msg = convertedJsonIntoDict["message"] as! NSString
                            } else {
                                error_msg = "Unknown Error"
                            }
                            print("error_msg",error_msg)*/
                            
                            DispatchQueue.main.async()
                            {
                                    progressHUD.hide()
                                    
                                    UIApplication.shared.endIgnoringInteractionEvents()
                            let alertController = UIAlertController(title: "Feedback", message: "Error in feedback submission. Please try later." , preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.navigationController?.isNavigationBarHidden = false
                                self.view.removeFromSuperview()
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                            }
                        }
                        
                    }
                } catch let error as NSError {
                    print(error)
                    
                    DispatchQueue.main.async()
                        {
                            progressHUD.hide()
                            
                            UIApplication.shared.endIgnoringInteractionEvents()
                            let alertController = UIAlertController(title: "Feedback", message: "Error in feedback submission. Please try later." , preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.navigationController?.isNavigationBarHidden = false
                                self.view.removeFromSuperview()
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                    }
                }
            
            }
        })
        
        dataTask.resume()
        
    }
    

    
    func removeNotification(notificationId:String)
    {
        let progressHUD = ProgressHUD()
        self.view.addSubview(progressHUD)
        //progressHUD.activityIndictor.color = UIColor.black
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        RemoveUserNotification.sharedInstance.loadData(notificationId:notificationId, completionHandler: { (status:Bool) -> () in
            DispatchQueue.main.async() {
                let login = Login.sharedInstance
                
                if login.status != nil
                {
                    let alertController = UIAlertController(title: "User status", message: login.message , preferredStyle: .alert)
                    let OKAction = UIAlertAction(title: "ok".localized, style: .default) {
                        (action:UIAlertAction!) in
                        login.logoutFromDeviceCache()
                        self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
                    }
                    alertController.addAction(OKAction)
                    self.present(alertController, animated: true, completion:nil)
                    progressHUD.hide()
                    
                    UIApplication.shared.endIgnoringInteractionEvents()
                    
                }
                if status
                {
                    DispatchQueue.main.async()
                        {
                            //self.loadLernerData()
                            //                            self.notifsData = []
                            //                            self.getNotifs()
                            progressHUD.hide()
                            //self.reviewNotificationData.desc()
                            //self.tableview.reloadData()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            print("Notification deleted sucessfuly")
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadList"), object: nil)
                            progressHUD.hide()
                    } //learner details service completed
                }
                else
                {
                    print("Problem while loading data ")
                    let alert = UIAlertController(title: "notificationerror".localized, message: "errordetails".localized, preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "ok".localized, style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    progressHUD.hide()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    
                    
                }

            }
            //if (self.notifsData.count >= 0 )
            //{
                            //}// end if learnerDetails.getLearnerCount
            
        })
    }


}
