//
//  Certificate.swift
//  LearningApp
//
//  Created by Sumit More on 1/2/17.
//  Copyright © 2017 Mac-04. All rights reserved.
//

import Foundation

public class Certificate: NSObject
{
    
    var certificates = [CertificateDetails]()
    var message:String!
    var status:String!
    
    
    //Singleton pattern
    //This is to make sure will use only one object for creating the login
    class var sharedInstance: Certificate
    {
        //2
        struct Singleton
        {
            //3
            static let instance = Certificate()
        }
        //4
        return Singleton.instance
    }
    
    func addCertificateData(certificateDetails:CertificateDetails)
    {
        certificates.append(certificateDetails)
    }
    
    func freeCertificateData()
    {
        certificates = [CertificateDetails]()
    }
    
}
