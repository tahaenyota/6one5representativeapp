//
//  Module1QuizQuestionsController.swift
//  LearningApp
//
//  Created by Sumit More on 12/10/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import Foundation
import UIKit

class Module1QuizQuestionsController: UIViewController {
    
    var QuetionStringArray = ["Rep_Quiz_M1_Q1".localized, "Rep_Quiz_M1_Q2".localized, "Rep_Quiz_M1_Q3".localized, "Rep_Quiz_M1_Q4".localized, "Rep_Quiz_M1_Q5".localized, ]
    
    var numberOfOptions = [3,2,2,3,3]
    var correctAnswers = [2,1,2,1,3]
    
    var options = [["Rep_Quiz_M1_Q1_O1".localized, "Rep_Quiz_M1_Q1_O2".localized, "Rep_Quiz_M1_Q1_O3".localized],["Rep_Quiz_M1_Q2_O1".localized, "Rep_Quiz_M1_Q2_O2".localized],["Rep_Quiz_M1_Q3_O1".localized, "Rep_Quiz_M1_Q3_O2".localized],["Rep_Quiz_M1_Q4_O1".localized, "Rep_Quiz_M1_Q4_O2".localized, "Rep_Quiz_M1_Q4_O3".localized],["Rep_Quiz_M1_Q5_O1".localized, "Rep_Quiz_M1_Q5_O2".localized, "Rep_Quiz_M1_Q5_O3".localized]]
    
    var feedback = [["Rep_Quiz_M1_Q1_O1F".localized, "Rep_Quiz_M1_Q1_O2F".localized, "Rep_Quiz_M1_Q1_O3F".localized],["Rep_Quiz_M1_Q2_O1F".localized, "Rep_Quiz_M1_Q2_O2F".localized],["Rep_Quiz_M1_Q3_O1F".localized, "Rep_Quiz_M1_Q3_O2F".localized],["Rep_Quiz_M1_Q4_O1F".localized, "Rep_Quiz_M1_Q4_O2F".localized, "Rep_Quiz_M1_Q4_O3F".localized],["Rep_Quiz_M1_Q5_O1F".localized, "Rep_Quiz_M1_Q5_O2F".localized, "Rep_Quiz_M1_Q5_O3F".localized]]
    
    @IBOutlet weak var QuizQuestionLabel: UILabel!
    
//    @IBOutlet weak var Option1Btn: UIButton!
//    
//    @IBOutlet weak var Option2Btn: UIButton!
//    
//    @IBOutlet weak var Option3Btn: UIButton!
//    
//    @IBOutlet weak var Option4Btn: UIButton!
    
    @IBOutlet weak var Option1Btn: UILabel!
    @IBOutlet weak var Option2Btn: UILabel!
    @IBOutlet weak var Option3Btn: UILabel!
    @IBOutlet weak var Option4Btn: UILabel!
    
    var SelectedIsland = 0
    var attempt = 0
    var flagNo: String = ""
    
    override func viewDidLoad() {
        
        QuizQuestionLabel.text = String(QuetionStringArray[SelectedIsland-1])
        
        if UserDefaults.standard.bool(forKey: "isPlusversion") == true {
            
            self.Option1Btn.removeConstraints(self.Option1Btn.constraints)
            let top_Option1Btn = NSLayoutConstraint(item: self.Option1Btn,
                                                    attribute: NSLayoutAttribute.top,
                                                    relatedBy: NSLayoutRelation.equal,
                                                    toItem: self.QuizQuestionLabel,
                                                    attribute: NSLayoutAttribute.top,
                                                    multiplier: 1,
                                                    constant: 145)
            let left_Option1Btn = NSLayoutConstraint(item: self.Option1Btn,
                                                     attribute: NSLayoutAttribute.left,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.left,
                                                     multiplier: 1,
                                                     constant: 105)
            let width_Option1Btn = NSLayoutConstraint(item: self.Option1Btn,
                                                      attribute: NSLayoutAttribute.width,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: nil,
                                                      attribute: NSLayoutAttribute.notAnAttribute,
                                                      multiplier: 1,
                                                      constant: 221)
            
            self.view.addConstraint(top_Option1Btn)
            self.view.addConstraint(left_Option1Btn)
            self.view.addConstraint(width_Option1Btn)
            
            self.Option2Btn.removeConstraints(self.Option2Btn.constraints)
            let top_Option2Btn = NSLayoutConstraint(item: self.Option2Btn,
                                                    attribute: NSLayoutAttribute.top,
                                                    relatedBy: NSLayoutRelation.equal,
                                                    toItem: self.Option1Btn,
                                                    attribute: NSLayoutAttribute.bottom,
                                                    multiplier: 1,
                                                    constant: 21)
            let left_Option2Btn = NSLayoutConstraint(item: self.Option2Btn,
                                                     attribute: NSLayoutAttribute.left,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.left,
                                                     multiplier: 1,
                                                     constant: 105)
            let width_Option2Btn = NSLayoutConstraint(item: self.Option2Btn,
                                                      attribute: NSLayoutAttribute.width,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: nil,
                                                      attribute: NSLayoutAttribute.notAnAttribute,
                                                      multiplier: 1,
                                                      constant: 221)
            
            self.view.addConstraint(top_Option2Btn)
            self.view.addConstraint(left_Option2Btn)
            self.view.addConstraint(width_Option2Btn)
            
            self.Option3Btn.removeConstraints(self.Option2Btn.constraints)
            let top_Option3Btn = NSLayoutConstraint(item: self.Option3Btn,
                                                    attribute: NSLayoutAttribute.top,
                                                    relatedBy: NSLayoutRelation.equal,
                                                    toItem: self.Option2Btn,
                                                    attribute: NSLayoutAttribute.bottom,
                                                    multiplier: 1,
                                                    constant: 21)
            let left_Option3Btn = NSLayoutConstraint(item: self.Option3Btn,
                                                     attribute: NSLayoutAttribute.left,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.left,
                                                     multiplier: 1,
                                                     constant: 105)
            let width_Option3Btn = NSLayoutConstraint(item: self.Option3Btn,
                                                      attribute: NSLayoutAttribute.width,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: nil,
                                                      attribute: NSLayoutAttribute.notAnAttribute,
                                                      multiplier: 1,
                                                      constant: 221)
            
            self.view.addConstraint(top_Option3Btn)
            self.view.addConstraint(left_Option3Btn)
            self.view.addConstraint(width_Option3Btn)
            
            self.Option4Btn.removeConstraints(self.Option2Btn.constraints)
            let top_Option4Btn = NSLayoutConstraint(item: self.Option4Btn,
                                                    attribute: NSLayoutAttribute.top,
                                                    relatedBy: NSLayoutRelation.equal,
                                                    toItem: self.Option3Btn,
                                                    attribute: NSLayoutAttribute.bottom,
                                                    multiplier: 1,
                                                    constant: 21)
            let left_Option4Btn = NSLayoutConstraint(item: self.Option4Btn,
                                                     attribute: NSLayoutAttribute.left,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: self.view,
                                                     attribute: NSLayoutAttribute.left,
                                                     multiplier: 1,
                                                     constant: 105)
            let width_Option4Btn = NSLayoutConstraint(item: self.Option4Btn,
                                                      attribute: NSLayoutAttribute.width,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: nil,
                                                      attribute: NSLayoutAttribute.notAnAttribute,
                                                      multiplier: 1,
                                                      constant: 221)
            
            self.view.addConstraint(top_Option4Btn)
            self.view.addConstraint(left_Option4Btn)
            self.view.addConstraint(width_Option4Btn)
            
        }

        var i = 0
        repeat {
            if i==0{
                Option1Btn.text = self.options[SelectedIsland-1][i]
            }else if i==1{
                Option2Btn.text = self.options[SelectedIsland-1][i]
            }else if i==2{
                Option3Btn.text = self.options[SelectedIsland-1][i]
            }else if i==3{
                Option4Btn.text = self.options[SelectedIsland-1][i]
            }
            
            i = i + 1
            
        } while i < (numberOfOptions[SelectedIsland-1])
        
        switch (numberOfOptions[SelectedIsland-1]) {
        case 2:
            Option1Btn.isHidden = false
            Option2Btn.isHidden = false
            Option3Btn.isHidden = true
            Option4Btn.isHidden = true
            break
        case 3:
            Option1Btn.isHidden = false
            Option2Btn.isHidden = false
            Option3Btn.isHidden = false
            Option4Btn.isHidden = true
            break
        case 4:
            Option1Btn.isHidden = false
            Option2Btn.isHidden = false
            Option3Btn.isHidden = false
            Option4Btn.isHidden = false
            break
        default:
            Option1Btn.isHidden = true
            Option2Btn.isHidden = true
            Option3Btn.isHidden = true
            Option4Btn.isHidden = true
        }
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        let btn1Tap = UITapGestureRecognizer(target: self, action: #selector(Module1QuizQuestionsController.btn1Tapped))
        Option1Btn.isUserInteractionEnabled = true
        Option1Btn.addGestureRecognizer(btn1Tap)
        
        let btn2Tap = UITapGestureRecognizer(target: self, action: #selector(Module1QuizQuestionsController.btn2Tapped))
        Option2Btn.isUserInteractionEnabled = true
        Option2Btn.addGestureRecognizer(btn2Tap)
        
        let btn3Tap = UITapGestureRecognizer(target: self, action: #selector(Module1QuizQuestionsController.btn3Tapped))
        Option3Btn.isUserInteractionEnabled = true
        Option3Btn.addGestureRecognizer(btn3Tap)
        
        let btn4Tap = UITapGestureRecognizer(target: self, action: #selector(Module1QuizQuestionsController.btn4Tapped))
        Option4Btn.isUserInteractionEnabled = true
        Option4Btn.addGestureRecognizer(btn4Tap)

    }
    
    var score = 0
    var level_score = 0
    
    func btn1Tapped(sender:UITapGestureRecognizer) {
//    @IBAction func Option1BtnTouch(_ sender: AnyObject) {
        
        let QuizHomeScreenView = self.navigationController!.viewControllers[0] as! Module1QuizController
        
        var right = false
        
        if correctAnswers[SelectedIsland-1]==1 && attempt == 0 {
            score = score + 2
            right = true
            print("Score and Attempt: ", score, attempt)
            switch (SelectedIsland) {
            case 2: QuizHomeScreenView.Level2Score = 2
                QuizHomeScreenView.totRight += 1
                print("Level 2 Score is 2")
                flagNo = "4"
                break
            case 4: QuizHomeScreenView.Level4Score = 2
                QuizHomeScreenView.totRight += 1
                print("Level 3 Score is 2")
                flagNo = "2"
                break
            default: break
            }
        } else if correctAnswers[SelectedIsland-1]==1 && attempt == 1{
            right = true
            score = score + 1
            level_score = 1
            print("Score and Attempt: ", score, attempt)
            switch (SelectedIsland) {
            case 2: QuizHomeScreenView.Level2Score = 1
                QuizHomeScreenView.totRight += 1
                print("Level 2 Score is 2")
                flagNo = "4"
                break
            case 4: QuizHomeScreenView.Level4Score = 1
                QuizHomeScreenView.totRight += 1
                print("Level 3 Score is 2")
                flagNo = "2"
                break
            default: break
            }
        }else if correctAnswers[SelectedIsland-1] != 1 && attempt == 1{
            level_score = 0
            switch (SelectedIsland) {
            case 1: flagNo = "5a"
                break
            case 3: flagNo = "3a"
                break
            case 5: flagNo = "1a"
                break
            default: break
            }
        }
        
        attempt+=1
        
        var alertController: UIAlertController
        if(right == true) {
            alertController = UIAlertController(title: "Feedback", message: feedback[SelectedIsland-1][0], preferredStyle: .alert)
        } else {
            if(attempt == 2) {
                alertController = UIAlertController(title: "Feedback", message: feedback[SelectedIsland-1][0], preferredStyle: .alert)
            } else {
                alertController = UIAlertController(title: "Feedback", message: "Rep_M1_Quiz_Feedback1".localized, preferredStyle: .alert)
            }
        }
        let backView = alertController.view.subviews.last?.subviews.last
        backView?.layer.cornerRadius = 10.0
        backView?.backgroundColor = UIColor.white
        
        self.present(alertController, animated: true, completion:nil)
        
        self.setFlagFor(self.flagNo)
        if right == false && attempt == 1 {
            let OKAction = UIAlertAction(title: "Try again", style: .default) { (action:UIAlertAction!) in
                print("Score and Attempt: ", self.score, self.attempt)
                if self.attempt >= 2 {
                    self.view.removeFromSuperview()
                    
                }
            }
            OKAction.setValue(UIColor.black, forKey: "titleTextColor")
            alertController.addAction(OKAction)
        } else {
            let OKAction = UIAlertAction(title: "Continue", style: .default) { (action:UIAlertAction!) in
                print("Score and Attempt: ", self.score, self.attempt)
                if self.attempt >= 2 {
                    self.view.removeFromSuperview()
                    
                }
                self.checkTotalAttempted()
                self.view.removeFromSuperview()
            }
            OKAction.setValue(UIColor.black, forKey: "titleTextColor")
            alertController.addAction(OKAction)
        }
        
    }
    
    func btn2Tapped(sender:UITapGestureRecognizer) {
//    @IBAction func Option2BtnTouch(_ sender: AnyObject) {
        
        let QuizHomeScreenView = self.navigationController!.viewControllers[0] as! Module1QuizController

        var right = false
        if correctAnswers[SelectedIsland-1]==2 && attempt == 0 {
            score = score + 2
            level_score = 2
            right = true
            print("Score and Attempt: ", score, attempt)
            switch (SelectedIsland) {
            case 1: QuizHomeScreenView.Level1Score = 2
                QuizHomeScreenView.totRight += 1
                print("Level 1 Score is 2")
                flagNo = "5"
                break
            case 3: QuizHomeScreenView.Level3Score = 2
                QuizHomeScreenView.totRight += 1
                print("Level 3 Score is 2")
                flagNo = "3"
                break
            default: break
            }
        } else if correctAnswers[SelectedIsland-1]==2 && attempt == 1{
            score = score + 1
            level_score = 1
            right = true
            switch (SelectedIsland) {
            case 1: QuizHomeScreenView.Level1Score = 1
                QuizHomeScreenView.totRight += 1
                print("Level 1 Score is 1")
                flagNo = "5"
                break
            case 3: QuizHomeScreenView.Level3Score = 1
                QuizHomeScreenView.totRight += 1
                print("Level 3 Score is 1")
                flagNo = "3"
                break
            default: break
            }
            print("Score and Attempt: ", score, attempt)
        }else if correctAnswers[SelectedIsland-1] != 2 && attempt == 1{
            level_score = 0
            switch (SelectedIsland) {
            case 2: flagNo = "4a"
                break
            case 4: flagNo = "2a"
                break
            case 5: flagNo = "1a"
                break
            default: break
            }
        }
        
        attempt+=1
        print("attempt btn1:",attempt)
        
        var alertController: UIAlertController
        if(right == true) {
            alertController = UIAlertController(title: "Feedback", message: feedback[SelectedIsland-1][1], preferredStyle: .alert)
        } else {
            if(attempt == 2) {
                alertController = UIAlertController(title: "Feedback", message: feedback[SelectedIsland-1][1], preferredStyle: .alert)
            } else {
                alertController = UIAlertController(title: "Feedback", message: "Rep_M6_Quiz_Feedback1".localized, preferredStyle: .alert)
            }
        }
        let backView = alertController.view.subviews.last?.subviews.last
        backView?.layer.cornerRadius = 10.0
        backView?.backgroundColor = UIColor.white
        
        self.present(alertController, animated: true, completion:nil)
        
        self.setFlagFor(self.flagNo)
        if right == false && attempt == 1 {
            let OKAction = UIAlertAction(title: "Try again", style: .default) { (action:UIAlertAction!) in
                print("Score and Attempt: ", self.score, self.attempt)
                if self.attempt >= 2 {
                    self.view.removeFromSuperview()
                    
                }
            }
            OKAction.setValue(UIColor.black, forKey: "titleTextColor")
            alertController.addAction(OKAction)
        } else {
            let OKAction = UIAlertAction(title: "Continue", style: .default) { (action:UIAlertAction!) in
                print("Score and Attempt: ", self.score, self.attempt)
                if self.attempt >= 2 {
                    self.view.removeFromSuperview()
                    
                }
                self.checkTotalAttempted()
                self.view.removeFromSuperview()
            }
            OKAction.setValue(UIColor.black, forKey: "titleTextColor")
            alertController.addAction(OKAction)
        }
        
    }
    
    func btn3Tapped(sender:UITapGestureRecognizer) {
//    @IBAction func Option3BtnTouch(_ sender: AnyObject) {
        
        let QuizHomeScreenView = self.navigationController!.viewControllers[0] as! Module1QuizController
        
        var right = false
        if correctAnswers[SelectedIsland-1]==3 && attempt == 0{
            score = score + 2
            level_score = 2
            right = true
            print("Score and Attempt: ", score, attempt)
            switch (SelectedIsland) {
            case 5: QuizHomeScreenView.Level5Score = 2
                QuizHomeScreenView.totRight += 1
                print("Level 5 Score is 2")
                flagNo = "1"
                break
            default: break
            }
        }else if correctAnswers[SelectedIsland-1]==3 && attempt == 1{
            score = score + 1
            level_score = 1
            right = true
            switch (SelectedIsland) {
            case 5: QuizHomeScreenView.Level5Score = 1
                QuizHomeScreenView.totRight += 1
                print("Level 5 Score is 1")
                flagNo = "1"
                break
            default: break
            }
            print("Score and Attempt: ", score, attempt)
            
        }else if correctAnswers[SelectedIsland-1] != 3 && attempt == 1{
            level_score = 0
            switch (SelectedIsland) {
            case 1: flagNo = "5a"
                break
            case 2: flagNo = "4a"
                break
            case 3: flagNo = "3a"
                break
            case 4: flagNo = "2a"
                break
            default: break
            }
        }
        
        print("attempt:",attempt)
        attempt+=1
        
        var alertController: UIAlertController
        if(right == true) {
            alertController = UIAlertController(title: "Feedback", message: feedback[SelectedIsland-1][2], preferredStyle: .alert)
        } else {
            if(attempt == 2) {
                alertController = UIAlertController(title: "Feedback", message: feedback[SelectedIsland-1][2], preferredStyle: .alert)
            } else {
                alertController = UIAlertController(title: "Feedback", message: "Rep_M1_Quiz_Feedback1".localized, preferredStyle: .alert)
            }
        }
        let backView = alertController.view.subviews.last?.subviews.last
        backView?.layer.cornerRadius = 10.0
        backView?.backgroundColor = UIColor.white
        
        self.present(alertController, animated: true, completion:nil)
        
        self.setFlagFor(self.flagNo)
        if right == false && attempt == 1 {
            let OKAction = UIAlertAction(title: "Try again", style: .default) { (action:UIAlertAction!) in
                print("Score and Attempt: ", self.score, self.attempt)
                if self.attempt >= 2 {
                    self.view.removeFromSuperview()
                    
                }
            }
            OKAction.setValue(UIColor.black, forKey: "titleTextColor")
            alertController.addAction(OKAction)
        } else {
            let OKAction = UIAlertAction(title: "Continue", style: .default) { (action:UIAlertAction!) in
                print("Score and Attempt: ", self.score, self.attempt)
                if self.attempt >= 2 {
                    self.view.removeFromSuperview()
                    
                }
                self.checkTotalAttempted()
                self.view.removeFromSuperview()
            }
            OKAction.setValue(UIColor.black, forKey: "titleTextColor")
            alertController.addAction(OKAction)
        }
        
    }
    
    func btn4Tapped(sender:UITapGestureRecognizer) {
//    @IBAction func Option4BtnTouch(_ sender: AnyObject) {
//        let QuizHomeScreenView = self.navigationController!.viewControllers[0] as! Module1QuizController
        
        var right = false
        if correctAnswers[SelectedIsland-1]==4 && attempt == 0{
            score = score + 2
            level_score = 2
            right = true
            print("Score and Attempt: ", score, attempt)
            switch (SelectedIsland) {
//            case 3: QuizHomeScreenView.Level3Score = 2
//            QuizHomeScreenView.totRight += 1
//            print("Level 3 Score is 2")
//            flagNo = "3"
//                break
//            case 4: QuizHomeScreenView.Level4Score = 2
//            QuizHomeScreenView.totRight += 1
//            print("Level 4 Score is 2")
//            flagNo = "2"
//                break
            default: break
            }
        }else if correctAnswers[SelectedIsland-1]==4 && attempt == 1{
            score = score + 1
            level_score = 1
            right = true
            switch (SelectedIsland) {
//            case 3: QuizHomeScreenView.Level3Score = 1
//            QuizHomeScreenView.totRight += 1
//            print("Level 3 Score is 1")
//            flagNo = "3"
//                break
//            case 4: QuizHomeScreenView.Level4Score = 1
//            QuizHomeScreenView.totRight += 1
//            print("Level 4 Score is 2")
//            flagNo = "2"
//                break
            default: break
            }
            print("Score and Attempt: ", score, attempt)
            
        }else if correctAnswers[SelectedIsland-1] != 3 && attempt == 1{
            level_score = 0
            switch (SelectedIsland) {
//            case 1: flagNo = "5a"
//                break
//            case 2: flagNo = "4a"
//                break
//            case 3: flagNo = "3a"
//                break
//            case 4: flagNo = "2a"
//                break
//            case 5: flagNo = "1a"
//                break
            default: break
            }
        }
        
        print("attempt:",attempt)
        attempt+=1
        
        var alertController: UIAlertController
        if(right == true) {
            alertController = UIAlertController(title: "Feedback", message: feedback[SelectedIsland-1][3], preferredStyle: .alert)
        } else {
            if(attempt == 2) {
                alertController = UIAlertController(title: "Feedback", message: feedback[SelectedIsland-1][3], preferredStyle: .alert)
            } else {
                alertController = UIAlertController(title: "Feedback", message: "Rep_M1_Quiz_Feedback1".localized, preferredStyle: .alert)
            }
        }
        let backView = alertController.view.subviews.last?.subviews.last
        backView?.layer.cornerRadius = 10.0
        backView?.backgroundColor = UIColor.white
        
        self.present(alertController, animated: true, completion:nil)
        
        self.setFlagFor(self.flagNo)
        if right == false && attempt == 1 {
            let OKAction = UIAlertAction(title: "Try again", style: .default) { (action:UIAlertAction!) in
                print("Score and Attempt: ", self.score, self.attempt)
                if self.attempt >= 2 {
                    self.view.removeFromSuperview()
                    
                }
            }
            OKAction.setValue(UIColor.black, forKey: "titleTextColor")
            alertController.addAction(OKAction)
        } else {
            let OKAction = UIAlertAction(title: "Continue", style: .default) { (action:UIAlertAction!) in
                print("Score and Attempt: ", self.score, self.attempt)
                if self.attempt >= 2 {
                    self.view.removeFromSuperview()
                    
                }
                self.checkTotalAttempted()
                self.view.removeFromSuperview()
            }
            OKAction.setValue(UIColor.black, forKey: "titleTextColor")
            alertController.addAction(OKAction)
        }
    }
    
    func checkTotalAttempted() {
        _ = self.navigationController!.viewControllers[0] as! Module1QuizController
//        if(QuizHomeScreenView.totalAttenedLevel == 5) {
//            QuizHomeScreenView.self.present(QuizHomeScreenView.alertControllerRes!, animated: true, completion:nil)
//        }
        
    }
    
    func setFlagFor(_ Flag: String){
        
        let QuizHomeScreenView = self.navigationController!.viewControllers[0] as! Module1QuizController
        
        switch SelectedIsland {
        case 1:
            print("Flag for Island 1")
            QuizHomeScreenView.score = score
            QuizHomeScreenView.lavel1FlagImgVw.image = UIImage(named: Flag)
            QuizHomeScreenView.lavel1Btn.isHidden=true
            QuizHomeScreenView.checkAttendLevelsForScore()
        case 2:
            print("Flag for Island 2")
            QuizHomeScreenView.score = score
            QuizHomeScreenView.lavel2FlagImgVw.image = UIImage(named: Flag)
            QuizHomeScreenView.lavel2Btn.isHidden=true
            QuizHomeScreenView.checkAttendLevelsForScore()
        case 3:
            print("Flag for Island 3")
            QuizHomeScreenView.score = score
            QuizHomeScreenView.lavel3FlagImgVw.image = UIImage(named: Flag)
            QuizHomeScreenView.lavel3Btn.isHidden=true
            QuizHomeScreenView.checkAttendLevelsForScore()
        case 4:
            print("Flag for Island 4")
            QuizHomeScreenView.score = score
            QuizHomeScreenView.lavel4FlagImgVw.image = UIImage(named: Flag)
            QuizHomeScreenView.lavel4Btn.isHidden=true
            QuizHomeScreenView.checkAttendLevelsForScore()
        case 5:
            print("Flag for Island 5")
            QuizHomeScreenView.score = score
            QuizHomeScreenView.lavel5FlagImgVw.image = UIImage(named: Flag)
            QuizHomeScreenView.lavel5Btn.isHidden=true
            QuizHomeScreenView.checkAttendLevelsForScore()
            
        default:
            print("No Flags")
        }
        
    }
    
}
