//
//  ModulesCollecnVwCell.swift
//  LearningApp
//
//  Created by Mac-04 on 20/08/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import UIKit

class ModulesCollecnVwCell: UICollectionViewCell {
    @IBOutlet weak var modelCellImageView: UIImageView!
    @IBOutlet weak var moduleCellHeadingLabel: UILabel!
    @IBOutlet weak var moduleRating: CosmosView!


}
