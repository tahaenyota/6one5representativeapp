//
//  ReviewInfoTableViewCell_2.swift
//  6one5Manager
//
//  Created by enyotalearning on 21/12/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit

class ReviewInfoTableViewCell_2: UITableViewCell {
    @IBOutlet weak var row2Button1: UIButton!
    @IBOutlet weak var row2Button2: UIButton!
    @IBOutlet weak var row2Button3: UIButton!
    @IBOutlet weak var row2Button4: UIButton!
    @IBOutlet weak var row2Button5: UIButton!
    @IBOutlet weak var row2Button6: UIButton!

    
    @IBOutlet weak var step1Title: UILabel!
    @IBOutlet weak var step2Title: UILabel!
    @IBOutlet weak var step3Title: UILabel!
    @IBOutlet weak var step4Title: UILabel!
    @IBOutlet weak var step5Title: UILabel!
    
    @IBOutlet weak var rowTitle: UILabel!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }
   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
