//
//  Module6IntroController.swift
//  LearningApp
//
//  Created by Sumit More on 12/7/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

//
//  Module5IntroController.swift
//  LearningApp
//
//  Created by Sumit More on 12/7/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import AVFoundation

class Module6IntroController: UIViewController, AVAudioPlayerDelegate {
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var multimediaIntro: UIButton!
    @IBOutlet weak var introductionImage: UIImageView!
    
    @IBOutlet weak var intro_m6e1l1: UILabel!
    
    @IBOutlet weak var intro_m6e2l1: UILabel!
    
    @IBOutlet weak var intro_m6e3l1: UILabel!
    @IBOutlet weak var intro_m6e3l2: UILabel!
    @IBOutlet weak var intro_m6e3l3: UILabel!
    @IBOutlet weak var intro_m6e3l4: UILabel!
    
    @IBOutlet weak var intro_m6e4b1: UIButton!
    @IBOutlet weak var intro_m6e4b2: UIButton!
    @IBOutlet weak var intro_m6e4b3: UIButton!
    @IBOutlet weak var intro_m6e4b4: UIButton!
    @IBOutlet weak var intro_m6e4b5: UIButton!
    @IBOutlet weak var intro_m6e4l1: UILabel!
    
    @IBAction func introFwdClick(_ sender: AnyObject) {
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        audioPlayerIntroAud?.pause()
    }
    
    @IBAction func bulb1Click(_ sender: AnyObject) {
        self.bulbNo = 1
        let popOverVC = UIStoryboard (name: "Module6SB", bundle: nil).instantiateViewController(withIdentifier: "m6_wQID") as! Module6IntroPopup
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        popOverVC.modalTransitionStyle = .crossDissolve
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    @IBAction func bulb2Click(_ sender: AnyObject) {
        self.bulbNo = 2
        let popOverVC = UIStoryboard (name: "Module6SB", bundle: nil).instantiateViewController(withIdentifier: "m6_wQID") as! Module6IntroPopup
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        popOverVC.modalTransitionStyle = .crossDissolve
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    @IBAction func bulb3Click(_ sender: AnyObject) {
        self.bulbNo = 3
        let popOverVC = UIStoryboard (name: "Module6SB", bundle: nil).instantiateViewController(withIdentifier: "m6_wQID") as! Module6IntroPopup
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        popOverVC.modalTransitionStyle = .crossDissolve
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    @IBAction func bulb4Click(_ sender: AnyObject) {
        self.bulbNo = 4
        let popOverVC = UIStoryboard (name: "Module6SB", bundle: nil).instantiateViewController(withIdentifier: "m6_wQID") as! Module6IntroPopup
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        popOverVC.modalTransitionStyle = .crossDissolve
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    @IBAction func bulb5Click(_ sender: AnyObject) {
        self.bulbNo = 5
        let popOverVC = UIStoryboard (name: "Module6SB", bundle: nil).instantiateViewController(withIdentifier: "m6_wQID") as! Module6IntroPopup
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        popOverVC.modalTransitionStyle = .crossDissolve
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    
    var playFlagIntroAud = 1
    var audioPlayerIntroAud: AVAudioPlayer?
    
    var GTTimer : Timer = Timer()
    var GTTimer2 : Timer = Timer()
    var isPaused = false
    var intervalVal: Int = 5
    var intervalVal2: Int = 1
    var bulbNo: Int = 0
    var questionNo: Int = 1
    
    @IBOutlet weak var introBtmLbl: UILabel!
    
    override func viewDidLoad() {
        
        self.introBtmLbl.text = "Rep_M6_IntroductionBtmLbl".localized
        
        let device = UIDevice.current.model
        print("Device type: " + device)
        
        if(device == "iPhone") {
            print("Changing size to 16")
            let attrs = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont.systemFont(ofSize: 16.0)
            ]
            self.navigationController!.navigationBar.titleTextAttributes = attrs
            
            let button: UIButton = UIButton(type: UIButtonType.custom)
            button.setImage(UIImage(named: "sof.png"), for: UIControlState.normal)
            button.backgroundColor = UIColor.white
            button.frame = CGRect(x: 0, y: 0, width: 56, height: 45)
            let barButton = UIBarButtonItem(customView: button)
            
            
            let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            negativeSpacer.width = -17;
            
            self.navigationItem.setRightBarButtonItems([negativeSpacer, barButton ], animated: false)
            
        } else {
            print("Changing size to 32")
            let attrs = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont.systemFont(ofSize: 32.0)
            ]
            self.navigationController!.navigationBar.titleTextAttributes = attrs
            
            let button: UIButton = UIButton(type: UIButtonType.custom)
            button.setImage(UIImage(named: "sof.png"), for: UIControlState.normal)
            button.backgroundColor = UIColor.white
            button.frame = CGRect(x: 0, y: 0, width: 56, height: 45)
            let barButton = UIBarButtonItem(customView: button)
            
            let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            negativeSpacer.width = -21;
            
            self.navigationItem.setRightBarButtonItems([negativeSpacer, barButton ], animated: false)
        }
        
        self.navigationItem.title = "Rep_M6_ModuleName".localized
        
        self.intro_m6e1l1.text = "Rep_Intro_M6_E1L1".localized
        
        self.intro_m6e2l1.text = "Rep_Intro_M6_E2L1".localized
        
        self.intro_m6e3l1.text = "Rep_Intro_M6_E3L1".localized
        self.intro_m6e3l2.text = "Rep_Intro_M6_E3L2".localized
        self.intro_m6e3l3.text = "Rep_Intro_M6_E3L3".localized
        self.intro_m6e3l4.text = "Rep_Intro_M6_E3L4".localized
        
        self.intro_m6e4b1.setTitle("Rep_Intro_M6_B1".localized, for: .normal)
        self.intro_m6e4b2.setTitle("Rep_Intro_M6_B2".localized, for: .normal)
        self.intro_m6e4b3.setTitle("Rep_Intro_M6_B3".localized, for: .normal)
        self.intro_m6e4b4.setTitle("Rep_Intro_M6_B4".localized, for: .normal)
        self.intro_m6e4b5.setTitle("Rep_Intro_M6_B5".localized, for: .normal)
        self.intro_m6e4l1.text = "Rep_Intro_M6_E4L1".localized

        self.introductionImage.image = UIImage(named: "rep_m6_intro1")

        self.intro_m6e1l1.isHidden = false
        self.intro_m6e2l1.isHidden = true
        self.intro_m6e3l1.isHidden = true
        self.intro_m6e3l2.isHidden = true
        self.intro_m6e3l3.isHidden = true
        self.intro_m6e3l4.isHidden = true
        self.intro_m6e4b1.isHidden = true
        self.intro_m6e4b2.isHidden = true
        self.intro_m6e4b3.isHidden = true
        self.intro_m6e4b4.isHidden = true
        self.intro_m6e4b5.isHidden = true
        self.intro_m6e4l1.isHidden = true
        
        if UserDefaults.standard.bool(forKey: "isPlusversion") == true {
            
            self.intro_m6e3l1.removeConstraints(self.intro_m6e3l1.constraints)
            
            
            let top_intro_m6e3l1 = NSLayoutConstraint(item: self.intro_m6e3l1,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M6_intro_m6e3l1Constraints_6splus[1]))
            
            
            self.intro_m6e3l2.removeConstraints(self.intro_m6e3l2.constraints)
            
            let top_intro_m6e3l2 = NSLayoutConstraint(item: self.intro_m6e3l2,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M6_intro_m6e3l2Constraints_6splus[1]))
            
            self.intro_m6e3l3.removeConstraints(self.intro_m6e3l3.constraints)
            
            
            let top_intro_m6e3l3 = NSLayoutConstraint(item: self.intro_m6e3l3,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M6_intro_m6e3l3Constraints_6splus[1]))
            
            self.intro_m6e3l4.removeConstraints(self.intro_m6e3l4.constraints)
            
            let top_intro_m6e3l4 = NSLayoutConstraint(item: self.intro_m6e3l4,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M6_intro_m6e3l4Constraints_6splus[1]))
            
            
            
            //            self.intro_m6e4b1.removeConstraints(self.intro_m6e4b1.constraints)
            
            
            
            self.intro_m6e4b5.removeConstraints(self.intro_m6e4b5.constraints)
            let lead_intro_m6e4b5 = NSLayoutConstraint(item: self.intro_m6e4b5,
                                                       attribute: NSLayoutAttribute.leading,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.view,
                                                       attribute: NSLayoutAttribute.leading,
                                                       multiplier: 1,
                                                       constant: CGFloat(Constants.M6_intro_m6e4b5Constraints_6splus[0]))
            
            let top_intro_m6e4b5 = NSLayoutConstraint(item: self.intro_m6e4b5,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M6_intro_m6e4b5Constraints_6splus[1]))
            
            let height_intro_m6e4b5 = NSLayoutConstraint(item: self.intro_m6e4b5,
                                                         attribute: NSLayoutAttribute.height,
                                                         relatedBy: NSLayoutRelation.equal,
                                                         toItem: nil,
                                                         attribute: NSLayoutAttribute.notAnAttribute,
                                                         multiplier: 1,
                                                         constant: CGFloat(Constants.M6_intro_m6e4b5Constraints_6splus[2]))
            
            let width_intro_m6e4b5 = NSLayoutConstraint(item: self.intro_m6e4b5,
                                                        attribute: NSLayoutAttribute.width,
                                                        relatedBy: NSLayoutRelation.equal,
                                                        toItem: nil,
                                                        attribute: NSLayoutAttribute.notAnAttribute,
                                                        multiplier: 1,
                                                        constant: CGFloat(Constants.M6_intro_m6e4b5Constraints_6splus[3]))
            
            
            self.view.addConstraint(lead_intro_m6e4b5)
            self.view.addConstraint(top_intro_m6e4b5)
            self.view.addConstraint(height_intro_m6e4b5)
            self.view.addConstraint(width_intro_m6e4b5)
//            self.intro_m6e4b5.backgroundColor = UIColor.red
            
            self.intro_m6e4b4.removeConstraints(self.intro_m6e4b4.constraints)
            let lead_intro_m6e4b4 = NSLayoutConstraint(item: self.intro_m6e4b4,
                                                       attribute: NSLayoutAttribute.left,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.view,
                                                       attribute: NSLayoutAttribute.left,
                                                       multiplier: 1,
                                                       constant: CGFloat(Constants.M6_intro_m6e4b4Constraints_6splus[0]))
            
            let top_intro_m6e4b4 = NSLayoutConstraint(item: self.intro_m6e4b4,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M6_intro_m6e4b4Constraints_6splus[1]))
            
            let height_intro_m6e4b4 = NSLayoutConstraint(item: self.intro_m6e4b4,
                                                         attribute: NSLayoutAttribute.height,
                                                         relatedBy: NSLayoutRelation.equal,
                                                         toItem: nil,
                                                         attribute: NSLayoutAttribute.notAnAttribute,
                                                         multiplier: 1,
                                                         constant: CGFloat(Constants.M6_intro_m6e4b4Constraints_6splus[2]))
            
            let width_intro_m6e4b4 = NSLayoutConstraint(item: self.intro_m6e4b4,
                                                        attribute: NSLayoutAttribute.width,
                                                        relatedBy: NSLayoutRelation.equal,
                                                        toItem: nil,
                                                        attribute: NSLayoutAttribute.notAnAttribute,
                                                        multiplier: 1,
                                                        constant: CGFloat(Constants.M6_intro_m6e4b4Constraints_6splus[3]))
            
            let parentViewConstraints: [NSLayoutConstraint] = self.view.constraints
            
            for parentConstraint in parentViewConstraints {
                if let viewBtn = parentConstraint.firstItem as? UIButton {
                    if viewBtn.currentTitle! == "Rep_Intro_M6_B4".localized {
                        self.view.removeConstraint(parentConstraint)
                    }
                }
            }
            
            
            self.view.addConstraint(lead_intro_m6e4b4)
            self.view.addConstraint(top_intro_m6e4b4)
            self.view.addConstraint(height_intro_m6e4b4)
            self.view.addConstraint(width_intro_m6e4b4)
//            self.intro_m6e4b4.backgroundColor = UIColor.red

            self.intro_m6e4b3.removeConstraints(self.intro_m6e4b3.constraints)
            let lead_intro_m6e4b3 = NSLayoutConstraint(item: self.intro_m6e4b3,
                                                       attribute: NSLayoutAttribute.leading,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.view,
                                                       attribute: NSLayoutAttribute.leading,
                                                       multiplier: 1,
                                                       constant: CGFloat(Constants.M6_intro_m6e4b3Constraints_6splus[0]))
            
            let top_intro_m6e4b3 = NSLayoutConstraint(item: self.intro_m6e4b3,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M6_intro_m6e4b3Constraints_6splus[1]))
            
            let height_intro_m6e4b3 = NSLayoutConstraint(item: self.intro_m6e4b3,
                                                         attribute: NSLayoutAttribute.height,
                                                         relatedBy: NSLayoutRelation.equal,
                                                         toItem: nil,
                                                         attribute: NSLayoutAttribute.notAnAttribute,
                                                         multiplier: 1,
                                                         constant: CGFloat(Constants.M6_intro_m6e4b3Constraints_6splus[2]))
            
            let width_intro_m6e4b3 = NSLayoutConstraint(item: self.intro_m6e4b3,
                                                        attribute: NSLayoutAttribute.width,
                                                        relatedBy: NSLayoutRelation.equal,
                                                        toItem: nil,
                                                        attribute: NSLayoutAttribute.notAnAttribute,
                                                        multiplier: 1,
                                                        constant: CGFloat(Constants.M6_intro_m6e4b3Constraints_6splus[3]))
            
            
            self.view.addConstraint(lead_intro_m6e4b3)
            self.view.addConstraint(top_intro_m6e4b3)
            self.view.addConstraint(height_intro_m6e4b3)
            self.view.addConstraint(width_intro_m6e4b3)
//            self.intro_m6e4b3.backgroundColor = UIColor.red

            
            self.intro_m6e4b2.removeConstraints(self.intro_m6e4b2.constraints)
            let lead_intro_m6e4b2 = NSLayoutConstraint(item: self.intro_m6e4b2,
                                                       attribute: NSLayoutAttribute.left,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.view,
                                                       attribute: NSLayoutAttribute.left,
                                                       multiplier: 1,
                                                       constant: CGFloat(Constants.M6_intro_m6e4b2Constraints_6splus[0]))
            
            let top_intro_m6e4b2 = NSLayoutConstraint(item: self.intro_m6e4b2,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M6_intro_m6e4b2Constraints_6splus[1]))
            
            let height_intro_m6e4b2 = NSLayoutConstraint(item: self.intro_m6e4b2,
                                                         attribute: NSLayoutAttribute.height,
                                                         relatedBy: NSLayoutRelation.equal,
                                                         toItem: nil,
                                                         attribute: NSLayoutAttribute.notAnAttribute,
                                                         multiplier: 1,
                                                         constant: CGFloat(Constants.M6_intro_m6e4b2Constraints_6splus[2]))
            
            let width_intro_m6e4b2 = NSLayoutConstraint(item: self.intro_m6e4b2,
                                                        attribute: NSLayoutAttribute.width,
                                                        relatedBy: NSLayoutRelation.equal,
                                                        toItem: nil,
                                                        attribute: NSLayoutAttribute.notAnAttribute,
                                                        multiplier: 1,
                                                        constant: CGFloat(Constants.M6_intro_m6e4b2Constraints_6splus[3]))
            
            
            let parentViewConstraints2: [NSLayoutConstraint] = self.view.constraints
            
            for parentConstraint in parentViewConstraints2 {
                if let viewBtn = parentConstraint.firstItem as? UIButton {
                    if viewBtn.currentTitle! == "Rep_Intro_M6_B2".localized {
                        self.view.removeConstraint(parentConstraint)
                    }
                }
            }
            
            
            self.view.addConstraint(lead_intro_m6e4b2)
            self.view.addConstraint(top_intro_m6e4b2)
            self.view.addConstraint(height_intro_m6e4b2)
            self.view.addConstraint(width_intro_m6e4b2)
//            self.intro_m6e4b2.backgroundColor = UIColor.red

            // M6_intro_m6e4b1Constraints_6splus
            
            self.intro_m6e4b1.removeConstraints(self.intro_m6e4b1.constraints)
            let lead_intro_m6e4b1 = NSLayoutConstraint(item: self.intro_m6e4b1,
                                                       attribute: NSLayoutAttribute.leading,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.view,
                                                       attribute: NSLayoutAttribute.leading,
                                                       multiplier: 1,
                                                       constant: CGFloat(Constants.M6_intro_m6e4b1Constraints_6splus[0]))
            
            let top_intro_m6e4b1 = NSLayoutConstraint(item: self.intro_m6e4b1,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M6_intro_m6e4b1Constraints_6splus[1]))
            
            let height_intro_m6e4b1 = NSLayoutConstraint(item: self.intro_m6e4b1,
                                                         attribute: NSLayoutAttribute.height,
                                                         relatedBy: NSLayoutRelation.equal,
                                                         toItem: nil,
                                                         attribute: NSLayoutAttribute.notAnAttribute,
                                                         multiplier: 1,
                                                         constant: CGFloat(Constants.M6_intro_m6e4b1Constraints_6splus[2]))
            
            let width_intro_m6e4b1 = NSLayoutConstraint(item: self.intro_m6e4b1,
                                                        attribute: NSLayoutAttribute.width,
                                                        relatedBy: NSLayoutRelation.equal,
                                                        toItem: nil,
                                                        attribute: NSLayoutAttribute.notAnAttribute,
                                                        multiplier: 1,
                                                        constant: CGFloat(Constants.M6_intro_m6e4b1Constraints_6splus[3]))
            
            
            self.view.addConstraint(lead_intro_m6e4b1)
            self.view.addConstraint(top_intro_m6e4b1)
            self.view.addConstraint(height_intro_m6e4b1)
            self.view.addConstraint(width_intro_m6e4b1)
//            self.intro_m6e4b1.backgroundColor = UIColor.red

            self.view.addConstraint(top_intro_m6e3l4)
            self.view.addConstraint(top_intro_m6e3l3)
            self.view.addConstraint(top_intro_m6e3l2)
            self.view.addConstraint(top_intro_m6e3l1)

        }
        
        intervalVal = 6
        self.bulbNo = 1

        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module6IntroController.screen2), userInfo: nil, repeats: false)
        
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module6IntroController.monitorTimer), userInfo: nil, repeats: true)
        
        if self.revealViewController() != nil {
            menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: UIControlEvents.touchUpInside)
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            
        }
        
        do{
            let urlVideo = Bundle.main.url(forResource: "REP_M06_S01", withExtension: "mp3")
            audioPlayerIntroAud = try AVAudioPlayer(contentsOf: urlVideo!)
            audioPlayerIntroAud!.delegate = self
            audioPlayerIntroAud!.prepareToPlay()
            audioPlayerIntroAud!.play()
        } catch {
            print("Error getting the audio file")
        }
        
    }
    
    func monitorTimer() {
        self.intervalVal = self.intervalVal - 1
        print("Interval in monitor: " + String(self.intervalVal))
    }
    
    func screen2(){
        
        print("In screen2")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "rep_m6_intro2")
        
        self.intro_m6e1l1.isHidden = true
        self.intro_m6e2l1.isHidden = false
        self.intro_m6e3l1.isHidden = true
        self.intro_m6e3l2.isHidden = true
        self.intro_m6e3l3.isHidden = true
        self.intro_m6e3l4.isHidden = true
        self.intro_m6e4b1.isHidden = true
        self.intro_m6e4b2.isHidden = true
        self.intro_m6e4b3.isHidden = true
        self.intro_m6e4b4.isHidden = true
        self.intro_m6e4b5.isHidden = true
        self.intro_m6e4l1.isHidden = true

        self.intervalVal = 7
        self.bulbNo = 2
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module6IntroController.screen3_1), userInfo: nil, repeats: false)
        
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module6IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen3_1(){
        
        print("In screen3.1")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "rep_m6_intro31")
        
        self.intro_m6e1l1.isHidden = true
        self.intro_m6e2l1.isHidden = true
        self.intro_m6e3l1.isHidden = false
        self.intro_m6e3l2.isHidden = true
        self.intro_m6e3l3.isHidden = true
        self.intro_m6e3l4.isHidden = true
        self.intro_m6e4b1.isHidden = true
        self.intro_m6e4b2.isHidden = true
        self.intro_m6e4b3.isHidden = true
        self.intro_m6e4b4.isHidden = true
        self.intro_m6e4b5.isHidden = true
        self.intro_m6e4l1.isHidden = true
        
        UIView.transition(with: self.intro_m6e3l1,
                          duration: 1.0,
                          options: [.transitionCrossDissolve],
                          animations: {
                            self.intro_m6e3l1.text = "Rep_Intro_M6_E3L1".localized
            }, completion: nil)
        
        self.intervalVal = 4
        self.bulbNo = 31

        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module6IntroController.screen3_2), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module6IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }

    func screen3_2(){
        
        print("In screen3.2")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "rep_m6_intro32")
        
        self.intro_m6e1l1.isHidden = true
        self.intro_m6e2l1.isHidden = true
        self.intro_m6e3l1.isHidden = false
        self.intro_m6e3l2.isHidden = false
        self.intro_m6e3l3.isHidden = true
        self.intro_m6e3l4.isHidden = true
        self.intro_m6e4b1.isHidden = true
        self.intro_m6e4b2.isHidden = true
        self.intro_m6e4b3.isHidden = true
        self.intro_m6e4b4.isHidden = true
        self.intro_m6e4b5.isHidden = true
        self.intro_m6e4l1.isHidden = true
        
        UIView.transition(with: self.intro_m6e3l2,
                          duration: 1.0,
                          options: [.transitionCrossDissolve],
                          animations: {
                            self.intro_m6e3l2.text = "Rep_Intro_M6_E3L2".localized
            }, completion: nil)
        
        self.intervalVal = 6
        self.bulbNo = 32
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module6IntroController.screen3_3), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module6IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }

    func screen3_3(){
        
        print("In screen3.3")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "rep_m6_intro33")
        
        self.intro_m6e1l1.isHidden = true
        self.intro_m6e2l1.isHidden = true
        self.intro_m6e3l1.isHidden = false
        self.intro_m6e3l2.isHidden = false
        self.intro_m6e3l3.isHidden = false
        self.intro_m6e3l4.isHidden = true
        self.intro_m6e4b1.isHidden = true
        self.intro_m6e4b2.isHidden = true
        self.intro_m6e4b3.isHidden = true
        self.intro_m6e4b4.isHidden = true
        self.intro_m6e4b5.isHidden = true
        self.intro_m6e4l1.isHidden = true
        
        UIView.transition(with: self.intro_m6e3l3,
                          duration: 1.0,
                          options: [.transitionCrossDissolve],
                          animations: {
                            self.intro_m6e3l3.text = "Rep_Intro_M6_E3L3".localized
            }, completion: nil)
        
        self.intervalVal = 9
        self.bulbNo = 33
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module6IntroController.screen3_4), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module6IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }

    func screen3_4(){
        
        print("In screen3.4")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "rep_m6_intro3")
        
        self.intro_m6e1l1.isHidden = true
        self.intro_m6e2l1.isHidden = true
        self.intro_m6e3l1.isHidden = false
        self.intro_m6e3l2.isHidden = false
        self.intro_m6e3l3.isHidden = false
        self.intro_m6e3l4.isHidden = false
        self.intro_m6e4b1.isHidden = true
        self.intro_m6e4b2.isHidden = true
        self.intro_m6e4b3.isHidden = true
        self.intro_m6e4b4.isHidden = true
        self.intro_m6e4b5.isHidden = true
        self.intro_m6e4l1.isHidden = true
        
        UIView.transition(with: self.intro_m6e3l4,
                          duration: 1.0,
                          options: [.transitionCrossDissolve],
                          animations: {
                            self.intro_m6e4l1.text = "Rep_Intro_M6_E4L1".localized
            }, completion: nil)
        
        self.intervalVal = 6
        self.bulbNo = 34

        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module6IntroController.screen4_1), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module6IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }

    func screen4_1(){
        
        print("In screen4.1")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "rep_m6_intro4")
        
        self.intro_m6e1l1.isHidden = true
        self.intro_m6e2l1.isHidden = true
        self.intro_m6e3l1.isHidden = true
        self.intro_m6e3l2.isHidden = true
        self.intro_m6e3l3.isHidden = true
        self.intro_m6e3l4.isHidden = true
        self.intro_m6e4b1.isHidden = false
        self.intro_m6e4b2.isHidden = false
        self.intro_m6e4b3.isHidden = false
        self.intro_m6e4b4.isHidden = false
        self.intro_m6e4b5.isHidden = false
        self.intro_m6e4l1.isHidden = true
        
        self.intro_m6e4b1.isEnabled = false
        self.intro_m6e4b2.isEnabled = false
        self.intro_m6e4b3.isEnabled = false
        self.intro_m6e4b4.isEnabled = false
        self.intro_m6e4b5.isEnabled = false

        self.intervalVal = 4
        self.bulbNo = 41
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module6IntroController.screen4_2), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module6IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }

    func screen4_2(){
        
        print("In screen4.2")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.intro_m6e4l1.isHidden = false
        
        UIView.transition(with: self.intro_m6e4l1,
                          duration: 1.0,
                          options: [.transitionCrossDissolve],
                          animations: {
                            self.intro_m6e4l1.text = "Rep_Intro_M6_E4L1".localized
            }, completion: nil)

        self.intro_m6e4b1.isEnabled = false
        self.intro_m6e4b2.isEnabled = false
        self.intro_m6e4b3.isEnabled = false
        self.intro_m6e4b4.isEnabled = false
        self.intro_m6e4b5.isEnabled = false
        
        self.intervalVal = 6
        self.bulbNo = 41
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module6IntroController.screen4_3), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module6IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }
    
    func screen4_3(){
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        print("In screen4.3")
        
        self.intro_m6e4b1.isEnabled = true
        self.intro_m6e4b2.isEnabled = true
        self.intro_m6e4b3.isEnabled = true
        self.intro_m6e4b4.isEnabled = true
        self.intro_m6e4b5.isEnabled = true
        
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("In Finish Playing")
        playFlagIntroAud = 3
        if let image = UIImage(named: "ic_replay_white_36pt.png") {
            multimediaIntro.setImage(image, for: .normal)
        }
    }
    
    @IBAction func multimediaIntroClicked(_ sender: AnyObject) {
        //        let layer = introductionImage.layer
        
        if (playFlagIntroAud == 3) {
            
            self.introductionImage.image = UIImage(named: "rep_m6_intro1")
            
            self.intro_m6e1l1.isHidden = false
            self.intro_m6e2l1.isHidden = true
            self.intro_m6e3l1.isHidden = true
            self.intro_m6e3l2.isHidden = true
            self.intro_m6e3l3.isHidden = true
            self.intro_m6e3l4.isHidden = true
            self.intro_m6e4b1.isHidden = true
            self.intro_m6e4b2.isHidden = true
            self.intro_m6e4b3.isHidden = true
            self.intro_m6e4b4.isHidden = true
            self.intro_m6e4b5.isHidden = true
            self.intro_m6e4l1.isHidden = true
            
            self.GTTimer.invalidate()
            self.GTTimer2.invalidate()
            
            intervalVal = 6
            self.bulbNo = 1
            print("In screen 1 after replay")
            let timeInterval: TimeInterval = TimeInterval(intervalVal)
            self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module6IntroController.screen2), userInfo: nil, repeats: false)
            
            let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module6IntroController.monitorTimer), userInfo: nil, repeats: true)
            
            playFlagIntroAud = 1
            if let image = UIImage(named: "ic_pause_white_36pt.png") {
                multimediaIntro.setImage(image, for: .normal)
                audioPlayerIntroAud?.play()
                //                resumeLayer(layer: layer)
            }
        } else if (playFlagIntroAud == 2) {
            
            let timeInterval: TimeInterval = TimeInterval(self.intervalVal)
            print("Interval: " + String(self.intervalVal))
            
            switch self.bulbNo {
            case 1: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module6IntroController.screen2), userInfo: nil, repeats: false)
                let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
                self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module6IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 2: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module6IntroController.screen3_1), userInfo: nil, repeats: false)
                let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
                self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module6IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 31: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module6IntroController.screen3_2), userInfo: nil, repeats: false)
                let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
                self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module6IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 32: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module6IntroController.screen3_3), userInfo: nil, repeats: false)
                let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
                self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module6IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 33: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module6IntroController.screen3_4), userInfo: nil, repeats: false)
                let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
                self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module6IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 34: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module6IntroController.screen4_1), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module6IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 41: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module6IntroController.screen4_2), userInfo: nil, repeats: false)
                let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
                self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module6IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 42: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module6IntroController.screen4_3), userInfo: nil, repeats: false)
                let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
                self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module6IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            default: break
            }
            
            playFlagIntroAud = 1
            if let image = UIImage(named: "ic_pause_white_36pt.png") {
                multimediaIntro.setImage(image, for: .normal)
                audioPlayerIntroAud?.play()
            }
        } else {
            
            self.GTTimer.invalidate()
            self.GTTimer2.invalidate()
            
            playFlagIntroAud = 2
            if let image = UIImage(named: "ic_play_arrow_white_36pt.png") {
                multimediaIntro.setImage(image, for: .normal)
                audioPlayerIntroAud?.pause()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        audioPlayerIntroAud?.stop()
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
    }
    
}
