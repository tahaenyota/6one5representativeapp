//
//  ReviewInfoTableViewCell_3.swift
//  6one5Manager
//
//  Created by enyotalearning on 21/12/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit

class ReviewInfoTableViewCell_3: UITableViewCell
{
    @IBOutlet weak var row3Button1: UIButton!
    @IBOutlet weak var row3Button2: UIButton!
    @IBOutlet weak var row3Button3: UIButton!
    @IBOutlet weak var row3Button4: UIButton!
    @IBOutlet weak var row3Button5: UIButton!
    
    
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label4: UILabel!
    @IBOutlet weak var label5: UILabel!
    
    @IBOutlet weak var rowTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
