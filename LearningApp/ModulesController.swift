//
//  ModulesController.swift
//  LearningApp
//
//  Created by Sumit More on 12/14/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import AVFoundation

class ModulesController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    var user_id: String = "61"
    var course_id: String = "2"
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBAction func introBtnClick(_ sender: AnyObject) {
        let module_id: String = "0"
        let prefs:UserDefaults = UserDefaults.standard
        prefs.set(module_id, forKey: "module_id")
        prefs.synchronize()
        
        let storyboard: UIStoryboard = UIStoryboard(name: "Module0SB", bundle: nil)
        let initViewController: UIViewController = storyboard.instantiateViewController(withIdentifier: "Module0InitiateID") as UIViewController
        self.present(initViewController, animated: true, completion: nil)

    }
    override func viewDidLoad() {
        
        collectionView!.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 2, right: 10)
        
        //let userImg  = UIImage(named: "user_pic2.png")
        //let imageView = UIImageView(image:userImg)
        
        //imageView.layer.cornerRadius = imageView.frame.size.width / 2;
        //imageView.clipsToBounds = true;
        
        //imageView.layer.borderWidth = 2;
        //imageView.layer.borderColor = UIColor.white.cgColor
        
//        let button = UIButton()ßß®
//        button.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
//        let color = UIColor(patternImage: UIImage(named: "user_pic2.png")!)
//        button.backgroundColor = color
//        let barButton = UIBarButtonItem()
//        barButton.customView = button
//        self.navigationItem.rightBarButtonItem = barButton
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self

        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        updateModuleData()
    }
    
    var listOfImages: [UIImage] = [ UIImage(named: "images1")!,
                                    UIImage(named: "images2")!,
                                    UIImage(named: "images3")!,
                                    UIImage(named: "images4")!,
                                    UIImage(named: "images5")!,
                                    UIImage(named: "images6")!
    ]
    
    var moduleTitle = [ "Rep_Module1".localized, "Rep_Module2".localized, "Rep_Module3".localized, "Rep_Module4".localized, "Rep_Module5".localized, "Rep_Module6".localized ]
    var moduleSubTitle = ["Not started","Not started","Not started","Not started","Not started","Not started"]
    var moduleRating = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    
    fileprivate let reuseIdentifier = "moduleCell2"
    //    private let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listOfImages.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ModuleCellController
        
        cell.backgroundColor = UIColor.white
        cell.modelCellImageView.image = listOfImages[(indexPath as NSIndexPath).row]
        cell.moduleCellHeadingLabel.text = moduleTitle[(indexPath as NSIndexPath).row] + " (" + moduleSubTitle[(indexPath as NSIndexPath).row] + ")"
        print("Index: \((indexPath as NSIndexPath).row)")
        cell.moduleRating.rating = moduleRating[(indexPath as NSIndexPath).row]
        
        // Configure the cell
        
        cell.layer.shadowColor = UIColor.gray.cgColor;
        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0);
        cell.layer.shadowRadius = 2.0;
        cell.layer.shadowOpacity = 1.0;
        cell.layer.masksToBounds = false;
        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath;
        
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(2.0 - 1))
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(2.0))
        let device = UIDevice.current.model
        if(device == "iPhone") {
            return CGSize(width: size - 10, height: 181)
        } else {
            return CGSize(width: size - 10, height: 321)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print((indexPath as NSIndexPath).row)
        if((indexPath as NSIndexPath).row == 0) {
            print("Here!")
            let module_id: String = "1"
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set(module_id, forKey: "module_id")
            prefs.synchronize()
            if moduleSubTitle[(indexPath as NSIndexPath).row] == "Not started"{
                
                self.updateLatestModuledata(moduleid: "1")
                
            }
            let storyboard: UIStoryboard = UIStoryboard(name: "Module1SB", bundle: nil)
            let initViewController: UIViewController = storyboard.instantiateViewController(withIdentifier: "Module1InitiateID") as UIViewController
            self.present(initViewController, animated: true, completion: nil)
        } else if((indexPath as NSIndexPath).row == 1) {
            print("Here!")
            let module_id: String = "2"
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set(module_id, forKey: "module_id")
            prefs.synchronize()
            if moduleSubTitle[(indexPath as NSIndexPath).row] == "Not started"{
                
                self.updateLatestModuledata(moduleid: "2")
                
            }
            let storyboard: UIStoryboard = UIStoryboard(name: "Module2SB", bundle: nil)
            let initViewController: UIViewController = storyboard.instantiateViewController(withIdentifier: "Module2InitiateID") as UIViewController
            self.present(initViewController, animated: true, completion: nil)
        } else if((indexPath as NSIndexPath).row == 2) {
            print("Here!")
            let module_id: String = "3"
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set(module_id, forKey: "module_id")
            prefs.synchronize()
            if moduleSubTitle[(indexPath as NSIndexPath).row] == "Not started"{
                
                self.updateLatestModuledata(moduleid: "3")
                
            }
            let storyboard: UIStoryboard = UIStoryboard(name: "Module3SB", bundle: nil)
            let initViewController: UIViewController = storyboard.instantiateViewController(withIdentifier: "Module3InitiateID") as UIViewController
            self.present(initViewController, animated: true, completion: nil)
        } else if((indexPath as NSIndexPath).row == 3) {
            print("Here!")
            let module_id: String = "4"
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set(module_id, forKey: "module_id")
            prefs.synchronize()
            if moduleSubTitle[(indexPath as NSIndexPath).row] == "Not started"{
                
                self.updateLatestModuledata(moduleid: "4")
                
            }
            let storyboard: UIStoryboard = UIStoryboard(name: "Module4SB", bundle: nil)
            let initViewController: UIViewController = storyboard.instantiateViewController(withIdentifier: "Module4InitiateID") as UIViewController
            self.present(initViewController, animated: true, completion: nil)
        } else if((indexPath as NSIndexPath).row == 4) {
            print("Here!")
            let module_id: String = "5"
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set(module_id, forKey: "module_id")
            prefs.synchronize()
            if moduleSubTitle[(indexPath as NSIndexPath).row] == "Not started"{
                
                self.updateLatestModuledata(moduleid: "5")
                
            }
            let storyboard: UIStoryboard = UIStoryboard(name: "Module5SB", bundle: nil)
            let initViewController: UIViewController = storyboard.instantiateViewController(withIdentifier: "Module5InitiateID") as UIViewController
            self.present(initViewController, animated: true, completion: nil)
        } else if((indexPath as NSIndexPath).row == 5) {
            print("Here!")
            let module_id: String = "6"
            let prefs:UserDefaults = UserDefaults.standard
            prefs.set(module_id, forKey: "module_id")
            prefs.synchronize()
            if moduleSubTitle[(indexPath as NSIndexPath).row] == "Not started"{
                
                self.updateLatestModuledata(moduleid: "6")
                
            }
            let storyboard: UIStoryboard = UIStoryboard(name: "Module6SB", bundle: nil)
            let initViewController: UIViewController = storyboard.instantiateViewController(withIdentifier: "Module6InitiateID") as UIViewController
            self.present(initViewController, animated: true, completion: nil)
        }
        
    }
    
    func updateModuleData() {
        
        let progressHUD = ProgressHUD()
        self.view.addSubview(progressHUD)
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            ]
        
        let prefs:UserDefaults = UserDefaults.standard
        user_id = prefs.value(forKey: "user_id") as! String!
        course_id = prefs.value(forKey: "course_id") as! String!
        
        let moduleURL =  Constants.salesRepModulesInfoURL + user_id + "/" + course_id
        let request = NSMutableURLRequest(url: NSURL(string: moduleURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("Error: \(error)")
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
            } else {
                let httpResponse = response as? HTTPURLResponse
                print("HTTP Response: \(httpResponse)")
                
                do {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        print("Print converted dictionary",convertedJsonIntoDict)
                        
                        // Get value by key
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        print(statusVal!)
                        
                        let prefs:UserDefaults = UserDefaults.standard

                        if(statusVal! == true)
                        {
                            OperationQueue.main.addOperation{
                                progressHUD.hide()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                if let modules = convertedJsonIntoDict["module"] as? NSDictionary {
                                    print("Modules: \(modules)")
                                    if let module1Data = modules["1"] as? NSDictionary {
                                        print("Module 1 Data: \(module1Data)")
                                        let mod1Title: String = (module1Data["status"] as! String)
                                        //                                        let mod1Title: String = (String(describing: module1Data["status"]))
                                        if mod1Title == "Module_Completeness".localized {
                                            prefs.set("attempted", forKey: "module1_attempted")
                                        }
                                        self.moduleSubTitle[0] = mod1Title
                                        self.moduleRating[0] = (module1Data["manager_review"] as! Double)
                                    }
                                    if let module2Data = modules["2"] as? NSDictionary {
                                        print("Module 2 Data: \(module2Data)")
                                        let mod2Title: String = (module2Data["status"] as! String)
                                        //                                        let mod2Title: String = (String(describing: module2Data["status"]))
                                        if mod2Title == "Module_Completeness".localized {
                                            prefs.set("attempted", forKey: "module2_attempted")
                                        }
                                        self.moduleSubTitle[1] = mod2Title
                                        self.moduleRating[1] = (module2Data["manager_review"] as! Double)
                                    }
                                    if let module3Data = modules["3"] as? NSDictionary {
                                        print("Module 3 Data: \(module3Data)")
                                        let mod3Title: String = (module3Data["status"] as! String)
                                        //                                        let mod3Title: String = (String(describing: module3Data["status"]))
                                        if mod3Title == "Module_Completeness".localized {
                                            prefs.set("attempted", forKey: "module3_attempted")
                                        }
                                        self.moduleSubTitle[2] = mod3Title
                                        self.moduleRating[2] = (module3Data["manager_review"] as! Double)
                                    }
                                    if let module4Data = modules["4"] as? NSDictionary {
                                        print("Module 4 Data: \(module4Data)")
                                        let mod4Title: String = (module4Data["status"] as! String)
                                        //                                        let mod4Title: String = (String(describing: module4Data["status"]))
                                        if mod4Title == "Module_Completeness".localized {
                                            prefs.set("attempted", forKey: "module4_attempted")
                                        }
                                        self.moduleSubTitle[3] = mod4Title
                                        self.moduleRating[3] = (module4Data["manager_review"] as! Double)
                                    }
                                    if let module5Data = modules["5"] as? NSDictionary {
                                        print("Module 5 Data: \(module5Data)")
                                        let mod5Title: String = (module5Data["status"] as! String)
                                        //                                        let mod5Title: String = (String(describing: module5Data["status"]))
                                        if mod5Title == "Module_Completeness".localized {
                                            prefs.set("attempted", forKey: "module5_attempted")
                                        }
                                        self.moduleSubTitle[4] = mod5Title
                                        self.moduleRating[4] = (module5Data["manager_review"] as! Double)
                                    }
                                    if let module6Data = modules["6"] as? NSDictionary {
                                        print("Module 6 Data: \(module6Data)")
                                        let mod6Title: String = (module6Data["status"] as! String)
                                        //                                        let mod6Title: String = (String(describing: module6Data["status"]))
                                        if mod6Title == "Module_Completeness".localized {
                                            prefs.set("attempted", forKey: "module6_attempted")
                                        }
                                        self.moduleSubTitle[5] = mod6Title
                                        self.moduleRating[5] = (module6Data["manager_review"] as! Double)
                                    }
                                    
                                    self.collectionView!.reloadData()
                                    
                                }
                            }
                        } else {
                            progressHUD.hide()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            var error_msg:NSString
                            if convertedJsonIntoDict["message"] as? NSString != nil {
                                error_msg = convertedJsonIntoDict["message"] as! NSString
                            } else {
                                error_msg = "Unknown Error"
                            }
                            print("error_msg",error_msg)
                        }
                    }
                } catch let error as NSError {
                    print(error)
                    progressHUD.hide()
                    UIApplication.shared.endIgnoringInteractionEvents()
                }
                
            }
        })
        
        dataTask.resume()
    }
    
    func updateLatestModuledata(moduleid:String)
        
    {
        
        let headers = [
            
            "cache-control": "no-cache",
            
            ]
        
        let login = Login.sharedInstance
        
        let prefs:UserDefaults = UserDefaults.standard
        let courseId = prefs.value(forKey: "course_id") as! String!
       

        user_id = login.getManagerId()//prefs.value(forKey: "user_id") as! String!
        
        course_id = courseId!
        
        
        
        let moduleURL =  Constants.salesManagerLatestModuleInfo + user_id + "/" + course_id + "/" +  moduleid
        
        let request = NSMutableURLRequest(url: NSURL(string: moduleURL)! as URL,
                                          
                                          cachePolicy: .useProtocolCachePolicy,
                                          
                                          timeoutInterval: 10.0)
        
        
        
        request.httpMethod = "GET"
        
        request.allHTTPHeaderFields = headers
        
        
        
        let session = URLSession.shared
        
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            if (error != nil) {
                
                print("Error: \(error)")
                
            } else {
                
                let httpResponse = response as? HTTPURLResponse
                
                print("HTTP Response: \(httpResponse)")
                
                
                
                do {
                    
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                        
                    {
                        
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        
                        //                        let reviewNotificationData = ReviewNotificationData.sharedInstance
                        
                        //
                        
                        //
                        
                        //
                        
                        //
                        
                        //                        if statusVal == nil
                        
                        //                        {
                        
                        //                            print("status val is null")
                        
                        //                            let statusVal = convertedJsonIntoDict["status"] as? String
                        
                        //                            print("status valule is \(statusVal)")
                        
                        //                            let login = Login.sharedInstance
                        
                        //                            login.status = String(describing: convertedJsonIntoDict["status"] as? Bool)
                        
                        //                            login.message = convertedJsonIntoDict["message"] as? String
                        
                        //
                        
                        //
                        
                        //                        }
                        
                        //                        else
                        
                        //                        {
                        
                        //                            //if(statusVal)!
                        
                        //                            {
                        
                        //
                        
                        //                                var count = convertedJsonIntoDict["count"] as? Int
                        
                        //                                count = count! - 1
                        
                        //                                for index in 0...count!
                        
                        ////                                {
                        
                        ////
                        
                        ////                                    if let module1Data = convertedJsonIntoDict["i\(index)"] as? NSDictionary
                        
                        ////                                    {
                        
                        ////
                        
                        ////                                        //print(" --- UserName \( module1Data["user_id"])")
                        
                        ////                                        let reviewDetails:ReviewDetails = ReviewDetails()
                        
                        ////                                        let userId:String = (module1Data["user_id"] as? String)!
                        
                        ////                                        let user_name:String = (module1Data["user_name"] as? String)!
                        
                        ////                                        let module_id:String = (module1Data["module_id"] as? String)!
                        
                        ////                                        let module_name:String = (module1Data["module_name"] as? String)!
                        
                        ////                                        //var course_id:String = (String(module1Data["course_id"]) as? String)!
                        
                        ////                                        
                        
                        ////                                        //print(" &&& UserName \( userId)")
                        
                        ////                                        reviewDetails.setUp(userId: userId,
                        
                        ////                                                            userName: user_name,
                        
                        ////                                                            moduleId: module_id,
                        
                        ////                                                            moduleName: module_name,
                        
                        ////                                                            courseId: "1" )
                        
                        ////                                        //reviewDetails.desc();
                        
                        ////                                        /*for (key, value) in module1Data
                        
                        ////                                         {
                        
                        ////                                         //print("quizData: Value: \(value) for key: \(key)")
                        
                        ////                                         }*/
                        
                        ////                                        reviewNotificationData.addReviewDetails(reviewDetails: reviewDetails)
                        
                        ////                                        
                        
                        ////                                    }
                        
                        ////                                }
                        
                        //                                
                        
                        //                                
                        
                        //                                
                        
                        //                                
                        
                        //                            }// end of if
                        
                        // }
                        
                        
                        
                        // completionHandler(reviewNotificationData)
                        
                    }
                    
                    
                    
                } catch let error as NSError
                    
                {
                    
                    print(error)
                    
                }
                
                
                
                
                
            }
            
            
            
            
            
        })
        
        
        
        dataTask.resume()
        
        
        
    }

    
}
