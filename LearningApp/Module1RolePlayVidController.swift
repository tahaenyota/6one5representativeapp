//
//  Module1VidController.swift
//  LearningApp
//
//  Created by Sumit More on 12/10/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import AVFoundation

class Module1RolePlayVidController: UIViewController, AVAudioPlayerDelegate {
    
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var multimediaRPVButton: UIButton!
    @IBOutlet weak var multimediaRPVAudio: UIButton!
    @IBOutlet weak var rp_lbl: UILabel!
    @IBOutlet weak var playPauseVid: UIButton!
    
    @IBAction func playPauseVidClicked(_ sender: AnyObject) {
        if (playerFlag == 1) {
            print("Will play the vid")
            playerFlag = 2
            player?.play()
            let butImg = UIImage(named: "pause_video.png")
            playPauseVid.setImage(butImg, for: .normal)
            
            if playFlagRPVAud == 1 {
                playFlagRPVAud = 2
                if let image = UIImage(named: "ic_play_arrow_white_36pt.png") {
                    multimediaRPVAudio.setImage(image, for: .normal)
                    audioPlayerRPVAud?.pause()
                }
                
            }
            
        } else {
            playerFlag = 1
            print("Will pause the vid")
            player?.pause()
            let butImg = UIImage(named: "play_video.png")
            playPauseVid.setImage(butImg, for: .normal)
        }
        
    }
    
    let playerViewController = AVPlayerViewController()
    var player: AVPlayer?
    var playerFlag = 1
    var playerQ: AVQueuePlayer?
    
    var playFlagRPVAud = 1
    var audioPlayerRPVAud: AVAudioPlayer?
    
    func addBoldText(fullString: NSString, boldPartOfString: NSString, font: UIFont!, boldFont: UIFont!) -> NSAttributedString {
        let nonBoldFontAttribute = [NSFontAttributeName:font!]
        let boldFontAttribute = [NSFontAttributeName:boldFont!]
        let boldString = NSMutableAttributedString(string: fullString as String, attributes:nonBoldFontAttribute)
        boldString.addAttributes(boldFontAttribute, range: fullString.range(of: boldPartOfString as String))
        return boldString
    }
    
    @IBOutlet weak var videoBtmLbl: UILabel!
    
    override func viewDidLoad() {
        
        let device = UIDevice.current.model
        if(device == "iPhone") {
            print("Changing size to 16")
            let attrs = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont.systemFont(ofSize: 16.0)
            ]
            self.navigationController!.navigationBar.titleTextAttributes = attrs
            
            let button: UIButton = UIButton(type: UIButtonType.custom)
            button.setImage(UIImage(named: "sof.png"), for: UIControlState.normal)
            button.backgroundColor = UIColor.white
            //set frame
            button.frame = CGRect(x: 0, y: 0, width: 56, height: 45)
            let barButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            
            
            let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            negativeSpacer.width = -17;
            
            self.navigationItem.setRightBarButtonItems([negativeSpacer, barButton ], animated: false)
            
        } else {
            print("Changing size to 32")
            let attrs = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont.systemFont(ofSize: 32.0)
            ]
            self.navigationController!.navigationBar.titleTextAttributes = attrs
            
            let button: UIButton = UIButton(type: UIButtonType.custom)
            button.setImage(UIImage(named: "sof.png"), for: UIControlState.normal)
            button.backgroundColor = UIColor.white
            //set frame
            button.frame = CGRect(x: 0, y: 0, width: 56, height: 45)
            let barButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            
            
            let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            negativeSpacer.width = -21;
            
            self.navigationItem.setRightBarButtonItems([negativeSpacer, barButton ], animated: false)
        }
        self.navigationItem.title = "Rep_M1_ModuleName".localized
        
        self.videoBtmLbl.text = "Rep_M1_RPVideoBtmLbl".localized
        
        self.rp_lbl.text = "Rep_M1_RolePlayLbl".localized
        
        menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: UIControlEvents.touchUpInside)
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        do{
            let urlVideo = Bundle.main.url(forResource: "REP_M01_S03", withExtension: "mp3")
            audioPlayerRPVAud = try AVAudioPlayer(contentsOf: urlVideo!)
            audioPlayerRPVAud!.delegate = self
            audioPlayerRPVAud!.prepareToPlay()
            audioPlayerRPVAud!.play()
        } catch {
            print("Error getting the audio file")
        }
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("In Finish Playing")
        playFlagRPVAud = 2
        if let image = UIImage(named: "ic_replay_white_36pt.png") {
            multimediaRPVAudio.setImage(image, for: .normal)
        }
        
        self.player?.pause()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let urlVideo = Bundle.main.url(forResource: "roleplay_video_m1", withExtension: "mp4")
        player = AVPlayer(url: urlVideo! as URL)
        let playerLayer = AVPlayerLayer(player: player)
        playerViewController.player = player
        playerLayer.frame = self.videoView.bounds
        self.videoView.layer.addSublayer(playerLayer)
        
        let asset = AVAsset(url: urlVideo!)
        _ = AVAssetImageGenerator(asset: asset)
        var time = asset.duration
        time.value = min(time.value, 2)
        
        self.player?.addObserver(self, forKeyPath: "currentItem", options: .initial, context: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(RolePlayVideoScreenVwCtrlr.itemDidFinishPlaying), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.player?.removeObserver(self, forKeyPath: "currentItem")
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if(keyPath == "currentItem") {
            print("Keypath: \(self.player?.currentItem)")
        }
    }
    func itemDidFinishPlaying(notification:NSNotification) {
        print("Video finished playing")
        player?.seek(to: kCMTimeZero)
        playFlagRPVAud = 2
        let butImg = UIImage(named: "play_video.png")
        playPauseVid.setImage(butImg, for: .normal)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        audioPlayerRPVAud?.stop()
    }
    
    @IBAction func multimediaRPVAudioClicked(_ sender: AnyObject) {
        if (playFlagRPVAud == 2) { // Audio paused.
            playFlagRPVAud = 1
            if let image = UIImage(named: "ic_pause_white_36pt.png") {
                multimediaRPVAudio.setImage(image, for: .normal)
                audioPlayerRPVAud?.play()
            }
            
            playerFlag = 1
            self.player?.pause()
            let butImg = UIImage(named: "play_video.png")
            playPauseVid.setImage(butImg, for: .normal)
            
        } else { // Audio playing
            playFlagRPVAud = 2
            if let image = UIImage(named: "ic_play_arrow_white_36pt.png") {
                multimediaRPVAudio.setImage(image, for: .normal)
                audioPlayerRPVAud?.pause()
            }
        }
    }
    
}
