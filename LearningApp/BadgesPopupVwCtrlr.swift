//
//  BadgesPopupVwCtrlr.swift
//  LearningApp
//
//  Created by Mac-04 on 29/08/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import UIKit

class BadgesPopupVwCtrlr: UIViewController {
    
    var text:String!
    @IBOutlet weak var borderView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        
        print("Height: \(super.view.frame.size.height)")
        self.borderView.layer.borderWidth = 1
        self.borderView.layer.borderColor = UIColor(red:239/255.0, green:239/255.0, blue:239/255.0, alpha: 1.0).cgColor
        
        let badgesCollectionView = self.parent as! BadgesCollectionViewController
        //self.navigationController!.viewControllers[0] as! BadgesCollectionViewController
        
        BadgesImageVw.image = badgesCollectionView.badgesImageArray[badgesCollectionView.badgeCount]
        BadgesPopDescLbl.text = badgesCollectionView.badgesDesc[badgesCollectionView.badgeCount] 
        badgeTitleLbl.text = badgesCollectionView.badgesTitle[badgesCollectionView.badgeCount] 

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var BadgesImageVw: UIImageView!
    @IBOutlet weak var BadgesPopDescLbl: UILabel!
    @IBOutlet weak var badgeTitleLbl: UILabel!
    
    
    @IBAction func BadgesPopupCloseBtn(_ sender: AnyObject) {
        self.view.removeFromSuperview()
    }

}
