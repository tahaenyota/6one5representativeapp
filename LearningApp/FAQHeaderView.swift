//
//  FAQHeaderView.swift
//  LearningApp
//
//  Created by Sumit More on 12/30/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import Foundation
import UIKit

class FAQHeaderView: UITableViewCell {
    
    // MARK: - Outlets    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var toggleButton: FAQButton!
    @IBOutlet weak var toggleImg: UIImageView!
    
    @IBOutlet weak var bgView: UIView!
}
