//
//  SalesRepTabbarController.swift
//  LearningApp
//
//  Created by Sumit More on 9/26/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import Foundation
import UIKit

class SalesRepTabbarController: UITabBarController, UITabBarControllerDelegate {
    
    @IBOutlet weak var salesRep: UITabBar!
    
    override func viewDidLoad() {
        
        self.delegate = self
        
        for item in self.salesRep.items! as [UITabBarItem] {
            let unselectedItem: NSDictionary = [NSForegroundColorAttributeName: UIColor.white]
            let selectedItem: NSDictionary = [NSForegroundColorAttributeName: UIColor(red: 22.0/255.0, green: 93.0/255.0, blue: 175.0/255.0, alpha: 1.0)]
            item.setTitleTextAttributes(unselectedItem as? [String : AnyObject], for: .normal)
            item.setTitleTextAttributes(selectedItem as? [String : AnyObject], for: .selected)
        }
        if #available(iOS 10.0, *) {
            self.tabBar.unselectedItemTintColor = UIColor.white
        } else {
            let arrayOfImageNameForUnselectedState = ["tb_home_u","tb_modules_u","tb_notification_u","tb_faqs_u"]
            let arrayOfImageNameForSelectedState = ["tb_home_s","tb_modules_s","tb_notification_s","tb_faqs_s"]
            if let count = self.tabBar.items?.count {
                for i in 0...(count-1) {
                    let imageNameForSelectedState   = arrayOfImageNameForSelectedState[i]
                    let imageNameForUnselectedState = arrayOfImageNameForUnselectedState[i]
                    
                    self.tabBar.items?[i].selectedImage = UIImage(named: imageNameForSelectedState)?.withRenderingMode(.alwaysOriginal)
                    self.tabBar.items?[i].image = UIImage(named: imageNameForUnselectedState)?.withRenderingMode(.alwaysOriginal)
                }
            }            // Fallback on earlier versions
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if let vc = viewController as? UINavigationController {
            vc.popViewController(animated: false);
        }
    }
}

