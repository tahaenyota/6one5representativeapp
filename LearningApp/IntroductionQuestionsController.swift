//
//  IntroductionQuestionsController.swift
//  LearningApp
//
//  Created by Sumit More on 11/24/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import Foundation
import UIKit

class IntroductionQuestionsController: UIViewController {
    
    @IBOutlet weak var questionsSampleLbl: UILabel!
    @IBOutlet weak var questionsExplanationLbl: UILabel!
    @IBOutlet weak var questionLbl: UILabel!
    @IBOutlet weak var bottomBar: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        let introductionScreenView = self.navigationController!.viewControllers[0] as! firstPageViewControler
        switch introductionScreenView.questionNo {
        case 1:
            self.questionLbl.text = "Rep_Intro_Q1".localized
            self.questionsSampleLbl.text = "Rep_Intro_Q1_Sample".localized
            self.questionsExplanationLbl.text = "Rep_Intro_Q1_Explanation".localized
            self.bottomBar.backgroundColor = UIColor.init(red: 201.0/255.0, green: 5.0/255.0, blue: 18.0/255.0, alpha: 1.0)
            break
        case 2:
            self.questionLbl.text = "Rep_Intro_Q2".localized
            self.questionsSampleLbl.text = "Rep_Intro_Q2_Sample".localized
            self.questionsExplanationLbl.text = "Rep_Intro_Q2_Explanation".localized
            self.bottomBar.backgroundColor = UIColor.init(red: 243.0/255.0, green: 145.0/255.0, blue: 0.0/255.0, alpha: 1.0)
            break
        case 3:
            self.questionLbl.text = "Rep_Intro_Q3".localized
            self.questionsSampleLbl.text = "Rep_Intro_Q3_Sample".localized
            self.questionsExplanationLbl.text = "Rep_Intro_Q3_Explanation".localized
            self.bottomBar.backgroundColor = UIColor.init(red: 140.0/255.0, green: 183.0/255.0, blue: 30.0/255.0, alpha: 1.0)
            break
        case 4:
            self.questionLbl.text = "Rep_Intro_Q4".localized
            self.questionsSampleLbl.text = "Rep_Intro_Q4_Sample".localized
            self.questionsExplanationLbl.text = "Rep_Intro_Q4_Explanation".localized
            self.bottomBar.backgroundColor = UIColor.init(red: 245.0/255.0, green: 201.0/255.0, blue: 0.0/255.0, alpha: 1.0)
            break
        case 5:
            self.questionLbl.text = "Rep_Intro_Q5".localized
            self.questionsSampleLbl.text = "Rep_Intro_Q5_Sample".localized
            self.questionsExplanationLbl.text = "Rep_Intro_Q5_Explanation".localized
            self.bottomBar.backgroundColor = UIColor.init(red: 169.0/255.0, green: 72.0/255.0, blue: 147.0/255.0, alpha: 1.0)
            break
        default:
            break
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func closeClick(_ sender: AnyObject) {
        self.view.removeFromSuperview()
    }
}
