//
//  Module4PracticeWithMgrController.swift
//  LearningApp
//
//  Created by Sumit More on 12/6/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import AVFoundation

class Module4PracticeWithMgrController: UIViewController, AVAudioPlayerDelegate {
    
    
    @IBOutlet weak var menuButton: UIButton!
    
    @IBOutlet weak var multimediaPWM: UIButton!
    
    @IBOutlet weak var practiceMgrLbl: UILabel!
    
    @IBOutlet weak var practiceMgrBtmTitle: UILabel!
    
    @IBOutlet weak var practiceWithMgrBtn: UIButton!
    
    @IBOutlet weak var practiceWithMgrImg: UIImageView!
    
    @IBAction func practiceWithManagerClicked(_ sender: AnyObject) {
        
        self.practiceWithMgrBtn.isEnabled = false

        /*var boxView = UIView()
        
        boxView = UIView(frame: CGRect(x: view.frame.midX - 125, y: view.frame.midY - 25, width: 251, height: 50))
        boxView.backgroundColor = UIColor.white
        boxView.alpha = 1
        boxView.layer.cornerRadius = 10
        
        //Here the spinnier is initialized
        let activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        activityView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        activityView.startAnimating()
        
        let textLabel = UILabel(frame: CGRect(x: 60, y: 0, width: 200, height: 50))
        textLabel.textColor = UIColor.gray
        textLabel.text = "Submitting request"
        
        boxView.addSubview(activityView)
        boxView.addSubview(textLabel)
        
        view.addSubview(boxView)*/
        
        let progressHUD = ProgressHUD()
        self.view.addSubview(progressHUD)
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            ]
        
        var user_id = "61"
        var course_id = "2"
        var module_id = "1"
        
        let prefs:UserDefaults = UserDefaults.standard
        user_id = prefs.value(forKey: "user_id") as! String!
        course_id = prefs.value(forKey: "course_id") as! String!
        module_id = prefs.value(forKey: "module_id") as! String!
        
        let postURL: String = Constants.salesRepPracticeWithManagerURL + user_id + "/" + course_id
        print("API URL: \(postURL)")
        
        let postData = NSMutableData(data: "data={\"module_id\":\"\(module_id)\"}".data(using: String.Encoding.utf8)!)
        
        let request = NSMutableURLRequest(url: NSURL(string: postURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            if (error != nil) {
                print("HTTP Error: \(error)")
                DispatchQueue.main.async {
                    
                    progressHUD.hide()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    
                    let alert = UIAlertController(title: "Request of review", message: "Error in connecting to the server. Please try later.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
                self.practiceWithMgrBtn.isEnabled = true
                
            } else {
                let httpResponse = response as? HTTPURLResponse
                print("HTTP Response: \(httpResponse)")
                do {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        print("Print converted dictionary",convertedJsonIntoDict)
                        
                        // Get value by key
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        
                        if(statusVal == true)
                        {
                            DispatchQueue.main.async {
                                
                                progressHUD.hide()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                
                                let alert = UIAlertController(title: "Request of review", message: "Request for review submitted successfully to the manager.", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                
                                prefs.set(true, forKey: "practice_with_mgr_m4")
                                prefs.synchronize()
                                self.practiceWithMgrBtn.isEnabled = false
                                self.practiceWithMgrBtn.backgroundColor = UIColor.gray
                                
                            }
                        } else {
                            var error_msg:NSString
                            if convertedJsonIntoDict["message"] as? NSString != nil {
                                error_msg = convertedJsonIntoDict["message"] as! NSString
                            } else {
                                error_msg = "Unknown Error"
                            }
                            DispatchQueue.main.async {
                                
                                progressHUD.hide()
                                UIApplication.shared.endIgnoringInteractionEvents()
                                
                                let alert = UIAlertController(title: "Request of review", message: "Error in connecting to the server. Please try later.", preferredStyle: UIAlertControllerStyle.alert)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                            }
                            print("error_msg",error_msg)
                            self.practiceWithMgrBtn.isEnabled = true
                        }
                    }
                } catch let error as NSError {
                    print(error)
                }
                
            }
        })
        
        dataTask.resume()
        
    }
    var playFlagPWM = 1
    var audioPlayerPWM: AVAudioPlayer?
    
    override func viewDidLoad() {
        
        let device = UIDevice.current.model
        if(device == "iPhone") {
            print("Changing size to 16")
            let attrs = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont.systemFont(ofSize: 16.0)
            ]
            self.navigationController!.navigationBar.titleTextAttributes = attrs
            
            let button: UIButton = UIButton(type: UIButtonType.custom)
            button.setImage(UIImage(named: "sof.png"), for: UIControlState.normal)
            button.backgroundColor = UIColor.white
            //set frame
            button.frame = CGRect(x: 0, y: 0, width: 56, height: 45)
            let barButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            
            
            let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            negativeSpacer.width = -17;
            
            self.navigationItem.setRightBarButtonItems([negativeSpacer, barButton ], animated: false)
            
        } else {
            print("Changing size to 32")
            let attrs = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont.systemFont(ofSize: 32.0)
            ]
            self.navigationController!.navigationBar.titleTextAttributes = attrs
            
            let button: UIButton = UIButton(type: UIButtonType.custom)
            button.setImage(UIImage(named: "sof.png"), for: UIControlState.normal)
            button.backgroundColor = UIColor.white
            //set frame
            button.frame = CGRect(x: 0, y: 0, width: 56, height: 45)
            let barButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            
            
            let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            negativeSpacer.width = -21;
            
            self.navigationItem.setRightBarButtonItems([negativeSpacer, barButton ], animated: false)
        }
        
        self.practiceWithMgrImg.image = UIImage(named: "manager_review2")

        self.navigationItem.title = "Rep_M4_ModuleName".localized
        
        self.practiceMgrBtmTitle.text = "Rep_M4_PracticeMgrBtmTitle".localized
        self.practiceMgrLbl.text = "Rep_M4_PracticeMgrLbl".localized
        
        let prefs:UserDefaults = UserDefaults.standard
        let has_prev_clicked: Bool = (prefs.value(forKey: "practice_with_mgr_m4") != nil)
        
        let welcomeDisc = prefs.value(forKey: "CloseModule") as! NSDictionary
        let fed = welcomeDisc["review"] as! Bool
        
        if (has_prev_clicked == true || fed == true) {
            self.practiceWithMgrBtn.isEnabled = false
            self.practiceWithMgrBtn.backgroundColor = UIColor.gray
        }
        
        menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: UIControlEvents.touchUpInside)
        
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        do{
            let urlVideo = Bundle.main.url(forResource: "REP_M04_S05", withExtension: "mp3")
            audioPlayerPWM = try AVAudioPlayer(contentsOf: urlVideo!)
            audioPlayerPWM!.delegate = self
            audioPlayerPWM!.prepareToPlay()
            audioPlayerPWM!.play()
        } catch {
            print("Error getting the audio file")
        }
        
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("In Finish Playing")
        playFlagPWM = 2
        if let image = UIImage(named: "ic_replay_white_36pt.png") {
            multimediaPWM.setImage(image, for: .normal)
        }
    }
    
    @IBAction func multimediaPWMClicked(_ sender: AnyObject) {
        if (playFlagPWM == 2) {
            playFlagPWM = 1
            if let image = UIImage(named: "ic_pause_white_36pt.png") {
                multimediaPWM.setImage(image, for: .normal)
                audioPlayerPWM?.play()
            }
        } else {
            playFlagPWM = 2
            if let image = UIImage(named: "ic_play_arrow_white_36pt.png") {
                multimediaPWM.setImage(image, for: .normal)
                audioPlayerPWM?.pause()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        audioPlayerPWM?.stop()
    }
    
}
