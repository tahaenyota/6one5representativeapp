//
//  FAQController.swift
//  LearningApp
//
//  Created by Sumit More on 12/30/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

class FAQController: UITableViewController {
    
    //
    // MARK: - Data
    //
    
    struct Section {
        var name: String!
        var items: [String]!
        var collapsed: Bool!
        
        init(name: String, items: [String], collapsed: Bool = true) {
            self.name = name
            self.items = items
            self.collapsed = collapsed
        }
    }
     var category0_sections = [Section]()
    var category1_sections = [Section]()
    var category2_sections = [Section]()
    var category3_sections = [Section]()
    var category4_sections = [Section]()
    let device = UIDevice.current.model
    
    var category1_iPhone_heights = [310,350,180,260,51,180,250,90]
    
    var category2_iPhone_heights = [210,121,251,201,161]
    var category3_iPhone_heights = [351,251]
    var category4_iPhone_heights = [276,101,101,101,176,161,101,161]

    var category1_iPad_heights = [321,360,180,310,51,190,300,130]
    var category2_iPad_heights = [201,151,301,176,176]
    var category3_iPad_heights = [376,275]
    var category4_iPad_heights = [276,101,101,101,176,181,121,181]

   

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Initialize the sections array
        // Here we have three sections: Mac, iPad, iPhone
        category0_sections = [
            
        ]
        category1_sections = [
            Section(name: "Category1_FAQ1".localized, items: ["Category1_FAQ1_Explanation".localized]),
            Section(name: "Category1_FAQ2".localized, items: ["Category1_FAQ2_Explanation".localized]),
            Section(name: "Category1_FAQ3".localized, items: ["Category1_FAQ3_Explanation".localized]),
            Section(name: "Category1_FAQ4".localized, items: ["Category1_FAQ4_Explanation".localized]),
            Section(name: "Category1_FAQ5".localized, items: ["Category1_FAQ5_Explanation".localized]),
            Section(name: "Category1_FAQ6".localized, items: ["Category1_FAQ6_Explanation".localized]),
            Section(name: "Category1_FAQ7".localized, items: ["Category1_FAQ7_Explanation".localized]),
            Section(name: "Category1_FAQ8".localized, items: ["Category1_FAQ8_Explanation".localized]),
        ]
        category2_sections = [
            Section(name: "Category2_FAQ1".localized, items: ["Category2_FAQ1_Explanation".localized]),
            Section(name: "Category2_FAQ2".localized, items: ["Category2_FAQ2_Explanation".localized]),
            Section(name: "Category2_FAQ3".localized, items: ["Category2_FAQ3_Explanation".localized]),
            Section(name: "Category2_FAQ4".localized, items: ["Category2_FAQ4_Explanation".localized]),
            Section(name: "Category2_FAQ5".localized, items: ["Category2_FAQ5_Explanation".localized]),
        ]
        category3_sections = [
            Section(name: "Category3_FAQ1".localized, items: ["Category3_FAQ1_Explanation".localized]),
            Section(name: "Category3_FAQ2".localized, items: ["Category3_FAQ2_Explanation".localized]),
        ]
        category4_sections = [
            Section(name: "Category4_FAQ1".localized, items: ["Category4_FAQ1_Explanation".localized]),
            Section(name: "Category4_FAQ2".localized, items: ["Category4_FAQ2_Explanation".localized]),
            Section(name: "Category4_FAQ3".localized, items: ["Category4_FAQ3_Explanation".localized]),
            Section(name: "Category4_FAQ4".localized, items: ["Category4_FAQ4_Explanation".localized]),
            Section(name: "Category4_FAQ5".localized, items: ["Category4_FAQ5_Explanation".localized]),
            Section(name: "Category4_FAQ6".localized, items: ["Category4_FAQ6_Explanation".localized]),
            Section(name: "Category4_FAQ7".localized, items: ["Category4_FAQ7_Explanation".localized]),
            Section(name: "Category4_FAQ8".localized, items: ["Category4_FAQ8_Explanation".localized]),
        ]
        
        /*let device = UIDevice.current.model
        if(device == "iPhone")
        {
            category1_iPhone_heights = [310,310,170,230,51,170,210,80]
        }
        else
        {
            category1_iPhone_heights = [310,350,180,260,51,180,250,90]
        }*/
    }
    
    //
    // MARK: - Table view delegate
    //
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    /*override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header = view as! UITableViewHeaderFooterView
        if(device == "iPhone") {
            if  section == 0 {
                header.backgroundView?.backgroundColor = UIColor.white
                header.textLabel?.textColor = UIColor.black
                header.textLabel?.text = header.textLabel?.text?.lowercased()
                header.textLabel?.font = UIFont.systemFont(ofSize:17.0)
            }else{
                header.textLabel?.font = UIFont.systemFont(ofSize: 17.0)
            }
            
        } else {
            if section == 0 {
                
                header.textLabel?.font = UIFont.boldSystemFont(ofSize: 25.0)
                
            }else{
                
                header.textLabel?.font = UIFont.systemFont(ofSize: 25.0)
            }
            
        }
        
    }*/
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
            
        case 0:  return "Category0Lbl".localized
        case 1:  return "Category1Lbl".localized
        case 2:  return "Category2Lbl".localized
        case 3:  return "Category3Lbl".localized
        case 4:  return "Category4Lbl".localized
        default: return ""
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            
            return 0
        case 1:
            var count = category1_sections.count
            for section in category1_sections {
                count += section.items.count
            }
            return count
        case 2:
            var count = category2_sections.count
            for section in category2_sections {
                count += section.items.count
            }
            return count
        case 3:
            var count = category3_sections.count
            for section in category3_sections {
                count += section.items.count
            }
            return count
        case 4:
            var count = category4_sections.count
            for section in category4_sections {
                count += section.items.count
            }
            return count
        default: return 1
        }

    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        if(device == "iPhone") {
            if section == 0 {
                return 500
            }else{
                return 30.0
            }
        }else{
            if section == 0 {
                return 500.0
            }else{
                return 30.0
            }
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        print("Device type: " + device)
        
        switch (indexPath as NSIndexPath).section {
        case 0:
            //            if category0_sections[section].collapsed! {
            //                return 0
            //            } else {
            //                if(device == "iPhone") {
            //                    return CGFloat(category1_iPhone_heights[section])
            //                } else {
            //                    return CGFloat(category1_iPad_heights[section])
            //                }
            //            }
            return 0
        case 1:
            let section = getSectionIndex((indexPath as NSIndexPath).row, category: (indexPath as NSIndexPath).section)
            let row = getRowIndex((indexPath as NSIndexPath).row, category: (indexPath as NSIndexPath).section)
            if row == 0 {
                if(device == "iPhone") {
                    switch section {
                    case 0: return 65.0
                    case 1: return 44.0
                    case 2: return 65.0
                    case 3: return 44.0
                    case 4: return 44.0
                    case 5: return 44.0
                    case 6: return 65.0
                    case 7: return 65.0
                    default: return 44.0
                    }
                } else {
                    switch section {
                    case 0: return 88.0
                    case 1: return 44.0
                    case 2: return 44.0
                    case 3: return 44.0
                    case 4: return 44.0
                    case 5: return 44.0
                    case 6: return 44.0
                    case 7: return 44.0
                    default: return 44.0
                    }
                }
            }
            if category1_sections[section].collapsed! {
                return 0
            } else {
                if(device == "iPhone") {
                    return CGFloat(category1_iPhone_heights[section])
                } else {
                    return CGFloat(category1_iPad_heights[section])
                }
            }
        case 2:
            let section = getSectionIndex((indexPath as NSIndexPath).row, category: (indexPath as NSIndexPath).section)
            let row = getRowIndex((indexPath as NSIndexPath).row, category: (indexPath as NSIndexPath).section)
            if row == 0 {
                if(device == "iPhone") {
                    switch section {
                    case 0: return 65.0
                    case 1: return 65.0
                    case 2: return 65.0
                    case 3: return 65.0
                    case 4: return 65.0
                    default: return 44.0
                    }
                } else {
                    switch section {
                    case 0: return 88.0
                    case 1: return 44.0
                    case 2: return 44.0
                    case 3: return 88.0
                    case 4: return 88.0
                    default: return 44.0
                    }
                }
            }
            if category2_sections[section].collapsed! {
                return 0
            } else {
                if(device == "iPhone") {
                    return CGFloat(category2_iPhone_heights[section])
                } else {
                    return CGFloat(category2_iPad_heights[section])
                }
            }
        case 3:
            let section = getSectionIndex((indexPath as NSIndexPath).row, category: (indexPath as NSIndexPath).section)
            let row = getRowIndex((indexPath as NSIndexPath).row, category: (indexPath as NSIndexPath).section)
            if row == 0 {
                if(device == "iPhone") {
                    switch section {
                    case 0: return 44.0
                    case 1: return 65.0
                    default: return 44.0
                    }
                } else {
                    switch section {
                    case 0: return 44.0
                    case 1: return 44.0
                    default: return 44.0
                    }
                }
            }
            if category3_sections[section].collapsed! {
                return 0
            } else {
                if(device == "iPhone") {
                    return CGFloat(category3_iPhone_heights[section])
                } else {
                    return CGFloat(category3_iPad_heights[section])
                }
            }
        case 4:
            let section = getSectionIndex((indexPath as NSIndexPath).row, category: (indexPath as NSIndexPath).section)
            let row = getRowIndex((indexPath as NSIndexPath).row, category: (indexPath as NSIndexPath).section)
            if row == 0 {
                if(device == "iPhone") {
                    switch section {
                    case 0: return 65.0
                    case 1: return 65.0
                    case 2: return 44.0
                    case 3: return 44.0
                    case 4: return 65.0
                    default: return 44.0
                    }
                } else {
                    switch section {
                    case 0: return 44.0
                    case 1: return 44.0
                    case 2: return 44.0
                    case 3: return 44.0
                    case 4: return 44.0
                    case 5: return 70.0
                    case 6: return 44.0
                    default: return 44.0
                    }
                }
            }
            if category4_sections[section].collapsed! {
                return 0
            } else {
                if(device == "iPhone") {
                    return CGFloat(category4_iPhone_heights[section])
                } else {
                    return CGFloat(category4_iPad_heights[section])
                }
            }
        default:
            return tableView.rowHeight
        }
    }
    
    
    
    
    public override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
        
        let view = UIView()
        
        
        
        if(device == "iPhone") {
            
            if  section == 0 {
                
                
                
                // let view = UIView()
                
                view.frame = CGRect(x: 0, y: 0, width: 347, height: 480)
                
                view.backgroundColor = UIColor.white
                
                let label = UILabel(frame: CGRect(x:5, y: 0, width: 347, height: 480))
                
                //label.center = CGPoint(x: 160, y: 285)
                
                //   label.backgroundColor = UIColor.white
                
                label.textAlignment = .left
                
                label.numberOfLines = 0
                
                //label.text = "Category0Lbl".localized
                
                label.text = "Welcome to this Retail Sales Professional Certification App.\n\nWe wish you well in developing your career in retail. Remember, many famous entrepreneurs, executives and owners of retailers and corporations—large and small—started their career on the shop floor. The effort you put into learning and applying the skills on this app will lay a solid foundation for your future success.\n\nTo be certified, you have to complete the 6 modules we've provided you with in this app, and have your store manager assess you on roles plays and practise with a customer for each module.\n\nWe wish you all the best!\n\nTo help you get the best out of this app, here are some frequently asked questions we've put together for you."
                
                view.addSubview(label)
                
                return view
                
            }else if(section == 1){
                
                
                
                // let view = UIView()
                
                view.frame = CGRect(x: 0, y: 0, width: 347, height: 44)
                
                let label = UILabel(frame: CGRect(x:5, y: -4, width: 347, height: 21))
                
                label.font = UIFont.boldSystemFont(ofSize: 18)
                
                label.textAlignment = .left
                
                label.numberOfLines = 0
                
                label.text = "Category1Lbl".localized
                
                
                
                view.addSubview(label)
                
                return view
                
            }else if(section == 2){
                
                
                
                // let view = UIView()
                
                view.frame = CGRect(x: 0, y: 0, width: 347, height: 44)
                
                let label = UILabel(frame: CGRect(x:5, y: -4, width: 347, height: 21))
                
                label.font = UIFont.boldSystemFont(ofSize: 18)
                
                label.textAlignment = .left
                
                label.numberOfLines = 0
                
                label.text = "Category2Lbl".localized
                
                view.addSubview(label)
                
                return view
                
            }else if(section == 3){
                
                
                
                // let view = UIView()
                
                view.frame = CGRect(x: 0, y: 0, width: 347, height: 44)
                
                // view.backgroundColor = UIColor.white
                
                let label = UILabel(frame: CGRect(x:5, y: -4, width: 347, height: 21))
                
                label.font = UIFont.boldSystemFont(ofSize: 18)
                
                label.textAlignment = .left
                
                label.numberOfLines = 0
                
                label.text = "Category3Lbl".localized
                
                view.addSubview(label)
                
                return view
                
            }else if(section == 4){
                
                
                
                // let view = UIView()
                
                view.frame = CGRect(x: 0, y: -50, width: 347, height: 44)
                
                let label = UILabel(frame: CGRect(x:5, y: -4, width: 347, height: 21))
                
                label.font = UIFont.boldSystemFont(ofSize: 18)
                
                label.textAlignment = .left
                
                label.numberOfLines = 0
                
                label.text = "Category4Lbl".localized
                
                view.addSubview(label)
                
                return view
                
                
                
            }else if(section == 5){
                
                
                
                // let view = UIView()
                
                view.frame = CGRect(x: 0, y: -50, width: 347, height: 44)
                
                let label = UILabel(frame: CGRect(x:5, y: -4, width: 347, height: 21))
                
                label.font = UIFont.boldSystemFont(ofSize: 18)
                
                label.textAlignment = .left
                
                label.numberOfLines = 0
                
                label.text = "Category5Lbl".localized
                
                view.addSubview(label)
                
                return view
                
            }
            
        }else
            
        {
            
            
            
            if  section == 0 {
                
                
                
                // let view = UIView()
                
                view.frame = CGRect(x: 0, y: 0, width: Constants.screenSize.width, height: 480)
                
                view.backgroundColor = UIColor.white
                
                let label = UILabel(frame: CGRect(x:5, y: 0, width: Constants.screenSize.width, height: 480))
                
                // label.backgroundColor = .red
                
                label.font = UIFont.systemFont(ofSize: 24)
                
                
                label.textAlignment = .left
                
                label.numberOfLines = 0
                
                //label.text = "Category0Lbl".localized
                
                label.text = "Welcome to this Retail Sales Professional Certification App.\n\nWe wish you well in developing your career in retail. Remember, many famous entrepreneurs, executives and owners of retailers and corporations—large and small—started their career on the shop floor. The effort you put into learning and applying the skills on this app will lay a solid foundation for your future success.\n\nTo be certified, you have to complete the 6 modules we've provided you with in this app, and have your store manager assess you on roles plays and practise with a customer for each module.\n\nWe wish you all the best!\n\nTo help you get the best out of this app, here are some frequently asked questions we've put together for you."
                
                view.addSubview(label)
                
                return view
                
            }else if(section == 1){
                
                
                
                // let view = UIView()
                
                view.frame = CGRect(x: 0, y: 0, width: Constants.screenSize.width, height: 44)
                
                let label = UILabel(frame: CGRect(x:5, y: -4, width: Constants.screenSize.width, height: 30))
                
                label.font = UIFont.boldSystemFont(ofSize: 25)
                
                label.textAlignment = .left
                
                label.numberOfLines = 0
                
                label.text = "Category1Lbl".localized
                
                
                
                view.addSubview(label)
                
                return view
                
            }else if(section == 2){
                
                
                
                // let view = UIView()
                
                view.frame = CGRect(x: 0, y: 0, width: Constants.screenSize.width, height: 44)
                
                let label = UILabel(frame: CGRect(x:5, y: -4, width: Constants.screenSize.width, height: 30))
                
                label.font = UIFont.boldSystemFont(ofSize: 25)
                
                label.textAlignment = .left
                
                label.numberOfLines = 0
                
                label.text = "Category2Lbl".localized
                
                view.addSubview(label)
                
                return view
                
            }else if(section == 3){
                
                
                
                // let view = UIView()
                
                view.frame = CGRect(x: 0, y: 0, width: Constants.screenSize.width, height: 44)
                
                // view.backgroundColor = UIColor.white
                
                let label = UILabel(frame: CGRect(x:5, y: -4, width: Constants.screenSize.width, height: 30))
                
                label.font = UIFont.boldSystemFont(ofSize: 25)
                
                label.textAlignment = .left
                
                label.numberOfLines = 0
                
                label.text = "Category3Lbl".localized
                
                view.addSubview(label)
                
                return view
                
            }else if(section == 4){
                
                
                
                // let view = UIView()
                
                view.frame = CGRect(x: 0, y: -50, width: Constants.screenSize.width, height: 44)
                
                let label = UILabel(frame: CGRect(x:5, y: -4, width: Constants.screenSize.width, height: 30))
                
                label.font = UIFont.boldSystemFont(ofSize: 25)
                
                label.textAlignment = .left
                
                label.numberOfLines = 0
                
                label.text = "Category4Lbl".localized
                
                view.addSubview(label)
                
                return view
                
                
                
            }else if(section == 5){
                
                
                
                // let view = UIView()
                
                view.frame = CGRect(x: 0, y: -50, width: Constants.screenSize.width, height: 44)
                
                let label = UILabel(frame: CGRect(x:5, y: -4, width: Constants.screenSize.width, height: 30))
                
                label.font = UIFont.boldSystemFont(ofSize: 25)
                
                label.textAlignment = .left
                
                label.numberOfLines = 0
                
                label.text = "Category5Lbl".localized
                
                view.addSubview(label)
                
                return view
                
            }
            
        }

        
        return view
        
        
        
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        print("Section: \((indexPath as NSIndexPath).section)")
        
        switch (indexPath as NSIndexPath).section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "header") as! FAQHeaderView
            return cell
            
        case 1:
            
            let section = getSectionIndex((indexPath as NSIndexPath).row, category: 1)
            
            let row = getRowIndex((indexPath as NSIndexPath).row, category: 1)
            
            print("Category: 0 - Row: \(row) - Section: \(section)")
            
            if row == 0 {
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "header") as! FAQHeaderView
                
                
                
                cell.titleLabel.text = category1_sections[section].name
                
                if(device == "iPhone") {
                    
                    cell.titleLabel.font = UIFont.boldSystemFont(ofSize: 16.0)
                    
                } else {
                    
                    cell.titleLabel.font = UIFont.boldSystemFont(ofSize: 25.0)
                    
                }
                
                cell.toggleButton.tag = section
                
                cell.toggleButton.toggleCategory = 1
                
                if category1_sections[section].collapsed! {
                    
                    // cell.arrowImage.setImage(UIImage(named: ), for: UIControlState.normal)
                    
                    cell.bgView.backgroundColor = .white
                    cell.toggleImg.image = UIImage(named: "arrow_side1.png")
                   
                    //cell.arrowImage.image = UIImage(named:"arrow_side1.png")
                    
                } else {
                    
                    // cell.toggleButton.setImage(UIImage(named: "arrow-down1.png"), for: UIControlState.normal)
                    
                    cell.bgView.backgroundColor = UIColor.lightGray
                    
                    //cell.arrowImage.image = UIImage(named:"arrow-down1.png")
                     cell.toggleImg.image = UIImage(named: "arrow-down1.png")
                }
                
                //                cell.toggleButton.setTitle(category1_sections[section].collapsed! ? ">" : "v", for: UIControlState())
                
                cell.toggleButton.addTarget(self, action: #selector(FAQController.toggleCollapse), for: .touchUpInside)
                
                return cell
                
            } else {
                
                //let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! FAQDetailView
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! FAQDetailView
                
                //cell.detailLabel.text = category1_sections[section].items[row - 1]
                
                var detailText = category1_sections[section].items[row - 1]
                detailText = Constants.FAQfontTag + detailText + Constants.FAQfontclosing
                let theAttributedString = try! NSAttributedString(data:detailText.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
                                                                  
                                                                  options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                                  
                                                                  documentAttributes: nil)
                
                
                
                cell.detailLabel.attributedText = theAttributedString
                
                /// cell.detailBGView.backgroundColor = UIColor.lightGray
                
                return cell
                
            }
            
            
//        case 1:
//            let section = getSectionIndex((indexPath as NSIndexPath).row, category: 1)
//            let row = getRowIndex((indexPath as NSIndexPath).row, category: 1)
//            print("Category: 0 - Row: \(row) - Section: \(section)")
//            if row == 0 {
//                let cell = tableView.dequeueReusableCell(withIdentifier: "header") as! FAQHeaderView
//                cell.titleLabel.text = category1_sections[section].name
//                if(device == "iPhone") {
//                    cell.titleLabel.font = UIFont.boldSystemFont(ofSize: 16.0)
//                } else {
//                    cell.titleLabel.font = UIFont.boldSystemFont(ofSize: 25.0)
//                }
//                cell.toggleButton.tag = section
//                cell.toggleButton.toggleCategory = 1
//                if category1_sections[section].collapsed! {
//                    cell.bgView.backgroundColor = UIColor.white
//                    cell.toggleImg.image = UIImage(named: "arrow_side1.png")
////                    cell.toggleButton.setImage(UIImage(named: "arrow_side1.png"), for: UIControlState.normal)
//                } else {
//                    cell.bgView.backgroundColor = UIColor.lightGray
//                    cell.toggleImg.image = UIImage(named: "arrow-down1.png")
////                    cell.toggleButton.setImage(UIImage(named: "arrow-down1.png"), for: UIControlState.normal)
//                }
////                cell.toggleButton.setTitle(category1_sections[section].collapsed! ? ">" : "v", for: UIControlState())
//                cell.toggleButton.addTarget(self, action: #selector(FAQController.toggleCollapse), for: .touchUpInside)
//                return cell
//            
//            } else {
//                
//                let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! FAQDetailView
//                let detailText = category1_sections[section].items[row - 1]
//                let theAttributedString = try! NSAttributedString(data:detailText.data(using: String.Encoding.utf8, allowLossyConversion: false)!,
//                                                                  
//                                                                  options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
//                                                                  
//                                                                  documentAttributes: nil)
//                
//                cell.detailLabel.attributedText = theAttributedString
//                return cell
//            }
        case 2:
            let section = getSectionIndex((indexPath as NSIndexPath).row, category: 2)
            let row = getRowIndex((indexPath as NSIndexPath).row, category: 2)
            print("Category: 1 - Row: \(row) - Section: \(section)")
            if row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "header") as! FAQHeaderView
                cell.titleLabel.text = category2_sections[section].name
                if(device == "iPhone") {
                    cell.titleLabel.font = UIFont.boldSystemFont(ofSize: 16.0)
                } else {
                    cell.titleLabel.font = UIFont.boldSystemFont(ofSize: 25.0)
                }
                cell.toggleButton.tag = section
                cell.toggleButton.toggleCategory = 2
                if category2_sections[section].collapsed! {
                    cell.bgView.backgroundColor = UIColor.white
                    cell.toggleImg.image = UIImage(named: "arrow_side1.png")
//                    cell.toggleButton.setImage(UIImage(named: "arrow_side1.png"), for: UIControlState.normal)
                } else {
                    cell.bgView.backgroundColor = UIColor.lightGray
                    cell.toggleImg.image = UIImage(named: "arrow-down1.png")
//                    cell.toggleButton.setImage(UIImage(named: "arrow-down1.png"), for: UIControlState.normal)
                }
                cell.toggleButton.addTarget(self, action: #selector(FAQController.toggleCollapse), for: .touchUpInside)
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! FAQDetailView
                cell.detailLabel.text = category2_sections[section].items[row - 1]
                
                return cell
            }
        case 3:
            let section = getSectionIndex((indexPath as NSIndexPath).row, category: 3)
            let row = getRowIndex((indexPath as NSIndexPath).row, category: 3)
            print("Category: 2 - Row: \(row) - Section: \(section)")
            if row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "header") as! FAQHeaderView
                cell.titleLabel.text = category3_sections[section].name
                if(device == "iPhone") {
                    cell.titleLabel.font = UIFont.boldSystemFont(ofSize: 16.0)
                } else {
                    cell.titleLabel.font = UIFont.boldSystemFont(ofSize: 25.0)
                }
                cell.toggleButton.tag = section
                cell.toggleButton.toggleCategory = 3
                if category3_sections[section].collapsed! {
                    cell.bgView.backgroundColor = UIColor.white
                    cell.toggleImg.image = UIImage(named: "arrow_side1.png")
//                    cell.toggleButton.setImage(UIImage(named: "arrow_side1.png"), for: UIControlState.normal)
                } else {
                    cell.bgView.backgroundColor = UIColor.lightGray
                    cell.toggleImg.image = UIImage(named: "arrow-down1.png")
//                    cell.toggleButton.setImage(UIImage(named: "arrow-down1.png"), for: UIControlState.normal)
                }
                cell.toggleButton.addTarget(self, action: #selector(FAQController.toggleCollapse), for: .touchUpInside)
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! FAQDetailView
                cell.detailLabel.text = category3_sections[section].items[row - 1]
                return cell
            }
        case 4:
            let section = getSectionIndex((indexPath as NSIndexPath).row, category: 4)
            let row = getRowIndex((indexPath as NSIndexPath).row, category: 4)
            print("Category: 3 - Row: \(row) - Section: \(section)")
            if row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "header") as! FAQHeaderView
                cell.titleLabel.text = category4_sections[section].name
                if(device == "iPhone") {
                    cell.titleLabel.font = UIFont.boldSystemFont(ofSize: 16.0)
                } else {
                    cell.titleLabel.font = UIFont.boldSystemFont(ofSize: 25.0)
                }
                cell.toggleButton.tag = section
                cell.toggleButton.toggleCategory = 4
                if category4_sections[section].collapsed! {
                    cell.bgView.backgroundColor = UIColor.white
                    cell.toggleImg.image = UIImage(named: "arrow_side1.png")
//                    cell.toggleButton.setImage(UIImage(named: "arrow_side1.png"), for: UIControlState.normal)
                } else {
                    cell.bgView.backgroundColor = UIColor.lightGray
                    cell.toggleImg.image = UIImage(named: "arrow-down1.png")
//                    cell.toggleButton.setImage(UIImage(named: "arrow-down1.png"), for: UIControlState.normal)
                }
                cell.toggleButton.addTarget(self, action: #selector(FAQController.toggleCollapse), for: .touchUpInside)
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! FAQDetailView
                cell.detailLabel.text = category4_sections[section].items[row - 1]
                return cell
            }
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! FAQDetailView
            cell.detailLabel.text = category3_sections[0].items[0]
            return cell
        }
    }
    
    
    
    //
    // MARK: - Event Handlers
    //
    func toggleCollapse(_ sender: FAQButton) {
        
        let section = sender.tag
        let category = sender.toggleCategory! as NSInteger
        
        
        switch category {
            //        case 0:
        //            break
        case 1:
            //DispatchQueue.main.async(){
             //   self.tableView.reloadData()
            //}
            let collapsed = category1_sections[section].collapsed
            category1_sections[section].collapsed = !collapsed!
            let indices = getHeaderIndices(category: category)
            let start = indices[section]
            let end = start + category1_sections[section].items.count
            tableView.beginUpdates()
            for i in start ..< end + 1 {
                tableView.reloadRows(at: [IndexPath(row: i, section: 1)], with: .automatic)
            }
            //self.tableView.reloadData()
            //let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! FAQDetailView
            //cell.backgroundColor = UIColor.blue
            
            tableView.endUpdates()
            break
        case 2:
            let collapsed = category2_sections[section].collapsed
            category2_sections[section].collapsed = !collapsed!
            let indices = getHeaderIndices(category: category)
            let start = indices[section]
            let end = start + category2_sections[section].items.count
            tableView.beginUpdates()
            for i in start ..< end + 1 {
                tableView.reloadRows(at: [IndexPath(row: i, section: 2)], with: .automatic)
            }
            tableView.endUpdates()
            break
        case 3:
            let collapsed = category3_sections[section].collapsed
            category3_sections[section].collapsed = !collapsed!
            let indices = getHeaderIndices(category: category)
            let start = indices[section]
            let end = start + category3_sections[section].items.count
            tableView.beginUpdates()
            for i in start ..< end + 1 {
                tableView.reloadRows(at: [IndexPath(row: i, section: 3)], with: .automatic)
            }
            tableView.endUpdates()
            break
        case 4:
            let collapsed = category4_sections[section].collapsed
            category4_sections[section].collapsed = !collapsed!
            let indices = getHeaderIndices(category: category)
            let start = indices[section]
            let end = start + category4_sections[section].items.count
            tableView.beginUpdates()
            for i in start ..< end + 1 {
                tableView.reloadRows(at: [IndexPath(row: i, section: 4)], with: .automatic)
            }
            tableView.endUpdates()
            break
        default:
            break
        }
    }

    
    //
    // MARK: - Helper Functions
    //
    func getSectionIndex(_ row: NSInteger, category: NSInteger) -> Int {
        let indices = getHeaderIndices(category: category)
        
        for i in 0..<indices.count {
            if i == indices.count - 1 || row < indices[i + 1] {
                return i
            }
        }
        
        return -1
    }
    
    func getRowIndex(_ row: NSInteger, category: NSInteger) -> Int {
        var index = row
        let indices = getHeaderIndices(category: category)
        for i in 0..<indices.count {
            if i == indices.count - 1 || row < indices[i + 1] {
                index -= indices[i]
                break
            }
        }
        //        print("Category: \(category) - Indices: \(indices) - Index: \(index)")
        
        return index
    }
    
    func getHeaderIndices(category: Int) -> [Int] {
        var index = 0
        var indices: [Int] = []
        
        switch category {
        case 0:
            return indices
        case 1:
            for section in category1_sections {
                indices.append(index)
                index += section.items.count + 1
            }
            return indices
        case 2:
            for section in category2_sections {
                indices.append(index)
                index += section.items.count + 1
            }
            return indices
        case 3:
            for section in category3_sections {
                indices.append(index)
                index += section.items.count + 1
            }
            return indices
        case 4:
            for section in category4_sections {
                indices.append(index)
                index += section.items.count + 1
            }
            return indices
        default: return [0]
        }
    }
    
    
}
