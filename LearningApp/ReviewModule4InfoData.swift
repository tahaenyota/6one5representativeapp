//
//  ReviewModule4InfoData.swift
//  6one5Manager
//
//  Created by enyotalearning on 06/01/17.
//  Copyright © 2017 enyotalearning. All rights reserved.
//

import Foundation

class ReviewModule4InfoData:ReviewModuleInfo
{
    
    override func AdddataforDiscripter()
    {
        discripter1 = DiscripterModule()
        discripter2 = DiscripterModule()
        discripter3 = DiscripterModule()
        discripter4 = DiscripterModule()
        discripter4Data = [String]()
        
        discripter1Data = [String]()
        discripter2Data = [String]()
        discripter3Data = [String]()        //discripter 1 Data
        discripter1.descripterText = "M4D1Text".localized
        
        discripter1.addBeginer(input: "M4D1B1".localized)
       
        
        discripter1.addLearner(input: "M4D1L1".localized)
        
        
        discripter1.addProfessional(input: "M4D1P1".localized)
       
        
        discripter1.addHighPerformer(input: "M4D1H1".localized)
        discripter1.addHighPerformer(input: "M4D1H2".localized)
        
        discripter1.addExpert(input: "M4D1E1".localized)
        discripter1.addExpert(input: "M4D1E2".localized)
       
        
        //discripter 2 Data
        discripter2.descripterText = "M4D2Text".localized
        
        discripter2.addBeginer(input: "M4D2B1".localized)
        
        discripter2.addLearner(input: "M4D2L1".localized)
        discripter2.addLearner(input: "M4D2L2".localized)
        
        discripter2.addProfessional(input: "M4D2P1".localized)

        
        discripter2.addHighPerformer(input: "M4D2H1".localized)
   
        
        discripter2.addExpert(input: "M4D2E1".localized)
        
        //discripter 3 Data
        discripter3.descripterText = "M4D3Text".localized
        
        discripter3.addBeginer(input: "M4D3B1".localized)
        discripter3.addBeginer(input: "M4D3B2".localized)
        
        discripter3.addLearner(input: "M4D3L1".localized)
        discripter3.addLearner(input: "M4D3L2".localized)
        
        
        discripter3.addProfessional(input: "M4D3P1".localized)
        
        discripter3.addHighPerformer(input: "M4D3H1".localized)
       
        discripter3.addExpert(input: "M4D3E1".localized)
        
        discripter1Data = createDataArray(dis: discripter1)
        discripter2Data = createDataArray(dis: discripter2)
        discripter3Data = createDataArray(dis: discripter3)
        
    }
    
    func createDataArray( dis:DiscripterModule) -> [String]
    {
        var disDataArray = [String]()
        disDataArray.append(getArrayData(dataArray: dis.beginer))
        disDataArray.append(getArrayData(dataArray: dis.learner))
        disDataArray.append(getArrayData(dataArray: dis.professional))
        disDataArray.append(getArrayData(dataArray: dis.highPerformer))
        disDataArray.append(getArrayData(dataArray: dis.expert))
        
        return disDataArray
    }
    
    
    func getArrayData(dataArray:[String]) ->String
    {
        var allText:String = "<ul>"
        
        for item in dataArray
        {
            allText.append("<li>" + item + "</li>")
        }
        
        return allText + "</ul>"
    }

}
