//
//  HelpPopupViewController.swift
//  LearningApp
//
//  Created by NonStop io on 03/10/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import UIKit

class HelpPopupViewController: UIViewController {
    @IBOutlet weak var HelpCloseBtn: UIButton!
    @IBOutlet weak var helpTextfield: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UserDefaults.standard.bool(forKey: "isPlusversion") == true {
            
            self.HelpCloseBtn.removeConstraints(self.HelpCloseBtn.constraints)
            let top_HelpCloseBtn = NSLayoutConstraint(item: self.HelpCloseBtn,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: 615)
            let height_HelpCloseBtn = NSLayoutConstraint(item: self.HelpCloseBtn,
                                                         attribute: NSLayoutAttribute.height,
                                                         relatedBy: NSLayoutRelation.equal,
                                                         toItem: nil,
                                                         attribute: NSLayoutAttribute.notAnAttribute,
                                                         multiplier: 1,
                                                         constant: 51)
            let left_HelpCloseBtn = NSLayoutConstraint(item: self.HelpCloseBtn,
                                                       attribute: NSLayoutAttribute.left,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.view,
                                                       attribute: NSLayoutAttribute.left,
                                                       multiplier: 1,
                                                       constant: 51)
            self.view.addConstraint(top_HelpCloseBtn)
            self.view.addConstraint(height_HelpCloseBtn)
            self.view.addConstraint(left_HelpCloseBtn)
            //            self.HelpCloseBtn.backgroundColor = UIColor.red
            
        }

        self.helpTextfield.text = "Rep_Quiz_M2_Help".localized
        self.helpTextfield.isScrollEnabled = false
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.helpTextfield.isScrollEnabled = true
    }
    
    @IBAction func CloseHelpPopupBtnTouch(_ sender: AnyObject) {
//        self.view.removeFromSuperview()        
        _ = navigationController?.popViewController(animated: false)
    }
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
