//
//  CertificateTblViewCell.swift
//  LearningApp
//
//  Created by Mac-04 on 22/08/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import UIKit

class CertificateTblViewCell: UITableViewCell {

    @IBOutlet weak var cerificateImgView: UIImageView!
    @IBOutlet weak var CertiDescriptionLbl: UILabel!
    @IBOutlet weak var CertiTitleLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
   
    
    override var frame: CGRect {
        get {
            return super.frame
        }
        set (newFrame) {
            var frame =  newFrame
//            frame.origin.y += 4
//            frame.size.height -= 2 * 4
            super.frame = frame
        }
    }
    
    
}
