//
//  firstPageViewControler.swift
//  LearningApp
//
//  Created by Mac-04 on 15/09/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class firstPageViewControler: UIViewController, AVAudioPlayerDelegate {

    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var multimediaIntro: UIButton!
    @IBOutlet weak var introductionImage: UIImageView!
    
    @IBOutlet weak var customersLbl: UILabel!
    @IBOutlet weak var askQLbl: UILabel!
    @IBOutlet weak var listenLbl: UILabel!
    @IBOutlet weak var confirmLbl: UILabel!
    @IBOutlet weak var salesPplLbl: UILabel!
    @IBOutlet weak var salesP1Lbl: UILabel!
    @IBOutlet weak var salesP2Lbl: UILabel!
    @IBOutlet weak var salesP3Lbl: UILabel!
    
    @IBOutlet weak var whyLbl: UIButton!
    @IBOutlet weak var whatLbl: UIButton!
    @IBOutlet weak var willLbl: UIButton!
    @IBOutlet weak var whenLbl: UIButton!
    @IBOutlet weak var howLbl: UIButton!
    @IBOutlet weak var wQLbl: UILabel!
    
    @IBOutlet weak var welcomeLbl: UILabel!
    @IBOutlet weak var understandLbl: UILabel!
    @IBOutlet weak var adviseLbl: UILabel!
    @IBOutlet weak var closeLbl: UILabel!
    @IBOutlet weak var thankLbl: UILabel!
    
    @IBAction func introFwdClick(_ sender: AnyObject) {
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        audioPlayerIntroAud?.pause()
    }
    
    @IBAction func whyClick(_ sender: AnyObject) {
        self.questionNo = 1
        let popOverVC = UIStoryboard (name: "Module2SB", bundle: nil).instantiateViewController(withIdentifier: "wQID") as! IntroductionQuestionsController
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        popOverVC.modalTransitionStyle = .crossDissolve
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    @IBAction func whatClick(_ sender: AnyObject) {
        self.questionNo = 2
        let popOverVC = UIStoryboard (name: "Module2SB", bundle: nil).instantiateViewController(withIdentifier: "wQID") as! IntroductionQuestionsController
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        popOverVC.modalTransitionStyle = .crossDissolve
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    @IBAction func willClick(_ sender: AnyObject) {
        self.questionNo = 3
        let popOverVC = UIStoryboard (name: "Module2SB", bundle: nil).instantiateViewController(withIdentifier: "wQID") as! IntroductionQuestionsController
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        popOverVC.modalTransitionStyle = .crossDissolve
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    @IBAction func whenClick(_ sender: AnyObject) {
        self.questionNo = 4
        let popOverVC = UIStoryboard (name: "Module2SB", bundle: nil).instantiateViewController(withIdentifier: "wQID") as! IntroductionQuestionsController
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        popOverVC.modalTransitionStyle = .crossDissolve
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    @IBAction func howClick(_ sender: AnyObject) {
        self.questionNo = 5
        let popOverVC = UIStoryboard (name: "Module2SB", bundle: nil).instantiateViewController(withIdentifier: "wQID") as! IntroductionQuestionsController
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        popOverVC.modalTransitionStyle = .crossDissolve
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    var playFlagIntroAud = 1
    var audioPlayerIntroAud: AVAudioPlayer?

//    var introductionImageArray: [UIImage] = [ UIImage(named: "img01.png")!,
//                                              UIImage(named: "img02.png")!,
//                                              UIImage(named: "img03.png")!,
//                                              UIImage(named: "img04.png")!,
//                                              UIImage(named: "img05.png")!]

    var GTTimer : Timer = Timer()
    var GTTimer2 : Timer = Timer()
    var isPaused = false
    var intervalVal: Int = 5
    var intervalVal2: Int = 1
    var screenNo: Int = 0
    var questionNo: Int = 1
    
    @IBOutlet weak var introBtmLbl: UILabel!
    
    override func viewDidLoad() {
        
        self.introBtmLbl.text = "Rep_M2_IntroductionBtmLbl".localized
        
//        let image = UIImage(named: "sof.png")
//        self.navigationItem.titleView = UIImageView(image: image)
        
        let device = UIDevice.current.model
        print("Device type: " + device)

        if(device == "iPhone") {
            print("Changing size to 16")
            let attrs = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont.systemFont(ofSize: 16.0)
            ]
            self.navigationController!.navigationBar.titleTextAttributes = attrs

            let button: UIButton = UIButton(type: UIButtonType.custom)
            button.setImage(UIImage(named: "sof.png"), for: UIControlState.normal)
            button.backgroundColor = UIColor.white
            //set frame
            button.frame = CGRect(x: 0, y: 0, width: 56, height: 45)
            let barButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            
            
            let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            negativeSpacer.width = -17;
            
            self.navigationItem.setRightBarButtonItems([negativeSpacer, barButton ], animated: false)

        } else {
            print("Changing size to 32")
            let attrs = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont.systemFont(ofSize: 32.0)
            ]
            self.navigationController!.navigationBar.titleTextAttributes = attrs

            let button: UIButton = UIButton(type: UIButtonType.custom)
            button.setImage(UIImage(named: "sof.png"), for: UIControlState.normal)
            button.backgroundColor = UIColor.white
            //set frame
            button.frame = CGRect(x: 0, y: 0, width: 56, height: 45)
            let barButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            
            
            let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            negativeSpacer.width = -21;
            
            self.navigationItem.setRightBarButtonItems([negativeSpacer, barButton ], animated: false)
        }

        self.navigationItem.title = "Rep_M2_ModuleName".localized

//        self.navigationItem.rightBarButtonItem = barButton

        //        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
//        imageView.contentMode = .scaleAspectFit
//        let image = UIImage(named: "sof")
//        imageView.image = image
//        self.navigationItem.rightBarButtonItem?.customView = imageView
//        self.navigationItem.rightBarButtonItem?.isEnabled = true
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: logo, style: UIBarButtonItemStyle.plain, target: nil, action: nil)

//        var image = UIImage(named: "sof.png")
//        image = image?.withRenderingMode(UIImageRenderingMode.automatic)
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: image, style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        
        self.customersLbl.isHidden = true
        self.askQLbl.isHidden = true
        self.listenLbl.isHidden = true
        self.confirmLbl.isHidden = true
        self.salesPplLbl.isHidden = true
        self.salesP1Lbl.isHidden = true
        self.salesP2Lbl.isHidden = true
        self.salesP3Lbl.isHidden = true
        self.whyLbl.isHidden = true
        self.whatLbl.isHidden = true
        self.willLbl.isHidden = true
        self.whenLbl.isHidden = true
        self.howLbl.isHidden = true
        self.wQLbl.isHidden = true
        self.welcomeLbl.isHidden = false
        self.understandLbl.isHidden = false
        self.adviseLbl.isHidden = false
        self.closeLbl.isHidden = false
        self.thankLbl.isHidden = false
        
        self.introductionImage.image = UIImage(named: "intro0")

        self.whyLbl.isHighlighted = true
        self.whyLbl.showsTouchWhenHighlighted = false
        self.whyLbl.adjustsImageWhenHighlighted = false
        self.whatLbl.isHighlighted = true
        self.whatLbl.showsTouchWhenHighlighted = false
        self.whatLbl.adjustsImageWhenHighlighted = false
        self.willLbl.isHighlighted = true
        self.willLbl.showsTouchWhenHighlighted = false
        self.willLbl.adjustsImageWhenHighlighted = false
        self.whenLbl.isHighlighted = true
        self.whenLbl.showsTouchWhenHighlighted = false
        self.whenLbl.adjustsImageWhenHighlighted = false
        self.howLbl.isHighlighted = true
        self.howLbl.showsTouchWhenHighlighted = false
        self.howLbl.adjustsImageWhenHighlighted = false

        if UserDefaults.standard.bool(forKey: "isPlusversion") == true {

            self.welcomeLbl.removeConstraints(self.welcomeLbl.constraints)
            let lead_welcome = NSLayoutConstraint(item: self.welcomeLbl,
                                                  attribute: NSLayoutAttribute.left,
                                                  relatedBy: NSLayoutRelation.equal,
                                                  toItem: self.view,
                                                  attribute: NSLayoutAttribute.left,
                                                  multiplier: 1,
                                                  constant: CGFloat(Constants.M2_welcomeLblConstraints_6splus[0]))
            
            let top_welcome = NSLayoutConstraint(item: self.welcomeLbl,
                                                 attribute: NSLayoutAttribute.top,
                                                 relatedBy: NSLayoutRelation.equal,
                                                 toItem: self.view,
                                                 attribute: NSLayoutAttribute.top,
                                                 multiplier: 1,
                                                 constant: CGFloat(Constants.M2_welcomeLblConstraints_6splus[1]))
            self.view.addConstraint(lead_welcome)
            self.view.addConstraint(top_welcome)
            
            self.understandLbl.removeConstraints(self.understandLbl.constraints)
            let lead_und = NSLayoutConstraint(item: self.understandLbl,
                                              attribute: NSLayoutAttribute.left,
                                              relatedBy: NSLayoutRelation.equal,
                                              toItem: self.view,
                                              attribute: NSLayoutAttribute.left,
                                              multiplier: 1,
                                              constant: CGFloat(Constants.M2_UnderstandLblConstraints_6splus[0]))
            let top_und = NSLayoutConstraint(item: self.understandLbl,
                                             attribute: NSLayoutAttribute.top,
                                             relatedBy: NSLayoutRelation.equal,
                                             toItem: self.view,
                                             attribute: NSLayoutAttribute.top,
                                             multiplier: 1,
                                             constant: CGFloat(Constants.M2_UnderstandLblConstraints_6splus[1]))
            self.view.addConstraint(lead_und)
            self.view.addConstraint(top_und)
            
            self.adviseLbl.removeConstraints(self.adviseLbl.constraints)
            let lead_advise = NSLayoutConstraint(item: self.adviseLbl,
                                                 attribute: NSLayoutAttribute.left,
                                                 relatedBy: NSLayoutRelation.equal,
                                                 toItem: self.view,
                                                 attribute: NSLayoutAttribute.left,
                                                 multiplier: 1,
                                                 constant: CGFloat(Constants.M2_AdviseLblConstraints_6splus[0]))
            
            let top_advise = NSLayoutConstraint(item: self.adviseLbl,
                                                attribute: NSLayoutAttribute.top,
                                                relatedBy: NSLayoutRelation.equal,
                                                toItem: self.view,
                                                attribute: NSLayoutAttribute.top,
                                                multiplier: 1,
                                                constant: CGFloat(Constants.M2_AdviseLblConstraints_6splus[1]))
            self.view.addConstraint(lead_advise)
            self.view.addConstraint(top_advise)
            
            self.closeLbl.removeConstraints(self.closeLbl.constraints)
            let lead_close = NSLayoutConstraint(item: self.closeLbl,
                                                attribute: NSLayoutAttribute.left,
                                                relatedBy: NSLayoutRelation.equal,
                                                toItem: self.view,
                                                attribute: NSLayoutAttribute.left,
                                                multiplier: 1,
                                                constant: CGFloat(Constants.M2_CloseLblConstraints_6splus[0]))
            let top_close = NSLayoutConstraint(item: self.closeLbl,
                                               attribute: NSLayoutAttribute.top,
                                               relatedBy: NSLayoutRelation.equal,
                                               toItem: self.view,
                                               attribute: NSLayoutAttribute.top,
                                               multiplier: 1,
                                               constant: CGFloat(Constants.M2_CloseLblConstraints_6splus[1]))
            self.view.addConstraint(lead_close)
            self.view.addConstraint(top_close)
            
            
            self.thankLbl.removeConstraints(self.thankLbl.constraints)
            let lead_thank = NSLayoutConstraint(item: self.thankLbl,
                                                attribute: NSLayoutAttribute.left,
                                                relatedBy: NSLayoutRelation.equal,
                                                toItem: self.view,
                                                attribute: NSLayoutAttribute.left,
                                                multiplier: 1,
                                                constant: CGFloat(Constants.M2_ThankLblConstraints_6splus[0]))
            
            let top_thank = NSLayoutConstraint(item: self.thankLbl,
                                               attribute: NSLayoutAttribute.top,
                                               relatedBy: NSLayoutRelation.equal,
                                               toItem: self.view,
                                               attribute: NSLayoutAttribute.top,
                                               multiplier: 1,
                                               constant: CGFloat(Constants.M2_ThankLblConstraints_6splus[1]))
            
            
            self.view.addConstraint(lead_thank)
            self.view.addConstraint(top_thank)
            
            self.customersLbl.removeConstraints(self.customersLbl.constraints)
            let top_customersLbl = NSLayoutConstraint(item: self.customersLbl,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M2_CustomersLblConstraints_6splus))
            self.view.addConstraint(top_customersLbl)
            
            self.askQLbl.removeConstraints(self.askQLbl.constraints)
            let lead_askQLbl = NSLayoutConstraint(item: self.askQLbl,
                                                attribute: NSLayoutAttribute.left,
                                                relatedBy: NSLayoutRelation.equal,
                                                toItem: self.view,
                                                attribute: NSLayoutAttribute.left,
                                                multiplier: 1,
                                                constant: CGFloat(Constants.M2_AskConstraints_6splus[0]))
            let top_askQLbl = NSLayoutConstraint(item: self.askQLbl,
                                                 attribute: NSLayoutAttribute.top,
                                                 relatedBy: NSLayoutRelation.equal,
                                                 toItem: self.view,
                                                 attribute: NSLayoutAttribute.top,
                                                 multiplier: 1,
                                                 constant: CGFloat(Constants.M2_AskConstraints_6splus[1]))
            let width_askQLbl = NSLayoutConstraint(item: self.askQLbl,
                                                     attribute: NSLayoutAttribute.width,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: nil,
                                                     attribute: NSLayoutAttribute.notAnAttribute,
                                                     multiplier: 1,
                                                     constant: CGFloat(Constants.M2_AskConstraints_6splus[2]))
            self.view.addConstraint(lead_askQLbl)
            self.view.addConstraint(top_askQLbl)
            self.view.addConstraint(width_askQLbl)

            self.listenLbl.removeConstraints(self.listenLbl.constraints)
            let lead_ListenLbl = NSLayoutConstraint(item: self.listenLbl,
                                                  attribute: NSLayoutAttribute.left,
                                                  relatedBy: NSLayoutRelation.equal,
                                                  toItem: self.view,
                                                  attribute: NSLayoutAttribute.left,
                                                  multiplier: 1,
                                                  constant: CGFloat(Constants.M2_ListenConstraints_6splus[0]))
            let top_ListenLbl = NSLayoutConstraint(item: self.listenLbl,
                                                    attribute: NSLayoutAttribute.top,
                                                    relatedBy: NSLayoutRelation.equal,
                                                    toItem: self.view,
                                                    attribute: NSLayoutAttribute.top,
                                                    multiplier: 1,
                                                    constant: CGFloat(Constants.M2_ListenConstraints_6splus[1]))
            let width_ListenLbl = NSLayoutConstraint(item: self.askQLbl,
                                                   attribute: NSLayoutAttribute.width,
                                                   relatedBy: NSLayoutRelation.equal,
                                                   toItem: nil,
                                                   attribute: NSLayoutAttribute.notAnAttribute,
                                                   multiplier: 1,
                                                   constant: CGFloat(Constants.M2_AskConstraints_6splus[2]))
            self.view.addConstraint(lead_ListenLbl)
            self.view.addConstraint(top_ListenLbl)
            self.view.addConstraint(width_ListenLbl)

            self.confirmLbl.removeConstraints(self.confirmLbl.constraints)
            let lead_confirmLbl = NSLayoutConstraint(item: self.confirmLbl,
                                                    attribute: NSLayoutAttribute.left,
                                                    relatedBy: NSLayoutRelation.equal,
                                                    toItem: self.view,
                                                    attribute: NSLayoutAttribute.left,
                                                    multiplier: 1,
                                                    constant: CGFloat(Constants.M2_ConfirmConstraints_6splus[0]))
            let top_confirmLbl = NSLayoutConstraint(item: self.confirmLbl,
                                                   attribute: NSLayoutAttribute.top,
                                                   relatedBy: NSLayoutRelation.equal,
                                                   toItem: self.view,
                                                   attribute: NSLayoutAttribute.top,
                                                   multiplier: 1,
                                                   constant: CGFloat(Constants.M2_ConfirmConstraints_6splus[1]))
            let width_confirmLbl = NSLayoutConstraint(item: self.confirmLbl,
                                                     attribute: NSLayoutAttribute.width,
                                                     relatedBy: NSLayoutRelation.equal,
                                                     toItem: nil,
                                                     attribute: NSLayoutAttribute.notAnAttribute,
                                                     multiplier: 1,
                                                     constant: CGFloat(Constants.M2_ConfirmConstraints_6splus[2]))
            self.view.addConstraint(lead_confirmLbl)
            self.view.addConstraint(top_confirmLbl)
            self.view.addConstraint(width_confirmLbl)

            self.whatLbl.removeConstraints(self.whatLbl.constraints)
            let lead_whatLbl = NSLayoutConstraint(item: self.whatLbl,
                                                  attribute: NSLayoutAttribute.leading,
                                                  relatedBy: NSLayoutRelation.equal,
                                                  toItem: self.view,
                                                  attribute: NSLayoutAttribute.leading,
                                                  multiplier: 1,
                                                  constant: CGFloat(Constants.M2_whatLblConstraints_6splus[0]))
            let top_whatLbl = NSLayoutConstraint(item: self.whatLbl,
                                                 attribute: NSLayoutAttribute.top,
                                                 relatedBy: NSLayoutRelation.equal,
                                                 toItem: self.view,
                                                 attribute: NSLayoutAttribute.top,
                                                 multiplier: 1,
                                                 constant: CGFloat(Constants.M2_whatLblConstraints_6splus[1]))
            let width_whatLbl = NSLayoutConstraint(item: self.whatLbl,
                                                      attribute: NSLayoutAttribute.width,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: nil,
                                                      attribute: NSLayoutAttribute.notAnAttribute,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M2_whatLblConstraints_6splus[2]))
            let height_whatLbl = NSLayoutConstraint(item: self.whatLbl,
                                                      attribute: NSLayoutAttribute.height,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: nil,
                                                      attribute: NSLayoutAttribute.notAnAttribute,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M2_whatLblConstraints_6splus[3]))
            self.view.addConstraint(lead_whatLbl)
            self.view.addConstraint(top_whatLbl)
            self.view.addConstraint(width_whatLbl)
            self.view.addConstraint(height_whatLbl)
//            self.whatLbl.backgroundColor = UIColor.red

            
            self.whyLbl.removeConstraints(self.whyLbl.constraints)
            let lead_whyLbl = NSLayoutConstraint(item: self.whyLbl,
                                                 attribute: NSLayoutAttribute.leading,
                                                 relatedBy: NSLayoutRelation.equal,
                                                 toItem: self.view,
                                                 attribute: NSLayoutAttribute.leading,
                                                 multiplier: 1,
                                                 constant: CGFloat(Constants.M2_whyLblConstraints_6splus[0]))               
            let top_whyLbl = NSLayoutConstraint(item: self.whyLbl,
                                                attribute: NSLayoutAttribute.top,
                                                relatedBy: NSLayoutRelation.equal,
                                                toItem: self.view,
                                                attribute: NSLayoutAttribute.top,
                                                multiplier: 1,
                                                constant: CGFloat(Constants.M2_whyLblConstraints_6splus[1]))
            let width_whyLbl = NSLayoutConstraint(item: self.whyLbl,
                                                   attribute: NSLayoutAttribute.width,
                                                   relatedBy: NSLayoutRelation.equal,
                                                   toItem: nil,
                                                   attribute: NSLayoutAttribute.notAnAttribute,
                                                   multiplier: 1,
                                                   constant: CGFloat(Constants.M2_whyLblConstraints_6splus[2]))
            let height_whyLbl = NSLayoutConstraint(item: self.whyLbl,
                                                    attribute: NSLayoutAttribute.height,
                                                    relatedBy: NSLayoutRelation.equal,
                                                    toItem: nil,
                                                    attribute: NSLayoutAttribute.notAnAttribute,
                                                    multiplier: 1,
                                                    constant: CGFloat(Constants.M2_whyLblConstraints_6splus[3]))
            self.view.addConstraint(lead_whyLbl)
            self.view.addConstraint(top_whyLbl)
            self.view.addConstraint(width_whyLbl)
            self.view.addConstraint(height_whyLbl)
//            self.whyLbl.backgroundColor = UIColor.red

            
            self.willLbl.removeConstraints(self.willLbl.constraints)
            let lead_willLbl = NSLayoutConstraint(item: self.willLbl,
                                                  attribute: NSLayoutAttribute.leading,
                                                  relatedBy: NSLayoutRelation.equal,
                                                  toItem: self.view,
                                                  attribute: NSLayoutAttribute.leading,
                                                  multiplier: 1,
                                                  constant: CGFloat(Constants.M2_willLblConstraints_6splus[0]))
            
            let top_willLbl = NSLayoutConstraint(item: self.willLbl,
                                                 attribute: NSLayoutAttribute.top,
                                                 relatedBy: NSLayoutRelation.equal,
                                                 toItem: self.view,
                                                 attribute: NSLayoutAttribute.top,
                                                 multiplier: 1,
                                                 constant: CGFloat(Constants.M2_willLblConstraints_6splus[1]))
            let width_willLbl = NSLayoutConstraint(item: self.willLbl,
                                                  attribute: NSLayoutAttribute.width,
                                                  relatedBy: NSLayoutRelation.equal,
                                                  toItem: nil,
                                                  attribute: NSLayoutAttribute.notAnAttribute,
                                                  multiplier: 1,
                                                  constant: CGFloat(Constants.M2_willLblConstraints_6splus[2]))
            let height_willLbl = NSLayoutConstraint(item: self.willLbl,
                                                   attribute: NSLayoutAttribute.height,
                                                   relatedBy: NSLayoutRelation.equal,
                                                   toItem: nil,
                                                   attribute: NSLayoutAttribute.notAnAttribute,
                                                   multiplier: 1,
                                                   constant: CGFloat(Constants.M2_willLblConstraints_6splus[3]))
            self.view.addConstraint(lead_willLbl)
            self.view.addConstraint(top_willLbl)
            self.view.addConstraint(width_willLbl)
            self.view.addConstraint(height_willLbl)
//            self.willLbl.backgroundColor = UIColor.red

            self.whenLbl.removeConstraints(self.whenLbl.constraints)
            let lead_whenLbl = NSLayoutConstraint(item: self.whenLbl,
                                                  attribute: NSLayoutAttribute.leading,
                                                  relatedBy: NSLayoutRelation.equal,
                                                  toItem: self.view,
                                                  attribute: NSLayoutAttribute.leading,
                                                  multiplier: 1,
                                                  constant: CGFloat(Constants.M2_whenLblConstraints_6splus[0]))
            
            let top_whenLbl = NSLayoutConstraint(item: self.whenLbl,
                                                 attribute: NSLayoutAttribute.top,
                                                 relatedBy: NSLayoutRelation.equal,
                                                 toItem: self.view,
                                                 attribute: NSLayoutAttribute.top,
                                                 multiplier: 1,
                                                 constant: CGFloat(Constants.M2_whenLblConstraints_6splus[1]))
            let width_whenLbl = NSLayoutConstraint(item: self.whenLbl,
                                                   attribute: NSLayoutAttribute.width,
                                                   relatedBy: NSLayoutRelation.equal,
                                                   toItem: nil,
                                                   attribute: NSLayoutAttribute.notAnAttribute,
                                                   multiplier: 1,
                                                   constant: CGFloat(Constants.M2_whenLblConstraints_6splus[2]))
            let height_whenLbl = NSLayoutConstraint(item: self.whenLbl,
                                                    attribute: NSLayoutAttribute.height,
                                                    relatedBy: NSLayoutRelation.equal,
                                                    toItem: nil,
                                                    attribute: NSLayoutAttribute.notAnAttribute,
                                                    multiplier: 1,
                                                    constant: CGFloat(Constants.M2_whenLblConstraints_6splus[3]))
            self.view.addConstraint(lead_whenLbl)
            self.view.addConstraint(top_whenLbl)
            self.view.addConstraint(width_whenLbl)
            self.view.addConstraint(height_whenLbl)
//            self.whenLbl.backgroundColor = UIColor.red
            
            self.howLbl.removeConstraints(self.howLbl.constraints)
            let lead_howLbl = NSLayoutConstraint(item: self.howLbl,
                                                 attribute: NSLayoutAttribute.leading,
                                                 relatedBy: NSLayoutRelation.equal,
                                                 toItem: self.view,
                                                 attribute: NSLayoutAttribute.leading,
                                                 multiplier: 1,
                                                 constant: CGFloat(Constants.M2_howLblConstraints_6splus[0]))
            
            let top_howLbl = NSLayoutConstraint(item: self.howLbl,
                                                attribute: NSLayoutAttribute.top,
                                                relatedBy: NSLayoutRelation.equal,
                                                toItem: self.view,
                                                attribute: NSLayoutAttribute.top,
                                                multiplier: 1,
                                                constant: CGFloat(Constants.M2_howLblConstraints_6splus[1]))
            let width_howLbl = NSLayoutConstraint(item: self.howLbl,
                                                   attribute: NSLayoutAttribute.width,
                                                   relatedBy: NSLayoutRelation.equal,
                                                   toItem: nil,
                                                   attribute: NSLayoutAttribute.notAnAttribute,
                                                   multiplier: 1,
                                                   constant: CGFloat(Constants.M2_howLblConstraints_6splus[2]))
            let height_howLbl = NSLayoutConstraint(item: self.howLbl,
                                                    attribute: NSLayoutAttribute.height,
                                                    relatedBy: NSLayoutRelation.equal,
                                                    toItem: nil,
                                                    attribute: NSLayoutAttribute.notAnAttribute,
                                                    multiplier: 1,
                                                    constant: CGFloat(Constants.M2_howLblConstraints_6splus[3]))
            self.view.addConstraint(lead_howLbl)
            self.view.addConstraint(top_howLbl)
            self.view.addConstraint(width_howLbl)
            self.view.addConstraint(height_howLbl)
//            self.howLbl.backgroundColor = UIColor.red
            
            
            //            trailingContraint.isActive = true
            //            view.updateConstraints()
            
        }
        
        intervalVal = 5
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(firstPageViewControler.screen1), userInfo: nil, repeats: false)

        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(firstPageViewControler.monitorTimer), userInfo: nil, repeats: true)

        if self.revealViewController() != nil {
            menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: UIControlEvents.touchUpInside)
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())

        }
        
        do{
            let urlVideo = Bundle.main.url(forResource: "REP_M02_S01", withExtension: "mp3")
            audioPlayerIntroAud = try AVAudioPlayer(contentsOf: urlVideo!)
            audioPlayerIntroAud!.delegate = self
            audioPlayerIntroAud!.prepareToPlay()
            audioPlayerIntroAud!.play()
        } catch {
            print("Error getting the audio file")
        }

    }

    override func viewDidAppear(_ animated: Bool) {
//        self.introductionImage.image = UIImage(named: "img05.png")
//        self.introductionImage.animationImages = introductionImageArray;
//        self.introductionImage.animationDuration = 32.0
//        self.introductionImage.animationRepeatCount = 1
//        self.introductionImage.startAnimating()
    }

    func monitorTimer() {
        self.intervalVal = self.intervalVal - 1
        print("Interval in monitor: " + String(self.intervalVal))
    }
    
    func screen1(){
        
        print("In screen1")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "intro1")

        self.customersLbl.isHidden = false
        self.askQLbl.isHidden = false
        self.listenLbl.isHidden = false
        self.confirmLbl.isHidden = false
        self.salesPplLbl.isHidden = true
        self.salesP1Lbl.isHidden = true
        self.salesP2Lbl.isHidden = true
        self.salesP3Lbl.isHidden = true
        self.whyLbl.isHidden = true
        self.whatLbl.isHidden = true
        self.willLbl.isHidden = true
        self.whenLbl.isHidden = true
        self.howLbl.isHidden = true
        self.wQLbl.isHidden = true
        self.welcomeLbl.isHidden = true
        self.understandLbl.isHidden = true
        self.adviseLbl.isHidden = true
        self.closeLbl.isHidden = true
        self.thankLbl.isHidden = true
        
        self.intervalVal = 7
        self.screenNo = 1
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(firstPageViewControler.screen2_1), userInfo: nil, repeats: false)

        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(firstPageViewControler.monitorTimer), userInfo: nil, repeats: true)
        
    }

    func screen2_1(){

        print("In screen2.1")

        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "intro2")

        self.customersLbl.isHidden = true
        self.askQLbl.isHidden = true
        self.listenLbl.isHidden = true
        self.confirmLbl.isHidden = true
        self.salesPplLbl.isHidden = false
        self.salesP1Lbl.isHidden = false
        self.salesP2Lbl.isHidden = true
        self.salesP3Lbl.isHidden = true
        self.whyLbl.isHidden = true
        self.whatLbl.isHidden = true
        self.willLbl.isHidden = true
        self.whenLbl.isHidden = true
        self.howLbl.isHidden = true
        self.wQLbl.isHidden = true
        self.welcomeLbl.isHidden = true
        self.understandLbl.isHidden = true
        self.adviseLbl.isHidden = true
        self.closeLbl.isHidden = true
        self.thankLbl.isHidden = true
        
        UIView.transition(with: self.salesP1Lbl,
                                  duration: 2.0,
                                  options: [.transitionCrossDissolve],
                                  animations: {
                                    
                                    self.salesP1Lbl.text = "Rep_Intro_S3L2".localized
                                    
            }, completion: nil)
        
        self.intervalVal = 9
        self.screenNo = 2
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(firstPageViewControler.screen2_2), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(firstPageViewControler.monitorTimer), userInfo: nil, repeats: true)
        
    }

    func screen2_2(){
        
        print("In screen22")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "intro2")
        
        self.customersLbl.isHidden = true
        self.askQLbl.isHidden = true
        self.listenLbl.isHidden = true
        self.confirmLbl.isHidden = true
        self.salesPplLbl.isHidden = false
        self.salesP1Lbl.isHidden = false
        self.salesP2Lbl.isHidden = false
        self.salesP3Lbl.isHidden = true
        self.whyLbl.isHidden = true
        self.whatLbl.isHidden = true
        self.willLbl.isHidden = true
        self.whenLbl.isHidden = true
        self.howLbl.isHidden = true
        self.wQLbl.isHidden = true
        self.welcomeLbl.isHidden = true
        self.understandLbl.isHidden = true
        self.adviseLbl.isHidden = true
        self.closeLbl.isHidden = true
        self.thankLbl.isHidden = true
        
        UIView.transition(with: self.salesP2Lbl,
                          duration: 2.0,
                          options: [.transitionCrossDissolve],
                          animations: {
                            
                            self.salesP2Lbl.text = "Rep_Intro_S3L3".localized
                            
            }, completion: nil)

        self.intervalVal = 6
        self.screenNo = 22
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(firstPageViewControler.screen2_3), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(firstPageViewControler.monitorTimer), userInfo: nil, repeats: true)
        
    }

    func screen2_3(){
        
        print("In screen23")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "intro2")
        
        self.customersLbl.isHidden = true
        self.askQLbl.isHidden = true
        self.listenLbl.isHidden = true
        self.confirmLbl.isHidden = true
        self.salesPplLbl.isHidden = false
        self.salesP1Lbl.isHidden = false
        self.salesP2Lbl.isHidden = false
        self.salesP3Lbl.isHidden = false
        self.whyLbl.isHidden = true
        self.whatLbl.isHidden = true
        self.willLbl.isHidden = true
        self.whenLbl.isHidden = true
        self.howLbl.isHidden = true
        self.wQLbl.isHidden = true
        self.welcomeLbl.isHidden = true
        self.understandLbl.isHidden = true
        self.adviseLbl.isHidden = true
        self.closeLbl.isHidden = true
        self.thankLbl.isHidden = true
        
        UIView.transition(with: self.salesP3Lbl,
                          duration: 2.0,
                          options: [.transitionCrossDissolve],
                          animations: {
                            
                            self.salesP3Lbl.text = "Rep_Intro_S3L4".localized
                            
            }, completion: nil)

        self.intervalVal = 3
        self.screenNo = 23
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(firstPageViewControler.screen3), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(firstPageViewControler.monitorTimer), userInfo: nil, repeats: true)
        
    }

    func screen3(){
        
        print("In screen3")

        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "intro3")
        
        self.customersLbl.isHidden = true
        self.askQLbl.isHidden = true
        self.listenLbl.isHidden = true
        self.confirmLbl.isHidden = true
        self.salesPplLbl.isHidden = true
        self.salesP1Lbl.isHidden = true
        self.salesP2Lbl.isHidden = true
        self.salesP3Lbl.isHidden = true
        self.whyLbl.isHidden = false
        self.whatLbl.isHidden = false
        self.willLbl.isHidden = false
        self.whenLbl.isHidden = false
        self.howLbl.isHidden = false
        self.wQLbl.isHidden = true
        self.welcomeLbl.isHidden = true
        self.understandLbl.isHidden = true
        self.adviseLbl.isHidden = true
        self.closeLbl.isHidden = true
        self.thankLbl.isHidden = true
        
        self.whyLbl.isEnabled = false
        self.whatLbl.isEnabled = false
        self.willLbl.isEnabled = false
        self.whenLbl.isEnabled = false
        self.howLbl.isEnabled = false

        self.intervalVal = 6
        self.screenNo = 3

        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(firstPageViewControler.screen4_1), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(firstPageViewControler.monitorTimer), userInfo: nil, repeats: true)
   
    }

    func screen4_1(){
        
        print("In screen4.1")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "intro3")
        
        self.customersLbl.isHidden = true
        self.askQLbl.isHidden = true
        self.listenLbl.isHidden = true
        self.confirmLbl.isHidden = true
        self.salesPplLbl.isHidden = true
        self.salesP1Lbl.isHidden = true
        self.salesP2Lbl.isHidden = true
        self.salesP3Lbl.isHidden = true
        self.whyLbl.isHidden = false
        self.whatLbl.isHidden = false
        self.willLbl.isHidden = false
        self.whenLbl.isHidden = false
        self.howLbl.isHidden = false
        self.wQLbl.isHidden = false
        self.welcomeLbl.isHidden = true
        self.understandLbl.isHidden = true
        self.adviseLbl.isHidden = true
        self.closeLbl.isHidden = true
        self.thankLbl.isHidden = true
        
        UIView.transition(with: self.wQLbl,
                          duration: 2.0,
                          options: [.transitionCrossDissolve],
                          animations: {
                            self.wQLbl.text = "Rep_Intro_Q_Nxt".localized                            
            }, completion: nil)

        self.intervalVal = 8
        self.screenNo = 41
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(firstPageViewControler.screen4_2), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(firstPageViewControler.monitorTimer), userInfo: nil, repeats: true)

    }

    func screen4_2(){
        
        print("In screen4.2")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.whyLbl.isEnabled = true
        self.whatLbl.isEnabled = true
        self.willLbl.isEnabled = true
        self.whenLbl.isEnabled = true
        self.howLbl.isEnabled = true
        
    }

    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("In Finish Playing")
        playFlagIntroAud = 3
        if let image = UIImage(named: "ic_replay_white_36pt.png") {
            multimediaIntro.setImage(image, for: .normal)
        }
    }

    @IBAction func multimediaIntroClicked(_ sender: AnyObject) {
        
        if (playFlagIntroAud == 3) {

            self.introductionImage.image = UIImage(named: "intro0")

            self.customersLbl.isHidden = true
            self.askQLbl.isHidden = true
            self.listenLbl.isHidden = true
            self.confirmLbl.isHidden = true
            self.salesPplLbl.isHidden = true
            self.salesP1Lbl.isHidden = true
            self.salesP2Lbl.isHidden = true
            self.salesP3Lbl.isHidden = true
            self.whyLbl.isHidden = true
            self.whatLbl.isHidden = true
            self.willLbl.isHidden = true
            self.whenLbl.isHidden = true
            self.howLbl.isHidden = true
            self.wQLbl.isHidden = true
            self.welcomeLbl.isHidden = false
            self.understandLbl.isHidden = false
            self.adviseLbl.isHidden = false
            self.closeLbl.isHidden = false
            self.thankLbl.isHidden = false
            
            self.GTTimer.invalidate()
            self.GTTimer2.invalidate()

            self.intervalVal = 5
            self.screenNo = 0
            print("In screen 1 after replay")
            let timeInterval: TimeInterval = TimeInterval(intervalVal)
            self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(firstPageViewControler.screen1), userInfo: nil, repeats: false)

            let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(firstPageViewControler.monitorTimer), userInfo: nil, repeats: true)
 
            playFlagIntroAud = 1
            if let image = UIImage(named: "ic_pause_white_36pt.png") {
                multimediaIntro.setImage(image, for: .normal)
                audioPlayerIntroAud?.play()
            }
        } else if (playFlagIntroAud == 2) {

            let timeInterval: TimeInterval = TimeInterval(self.intervalVal)
            print("Interval: " + String(self.intervalVal))
            
            switch self.screenNo {
            case 0: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(firstPageViewControler.screen1), userInfo: nil, repeats: false)
                let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
                self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(firstPageViewControler.monitorTimer), userInfo: nil, repeats: true)
                break
            case 1: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(firstPageViewControler.screen2_1), userInfo: nil, repeats: false)
                let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
                self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(firstPageViewControler.monitorTimer), userInfo: nil, repeats: true)
                break
            case 2: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(firstPageViewControler.screen2_2), userInfo: nil, repeats: false)
                let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
                self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(firstPageViewControler.monitorTimer), userInfo: nil, repeats: true)
                break
            case 22: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(firstPageViewControler.screen2_3), userInfo: nil, repeats: false)
                let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
                self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(firstPageViewControler.monitorTimer), userInfo: nil, repeats: true)
                break
            case 23: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(firstPageViewControler.screen3), userInfo: nil, repeats: false)
                let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
                self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(firstPageViewControler.monitorTimer), userInfo: nil, repeats: true)
                break
            case 3: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(firstPageViewControler.screen4_1), userInfo: nil, repeats: false)
                let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
                self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(firstPageViewControler.monitorTimer), userInfo: nil, repeats: true)
                break
            case 41: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(firstPageViewControler.screen4_1), userInfo: nil, repeats: false)
                let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
                self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(firstPageViewControler.monitorTimer), userInfo: nil, repeats: true)
                break
            default: break
            }
//            self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(firstPageViewControler.screen1), userInfo: nil, repeats: false)

            playFlagIntroAud = 1
            if let image = UIImage(named: "ic_pause_white_36pt.png") {
                multimediaIntro.setImage(image, for: .normal)
                audioPlayerIntroAud?.play()
//                resumeLayer(layer: layer)
            }
        } else {
            
            self.GTTimer.invalidate()
            self.GTTimer2.invalidate()
            
            playFlagIntroAud = 2
            if let image = UIImage(named: "ic_play_arrow_white_36pt.png") {
                multimediaIntro.setImage(image, for: .normal)
                audioPlayerIntroAud?.pause()
//                pauseLayer(layer: layer)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        audioPlayerIntroAud?.stop()
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
    }
    
//    func pauseLayer(layer: CALayer) {
//        let pausedTime: CFTimeInterval = layer.convertTime(CACurrentMediaTime(), from: nil)
//        layer.speed = 0.0
//        layer.timeOffset = pausedTime
//    }
//    
//    func resumeLayer(layer: CALayer) {
//        let pausedTime: CFTimeInterval = layer.timeOffset
//        layer.speed = 1.0
//        layer.timeOffset = 0.0
//        layer.beginTime = 0.0
//        let timeSincePause: CFTimeInterval = layer.convertTime(CACurrentMediaTime(), from: nil) - pausedTime
//        layer.beginTime = timeSincePause
//    }

    
}
