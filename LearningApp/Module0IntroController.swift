//
//  Module0IntroController.swift
//  LearningApp
//
//  Created by Sumit More on 12/14/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import AVFoundation

class Module0IntroController: UIViewController, AVAudioPlayerDelegate {
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var multimediaIntro: UIButton!
    @IBOutlet weak var introductionImage: UIImageView!
    
    @IBOutlet weak var welcomeLbl: UILabel!
    @IBOutlet weak var understandLbl: UILabel!
    @IBOutlet weak var adviseLbl: UILabel!
    @IBOutlet weak var closeLbl: UILabel!
    @IBOutlet weak var thankLbl: UILabel!
    
    @IBOutlet weak var intro_m0e1l1: UILabel!
    @IBOutlet weak var intro_m0e2l1: UILabel!
    
    @IBOutlet weak var intro_m0e10l1: UILabel!
    @IBOutlet weak var intro_m0e10l2: UILabel!
    
    @IBOutlet weak var intro_m0e12l1: UILabel!
    @IBOutlet weak var intro_m0e12l2: UILabel!
    @IBOutlet weak var intro_m0e12l3: UILabel!
    @IBOutlet weak var intro_m0e12l4: UILabel!
    @IBOutlet weak var intro_m0e12l5: UILabel!
    
    @IBOutlet weak var intro_m0e14l1: UILabel!
    @IBOutlet weak var intro_m0e15l1: UILabel!
    
    @IBOutlet weak var intro_m0e16l1: UILabel!
    
    @IBAction func introFwdClick(_ sender: AnyObject) {
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        audioPlayerIntroAud?.pause()
    }
    
    var playFlagIntroAud = 1
    var audioPlayerIntroAud: AVAudioPlayer?
    
    var GTTimer : Timer = Timer()
    var GTTimer2 : Timer = Timer()
    var isPaused = false
    var intervalVal: Int = 5
    var intervalVal2: Int = 1
    var bulbNo: Int = 0
    var questionNo: Int = 1
    
    @IBOutlet weak var introBtmLbl: UILabel!
    
    override func viewDidLoad() {
        
        self.introBtmLbl.text = "Rep_M0_IntroductionBtmLbl".localized
        
        let device = UIDevice.current.model
        print("Device type: " + device)
        
        if(device == "iPhone") {
            print("Changing size to 16")
            let attrs = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont.systemFont(ofSize: 16.0)
            ]
            self.navigationController!.navigationBar.titleTextAttributes = attrs
            
            let button: UIButton = UIButton(type: UIButtonType.custom)
            button.setImage(UIImage(named: "sof.png"), for: UIControlState.normal)
            button.backgroundColor = UIColor.white
            button.frame = CGRect(x: 0, y: 0, width: 56, height: 45)
            let barButton = UIBarButtonItem(customView: button)
            
            
            let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            negativeSpacer.width = -17;
            
            self.navigationItem.setRightBarButtonItems([negativeSpacer, barButton ], animated: false)
            
        } else {
            print("Changing size to 32")
            let attrs = [
                NSForegroundColorAttributeName: UIColor.white,
                NSFontAttributeName: UIFont.systemFont(ofSize: 32.0)
            ]
            self.navigationController!.navigationBar.titleTextAttributes = attrs
            
            let button: UIButton = UIButton(type: UIButtonType.custom)
            button.setImage(UIImage(named: "sof.png"), for: UIControlState.normal)
            button.backgroundColor = UIColor.white
            //set frame
            button.frame = CGRect(x: 0, y: 0, width: 56, height: 45)
            let barButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            
            
            let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.fixedSpace, target: nil, action: nil)
            negativeSpacer.width = -21;
            
            self.navigationItem.setRightBarButtonItems([negativeSpacer, barButton ], animated: false)
        }
        
        self.navigationItem.title = "Rep_M0_ModuleName".localized
        
        self.welcomeLbl.isHidden = true
        self.understandLbl.isHidden = true
        self.adviseLbl.isHidden = true
        self.closeLbl.isHidden = true
        self.thankLbl.isHidden = true
        
        self.intro_m0e1l1.isHidden = false
        self.intro_m0e2l1.isHidden = true

        self.intro_m0e10l1.isHidden = true
        self.intro_m0e10l2.isHidden = true

        self.intro_m0e12l1.isHidden = true
        self.intro_m0e12l2.isHidden = true
        self.intro_m0e12l3.isHidden = true
        self.intro_m0e12l4.isHidden = true
        self.intro_m0e12l5.isHidden = true

        self.intro_m0e14l1.isHidden = true

        self.intro_m0e15l1.isHidden = true

        self.intro_m0e16l1.isHidden = true

        self.introductionImage.image = UIImage(named: "rep_m0_intro2")
        
        self.welcomeLbl.text = "Rep_Module1".localized
        self.understandLbl.text = "Rep_Module2".localized
        self.adviseLbl.text = "Rep_Module3".localized
        self.closeLbl.text = "Rep_Module4".localized
        self.thankLbl.text = "Rep_Module5".localized
        
        self.intro_m0e1l1.text = "Rep_Intro_M0_E1L1".localized
        self.intro_m0e2l1.text = "Rep_Intro_M0_E2L1".localized
        self.intro_m0e10l1.text = "Rep_Intro_M0_E10L1".localized
        self.intro_m0e10l2.text = "Rep_Intro_M0_E10L2".localized

        self.intro_m0e12l1.text = "Rep_Intro_M0_E12L1".localized
        self.intro_m0e12l2.text = "Rep_Intro_M0_E12L2".localized
        self.intro_m0e12l3.text = "Rep_Intro_M0_E12L3".localized
        self.intro_m0e12l4.text = "Rep_Intro_M0_E12L4".localized
        self.intro_m0e12l5.text = "Rep_Intro_M0_E12L5".localized

        self.intro_m0e14l1.text = "Rep_Intro_M0_E14L1".localized
        
        self.intro_m0e15l1.text = "Rep_Intro_M0_E15L1".localized

        self.intro_m0e16l1.text = "Rep_Intro_M0_E16L1".localized

        if UserDefaults.standard.bool(forKey: "isPlusversion") == true {
            
            self.welcomeLbl.removeConstraints(self.welcomeLbl.constraints)
            let lead_welcome = NSLayoutConstraint(item: self.welcomeLbl,
                                                  attribute: NSLayoutAttribute.left,
                                                  relatedBy: NSLayoutRelation.equal,
                                                  toItem: self.view,
                                                  attribute: NSLayoutAttribute.left,
                                                  multiplier: 1,
                                                  constant: CGFloat(Constants.M0_welcomeLblConstraints_6splus[0]))
            let top_welcome = NSLayoutConstraint(item: self.welcomeLbl,
                                                 attribute: NSLayoutAttribute.top,
                                                 relatedBy: NSLayoutRelation.equal,
                                                 toItem: self.view,
                                                 attribute: NSLayoutAttribute.top,
                                                 multiplier: 1,
                                                 constant: CGFloat(Constants.M0_welcomeLblConstraints_6splus[1]))
            self.view.addConstraint(lead_welcome)
            self.view.addConstraint(top_welcome)
            
            self.understandLbl.removeConstraints(self.understandLbl.constraints)
            let lead_und = NSLayoutConstraint(item: self.understandLbl,
                                              attribute: NSLayoutAttribute.left,
                                              relatedBy: NSLayoutRelation.equal,
                                              toItem: self.view,
                                              attribute: NSLayoutAttribute.left,
                                              multiplier: 1,
                                              constant: CGFloat(Constants.M0_UnderstandLblConstraints_6splus[0]))
            let top_und = NSLayoutConstraint(item: self.understandLbl,
                                             attribute: NSLayoutAttribute.top,
                                             relatedBy: NSLayoutRelation.equal,
                                             toItem: self.view,
                                             attribute: NSLayoutAttribute.top,
                                             multiplier: 1,
                                             constant: CGFloat(Constants.M0_UnderstandLblConstraints_6splus[1]))
            self.view.addConstraint(lead_und)
            self.view.addConstraint(top_und)
            
            self.adviseLbl.removeConstraints(self.adviseLbl.constraints)
            let lead_advise = NSLayoutConstraint(item: self.adviseLbl,
                                                 attribute: NSLayoutAttribute.left,
                                                 relatedBy: NSLayoutRelation.equal,
                                                 toItem: self.view,
                                                 attribute: NSLayoutAttribute.left,
                                                 multiplier: 1,
                                                 constant: CGFloat(Constants.M0_AdviseLblConstraints_6splus[0]))
            let top_advise = NSLayoutConstraint(item: self.adviseLbl,
                                                attribute: NSLayoutAttribute.top,
                                                relatedBy: NSLayoutRelation.equal,
                                                toItem: self.view,
                                                attribute: NSLayoutAttribute.top,
                                                multiplier: 1,
                                                constant: CGFloat(Constants.M0_AdviseLblConstraints_6splus[1]))
            self.view.addConstraint(lead_advise)
            self.view.addConstraint(top_advise)
            
            self.closeLbl.removeConstraints(self.closeLbl.constraints)
            let lead_close = NSLayoutConstraint(item: self.closeLbl,
                                                attribute: NSLayoutAttribute.left,
                                                relatedBy: NSLayoutRelation.equal,
                                                toItem: self.view,
                                                attribute: NSLayoutAttribute.left,
                                                multiplier: 1,
                                                constant: CGFloat(Constants.M0_CloseLblConstraints_6splus[0]))
            let top_close = NSLayoutConstraint(item: self.closeLbl,
                                               attribute: NSLayoutAttribute.top,
                                               relatedBy: NSLayoutRelation.equal,
                                               toItem: self.view,
                                               attribute: NSLayoutAttribute.top,
                                               multiplier: 1,
                                               constant: CGFloat(Constants.M0_CloseLblConstraints_6splus[1]))
            self.view.addConstraint(lead_close)
            self.view.addConstraint(top_close)

            self.thankLbl.removeConstraints(self.thankLbl.constraints)
            let lead_thank = NSLayoutConstraint(item: self.thankLbl,
                                                attribute: NSLayoutAttribute.left,
                                                relatedBy: NSLayoutRelation.equal,
                                                toItem: self.view,
                                                attribute: NSLayoutAttribute.left,
                                                multiplier: 1,
                                                constant: CGFloat(Constants.M0_ThankLblConstraints_6splus[0]))
            let top_thank = NSLayoutConstraint(item: self.thankLbl,
                                               attribute: NSLayoutAttribute.top,
                                               relatedBy: NSLayoutRelation.equal,
                                               toItem: self.view,
                                               attribute: NSLayoutAttribute.top,
                                               multiplier: 1,
                                               constant: CGFloat(Constants.M0_ThankLblConstraints_6splus[1]))
            self.view.addConstraint(lead_thank)
            self.view.addConstraint(top_thank)
            
            self.intro_m0e1l1.removeConstraints(self.intro_m0e1l1.constraints)
            let top_intro_m0e1l1 = NSLayoutConstraint(item: self.intro_m0e1l1,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M0_intro_m0e1l1Constraints_6splus[1]))
            self.view.addConstraint(top_intro_m0e1l1)
            
            self.intro_m0e2l1.removeConstraints(self.intro_m0e2l1.constraints)
            let top_intro_m0e2l1 = NSLayoutConstraint(item: self.intro_m0e2l1,
                                                      attribute: NSLayoutAttribute.top,
                                                      relatedBy: NSLayoutRelation.equal,
                                                      toItem: self.view,
                                                      attribute: NSLayoutAttribute.top,
                                                      multiplier: 1,
                                                      constant: CGFloat(Constants.M0_intro_m0e2l1Constraints_6splus[1]))
            self.view.addConstraint(top_intro_m0e2l1)
            
            self.intro_m0e10l1.removeConstraints(self.intro_m0e10l1.constraints)
            let width_intro_m0e10l1 = NSLayoutConstraint(item: self.intro_m0e10l1,
                                                         attribute: NSLayoutAttribute.width,
                                                         relatedBy: NSLayoutRelation.equal,
                                                         toItem: nil,
                                                         attribute: NSLayoutAttribute.notAnAttribute,
                                                         multiplier: 1,
                                                         constant: CGFloat(Constants.M0_intro_m0e10l1Constraints_6splus[0]))
            let top_intro_m0e10l1 = NSLayoutConstraint(item: self.intro_m0e10l1,
                                                       attribute: NSLayoutAttribute.top,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.view,
                                                       attribute: NSLayoutAttribute.top,
                                                       multiplier: 1,
                                                       constant: CGFloat(Constants.M0_intro_m0e10l1Constraints_6splus[1]))
            self.view.addConstraint(width_intro_m0e10l1)
            self.view.addConstraint(top_intro_m0e10l1)

            self.intro_m0e10l2.removeConstraints(self.intro_m0e10l2.constraints)
            let width_intro_m0e10l2 = NSLayoutConstraint(item: self.intro_m0e10l2,
                                                         attribute: NSLayoutAttribute.width,
                                                         relatedBy: NSLayoutRelation.equal,
                                                         toItem: nil,
                                                         attribute: NSLayoutAttribute.notAnAttribute,
                                                         multiplier: 1,
                                                         constant: CGFloat(Constants.M0_intro_m0e10l2Constraints_6splus[0]))
            let top_intro_m0e10l2 = NSLayoutConstraint(item: self.intro_m0e10l2,
                                                       attribute: NSLayoutAttribute.top,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.view,
                                                       attribute: NSLayoutAttribute.top,
                                                       multiplier: 1,
                                                       constant: CGFloat(Constants.M0_intro_m0e10l2Constraints_6splus[1]))
            let left_intro_m0e10l2 = NSLayoutConstraint(item: self.intro_m0e10l2,
                                                       attribute: NSLayoutAttribute.left,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.view,
                                                       attribute: NSLayoutAttribute.left,
                                                       multiplier: 1,
                                                       constant: CGFloat(Constants.M0_intro_m0e10l2Constraints_6splus[2]))
            self.view.addConstraint(width_intro_m0e10l2)
            self.view.addConstraint(top_intro_m0e10l2)
            self.view.addConstraint(left_intro_m0e10l2)

            self.intro_m0e12l1.removeConstraints(self.intro_m0e12l1.constraints)
            let top_intro_m0e12l1 = NSLayoutConstraint(item: self.intro_m0e12l1,
                                                       attribute: NSLayoutAttribute.top,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.view,
                                                       attribute: NSLayoutAttribute.top,
                                                       multiplier: 1,
                                                       constant: CGFloat(Constants.M0_intro_m0e12l1Constraints_6splus[1]))
            let left_intro_m0e12l1 = NSLayoutConstraint(item: self.intro_m0e12l1,
                                                       attribute: NSLayoutAttribute.left,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.view,
                                                       attribute: NSLayoutAttribute.left,
                                                       multiplier: 1,
                                                       constant: CGFloat(Constants.M0_intro_m0e12l1Constraints_6splus[0]))
            self.view.addConstraint(top_intro_m0e12l1)
            self.view.addConstraint(left_intro_m0e12l1)
            
            self.intro_m0e12l2.removeConstraints(self.intro_m0e12l2.constraints)
            let top_intro_m0e12l2 = NSLayoutConstraint(item: self.intro_m0e12l2,
                                                       attribute: NSLayoutAttribute.top,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.view,
                                                       attribute: NSLayoutAttribute.top,
                                                       multiplier: 1,
                                                       constant: CGFloat(Constants.M0_intro_m0e12l2Constraints_6splus[1]))
            self.view.addConstraint(top_intro_m0e12l2)
            
            self.intro_m0e12l3.removeConstraints(self.intro_m0e12l3.constraints)
            let top_intro_m0e12l3 = NSLayoutConstraint(item: self.intro_m0e12l3,
                                                       attribute: NSLayoutAttribute.top,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.view,
                                                       attribute: NSLayoutAttribute.top,
                                                       multiplier: 1,
                                                       constant: CGFloat(Constants.M0_intro_m0e12l3Constraints_6splus[1]))
            self.view.addConstraint(top_intro_m0e12l3)
            
            self.intro_m0e12l4.removeConstraints(self.intro_m0e12l4.constraints)
            let top_intro_m0e12l4 = NSLayoutConstraint(item: self.intro_m0e12l4,
                                                       attribute: NSLayoutAttribute.top,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.view,
                                                       attribute: NSLayoutAttribute.top,
                                                       multiplier: 1,
                                                       constant: CGFloat(Constants.M0_intro_m0e12l4Constraints_6splus[1]))
            self.view.addConstraint(top_intro_m0e12l4)
            
            self.intro_m0e12l5.removeConstraints(self.intro_m0e12l5.constraints)
            let top_intro_m0e12l5 = NSLayoutConstraint(item: self.intro_m0e12l5,
                                                       attribute: NSLayoutAttribute.top,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.view,
                                                       attribute: NSLayoutAttribute.top,
                                                       multiplier: 1,
                                                       constant: CGFloat(Constants.M0_intro_m0e12l5Constraints_6splus[1]))
            self.view.addConstraint(top_intro_m0e12l5)
            
            self.intro_m0e16l1.removeConstraints(self.intro_m0e16l1.constraints)
            let top_intro_m0e16l1 = NSLayoutConstraint(item: self.intro_m0e16l1,
                                                       attribute: NSLayoutAttribute.top,
                                                       relatedBy: NSLayoutRelation.equal,
                                                       toItem: self.view,
                                                       attribute: NSLayoutAttribute.top,
                                                       multiplier: 1,
                                                       constant: 595)
            self.view.addConstraint(top_intro_m0e16l1)
            
        }
        
        intervalVal = 5
        self.bulbNo = 1
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen1), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
        
        if self.revealViewController() != nil {
            menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: UIControlEvents.touchUpInside)
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            
        }
        
        do{
            let urlVideo = Bundle.main.url(forResource: "REP_M00_S01", withExtension: "mp3")
            audioPlayerIntroAud = try AVAudioPlayer(contentsOf: urlVideo!)
            audioPlayerIntroAud!.delegate = self
            audioPlayerIntroAud!.prepareToPlay()
            audioPlayerIntroAud!.play()
        } catch {
            print("Error getting the audio file")
        }
        
    }
    
    func monitorTimer() {
        self.intervalVal = self.intervalVal - 1
        print("Interval in monitor: " + String(self.intervalVal))
    }
    
    func screen1(){
        
        print("In screen1")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.intro_m0e1l1.isHidden = true
        self.intro_m0e2l1.isHidden = false

        self.introductionImage.image = UIImage(named: "rep_m0_intro3")
        
        self.intervalVal = 15
        self.bulbNo = 2
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen2), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }

    func screen2(){
        
        print("In screen2")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "rep_m0_intro1")
        
        self.intro_m0e2l1.isHidden = true
        
        self.welcomeLbl.isHidden = false
        self.understandLbl.isHidden = false
        self.adviseLbl.isHidden = false
        self.closeLbl.isHidden = false
        self.thankLbl.isHidden = false

        self.intervalVal = 8
        self.bulbNo = 3
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen3), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }

    func screen3(){
        
        print("In screen3")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "rep_m0_intro45")
        
        self.welcomeLbl.isHidden = true
        self.understandLbl.isHidden = true
        self.adviseLbl.isHidden = true
        self.closeLbl.isHidden = true
        self.thankLbl.isHidden = true

        self.intervalVal = 3
        self.bulbNo = 4
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen4), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }

    func screen4(){
        
        print("In screen4")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "rep_m0_intro40")
        
        self.intervalVal = 3
        self.bulbNo = 5
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen5), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }

    func screen5(){
        
        print("In screen5")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "rep_m0_intro41")
        
        self.intervalVal = 6
        self.bulbNo = 6
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen6), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }

    func screen6(){
        
        print("In screen6")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "rep_m0_intro42")
        
        self.intervalVal = 4
        self.bulbNo = 7
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen7), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }

    func screen7(){
        
        print("In screen7")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "rep_m0_intro43")
        
        self.intervalVal = 4
        self.bulbNo = 8
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen8), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }

    func screen8(){
        
        print("In screen8")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "rep_m0_intro44")
        
        self.intervalVal = 7
        self.bulbNo = 9
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen9), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
        
    }

    func screen9(){
        
        print("In screen9")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "rep_m0_intro50")
        
        self.intro_m0e10l1.isHidden = false
        self.intro_m0e10l2.isHidden = false

        self.intervalVal = 5
        self.bulbNo = 10
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen10), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
    }

    func screen10(){
        
        print("In screen10")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "rep_m0_intro5")
        
        self.intervalVal = 4
        self.bulbNo = 11
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen11), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
    }

    func screen11(){
        
        print("In screen11")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "rep_m0_intro6")
        
        self.intro_m0e10l1.isHidden = true
        self.intro_m0e10l2.isHidden = true

        self.intervalVal = 4
        self.bulbNo = 12
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen12), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
    }

    func screen12(){
        
        print("In screen12")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "rep_m0_intro61")
        
        self.intro_m0e12l1.isHidden = false
        self.intro_m0e12l2.isHidden = false
        self.intro_m0e12l3.isHidden = false
        self.intro_m0e12l4.isHidden = false
        self.intro_m0e12l5.isHidden = false
        
        self.intervalVal = 6
        self.bulbNo = 13
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen13), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
    }

    func screen13(){
        
        print("In screen13")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "rep_m0_intro7")
        
        self.intro_m0e12l1.isHidden = true
        self.intro_m0e12l2.isHidden = true
        self.intro_m0e12l3.isHidden = true
        self.intro_m0e12l4.isHidden = true
        self.intro_m0e12l5.isHidden = true

        self.intervalVal = 9
        self.bulbNo = 14
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen14), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
    }

    func screen14(){
        
        print("In screen14")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "rep_m0_intro80")
        
        self.intro_m0e14l1.isHidden = false
        
        self.intervalVal = 6
        self.bulbNo = 15
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen15), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
    }

    func screen15(){
        
        print("In screen15")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "rep_m0_intro8")
        
        self.intro_m0e15l1.isHidden = false

        self.intervalVal = 4
        self.bulbNo = 16
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen16), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
    }

    func screen16(){
        
        print("In screen16")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
        self.introductionImage.image = UIImage(named: "rep_m0_intro9")
        
        self.intro_m0e14l1.isHidden = true
        self.intro_m0e15l1.isHidden = true
        self.intro_m0e16l1.isHidden = false

        self.intervalVal = 7
        self.bulbNo = 17
        
        let timeInterval: TimeInterval = TimeInterval(intervalVal)
        self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen17), userInfo: nil, repeats: false)
        let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
        self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
    }

    func screen17(){
        
        print("In screen17")
        
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
        
    }

    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("In Finish Playing")
        playFlagIntroAud = 3
        if let image = UIImage(named: "ic_replay_white_36pt.png") {
            multimediaIntro.setImage(image, for: .normal)
        }
    }
    
    @IBAction func multimediaIntroClicked(_ sender: AnyObject) {
        //        let layer = introductionImage.layer
        
        if (playFlagIntroAud == 3) {
            
            self.introductionImage.image = UIImage(named: "rep_m0_intro2")
            
            self.welcomeLbl.isHidden = true
            self.understandLbl.isHidden = true
            self.adviseLbl.isHidden = true
            self.closeLbl.isHidden = true
            self.thankLbl.isHidden = true
            
            self.intro_m0e1l1.isHidden = false
            self.intro_m0e2l1.isHidden = true
            
            self.intro_m0e10l1.isHidden = true
            self.intro_m0e10l2.isHidden = true
            
            self.intro_m0e12l1.isHidden = true
            self.intro_m0e12l2.isHidden = true
            self.intro_m0e12l3.isHidden = true
            self.intro_m0e12l4.isHidden = true
            self.intro_m0e12l5.isHidden = true
            
            self.intro_m0e14l1.isHidden = true
            
            self.intro_m0e15l1.isHidden = true
            
            self.intro_m0e16l1.isHidden = true

            self.intervalVal = 5
            self.bulbNo = 1
            print("In screen 1 after replay")
            let timeInterval: TimeInterval = TimeInterval(intervalVal)
            self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen1), userInfo: nil, repeats: false)
            
            let timeInterval2: TimeInterval = TimeInterval(intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
            
            playFlagIntroAud = 1
            if let image = UIImage(named: "ic_pause_white_36pt.png") {
                multimediaIntro.setImage(image, for: .normal)
                audioPlayerIntroAud?.play()
                //                resumeLayer(layer: layer)
            }
        } else if (playFlagIntroAud == 2) {
            
            let timeInterval: TimeInterval = TimeInterval(self.intervalVal)
            print("Interval: " + String(self.intervalVal))
            
            switch self.bulbNo {
            case 1: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen1), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 2: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen2), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 3: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen3), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 4: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen4), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 5: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen5), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 6: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen6), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 7: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen7), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 8: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen8), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 9: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen9), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 10: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen10), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 11: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen11), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 12: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen12), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 13: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen13), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 14: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen14), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 15: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen15), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 16: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen16), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            case 17: self.GTTimer = Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(Module0IntroController.screen17), userInfo: nil, repeats: false)
            let timeInterval2: TimeInterval = TimeInterval(self.intervalVal2)
            self.GTTimer2 = Timer.scheduledTimer(timeInterval: timeInterval2, target: self, selector: #selector(Module0IntroController.monitorTimer), userInfo: nil, repeats: true)
                break
            default: break
            }
            
            playFlagIntroAud = 1
            if let image = UIImage(named: "ic_pause_white_36pt.png") {
                multimediaIntro.setImage(image, for: .normal)
                audioPlayerIntroAud?.play()
            }
        } else {
            
            self.GTTimer.invalidate()
            self.GTTimer2.invalidate()
            
            playFlagIntroAud = 2
            if let image = UIImage(named: "ic_play_arrow_white_36pt.png") {
                multimediaIntro.setImage(image, for: .normal)
                audioPlayerIntroAud?.pause()
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        audioPlayerIntroAud?.stop()
        self.GTTimer.invalidate()
        self.GTTimer2.invalidate()
    }
    
}
