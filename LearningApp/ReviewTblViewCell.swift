//
//  ReviewTblViewCell.swift
//  LearningApp
//
//  Created by Mac-04 on 24/08/16.
//  Copyright © 2016 Mac-04. All rights reserved.
//

import UIKit

class ReviewTblViewCell: UITableViewCell {

    @IBOutlet weak var reviewTblCellLabel1: UILabel!
    
    @IBOutlet weak var reviewTblCellLabel2: UILabel!
    
    
    
    override func awakeFromNib() {
         super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
