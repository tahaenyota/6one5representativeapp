//
//  ExpandableCell.swift
//  ExpandableCellsExample
//
//  Created by DC on 28.08.2016.
//  Copyright © 2016 Dawid Cedrych. All rights reserved.
//

import UIKit

class ExpandableCell: UITableViewCell {
    
    @IBOutlet weak var img: UIImageView!
  

    @IBOutlet weak var sectionTitleLabel: UILabel!
    @IBOutlet weak var imgHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var arrowImage: UIImageView!
    @IBOutlet weak var textviewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var textview: UITextView!
    var cellIndex:Int = 0
    var isExpanded:Bool = false
        {
        didSet
        {
            if !isExpanded {
                self.imgHeightConstraint.constant = 0.0
                self.textviewHeightConstraint.constant = 0.0
            } else {
                
                if (Constants.device == "iPhone"){
                    self.imgHeightConstraint.constant = 400.0
                    switch cellIndex {
                    case 0:
                        self.textviewHeightConstraint.constant = 300.0
                    case 1:
                        self.textviewHeightConstraint.constant = 380.0 // Home
                    case 2:
                        self.textviewHeightConstraint.constant = 1060.0 //	Dashboard
                    case 3:
                        self.textviewHeightConstraint.constant = 180.0 //MM
                    case 4:
                        self.textviewHeightConstraint.constant = 500.0 //module
                    case 5:
                        self.textviewHeightConstraint.constant = 200.0//cert
                    case 6:
                        self.textviewHeightConstraint.constant = 270.0//SC
                    case 7:
                        self.textviewHeightConstraint.constant = 100.0 // FAQ
                    case 8:
                        self.textviewHeightConstraint.constant = 200.0 //Badges
                    case 9:
                        self.textviewHeightConstraint.constant = 350.0 // profile
                    case 10:
                        self.textviewHeightConstraint.constant = 270.0 // Notif
                    case 11:
                        self.textviewHeightConstraint.constant = 140.0 // help
                    case 12:
                        self.textviewHeightConstraint.constant = 280.0 //Personal
                    case 13:
                        self.textviewHeightConstraint.constant = 150.0 //TC
                    case 14:
                        self.textviewHeightConstraint.constant = 60.0
                    default:
                        break
                    }

                }else{
                    self.imgHeightConstraint.constant = (Constants.screenSize.height) - 400
                    //self.imgHeightConstraint.constant = 400.0
                    switch cellIndex {
                    case 0:
                        self.textviewHeightConstraint.constant = 300.0
                    case 1:
                        self.textviewHeightConstraint.constant = 500.0 // Home
                    case 2:
                        self.textviewHeightConstraint.constant = 1100.0 //Dashboard
                    case 3:
                        self.textviewHeightConstraint.constant = 200.0 //MM
                    case 4:
                        self.textviewHeightConstraint.constant = 500.0 //module
                    case 5:
                        self.textviewHeightConstraint.constant = 270.0//cert
                    case 6:
                        self.textviewHeightConstraint.constant = 320.0//SC
                    case 7:
                        self.textviewHeightConstraint.constant = 150.0 // FAQ
                    case 8:
                        self.textviewHeightConstraint.constant = 270.0 //Badges
                    case 9:
                        self.textviewHeightConstraint.constant = 380.0 // profile
                    case 10:
                        self.textviewHeightConstraint.constant = 280.0 // Notif
                    case 11:
                        self.textviewHeightConstraint.constant = 150.0 // help
                    case 12:
                        self.textviewHeightConstraint.constant = 290.0 //Personal
                    case 13:
                        self.textviewHeightConstraint.constant = 150.0 //TC
                    case 14:
                        self.textviewHeightConstraint.constant = 140.0
                    default:
                        break
                    }

                }
                

            }
        }
    }

}
